import UIKit

class DoctorTeamViewController: UIViewController {

    @IBOutlet var docimg: UIImageView!
    @IBOutlet var docimg1: UIImageView!
    @IBOutlet var docimg2: UIImageView!
    @IBOutlet var docimg3: UIImageView!
    @IBOutlet var docimg4: UIImageView!
    @IBOutlet var docimg5: UIImageView!
    @IBOutlet var docimg6: UIImageView!
    
    @IBOutlet var diagonalline1: UIView!
    @IBOutlet var diagonalline2: UIView!
    @IBOutlet var diagonalline3: UIView!
    @IBOutlet var diagonalline4: UIView!
    @IBOutlet var diagonalline5: UIView!
    
    @IBOutlet var myteamButtonview: UIButton!
    @IBOutlet var teamdoc1Buttonview: UIButton!
    @IBOutlet var teamdoc2Buttonview: UIButton!
    @IBOutlet var teamdoc3Buttonview: UIButton!
    @IBOutlet var teamdoc4Buttonview: UIButton!
    @IBOutlet var teamdoc5Buttonview: UIButton!
    @IBOutlet var teamdoc6Buttonview: UIButton!
    @IBOutlet weak var docimage5YConstraint: NSLayoutConstraint!
    @IBOutlet weak var docimage2YConstaint: NSLayoutConstraint!

    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navBarColor.topItem?.title = "Trusted Team"
        title =  "Trusted Team"
        
        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 40.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 812 {
            //iphone x
            docimg.layer.cornerRadius = 38.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docimage5YConstraint.constant = 90
            docimage2YConstaint.constant = 78
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            
        }
        
        LoadProfileImage()
        let isonline = (UserDefaults.standard.object(forKey: "doctoronline") as? Bool) ?? false
        
        if(isonline) {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }

        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    func LoadProfileImage() {
    }

    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
//        print("leftButtonAction pressed")
//        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeviewVC") as? TheHomeViewController else { fatalError("Error") }
//        self.nav = UINavigationController(rootViewController: homeViewController)
//        self.present(self.nav!, animated: false, completion: nil)
        
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func BacktoCOTButton(_ sender: UIButton) {
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromRight
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
