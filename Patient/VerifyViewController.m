//
//  VerifyViewController.m
//  Patient
//
//  Created by Manish Verma on 09/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "VerifyViewController.h"

@import FirebaseAuth;
@import FirebaseAuth.FIRAdditionalUserInfo;
#import <FirebaseAuth/FIRUserInfo.h>


@interface VerifyViewController ()
{
    NSString *usermobilenumber;
    NSString *VerficationID;
    NSString *accesstoken;
    int sendbtncount;
    bool existinguser;
    NSString *newuserlogin;
    NSString *newuserpwd;
}
@property (weak, nonatomic) IBOutlet UILabel *mobilenumberlb;
@property (weak, nonatomic) IBOutlet UITextField *otppin1;
@property (weak, nonatomic) IBOutlet UITextField *otppin2;
@property (weak, nonatomic) IBOutlet UITextField *otppin3;
@property (weak, nonatomic) IBOutlet UITextField *otppin4;
@property (weak, nonatomic) IBOutlet UITextField *otppin5;
@property (weak, nonatomic) IBOutlet UITextField *otppin6;
@property (weak, nonatomic) IBOutlet UIButton *resendButtonview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;
@property (strong, nonatomic) UINavigationController *nav;

@end

@implementation VerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationItem.leftBarButtonItem.accessibilityElementsHidden = true;
    [self.navigationItem setHidesBackButton:YES];
    
    self.resendButtonview.enabled = NO;
    sendbtncount = 0;
    existinguser = false;
    
    [NSTimer scheduledTimerWithTimeInterval:30.0
                                     target:self
                                   selector:@selector(enableResendbuttonnow:)
                                   userInfo:nil
                                    repeats:NO];
    
    usermobilenumber = [[NSUserDefaults standardUserDefaults] stringForKey:@"userphonenumber"];
    
    // NSString *phoneString = @"+919845702415";
    UInt8 len = [usermobilenumber length];
    
    if ( len == 13)
    {
    
    NSString *displaymobilestring = [NSString stringWithFormat:@"%@ %@ %@", [usermobilenumber substringToIndex:3], [usermobilenumber substringWithRange:NSMakeRange(3, 5)],[usermobilenumber substringFromIndex:8]];
    self.mobilenumberlb.text = displaymobilestring;
    }
    else if ( len == 12)
    {
        NSString *displaymobilestring = [NSString stringWithFormat:@"%@ %@ %@", [usermobilenumber substringToIndex:2], [usermobilenumber substringWithRange:NSMakeRange(2, 5)],[usermobilenumber substringFromIndex:8]];
        self.mobilenumberlb.text = displaymobilestring;
    }
    else
    {
        self.mobilenumberlb.text = usermobilenumber;
    }
    
     usermobilenumber = [[NSUserDefaults standardUserDefaults] stringForKey:@"userphonenumber"];
    
    NSLog(@"VerifyViewController:The Mobile number is %@", usermobilenumber);
    
  //  existinguser = [[NSUserDefaults standardUserDefaults] stringForKey:@"existinguser"];
     existinguser = [[[NSUserDefaults standardUserDefaults] valueForKey:@"existinguser"] boolValue];
    
    self.otppin1.delegate = self;
    self.otppin2.delegate = self;
    self.otppin3.delegate = self;
    self.otppin4.delegate = self;
    self.otppin5.delegate = self;
    self.otppin6.delegate = self;

    self.progressIndicator.hidden = true;
    
    
    double devtype = [self checkdevicetrype];
    if (devtype == 736) //iphone 6 plus
    {
    
    }
    
    if (devtype == 812) //iphone x
    {
     
    }
    
    if (devtype == 667) //iphone 6
    {
       
    }
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(20, 20, 44.0f, 30.0f)];
    [backButton setImage:[UIImage imageNamed:@"backBtnwhite"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
}

- (void) BackButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)enableResendbuttonnow:(NSTimer*)timer
{
    NSLog(@"ViewConteroller:enableResendbuttonnow() RESEND Button is enabled");
    self.resendButtonview.enabled = YES;
}

-(double)checkdevicetrype
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812)
        {
            NSLog(@"iPhone X");
            return screenSize.height;
        }
        
        if (screenSize.height == 736)
        {
            NSLog(@"iPhone 6 plus");
            return screenSize.height;
        }
        
        if (screenSize.height == 667)
        {
            NSLog(@"iPhone 6");
            return screenSize.height;
        }
        if (screenSize.height == 568)
        {
            NSLog(@"iPhone 5");
            return screenSize.height;
        }
        
    }
    return 0;
}


//Dismiss keyboard
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"ViewConteroller:touchesBegan");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
    
}

- (IBAction)ConfirmButton:(UIButton *)sender {
    
    if(([self.otppin1.text length] < 1) || ([self.otppin2.text length] < 1) || ([self.otppin3.text length] < 1) || ([self.otppin4.text length] < 1) || ([self.otppin5.text length] < 1) || ([self.otppin6.text length] < 1))
    {
        NSLog(@"VerifyViewController:The OTP pin is not entered");
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Your have not entered 6 digit OTP!"
                                   preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                   }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return;
        
    }
    else
    {
            self.progressIndicator.hidden = false;
            [self.progressIndicator startAnimating];
            
            NSString *totalnumber=[NSString stringWithFormat:@"%@%@%@%@%@%@",self.otppin1.text,self.otppin2.text,self.otppin3.text,self.otppin4.text,self.otppin5.text,self.otppin6.text];
            
            FIRAuthCredential *credential = [[FIRPhoneAuthProvider provider]
                                             credentialWithVerificationID:[[NSUserDefaults standardUserDefaults] stringForKey:@"authVID"]
                                             verificationCode:totalnumber];
            
            
            [[FIRAuth auth] signInWithCredential:credential
                                      completion:^(FIRUser *user, NSError *error) {
                                          if (error) {
                                              NSLog(@"VerifyViewController:Error in Firebase signInWithCredential:: %@",[error localizedDescription]);
                                              
                                              UIAlertController *alert= [UIAlertController
                                                                         alertControllerWithTitle:@"Alert"
                                                                         message:[error localizedDescription]
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                              UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * action){
                                                                                             //Do Some action here
                                                                                         }];
                                              [alert addAction:ok];
                                              [self presentViewController:alert animated:YES completion:nil];
                                              return;
                                              
                                          }
                                          // User successfully signed in. Get user data from the FIRUser object
                                          NSLog(@"VerifyViewController:Firebase signInWithCredential SUCCESS");
                                          NSLog(@"VerifyViewController:Firebase Authenticated Phone Number : %@ ", user.phoneNumber);
                                          NSLog(@"VerifyViewController:Firebase Authenticated Provider ID : %@ ", user.providerID);
                                          NSLog(@"VerifyViewController:Firebase Authenticated user ID : %@ ", user.uid);
                                         // NSLog(@"VerifyViewController:Firebase Authenticated user : %@ ", user);
                                          
                    if(existinguser)
                    {
                        NSString *stringWithoutpluswith_p = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@"p_"];
                        NSString *stringPWDWithoutplus = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                        [[NSUserDefaults standardUserDefaults]
                        setValue:stringWithoutpluswith_p forKey:@"userlogin"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [[NSUserDefaults standardUserDefaults]
                        setValue:stringPWDWithoutplus forKey:@"userpwd"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                                              
                        [QBRequest logInWithUserLogin:stringWithoutpluswith_p password:stringPWDWithoutplus
                                successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user)
                                {
                                    // Login succeeded
                                    NSLog(@"VerifyViewController:QB Existing Login is  SUCCESS");
                                                   
                                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    [[NSUserDefaults standardUserDefaults]
                                     setValue:@"OnBoardDone" forKey:@"OnBoard"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"userid"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    //gotoHomepg
                                    [self performSegueWithIdentifier:@"gotoHomepg" sender:nil];
                                                   
                                } errorBlock:^(QBResponse * _Nonnull response) {
                                    // Handle error
                                    NSLog(@"VerifyViewController:QB Existing User Login !! ERROR !! : %@",response);
                        }];
                    }
                    else
                    {
                        NSString *stringWithoutpluswith_p = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@"p_"];
                        NSString *stringPWDWithoutplus = [user.phoneNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
                                              
                        [[NSUserDefaults standardUserDefaults]
                        setValue:stringWithoutpluswith_p forKey:@"userlogin"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                                              
                        [[NSUserDefaults standardUserDefaults]
                        setValue:stringPWDWithoutplus forKey:@"userpwd"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                         NSString *fullname = [[NSUserDefaults standardUserDefaults] valueForKey:@"fullname"];
                        
                        newuserlogin = stringWithoutpluswith_p;
                        newuserpwd = stringPWDWithoutplus;
                                              
                        QBUUser *userqb = [QBUUser user];
                        userqb.password = stringPWDWithoutplus;
                        userqb.login = stringWithoutpluswith_p;
                        userqb.fullName = fullname;
                        userqb.phone = usermobilenumber;
                        userqb.tags = [@[@"Pat0", @"IOS"] mutableCopy];
                        // Registration/sign up of User
                        [QBRequest signUp:userqb successBlock:^(QBResponse *response, QBUUser *user) {
                        // Sign up was successful
                        NSLog(@"VerifyViewController:QB User SignUp for New Registration was SUCCESS");
                        NSLog(@"VerifyViewController:QB - New User");
                        [self loginafterRegistration];
                                                  
                        } errorBlock:^(QBResponse *response) {
                            // Handle error here
                            NSLog(@"VerifyViewController:QB User SignUp for New Registration!! ERROR !! : %@",response);
                            
                            NSString *errorstring = @"Registration Error";
                            if(response.status == 422)
                            {
                                errorstring = @"Email has already been Taken";
                            }
                            
                            self.progressIndicator.hidden = false;
                            [self.progressIndicator startAnimating];
                            UIAlertController *alert= [UIAlertController
                                                       alertControllerWithTitle:@"Error !!"
                                                       message:errorstring
                                                       preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action){
                                                                           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                           UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"RegisterViewController"];
                                                                           [self presentViewController:VC animated:NO completion:nil];
                                                                           return;
                                                                       }];
                            [alert addAction:ok];
                            [self presentViewController:alert animated:YES completion:nil];
                        }];
                    }
                                      }];
        }
}

// We need to login after successful New User Registration
- (void)loginafterRegistration {
    
    NSLog(@"VerifyViewController:QB New User Login");
    // Authenticate user
    [QBRequest logInWithUserLogin:newuserlogin
                         password:newuserpwd
                     successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user)
     {
         // Login succeeded
         //new user hence go to Primary details page gotoPrimarydetails
         NSLog(@"VerifyViewController:QB New User Login SUCCESS");
         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         [[NSUserDefaults standardUserDefaults]
          setValue:@"OnBoardDone" forKey:@"OnBoard"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         [[NSUserDefaults standardUserDefaults] setInteger:user.ID forKey:@"userid"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         
         [self performSegueWithIdentifier:@"gotoPrimarydetails" sender:nil];
         
     } errorBlock:^(QBResponse * _Nonnull response) {
         // Handle error
         NSLog(@"VerifyViewController:QB New User Login ERROR !! : %@",response);
     }];
}

- (IBAction)ReSendButton:(UIButton *)sender {
    
    sendbtncount += 1;
    if (sendbtncount == 3) {
        sendbtncount = 0;
        self.resendButtonview.enabled = NO;
        UIAlertController *alert= [UIAlertController
                                   alertControllerWithTitle:@"Alert"
                                   message:@"Resent OTP attemps exceeded limit. Please try after some time."
                                   preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action){
                                                       //Do Some action here
                                                   }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    self.progressIndicator.hidden = false;
    
    [[FIRPhoneAuthProvider provider] verifyPhoneNumber:usermobilenumber
                                            UIDelegate:nil
                                            completion:^(NSString * _Nullable verificationID, NSError * _Nullable error) {
                                                if (error) {
                                                    NSLog(@"VerifyViewController:Firebase Auth Error %@",[error localizedDescription]);
                                                    return;
                                                }
                                                
                                                VerficationID = verificationID;
                                                
                                                [[NSUserDefaults standardUserDefaults]
                                                 setValue:VerficationID forKey:@"authVID"];
                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                
                                                self.progressIndicator.hidden = true;
                                                 [self.progressIndicator stopAnimating];
                                                 }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if ((textField.text.length < 1) && (string.length > 0))
    {
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        
        return NO;
        
    }else if ((textField.text.length >= 1) && (string.length > 0)){
        //FOR MAXIMUM 1 TEXT
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder)
            [nextResponder becomeFirstResponder];
        
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length == 0)){
        // on deleteing value from Textfield
        
        NSInteger prevTag = textField.tag - 1;
        // Try to find prev responder
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (prevResponder)
            // Found next responder, so set it.
            [prevResponder becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
