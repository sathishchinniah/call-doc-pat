//
//  GData.m
//  Patient
//
//  Created by Manish Verma on 15/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import "GData.h"

static GData *shared = NULL;

@implementation GData

@synthesize theCurrentUser, scanidfor, isPatientFetched,  PatientName, incomingcallUserName, populatedArray, doctorslistArray, calledUserName, chatdialogusersindex, chatDialoguserscontext,  ischatuserfound, isalreadyloggedin;



-(id)init{
    
    self = [super init];
    
    if(self){

        scanidfor=0; // 0 = for physician, =1 for patient
        PatientName=@"Patient Name";
        incomingcallUserName = @"";
        calledUserName=@"";
        isPatientFetched = false;
        populatedArray=[[NSMutableArray alloc]init];
        doctorslistArray=[[NSMutableArray alloc]init];
        chatDialoguserscontext=[[NSMutableArray alloc]init];
        theCurrentUser;
        chatdialogusersindex = 0;
        ischatuserfound = false;
        isalreadyloggedin = false;

    }
    return self;
    
}





+ (GData *)sharedData
{
    
    {
        if ( !shared || shared == NULL )
        {
            // allocate the shared instance, because it hasn't been done yet
            shared = [[GData alloc] init];
        }
        
        return shared;
    }
    
}

@end
