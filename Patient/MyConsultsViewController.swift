import UIKit

var theuserid: UInt = 0
var theindexval: Int = 0
var thedateval = ""
var consultdatelist = ["18 Sep 2017", "10 Sep 2017", "2 July 2017", "11 June 2017", "5 May 2017", "10 Nov 2016"]
var PatientConsultsdetailsArray : [QBCOCustomObject] = []

class MyConsultsViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet var bottomView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var myConsultTableView: UITableView! {
        didSet {
            myConsultTableView.tableFooterView = UIView()
        }
    }

    @IBOutlet var docspecialitylb: UILabel!
    @IBOutlet var docnamelb: UILabel!
    @IBOutlet var docprofileimg: UIImageView!
    @IBOutlet weak var topViewYConstraint: NSLayoutConstraint!
    
    var nav: UINavigationController?
    var myindexval: Int = 100
    var thelastvc: String = ""
    var patientcalldoccodepassed = ""
    var docuseridpassed: UInt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBarColor = self.navigationController!.navigationBar
        self.title = "My Consults With"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        // this removes navigation bar horizental line
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        topView.layer.shadowOffset = CGSize(width: 0, height: 2)
        topView.layer.shadowColor = UIColor(red:0.67, green:0.77, blue:0.8, alpha:0.61).cgColor
        topView.layer.shadowOpacity = 1
        topView.layer.shadowRadius = 4
        
        bottomView.layer.shadowOffset = CGSize(width: 0, height: -1)
        bottomView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.16).cgColor
        bottomView.layer.shadowOpacity = 1
        bottomView.layer.shadowRadius = 2
        
        self.ReadyConsultsDetails()
        
        var profileurl = (DoctorsArray[myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
        if (profileurl == nil || profileurl.isEmpty) {
            /* string is  blank */
            profileurl = ""
        }
        self.docprofileimg.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)

        docprofileimg.layer.cornerRadius = docprofileimg.frame.size.width / 2;
        docprofileimg.clipsToBounds = true;
        docprofileimg.layer.borderWidth = 2.0
        docprofileimg.layer.borderColor = UIColor.green.cgColor
        
        docnamelb.text = "Dr. " + (DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String)!
        docspecialitylb.text = (DoctorsArray[myindexval].fields?.value(forKey: "doctorspeciality") as? String)!

        if (docspecialitylb.text == "ent") || (docspecialitylb.text == "gp") {
            docspecialitylb.text = docspecialitylb.text?.uppercased()
        } else {
            docspecialitylb.text = docspecialitylb.text?.capitalized
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController!.navigationBar.topItem?.title = ""
    }

    func backBtnPressed(_ sender: Any) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    public func tableView(_ myConsultTableView : UITableView, numberOfRowsInSection section: Int) -> Int {
        return PatientConsultsdetailsArray.count
    }
    
    public func tableView(_ myConsultTableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        UITableViewCell.appearance().backgroundColor = UIColor.clear
        let cell = myConsultTableView.dequeueReusableCell(withIdentifier: "myConsultsCell", for: indexPath) as! myConsultsTableViewCell
        cell.selectionStyle = .none
        
        let createdat  = PatientConsultsdetailsArray[indexPath.row].createdAt! as NSDate
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
        let lastdatetimelb = fulldate[0]
        let lastdatebtn = fulldate[1]
        
        cell.dateButton.setTitle(lastdatebtn, for: .normal)
        cell.timelb.text = lastdatetimelb
        cell.consultstatuslb.text = "Consult closes on"
        let add72hours = createdat.addingTimeInterval(259200.0)
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM"
        let closedateString = dayTimePeriodFormatter.string(from: add72hours as Date)
        let closefulldate = closedateString.components(separatedBy: "|")
        let closedatetimelb = closefulldate[0]
        let closelastdate = closefulldate[1]

        cell.consultclosedttmlb.text =  closelastdate + " @ " + closedatetimelb
        cell.notificationBtn.isHidden = true
        cell.myconsultcellButton.tag = indexPath.row
        cell.myconsultcellButton.addTarget(self,action:#selector(myconsultcellButtonAction), for:.touchUpInside)
        
        return cell
    }
    
    @objc func myconsultcellButtonAction(sender: UIButton) {
        glastVC = "MyConsult"
        print(sender.tag)
        let createdat  = PatientConsultsdetailsArray[sender.tag].createdAt! as NSDate
        let currenttime: Int = Int(NSDate().timeIntervalSince1970)
        let createdatepochtime = Int(createdat.timeIntervalSince1970)
        let diff = currenttime - createdatepochtime
        let hourslasped = diff/3600

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
        let createdattimelb = fulldate[0]
        let createdatdatelb = fulldate[1]
        let VC1 = ConsultDetailsViewController.storyboardInstance()
        VC1.docnamepassed = self.docnamelb.text!
        VC1.datepassed = createdattimelb + " | " + createdatdatelb
        VC1.indexval = sender.tag
        theindexval = sender.tag
        thedateval = createdatdatelb
        VC1.hourslaspedpassed = hourslasped
        VC1.docuseridpassed = docuseridpassed
        VC1.doctorsarrayindexval = myindexval
        VC1.consultrecordIDpassed = PatientConsultsdetailsArray[sender.tag].id! as String
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    //MARK:- Storyboard Instance
    
    static func storyboardInstance() -> MyConsultsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyConsult") as! MyConsultsViewController
    }
    
    func ReadyConsultsDetails() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = docuseridpassed
        if patientcdcode.count > gindexval {
            getRequest["patcalldoccode"] = patientcdcode[gindexval]
        }        
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                PatientConsultsdetailsArray = contributors!
                PatientConsultsdetailsArray.reverse()
            } else {
                PatientConsultsdetailsArray = contributors!
            }

            self.myConsultTableView.reloadData()

        }) { (response) in
            //Error handling
            print("PatientDetailsViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
}
