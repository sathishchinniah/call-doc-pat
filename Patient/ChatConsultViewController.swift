//
//  ChatConsultViewController.swift
//  Patient
//
//  Created by Kirthika Raukutam on 06/12/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit

class ChatConsultViewController: UIViewController {

    @IBOutlet weak var allConsultsContainer: UIView!
    @IBOutlet weak var chatViewContainer: UIView!
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var shareConsultButton: UIButton!
    
    var allConsultsVC = AllConsultsViewController()
    
    //For chatviewcontroller
    let maxCharactersNumber = 1024
    var consultrecordIDpassed = ""
    var docprofileimageurl = ""
    var dialog: QBChatDialog!
    var willResignActiveBlock: AnyObject?
    var attachmentCellsMap: NSMapTable<AnyObject, AnyObject>!
    var detailedCells: Set<String> = []
    var opponentDoctorId = 0 as UInt
    var profPicURL = ""
    var indexVal = 0
    var pagerIndex = 0
    var docselected = 0 as UInt
    var currentConsult = QBCOCustomObject()
    
    var typingTimer: Timer?
    var popoverController: UIPopoverController?
    
//    lazy var imagePickerViewController : UIImagePickerController = {
//        let imagePickerViewController = UIImagePickerController()
//        imagePickerViewController.delegate = self
//
//        return imagePickerViewController
//    }()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatButton.alpha = 0.5
        allConsultsContainer.isHidden = true
        self.navigationController!.navigationBar.isHidden = false
        let navBarColor = self.navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.title = ""
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.white
        
        UINavigationBar.appearance().barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIApplication.shared.statusBarStyle = .default
        navigationController?.navigationBar.backgroundColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navigationController?.navigationBar.tintColor = UIColor.white
        let backBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "backBtnwhite"), style: .done, target: self, action: #selector(backBarButtonTapped(_:)))
        navigationItem.leftBarButtonItem = backBarButton
//        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
//        navBar.backgroundColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
//
//        self.view.addSubview(navBar)
//
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
   
    @IBAction func chatButtonTouchUpInside(_ sender: Any) {
        chatButton.alpha = 0.5
        shareConsultButton.alpha = 1
        
        chatViewContainer.isHidden = false
        allConsultsContainer.isHidden = true
        
        
    }
    
    @IBAction func shareConsultTouchUpInside(_ sender: Any) {
        
        chatButton.alpha = 1
        shareConsultButton.alpha = 0.5
        
        chatViewContainer.isHidden = true
        allConsultsContainer.isHidden = false
        
        
       // performSegue(withIdentifier: "displayAllConsultsViewController", sender: self)
    }
    
    @objc func backBarButtonTapped(_ sender: UIBarButtonItem) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
//        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
//        let nav = UINavigationController(rootViewController: homeViewController)
//        self.present(nav, animated: true, completion: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "displayAllConsultsViewController" {
            self.allConsultsVC.viewDidLayoutSubviews()
            self.allConsultsVC.allConsultTableView.reloadData()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "displayAllConsultsViewController" {
            if let childVC = segue.destination as? AllConsultsViewController {
                allConsultsVC = childVC
                childVC.currentConsult = self.currentConsult
            }
        }
        if segue.identifier == "displayChatViewController" {
            loadChatViewController(withDestination: segue.destination as? ChatViewController)
        }
    }
    
    func loadChatViewController(withDestination: ChatViewController?) {
        
        if (!consultrecordIDpassed.isEmpty) {
            let user = QBUUser()
            user.id =  docselected
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.occupantIDs = [user.id] as [NSNumber]
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                //let storyboard = UIStoryboard.init(name: "Consult", bundle: nil)
                let chatvc = withDestination//storyboard.instantiateViewController(withIdentifier: "ChatLogsViewController") as! ChatLogsViewController
                //chatvc?.ConsultRecordID = self.consultrecordIDpassed
                chatvc?.dialog = createdDialog
                chatvc?.pagerIndex = self.pagerIndex
                chatvc?.indexVal = self.indexVal
                chatvc?.opponentDoctorId = self.docselected
                chatvc?.consultrecordIDpassed = self.consultrecordIDpassed//PatientConsultsdetailsArray[0].id! as String
                chatvc?.docprofileimageurl = (DoctorsArray[self.indexVal].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
                chatvc?.reloadInputViews()
                
                
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(String(describing: response))")
            })
        }
    }

}
