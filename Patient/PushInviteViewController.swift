import UIKit

class PushInviteViewController: UIViewController {

    @IBOutlet weak var profileimage: UIImageView! {
        didSet {
            profileimage.layer.cornerRadius = profileimage.frame.size.width / 2;
            profileimage.clipsToBounds = true;
            profileimage.layer.borderWidth = 0.3
            profileimage.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var docprofilename: UILabel!
    
    var nav: UINavigationController?
    var profilenamepassed = ""
    var profileimageurl = ""
    var docspeciality = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileimage.sd_setImage(with: URL(string: profileimageurl), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        docprofilename.text = profilenamepassed + "   " + docspeciality
    }
    
    @IBAction func OKButtonClick(_ sender :UIButton) {
        navigationController?.isNavigationBarHidden = false
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
}
