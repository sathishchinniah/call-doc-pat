import UIKit
import DropDown
import KMPlaceholderTextView

class PrimaryDetailsTableViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var heightFeetTextField: UITextField!
    @IBOutlet weak var heightInchesTextField: UITextField!
    @IBOutlet weak var bloodGroupTextField: UITextField!
    @IBOutlet weak var allergiesTextField: UITextField!
    @IBOutlet weak var chronicHealthIssuesTextField: UITextField!
    @IBOutlet weak var currentMedicationTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var pincodeTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var addressfield: KMPlaceholderTextView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var citystateactivityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressSeparator: UIView!
    @IBOutlet weak var pincodeLabel: UILabel!
    @IBOutlet weak var pincodeSeparator: UIView!
    @IBOutlet weak var cityStackView: UIStackView!
    
    
    var primaryDetailsVC: PrimaryDetailsViewController?
    let dropDownGender = DropDown()
    let dropDownage = DropDown()
    let dropDownblood = DropDown()
    let dropDownhightfeet = DropDown()
    let dropDownhightinch = DropDown()
    let dropDownweight = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()

        addressfield.sizeToFit()

        // Check if we are coming to this view controller for the first time
        if GlobalDocdata.firsttimeview {
            UserDefaults.standard.setValue("", forKey: "gender")
            UserDefaults.standard.setValue("", forKey: "bloodgroup")
            UserDefaults.standard.setValue("", forKey: "age")
            UserDefaults.standard.setValue("", forKey: "hightft")
            UserDefaults.standard.setValue("", forKey: "hightinch")
            UserDefaults.standard.setValue("", forKey: "weight")
            UserDefaults.standard.setValue("", forKey: "allergies")
            UserDefaults.standard.setValue("", forKey: "chronichealth")
            UserDefaults.standard.setValue("", forKey: "currentmedication")
            UserDefaults.standard.synchronize()

        } else {

            if isKeyPresentInUserDefaults(key: "useremail") {
                emailTextField.text = UserDefaults.standard.string(forKey: "useremail")!
            }

            if isKeyPresentInUserDefaults(key: "gender") {
                genderTextField.text = UserDefaults.standard.string(forKey: "gender")!
            }

            if isKeyPresentInUserDefaults(key: "bloodgroup") {
                bloodGroupTextField.text = UserDefaults.standard.string(forKey: "bloodgroup")!
            }

            if isKeyPresentInUserDefaults(key: "age") {
                ageTextField.text = UserDefaults.standard.string(forKey: "age")!
            }

            if isKeyPresentInUserDefaults(key: "hightft") {
                heightFeetTextField.text = UserDefaults.standard.string(forKey: "hightft")!
            }

            if isKeyPresentInUserDefaults(key: "hightinch") {
                heightInchesTextField.text = UserDefaults.standard.string(forKey: "hightinch")!
            }

            if isKeyPresentInUserDefaults(key: "weight") {
                weightTextField.text = UserDefaults.standard.string(forKey: "weight")!
            }

            if isKeyPresentInUserDefaults(key: "allergies") {
                allergiesTextField.text = UserDefaults.standard.string(forKey: "allergies")!
            }

            if isKeyPresentInUserDefaults(key: "chronichealth") {
                chronicHealthIssuesTextField.text = UserDefaults.standard.string(forKey: "chronichealth")!
            }

            if isKeyPresentInUserDefaults(key: "currentmedication") {
                currentMedicationTextField.text = UserDefaults.standard.string(forKey: "currentmedication")!
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

    //MARK:- TextField delegate methods

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if (textField == self.genderTextField) {
            textField.endEditing(true)
            dropDownGender.anchorView = self.genderTextField
            dropDownGender.dataSource = ["MALE", "FEMALE"]
            dropDownGender.show()
            dropDownGender.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.genderTextField.text = item
            }
        }

        if (textField == self.ageTextField) {
            textField.endEditing(true)

            dropDownage.anchorView = self.ageTextField
            dropDownage.dataSource = ["Infant", "3 mo - 6 mo", "6 mo - 1 yr", "1 yr","2 yrs","3 yrs", "4 yrs", "5 yrs", "6 yrs","7 yrs", "8 yrs", "9 yrs", "10 yrs", "11 yrs",
                                      "12 yrs", "13 yrs","14 yrs", "15 yrs", "16 yrs", "17 yrs", "18 yrs", "19 yrs", "20 yrs", "21 yrs", "22 yrs", "23 yrs", "24 yrs", "25 yrs",
                                      "26 yrs", "27 yrs", "28 yrs", "29 yrs", "30 yrs", "31 yrs", "32 yrs", "33 yrs", "34 yrs", "35 yrs", "36 yrs", "37 yrs","38 yrs", "39 yrs",
                                      "40 yrs", "41 yrs", "42 yrs", "43 yrs", "44 yrs", "45 yrs", "46 yrs", "47 yrs", "48 yrs", "49 yrs", "50 yrs", "51 yrs", "52 yrs", "53 yrs",
                                      "54 yrs", "55 yrs", "56 yrs", "57 yrs", "58 yrs", "59 yrs", "60 yrs" , "61 yrs", "62 yrs", "63 yrs", "64 yrs", "65 yrs", "66 yrs", "67 yrs",
                                      "68 yrs", "69 yrs", "70 yrs", "71 yrs", "72 yrs", "73 yrs","74 yrs", "75 yrs", "76 yrs", "77 yrs", "78 yrs", "79 yrs", "80 yrs", "81 yrs",
                                      "82 yrs", "83 yrs", "84 yrs", "85 yrs","86 yrs", "87 yrs", "88 yrs", "89 yrs", "90 yrs", "91 yrs", "92 yrs", "93 yrs", "94 yrs", "95 yrs",
                                      "96 yrs", "97 yrs", "98 yrs", "99 yrs"]

            dropDownage.direction = .bottom
            dropDownage.show()
            dropDownage.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.ageTextField.text = item
            }
        }

        if (textField == self.bloodGroupTextField) {
            textField.endEditing(true)
            dropDownblood.anchorView = self.bloodGroupTextField
            dropDownblood.dataSource = ["A+","A-", "B+", "B-", "AB+","AB-", "O+", "O-"]
            dropDownblood.show()
            dropDownblood.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Boolgroup Selected item: \(item) at index: \(index)")
                self.bloodGroupTextField.text = item
            }
        }

        if (textField == self.heightFeetTextField) {
            textField.endEditing(true)
            dropDownhightfeet.anchorView = self.heightFeetTextField
            dropDownhightfeet.dataSource = ["1 feet","2 feet", "3 feet", "4 feet", "5 feet","6 feet", "7 feet", "8 feet"]
            dropDownhightfeet.show()
            dropDownhightfeet.selectionAction = { [unowned self] (index: Int, item: String) in
                print("hightfeet Selected item: \(item) at index: \(index)")
                self.heightFeetTextField.text = item
            }
        }

        if (textField == self.heightInchesTextField) {
            textField.endEditing(true)
            dropDownhightinch.anchorView = self.heightInchesTextField
            dropDownhightinch.dataSource = ["0 inch","1 inch","2 inch", "3 inch", "4 inch", "5 inch","6 inch", "7 inch", "8 inch","9 inch", "10 inch","11 inch"]
            dropDownhightinch.show()
            dropDownhightinch.selectionAction = { [unowned self] (index: Int, item: String) in
                print("hightfeet Selected item: \(item) at index: \(index)")
                self.heightInchesTextField.text = item
            }
        }

        if (textField == self.weightTextField) {
            textField.endEditing(true)
            dropDownweight.anchorView = self.weightTextField
            dropDownweight.dataSource = ["1 Kg","2 Kg", "3 Kg", "4 Kg", "5 Kg","6 Kg", "7 Kg", "8 Kg","9 Kg", "10 Kg","11 Kg", "12 Kg", "13 Kg", "14 Kg", "15 Kg","16 Kg", "17 Kg", "18 Kg","19 Kg", "20 Kg", "21 Kg","22 Kg", "23 Kg", "24 Kg", "25 Kg","26 Kg", "27 Kg", "28 Kg","29 Kg", "30 Kg","31 Kg", "32 Kg", "33 Kg", "34 Kg", "35 Kg","36 Kg", "37 Kg", "38 Kg","39 Kg", "40 Kg", "41 Kg","42 Kg", "43 Kg", "44 Kg", "45 Kg","46 Kg", "47 Kg", "48 Kg","49 Kg", "50 Kg","51 Kg", "52 Kg", "53 Kg", "54 Kg", "55 Kg","56 Kg", "57 Kg", "58 Kg","59 Kg", "60 Kg","61 Kg","62 Kg", "63 Kg", "64 Kg", "65 Kg","66 Kg", "67 Kg", "68 Kg","69 Kg", "70 Kg","71 Kg","72 Kg", "73 Kg", "74 Kg", "75 Kg","76 Kg", "77 Kg", "78 Kg","79 Kg", "80 Kg", "81 Kg","82 Kg", "83 Kg", "84 Kg", "85 Kg","86 Kg", "87 Kg", "88 Kg","89 Kg", "90 Kg", "91 Kg","92 Kg", "93 Kg", "94 Kg", "95 Kg","96 Kg", "97 Kg", "98 Kg","99 Kg", "100 Kg", "101 Kg","102 Kg", "103 Kg", "104 Kg", "105 Kg","106 Kg", "107 Kg", "108 Kg","109 Kg", "110 Kg", "111 Kg", "112 Kg","113 Kg", "114 Kg", "115 Kg", "116 Kg","117 Kg", "118 Kg", "119 Kg","120 Kg", "121 Kg"]
            dropDownweight.direction = .bottom
            dropDownweight.show()
            dropDownweight.selectionAction = { [unowned self] (index: Int, item: String) in
                print("hightfeet Selected item: \(item) at index: \(index)")
                self.weightTextField.text = item
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == bloodGroupTextField {
            allergiesTextField.becomeFirstResponder()
        } else if textField == allergiesTextField {
            chronicHealthIssuesTextField.becomeFirstResponder()
        } else if textField == chronicHealthIssuesTextField {
            currentMedicationTextField.becomeFirstResponder()
        } else if pincodeTextField.becomeFirstResponder() {
            pincodeTextField.resignFirstResponder()  // this should kill the KB
        }
        return true

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == allergiesTextField {
            let allowedcharacters = CharacterSet.letters
            let characterSet = CharacterSet(charactersIn: string)
            return allowedcharacters.isSuperset(of: characterSet)
        }
        return true
    }


}
