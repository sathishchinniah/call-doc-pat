import UIKit

var problemlist = ["Skin Hair's & Nails", "Men's Health","Women's Health", "Bone & Joint", "Physiotherapy", "Mental Health", "Digestive", "Urinary", "Tooth & Mouth", "Ear, Nose & Throat", "Child Health", "Brain & Nerves", "Diet & Weight", "General Health", "Eye & Vision", "Breathing", "Diabetes", "Heart", "Cancer"]

class ConsultAnotherDocViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var problemTableView: UITableView!
    
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let btnImage = UIImage(named: "backBtn-1")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton

        let rtbtnImage = UIImage(named: "FaceBtn")
        let rightbtn = UIButton(type: .custom)
        rightbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        rightbtn.setImage(rtbtnImage, for: .normal)
        let profileButton = UIBarButtonItem(customView: rightbtn)
        navigationItem.rightBarButtonItem = profileButton
    }
    
    @objc func backBtnPressed(_ sender: Any) {
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    public func tableView(_ problemTableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return problemlist.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        UITableViewCell.appearance().backgroundColor = UIColor.clear
        let cell = (tableView.dequeueReusableCell(withIdentifier: "problemcell", for: indexPath) as? problemTableViewCell) ?? problemTableViewCell()
        
        cell.selectionStyle = .none
        cell.specialitylb.text = problemlist[indexPath.row]
        cell.problemcellButton.tag = indexPath.row
        cell.problemcellButton.addTarget(self,action:#selector(problemselectedAction), for:.touchUpInside)
        
        return cell
    }
    
    @objc func problemselectedAction(sender: UIButton) {
        print(sender.tag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
