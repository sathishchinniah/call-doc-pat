import UIKit

class TrustedNetworkViewController: UIViewController {

    @IBOutlet var docnamelb: UILabel!
    @IBOutlet var namelabel1: UILabel!
    @IBOutlet var namelabel2: UILabel!
    @IBOutlet var namelabel3: UILabel!
    @IBOutlet var namelabel4: UILabel!
    @IBOutlet var namelabel5: UILabel!
    @IBOutlet var namelabel6: UILabel!
    @IBOutlet var docimg: UIImageView!
    @IBOutlet var docimg1: UIImageView!
    @IBOutlet var docimg2: UIImageView!
    @IBOutlet var docimg3: UIImageView!
    @IBOutlet var docimg4: UIImageView!
    @IBOutlet var docimg5: UIImageView!
    @IBOutlet var docimg6: UIImageView!
    @IBOutlet var diagonalline1: UIView!
    @IBOutlet var diagonalline2: UIView!
    @IBOutlet var diagonalline3: UIView!
    @IBOutlet var diagonalline4: UIView!
    @IBOutlet var line5: UIView!
    @IBOutlet var line6: UIView!
    
    @IBOutlet var genericButtonview: UIButton!
    @IBOutlet var doc1Buttonview: UIButton!
    @IBOutlet var doc2Buttonview: UIButton!
    @IBOutlet var doc3Buttonview: UIButton!
    @IBOutlet var doc4Buttonview: UIButton!
    @IBOutlet var doc5Buttonview: UIButton!
    @IBOutlet var doc6Buttonview: UIButton!
    
    @IBOutlet var docnamelbTopMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var docimage5YConstraint: NSLayoutConstraint!
    @IBOutlet weak var docimage2YConstraint: NSLayoutConstraint!
    @IBOutlet weak var docimage4XConstraint: NSLayoutConstraint!
    @IBOutlet weak var docimage4YConstraint: NSLayoutConstraint!
    
    enum specialities: String {
        case cardiologist = "Cardiologist"
        case dermatologist = "Dermatologist"
        case gastroentrologist = "Gastroentrologist"
        case ent = "E.N.T."
        case endocrinologist = "Endocrinologist"
        case pulmonoligist = "Pulmonoligist"
        case pediatrician = "Pediatrician"
        case nephrologist = "Nephrologist"
        case gp = "G.P."
        case gynaecologist = "Gynaecologist"
        case neurologist = "Neurologist"
        case dietician = "Dietician"
        case psychiatrist = "Psychiatrist"
        case oncologist = "Oncologist"
        case orthopedic = "Orthopedic"
        case sexologist = "Sexologist"
        case altMedicine = "Alt Medicine"
    }
    
    var nav: UINavigationController?
    var stringpassed = ""
    var myindexval: Int = 0
    var docuserid: UInt = 0
    
    var receivedrecordcount = 0
    var finalArray:[QBCOCustomObject] = []
    var position = 0
    var firstname = ""
    var lastname = ""
    var doc1profileaction: Bool = false,  doc2profileaction = false, doc3profileaction = false, doc4profileaction = false, doc5profileaction = false, doc6profileaction = false
    var doc1fullname: String = "", doc2fullname = "", doc3fullname = "", doc4fullname = "", doc5fullname = "", doc6fullname = ""
    var doc1imgURL: String = "", doc2imgURL = "", doc3imgURL = "", doc4imgURL = "", doc5imgURL = "", doc6imgURL = ""
    var doc1userid: Int = 0, doc2userid = 0, doc3userid = 0, doc4userid = 0, doc5userid = 0, doc6userid = 0
    var doc1speciality: String = "", doc2speciality = "", doc3speciality = "", doc4speciality = "", doc5speciality = "", doc6speciality = ""
    var doc1Data: QBCOCustomObject!
    var doc2Data: QBCOCustomObject!
    var doc3Data: QBCOCustomObject!
    var doc4Data: QBCOCustomObject!
    var doc5Data: QBCOCustomObject!
    var doc6Data: QBCOCustomObject!
    var docStatus = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navBarColor.topItem?.title = "Trusted Network"
        title = "Trusted Network"
        
        docnamelb.text = "Dr " + (DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String)!
        
        namelabel1.layer.cornerRadius = 8
        namelabel1.layer.masksToBounds = true
        namelabel2.layer.cornerRadius = 8
        namelabel2.layer.masksToBounds = true
        namelabel3.layer.cornerRadius = 8
        namelabel3.layer.masksToBounds = true
        namelabel4.layer.cornerRadius = 8
        namelabel4.layer.masksToBounds = true
        namelabel5.layer.cornerRadius = 8
        namelabel5.layer.masksToBounds = true
        namelabel6.layer.cornerRadius = 8
        namelabel6.layer.masksToBounds = true
        
        genericButtonview.setTitle(stringpassed , for: .normal)
        
        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 40.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 812 {
            //iphone x
            docimg.layer.cornerRadius = 38.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docnamelbTopMarginConstraint.constant = 50
            docimage5YConstraint.constant = 90
            docimage2YConstraint.constant = 80
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
        }
        
        LoadProfileImage()
        let isonline = true
        
        if(isonline) {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }
        
        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.updateTheView()
        if docStatus == "online" {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else if docStatus == "offline" {
            docimg.layer.borderColor = UIColor.red.cgColor
        } else if docStatus == "pause" {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }
    }
    
    
    func LoadProfileImage() {
        var profileurl = (DoctorsArray[myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
        if (profileurl == nil || profileurl.isEmpty) {
            profileurl = ""
        }
        self.docimg.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "Profile@2x.png"), options: .cacheMemoryOnly)
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
//        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
//        self.nav = UINavigationController(rootViewController: homeViewController)
//        self.present(self.nav!, animated: false, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func BacktoCOTButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func Doc1AddButtion(_ sender: UIButton) {

        let vc = COTProfileViewController.storyboardInstance()
        vc.docfullname = doc1fullname
        vc.imageURLpassed = doc1imgURL
        vc.docspeciality = doc1speciality
        vc.docuserid = doc1userid
        vc.doctorSelected = doc1Data
        
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Doc2AddButtion(_ sender: UIButton) {
        let vc = COTProfileViewController.storyboardInstance()
        vc.docfullname = doc2fullname
        vc.imageURLpassed = doc2imgURL
        vc.docspeciality = doc2speciality
        vc.docuserid = doc2userid
        vc.doctorSelected = doc2Data
        
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func Doc3AddButtion(_ sender: UIButton) {
        let vc = COTProfileViewController.storyboardInstance()
        vc.docfullname = doc3fullname
        vc.imageURLpassed = doc3imgURL
        vc.docspeciality = doc3speciality
        vc.docuserid = doc3userid
        vc.doctorSelected = doc3Data
        
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func Doc4AddButtion(_ sender: UIButton) {
        let vc = COTProfileViewController.storyboardInstance()
        vc.docfullname = doc4fullname
        vc.imageURLpassed = doc4imgURL
        vc.docspeciality = doc4speciality
        vc.docuserid = doc4userid
        vc.doctorSelected = doc4Data
        
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func Doc5AddButtion(_ sender: UIButton) {
        let vc = COTProfileViewController.storyboardInstance()
        vc.docfullname = doc5fullname
        vc.imageURLpassed = doc5imgURL
        vc.docspeciality = doc5speciality
        vc.docuserid = doc5userid
        vc.doctorSelected = doc5Data
        
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func Doc6AddButtion(_ sender: UIButton) {
        let vc = COTProfileViewController.storyboardInstance()
        vc.docfullname = doc6fullname
        vc.imageURLpassed = doc6imgURL
        vc.docspeciality = doc6speciality
        vc.docuserid = doc6userid
        vc.doctorSelected = doc6Data
        
        // this line will hide back button title on next viewcontroller navigation bar
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateTheView() {
        let speciality = specialities(rawValue: stringpassed)
        
        switch speciality {
        case .some(.cardiologist):
            self.handlingofCrdiologists()
            break
        case .some(.dermatologist):
            self.handlingofDermatologist()
        case .some(.gastroentrologist):
            self.handlingofGastroentrologist()
        case .some(.ent):
            self.handlingofEnt()
        case .some(.endocrinologist):
            self.handlingofEndocrinologist()
        case .some(.pulmonoligist):
            self.handlingofPulmonoligist()
        case .some(.pediatrician):
            self.handlingofPediatrician()
        case .some(.nephrologist):
            self.handlingofNephrologist()
        case .some(.gp):
            self.handlingofGP()
        case .some(.gynaecologist):
            self.handlingofGynaecologist()
        case .some(.neurologist):
            self.handlingofNeurologist()
        case .some(.dietician):
            self.handlingofDietician()
        case .some(.psychiatrist):
            self.handlingofPsychiatrist()
        case .some(.oncologist):
            self.handlingofOncologist()
        case .some(.orthopedic):
            self.handlingofOrthopedic()
        case .some(.sexologist):
            self.handlingofSexologist()
        case .some(.altMedicine):
            self.handlingofAltMedicine()
        case .none:
            break
        }
    }
    
    func handlingofCrdiologists() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] =  docuserid
        getRequest["inviteespeciality"] = "cardiologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofCrdiologists(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofCrdiologists(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofDermatologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "dermatologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofDermatologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofDermatologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofGastroentrologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "gastroentrologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofGastroentrologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofGastroentrologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofEnt() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "ent"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofEnt(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofEnt(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
        
    }
    
    func handlingofEndocrinologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "endocrinologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofEndocrinologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofEndocrinologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }

    }
    
    func handlingofPulmonoligist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "pulmonoligist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofPulmonoligist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofPulmonoligist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofPediatrician() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "paediatrician"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofPediatrician(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofPediatrician(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofNephrologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "nephrologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofNephrologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofNephrologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofGP() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "general"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofGP(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofGP(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofGynaecologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "gynaecologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofGynaecologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofGynaecologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofNeurologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "neurologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofNeurologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofNeurologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofDietician() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "dietician"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofDietician(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofDietician(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofPsychiatrist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "psychiatrist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofPsychiatrist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofPsychiatrist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofOncologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "oncologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofOncologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofOncologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofOrthopedic() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "orthopaedic"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofOrthopedic(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofOrthopedic(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofSexologist() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "sexologist"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofSexologist(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofSexologist(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    func handlingofAltMedicine() {
        var recordcount = 0
        var recordArray : [QBCOCustomObject] = []
        // we need to read RelSpeciality Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["inviteespeciality"] = "altmedicine"
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("TrustedNetworkViewController:handlingofAltMedicine(): Success ")
            if contributors?.count == 0 {
                // No entry found
                recordcount = 0
            } else {
                // entry found
                recordcount = (contributors?.count)!
                recordArray = contributors!
                self.PopulateData(recordcount: recordcount, recordArray: recordArray)
            }
        }) { (response) in
            //Handle Error
            print("TrustedNetworkViewController:handlingofAltMedicine(): Response error: \(String(describing: response.error?.description))")
        }
        if recordcount == 0 {
            diagonalline1.isHidden = true
            docimg1.isHidden = true
            doc1Buttonview.isHidden = true
            namelabel1.isHidden = true
            diagonalline2.isHidden = true
            docimg2.isHidden = true
            doc2Buttonview.isHidden = true
            namelabel2.isHidden = true
            diagonalline3.isHidden = true
            docimg3.isHidden = true
            doc3Buttonview.isHidden = true
            namelabel3.isHidden = true
            diagonalline4.isHidden = true
            docimg4.isHidden = true
            doc4Buttonview.isHidden = true
            namelabel4.isHidden = true
            line5.isHidden = true
            docimg5.isHidden = true
            doc5Buttonview.isHidden = true
            namelabel5.isHidden = true
            line6.isHidden = true
            docimg6.isHidden = true
            doc6Buttonview.isHidden = true
            namelabel6.isHidden = true
        }
    }
    
    
    func PopulateData(recordcount: Int, recordArray: [QBCOCustomObject]) {
        print("TrustedNetworkViewController:PopulateData()")
        self.receivedrecordcount = recordArray.count
        for i in 0..<self.receivedrecordcount {
            
            if  let username = (recordArray[i].fields?.value(forKey: "inviteefullname") as? String)  {

                if i == 0 { // for first position
                    self.doc1Data = recordArray[i]
                    self.doc1profileaction = true
                    self.namelabel1.text = "Dr. " + username
                    self.docimg1.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg1.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg1.layer.borderWidth = 0.5
                    self.docimg1.layer.cornerRadius = self.docimg1.bounds.size.height / 2
                    self.docimg1.layer.masksToBounds = true
                    //pass data for next view
                    doc1fullname = username
                    doc1imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc1userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc1speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.diagonalline2.isHidden = true
                    self.namelabel1.isHidden = false
                    docimg1.isHidden = false
                    doc1Buttonview.isHidden = false
                    diagonalline1.isHidden = false
                    self.docimg2.isHidden = true
                    self.doc2Buttonview.isHidden = true
                    self.namelabel2.isHidden = true
                    self.diagonalline3.isHidden = true
                    self.docimg3.isHidden = true
                    self.doc3Buttonview.isHidden = true
                    self.namelabel3.isHidden = true
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 1 { // for second position
                    self.doc2Data = recordArray[i]
                    self.doc2profileaction = true
                    self.namelabel2.text = "Dr. " + username
                    self.docimg2.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg2.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg2.layer.borderWidth = 0.5
                    self.docimg2.layer.cornerRadius = self.docimg2.bounds.size.height / 2
                    self.docimg2.layer.masksToBounds = true
                    //pass data for next view
                    doc2fullname = username
                    doc2imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc2userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc2speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel2.isHidden = false
                    self.docimg2.isHidden = false
                    self.doc2Buttonview.isHidden = false
                    self.namelabel2.isHidden = false
                    self.diagonalline3.isHidden = true
                    self.docimg3.isHidden = true
                    self.doc3Buttonview.isHidden = true
                    self.namelabel3.isHidden = true
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 2 { // for third position
                    self.doc3Data = recordArray[i]
                    self.doc3profileaction = true
                    self.namelabel3.text = "Dr. " + username
                    self.docimg3.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg3.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg3.layer.borderWidth = 0.5
                    self.docimg3.layer.cornerRadius = self.docimg3.bounds.size.height / 2
                    self.docimg3.layer.masksToBounds = true
                    //pass data for next view
                    doc3fullname = username
                    doc3imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc3userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc3speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel3.isHidden = false
                    self.diagonalline3.isHidden = false
                    self.docimg3.isHidden = false
                    self.doc3Buttonview.isHidden = false
                    self.diagonalline4.isHidden = true
                    self.docimg4.isHidden = true
                    self.doc4Buttonview.isHidden = true
                    self.namelabel4.isHidden = true
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 3 { // for forth position
                    self.doc4Data = recordArray[i]
                    self.doc4profileaction = true
                    self.namelabel4.text = "Dr. " + username
                    self.docimg4.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg4.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg4.layer.borderWidth = 0.5
                    self.docimg4.layer.cornerRadius = self.docimg4.bounds.size.height / 2
                    self.docimg4.layer.masksToBounds = true
                    //pass data for next view
                    doc4fullname = username
                    doc4imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc4userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc4speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel4.isHidden = false
                    self.diagonalline4.isHidden = false
                    self.docimg4.isHidden = false
                    self.doc4Buttonview.isHidden = false
                    self.line5.isHidden = true
                    self.docimg5.isHidden = true
                    self.doc5Buttonview.isHidden = true
                    self.namelabel5.isHidden = true
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }

                if i == 4 { // for fifth position
                    self.doc5Data = recordArray[i]
                    self.doc5profileaction = true
                    self.namelabel5.text = "Dr. " + username
                    self.docimg5.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg5.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg5.layer.borderWidth = 0.5
                    self.docimg5.layer.cornerRadius = self.docimg5.bounds.size.height / 2
                    self.docimg5.layer.masksToBounds = true
                    //pass data for next view
                    doc5fullname = username
                    doc5imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc5userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc5speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel5.isHidden = false
                    self.line5.isHidden = false
                    self.docimg5.isHidden = false
                    self.doc5Buttonview.isHidden = false
                    self.line6.isHidden = true
                    self.docimg6.isHidden = true
                    self.doc6Buttonview.isHidden = true
                    self.namelabel6.isHidden = true
                }
                
                if i == 5 { // for sixth position
                    self.doc6Data = recordArray[i]
                    self.doc6profileaction = true
                    self.namelabel6.text = "Dr. " + username
                    self.docimg6.sd_setImage(with: URL(string: (recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
                    self.docimg6.layer.borderColor = UIColor.lightGray.cgColor
                    self.docimg6.layer.borderWidth = 0.5
                    self.docimg6.layer.cornerRadius = self.docimg6.bounds.size.height / 2
                    self.docimg6.layer.masksToBounds = true
                    //pass data for next view
                    doc6fullname = username
                    doc6imgURL = ((recordArray[i].fields?.value(forKey: "inviteeprofileimageURL") as? String)!)
                    doc6userid = ((recordArray[i].fields?.value(forKey: "inviteedocuserid") as? Int)!)
                    doc6speciality = ((recordArray[i].fields?.value(forKey: "inviteespeciality") as? String)!)
                    self.namelabel6.isHidden = false
                    self.line6.isHidden = false
                    self.docimg6.isHidden = false
                    self.doc6Buttonview.isHidden = false
                }
                
            }
        }
    }

}
