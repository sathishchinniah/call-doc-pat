import UIKit
import MIBadgeButton_Swift
//import WibmoPaySDK
import Toast_Swift

var paymentCompleted = false
var currentConsultID = ""
var teamDocConsulted = false
var teamDocConsultID = ""
var dummpstatus = ["Online till 06:30 PM", "Expected Shortly",""]
let kDialogsPageLimit:UInt = 100

class DocConsultViewController: UIViewController, UISearchBarDelegate, QMChatServiceDelegate, QMChatConnectionDelegate, QMAuthServiceDelegate {
    
    @IBOutlet weak var callDocChatValueLabel: UILabel!
    @IBOutlet weak var docFeeChatValueLabel: UILabel!
    @IBOutlet weak var totalChatValue: UILabel!
    @IBOutlet weak var profileimageviewContainer: UIView!
    @IBOutlet var docprofileimageview: UIImageView!
    @IBOutlet var docnamelb: UILabel!
    @IBOutlet var docspacility: UILabel!
    @IBOutlet var docstatuslb: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var bgviewimg: UIImageView!
    @IBOutlet var dropdownspeaknow: UIView!
    @IBOutlet var dropdownconsultchargelbtxt: UILabel!
    @IBOutlet var docfeeval: UILabel!
    @IBOutlet var calldocval: UILabel!
    @IBOutlet var totalval: UILabel!
    @IBOutlet var bottombarview: UIView!
    @IBOutlet var chatUploadButtonview1: UIButton!
    @IBOutlet var speakNowButtonView: UIButton!
    @IBOutlet var dropdownchatview: UIView!
    @IBOutlet var dropdownconsultchargechatlbtxt: UILabel!
    @IBOutlet weak var topViewhightConstraint: NSLayoutConstraint!
    @IBOutlet var COTButtonview: UIButton!
    @IBOutlet weak var topimgeYdistanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var COTimgeYdistanceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var notifyMeView: UIView!
    @IBOutlet weak var notifyMeSwitch: UISwitch!
    
    lazy var paymentInstance = PaytmGateway.sharedInstance
    static let defaultArray = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false]//[false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    
    var mondayArrayOfBools = defaultArray
    var tuesdayArrayOfBools = defaultArray
    var wednesdayArrayOfBools = defaultArray
    var thursdayArrayOfBools = defaultArray
    var fridayArrayOfBools = defaultArray
    var saturdayArrayOfBools = defaultArray
    var sundayArrayOfBools = defaultArray
    var nextScheduleDay = ""
    var nextOnlineDay = ""
    var currentOnline = false
    var currentStatus = ""
    
    var nav: UINavigationController?
    private var didEnterBackgroundDate: NSDate?
    private var observer: NSObjectProtocol?
    var session : QBRTCSession!
    var dataPointer = GData.shared()
    var occupantsList = [NSNumber]()
    var dialog: QBChatDialog!
    var myindexval: Int = 100
    var thelastvc: String = ""
    var docname = ""
    var ConsultType = ""
    //var merchant:PGMerchantConfiguration!
    var doctorResumeConsult = true
    var doctorSchedularStatus = true
    var currentPatientName = ""
    var pagerIndexValue = 0
    var chatdialog: QBChatDialog!
    var consultID = ""
    var consultCategory = ""
    var doctorVerified = false
    var sourceView = ""
    var teamDocSelected = QBCOCustomObject()
//    var teamDocConsulted = false
//    var teamDocConsultID = ""
    
    enum specialities: String {
        case cardiologist = "Cardiologist"
        case dermatologist = "Dermatologist"
        case gastroentrologist = "Gastroentrologist"
        case ent = "E.N.T."
        case endocrinologist = "Endocrinologist"
        case pulmonoligist = "Pulmonoligist"
        case pediatrician = "Pediatrician"
        case nephrologist = "Nephrologist"
        case gp = "G.P."
        case gynaecologist = "Gynaecologist"
        case neurologist = "Neurologist"
        case dietician = "Dietician"
        case psychiatrist = "Psychiatrist"
        case oncologist = "Oncologist"
        case orthopedic = "Orthopedic"
        case sexologist = "Sexologist"
        case altMedicine = "Alt Medicine"
    }
    //
    //    struct MerchantDetails: MerchantTxnProtocol {
    //
    //        var merchantName: String
    //
    //        var merchantKey: Strin
    //
    //        var merchantCallbackUrl: String
    //
    //        var merchantTxnAmount: String
    //
    //        var merchantMessage: String
    //
    //        var merchantTxnId: String
    //
    //        var merchantPaymentOption: String
    //
    //        var payerName: String
    //
    //        var payerMobile: String
    //
    //        var payerEmailId: String
    //
    //        var buildEnv: BuildEnvironment
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        COTButtonview.isHidden = false
        navigationController?.navigationBar.tintColor = UIColor.white
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "greenbarbg")?.draw(in: self.view.bounds)
        let bgimage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.bottombarview.backgroundColor = UIColor(patternImage: bgimage)
        GlobalVariables.gcurrentdocfee = 0 // reset to zero
        
        if phone && maxLength == 812 { //iphone X
            COTimgeYdistanceConstraint.constant = 1.0
            topimgeYdistanceConstraint.constant = 55.0
        } else if phone && maxLength == 736 { //iphone 6 plus
            COTimgeYdistanceConstraint.constant = 1.0
            topimgeYdistanceConstraint.constant = 55.0
        } else if phone && maxLength == 667 { //iphone 6
            COTimgeYdistanceConstraint.constant = 1.0
            topimgeYdistanceConstraint.constant = 30.0
        } else if phone && maxLength == 568 { // iphone 5S
            COTimgeYdistanceConstraint.constant = 1.0
            topimgeYdistanceConstraint.constant = 20.0
        }
        docfeeval.text = ""
        calldocval.text = ""
        totalval.text = ""
        
        self.GetDoctorConsultCharges()
        
        let shadow0 = UIView(frame: CGRect(x: 0, y: 0, width: topView.frame.width, height: topView.frame.height))
        shadow0.layer.shadowOffset = CGSize.zero
        shadow0.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.15).cgColor
        shadow0.layer.shadowOpacity = 1
        shadow0.layer.shadowRadius = 2
        topView.addSubview(shadow0)
        
        let shadow1 = UIView(frame: CGRect(x: 0, y: 0, width: topView.frame.width, height: topView.frame.height))
        shadow1.layer.shadowOffset = CGSize(width: 0, height: 1)
        shadow1.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.23).cgColor
        shadow1.layer.shadowOpacity = 1
        shadow1.layer.shadowRadius = 1
        topView.addSubview(shadow1)
        
        dropdownspeaknow.layer.cornerRadius = 10
        dropdownchatview.layer.cornerRadius = 10
        dropdownspeaknow.isHidden = true
        dropdownchatview.isHidden = true
        chatUploadButtonview1.isHidden = true
        speakNowButtonView.isEnabled = false
        
       
        
        docprofileimageview.layer.cornerRadius = docprofileimageview.frame.size.width / 2;
        docprofileimageview.clipsToBounds = true;
        docprofileimageview.layer.borderWidth = 4.0
        docprofileimageview.layer.borderColor = UIColor.white.cgColor
        
        profileimageviewContainer.layer.cornerRadius = profileimageviewContainer.frame.size.width / 2;
        profileimageviewContainer.clipsToBounds = true;
        profileimageviewContainer.layer.borderWidth = 2.0
        profileimageviewContainer.layer.borderColor = UIColor.init(red: 0/255, green: 177/255, blue: 100/255, alpha: 1).cgColor
        
        if sourceView == "HOME" {
            docnamelb.text = "Dr " + ((DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String) ?? "" )
            
            docspacility.text = (DoctorsArray[myindexval].fields?.value(forKey: "doctorspeciality") as? String) ?? ""
            if (docspacility.text == "ent") || (docspacility.text == "gp") {
                docspacility.text = docspacility.text?.uppercased()
            } else {
                docspacility.text = docspacility.text?.capitalized
            }
            
            var profileurl = (DoctorsArray[myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
            if (profileurl == nil || profileurl.isEmpty) {
                /* string is  blank */
                profileurl = ""
            }
            teamDocConsulted = false
            self.docprofileimageview.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)
        } else {
            docnamelb.text = "Dr " + ((teamDocSelected.fields?.value(forKey: "inviteedocfullname") as? String) ?? "" )
            teamDocConsulted = true
//            docspacility.text = (DoctorsArray[myindexval].fields?.value(forKey: "doctorspeciality") as? String) ?? ""
//            if (docspacility.text == "ent") || (docspacility.text == "gp") {
//                docspacility.text = docspacility.text?.uppercased()
//            } else {
//                docspacility.text = docspacility.text?.capitalized
//            }
            
            var profileurl = (teamDocSelected.fields?.value(forKey: "inviteeprofileURL") as? String)!
            if (profileurl == nil || profileurl.isEmpty) {
                /* string is  blank */
                profileurl = ""
            }
            
            self.docprofileimageview.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)
        }
        
        
        docstatuslb.text = "Online till 06:30 PM" // [myindexval]
        self.getDoctorStatus()
        navigationItem.title = "Online"
        
        //left side Back Button
        let btnImage = UIImage(named: "backBtn-1")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton
        
        // right side Profile Button
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageview.sd_setImage(with: URL(string: patientimageurl[gindexval]), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        imageview.contentMode = UIViewContentMode.scaleToFill
        imageview.layer.cornerRadius = 20
        imageview.layer.masksToBounds = true
        containView.addSubview(imageview)
        let rightBarButton = UIBarButtonItem(customView: containView)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        self.DocConsultReadyConsultsDetails()
    }
    
    
    
    @objc func backBtnPressed(_ sender: Any) {
        guard let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        configureScreenStatus()
        
        
        super.viewWillAppear(true)
        navigationItem.title = "Online"
        navigationController!.navigationBar.barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.title = ""
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func ProfileButton(_ sender: UIButton) {
        let id = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? 0
        self.getDoctorGender(doctoruserid: id)
    }
    
    @IBAction func COTButton(_ sender: UIButton) {
        
        print("COT Button pressed")
        guard let cotViewController = self.storyboard!.instantiateViewController(withIdentifier: "CircleOfTrust") as? CircleOftrustViewController else { fatalError("Error") }
        cotViewController.myindexval = myindexval
        cotViewController.docStatus = self.currentStatus
        let selecteddocuid = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
        cotViewController.docuserid = selecteddocuid
        
        // these lines of code removes left and right animation
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        let animation = CATransition()
        animation.type = kCATransitionFade
        self.navigationController?.view.layer.add(animation, forKey: "someAnimation")
        self.nav = UINavigationController(rootViewController: cotViewController)
        _ = self.present(self.nav!, animated: false, completion: nil)
        CATransaction.commit()
        
    }
    
    @IBAction func notifyMeSwitchStatusChanged(_ sender: Any) {
    }
    
    
    @IBAction func SpeakNowButton(_ sender: UIButton) {
        
//        if self.doctorVerified == false {
//          return
//        }
        
        if sender.titleLabel?.text == "Consult trusted team" {
            guard let myteamviewController = self.storyboard?.instantiateViewController(withIdentifier: "TheDoctorTeamView") as? TheDoctorTeamViewController else { fatalError("Error") }
            myteamviewController.myindexval = myindexval
            myteamviewController.docuserid = (DoctorsArray[myindexval]/*teamDocSelected*/.fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
            myteamviewController.sourceController = "DocConsult"
            myteamviewController.docStatus = self.currentStatus
            let getRequest = NSMutableDictionary()
            getRequest["user_id"] = (DoctorsArray[myindexval]/*teamDocSelected*/.fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
            QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                //Handle Success
                print("NotificationTableViewController:addbadgebuttoncounts(): Reading DocCount Table !")
                if ((contributors?.count) ?? 0) > 0 {
                    let invitorRecordID = (contributors?[0].id) ?? ""
                    myteamviewController.DocCounttablerecordID =  invitorRecordID
                }
                
                self.navigationController?.pushViewController(myteamviewController, animated: true)
                return
            }) { (errorresponse) in
                    print("NotificationTableViewController:addbadgebuttoncounts() Response error: \(String(describing: errorresponse.error?.description))")
                return
            }
          return
        }
        if (ConsultType == "EXISTING") {
            print("DocConsultViewController:SpeakNowButton: EXISTING CONSULT ")
            if teamDocConsulted == true {
                let docselected =  (teamDocSelected.fields?.value(forKey: "inviteedocuserid") as? UInt) ?? 0
                let user = QBUUser()
                user.id =   docselected
                let VC1 = CallViewControllerswft.storyboardInstance()
                VC1.callingnumber = docselected
                VC1.doctorname = (teamDocSelected.fields?.value(forKey: "inviteedocfullname") as? String) ?? ""
                VC1.consultrecordIDpassed = PatientConsultsdetailsArray[0].id! as String
                VC1.callingPatientName = currentPatientName
                VC1.pagerIndex = pagerIndexValue
                VC1.isFirstConsult = false
                VC1.profileImage = (teamDocSelected.fields?.value(forKey: "inviteeprofileURL") as? String) ?? ""
                VC1.currentConsult = PatientConsultsdetailsArray[0]
                //let nav = UINavigationController(rootViewController: VC1)
                //self.present(nav, animated: true)
                //(UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.setViewControllers([VC1], animated: true)
                self.navigationController?.setViewControllers([VC1], animated: true)
                self.navigationController?.pushViewController(VC1, animated: true)
                return
            }
            
            let docselected =  (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
            let user = QBUUser()
            user.id =   docselected
            let VC1 = CallViewControllerswft.storyboardInstance()
            VC1.callingnumber = docselected
            VC1.doctorname = (DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String)!
            VC1.consultrecordIDpassed = PatientConsultsdetailsArray[0].id! as String
            VC1.callingPatientName = currentPatientName
            VC1.pagerIndex = pagerIndexValue
            VC1.isFirstConsult = false
            VC1.profileImage = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
            VC1.currentConsult = PatientConsultsdetailsArray[0]
            //let nav = UINavigationController(rootViewController: VC1)
            //self.present(nav, animated: true)
            //(UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)?.setViewControllers([VC1], animated: true)
            self.navigationController?.setViewControllers([VC1], animated: true)
            self.navigationController?.pushViewController(VC1, animated: true)
        }
        else if (ConsultType == "NEW")
        {
            print("DocConsultViewController:SpeakNowButton: NEW CONSULT ")
            dropdownspeaknow.isHidden = false
            dropdownchatview.isHidden = true
            chatUploadButtonview1.isHidden = false
            bottombarview.isHidden = true
            docfeeval.text = ""
            calldocval.text = ""
            totalval.text = ""
            dropdownconsultchargelbtxt.text = ""
            dropdownconsultchargelbtxt.text = "Total consultation charges can be up to Rs "
            dropdownconsultchargelbtxt.text =   (dropdownconsultchargelbtxt.text ?? "") + String(GlobalVariables.gcurrentdocfee)
            docfeeval.text = String(GlobalVariables.gcurrentdocfee)
            calldocval.text = "40"
            totalval.text = String(GlobalVariables.gcurrentdocfee + 40)
            
            if phone && maxLength == 812 {
                let newMultiplier:CGFloat = 0.461
                COTButtonview.isHidden = false
                topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
            } else {
                let newMultiplier:CGFloat = 0.44
                COTButtonview.isHidden = true
                topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
            }
        }
    }
    
    
    @IBAction func ChatButton(_ sender: UIButton) {
        
//        if self.doctorVerified == false {
//            return
//        }
        
        if (ConsultType == "EXISTING")
        {
            
            guard let chatConsultViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatConsultViewController") as? ChatConsultViewController else { fatalError("Error in opening ChatConsultViewController") }
            
            let docselected = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
            chatConsultViewController.docselected = docselected
            chatConsultViewController.pagerIndex = self.pagerIndexValue
            chatConsultViewController.indexVal = self.myindexval
            if PatientConsultsdetailsArray.count > 0 {
                chatConsultViewController.consultrecordIDpassed = (PatientConsultsdetailsArray[0].id as? String) ?? ""
                chatConsultViewController.currentConsult = PatientConsultsdetailsArray[0]
            }
            
            chatConsultViewController.docprofileimageurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
            self.navigationController?.pushViewController(chatConsultViewController, animated: true)
            
//            let user = QBUUser()
//            user.id =   docselected
//            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
//            chatDialog.occupantIDs = [user.id] as [NSNumber]
//            self.chatdialog = chatDialog
//            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
//                let lastMsgDate = chatDialog.lastMessageDate
//                guard let chatvc = self.storyboard!.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { fatalError("Error in opening ChatViewController") }
//                chatvc.dialog = createdDialog
//                self.createPushNotification()
//                chatvc.pagerIndex = self.pagerIndexValue
//                chatvc.indexVal = self.myindexval
//                chatvc.consultrecordIDpassed = PatientConsultsdetailsArray[0].id! as String
//                chatvc.docprofileimageurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
//                self.navigationController?.pushViewController(chatvc, animated: true)
//            }, errorBlock: {(response: QBResponse!) in
//                print("Error response + \(String(describing: response))")
//            })
            
        }
        else if (ConsultType == "NEW")
        {
            dropdownchatview.isHidden = false
            bottombarview.isHidden = true
            COTButtonview.isHidden = true
            docFeeChatValueLabel.text = ""
            callDocChatValueLabel.text = ""
            totalChatValue.text = ""
            dropdownconsultchargechatlbtxt.text = ""
            dropdownconsultchargechatlbtxt.text = "Total consultation charges can be up to Rs "
            dropdownconsultchargechatlbtxt.text = (dropdownconsultchargechatlbtxt.text ?? "") + String(GlobalVariables.gcurrentdocfee)
            docFeeChatValueLabel.text = String(GlobalVariables.gcurrentdocfee)
            callDocChatValueLabel.text = "40"
            totalChatValue.text = String(GlobalVariables.gcurrentdocfee + 40)
            if phone && maxLength == 812 {
                let newMultiplier:CGFloat = 0.461
                COTButtonview.isHidden = false
                topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
            } else {
                let newMultiplier:CGFloat = 0.42
                COTButtonview.isHidden = true
                topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
            }
        }
    }
    
    @IBAction func LastPresciptionButton(_ sender: MIBadgeButton) {
        let VC = ConsultDetailsViewController.storyboardInstance()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func LastConsultButton(_ sender: UIButton) {
        let VC1 = MyConsultsViewController.storyboardInstance()
        VC1.myindexval = myindexval
        VC1.docuseridpassed = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    @IBAction func dropdownCancelButton(_ sender: UIButton) {
        dropdownspeaknow.isHidden = true
        chatUploadButtonview1.isHidden = true
        bottombarview.isHidden = false
        COTButtonview.isHidden = false
        let newMultiplier:CGFloat = 0.50
        topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
    }
    
    // This function allows make video calls
    @IBAction func dropdownCallButton(_ sender: UIButton) {
        
        let docselected =  (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
        let user = QBUUser()
        user.id =   docselected
        let VC1 = CallViewControllerswft.storyboardInstance()
        VC1.callingPatientName = currentPatientName
        VC1.pagerIndex = pagerIndexValue
        VC1.callingnumber = docselected
        VC1.doctorname = (DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String) ?? ""
        VC1.profileImage = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
        VC1.isFirstConsult = true
        VC1.currentConsult = PatientConsultsdetailsArray[0]
        if !GlobalDocdata.mobilenumber.isEmpty {
            print("PaymentWebViewController:storing mobile number in first attempt")
        } else {
            if let tmpgmobile = UserDefaults.standard.string(forKey: "userphonenumber")  {
                print("PaymentWebViewController:storing mobile number in second attempt")
                GlobalDocdata.mobilenumber = tmpgmobile
            }
        }
        
        if (ConsultType == "NEW") {
            //  VC1.consultrecordIDpassed = ""
            consultCategory = "CALL"
            //createEntryIntoConsultsTable()
            //PayTm payment initialization
            paymentInstance.createOrderWith(sender: self, customerID: generateRandomCharacters(with: "CUST"), amount: String(GlobalVariables.gcurrentdocfee)/*"10.00"*/, orderID:generateRandomCharacters(with: "ORDER"))
            
            //  VC1.consultrecordIDpassed = String(user.id)//PatientConsultsdetailsArray[0].id! as String
            //self.navigationController?.pushViewController(VC1, animated: true)
            
        } else if (ConsultType == "EXISTING") {
            VC1.consultrecordIDpassed = PatientConsultsdetailsArray[0].id! as String
            self.navigationController?.pushViewController(VC1, animated: true)
        }
    }
    
    func getDoctorStatus() {
       docstatuslb.text = ""
        getSchedularDatafromBackend { (success) in
            if success {
                let dayOfTheWeek = Calendar.current.component(.weekday, from: Date())
                var nextScheduleArray = [Bool]()
                if dayOfTheWeek == 1 {
                    let status = self.getCurrentSchedule(array: self.sundayArrayOfBools)
                    self.docstatuslb.text = status
                }
                
                if dayOfTheWeek == 2 {
                    let status = self.getCurrentSchedule(array: self.mondayArrayOfBools)
                    self.docstatuslb.text = status
                }
                
                if dayOfTheWeek == 3 {
                    let status = self.getCurrentSchedule(array: self.tuesdayArrayOfBools)
                    self.docstatuslb.text = status
                }
                
                if dayOfTheWeek == 4 {
                    let status = self.getCurrentSchedule(array: self.wednesdayArrayOfBools)
                    self.docstatuslb.text = status
                }
                
                if dayOfTheWeek == 5 {
                    let status = self.getCurrentSchedule(array: self.thursdayArrayOfBools)
                    self.docstatuslb.text = status
                }
                
                if dayOfTheWeek == 6 {
                    let status = self.getCurrentSchedule(array: self.fridayArrayOfBools)
                    self.docstatuslb.text = status
                    
                }
                
                if dayOfTheWeek == 7 {
                    let status = self.getCurrentSchedule(array: self.saturdayArrayOfBools)
                    self.docstatuslb.text = status
                }
                self.view.reloadInputViews()
            }
        }
    }
    
    func getSchedularDatafromBackend(completion: @escaping (Bool) -> ()) {
        let getRequest = NSMutableDictionary()
        if teamDocConsulted == true {
            getRequest["user_id"] = ((teamDocSelected.fields?.value(forKey: "inviteedocuserid") as? UInt) ?? (0 as UInt) )
        } else {
            getRequest["user_id"] = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
        }
        //QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            
            if ((contributors?.count)! > 0) {
                self.mondayArrayOfBools = contributors![0].fields?.value(forKey: "Monday") as! [Bool]
                self.tuesdayArrayOfBools = contributors![0].fields?.value(forKey: "Tuesday") as! [Bool]
                self.wednesdayArrayOfBools = contributors![0].fields?.value(forKey: "Wednesday") as! [Bool]
                self.thursdayArrayOfBools = contributors![0].fields?.value(forKey: "Thursday") as! [Bool]
                self.fridayArrayOfBools = contributors![0].fields?.value(forKey: "Friday") as! [Bool]
                self.saturdayArrayOfBools = contributors![0].fields?.value(forKey: "Saturday") as! [Bool]
                self.sundayArrayOfBools = contributors![0].fields?.value(forKey: "Sunday") as! [Bool]
                
            }
            completion(true)
            
        }) { (errorresponse) in
            print("HomeViewController: getSchedularDatafromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func getCurrentSchedule(array: [Bool]) -> String {
        
        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute
        
        var selectedSchedules = [Double]()
        var unSelectedSchedules = [Double]()
        
        for (index, item) in array.enumerated() {
            let time: Double = (Double(index) / 2.0)
            if item {
                selectedSchedules.append(time)
            } else {
                unSelectedSchedules.append(time)
            }
        }
        
        for (index, selectedTime) in selectedSchedules.enumerated() {
            if selectedTime == currentTime{
                let nextScheduleIndex = index + 1
               // updateOnlineStatus(status: true)
                if (nextScheduleIndex + 1) <= selectedSchedules.count {
                    currentOnline = true
                    let time = getScheduleTime(index: nextScheduleIndex, selectedSchedules: selectedSchedules)
                    //                    runTimerToUpdateOnlineOfflineStatus(time: time)
                    return "Online till \(time)"
                } else {
                    return "Online"
                }
            }
        }
        
        for (index, unSelectedTime) in unSelectedSchedules.enumerated() {
            if unSelectedTime == currentTime {
                let nextScheduleIndex = index + 1
               // updateOnlineStatus(status: false)
                if (nextScheduleIndex + 1) <= unSelectedSchedules.count {
                    currentOnline = false
                    let time = getScheduleTime(index: nextScheduleIndex, selectedSchedules: unSelectedSchedules)
                    //                    runTimerToUpdateOnlineOfflineStatus(time: time)
                    return "Next Online at \(nextOnlineDay) \(time)"
                } else {
                    return "Offline"
                }
            }
        }
        
        return ""
    }
    
    func getScheduleTime(index: Int, selectedSchedules: [Double]) -> String {
        
        let nextSchedules = currentOnline ? Array(selectedSchedules[(index - 1)..<selectedSchedules.count]) : Array(selectedSchedules[(index - 1)..<selectedSchedules.count])//Array(selectedSchedules[index..<selectedSchedules.count])
        
        var nextScheduleTime = 0.0
        
        for (nIndex, nextSchedule) in nextSchedules.enumerated() {
            if nIndex == 0 {
                nextScheduleTime = nextSchedule
            } else {
                if nextSchedule == (nextScheduleTime + 0.5) {
                    nextScheduleTime = nextSchedule
                }
            }
            
            
        }
        
        if nextScheduleTime + 0.5 == 24.0 {
            //return "12.00 AM"
            let nextSchedule = getNextScheduleArray(currentDay: Calendar.current.component(.weekday, from: Date()))
            if nextSchedule.first == true || !currentOnline {
                let time = getNextScheduleTime(nextSchedule: nextSchedule)
                nextOnlineDay = nextScheduleDay
                return time
            } else {
                return "12.00 AM"
            }
            
            //return getCurrentSchedule(array: nextSchedule)
            
        }
        
        if nextScheduleTime + 0.5 == 24.5 {
            return "12.00 AM"
        }
        nextOnlineDay = "Today"
        return  getTimeFormatted(time: (nextScheduleTime + 0.5))
        
    }
    
    func getNextScheduleArray(currentDay: Int) -> [Bool] {
        
        var day: Int
        if currentDay == 7 {
            day = 0
        } else {
            day = currentDay
        }
        //var day = currentDay
        var nextScheduleArray = [Bool]()
        
        let arrayOfSchedules = [self.sundayArrayOfBools, self.mondayArrayOfBools, self.tuesdayArrayOfBools, self.wednesdayArrayOfBools, self.thursdayArrayOfBools, self.fridayArrayOfBools, self.saturdayArrayOfBools]
        let daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        
        while nextScheduleArray.isEmpty {
            let tempArr = arrayOfSchedules[day]
            for item in tempArr {
                if item {
                    nextScheduleArray = tempArr
                    //let index = arrayOfSchedules.index(of: tempArr)
                    if /*index*/day == Calendar.current.component(.weekday, from: Date()) {
                        //next day
                        nextScheduleDay = "Tomorrow"
                    } else {
                        //get the day
                        nextScheduleDay = daysOfWeek[day/*index!*/]
                    }
                    print("nextScheduleDay\(nextScheduleDay)")
                    return nextScheduleArray//break
                }
            }
            day = day + 1
            if day > 6 {
                day = 0
            }
            //continue
        }
        
        return nextScheduleArray
    }
    
    func getNextScheduleTime(nextSchedule: [Bool]) -> String {
        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute
        
        var selectedSchedules = [Double]()
        var unSelectedSchedules = [Double]()
        
        for (index, item) in nextSchedule.enumerated() {
            let time: Double = (Double(index) / 2.0)
            if item {
                selectedSchedules.append(time)
            } else {
                unSelectedSchedules.append(time)
            }
        }
        if selectedSchedules.first == 00.00 {
            
        } else { }
        for (index, selectedTime) in selectedSchedules.enumerated() {
            // if selectedTime/* == currentTime */{
            let nextScheduleIndex = index/* + 1*/
            //updateOnlineStatus(status: true)
            if (nextScheduleIndex + 1) <= selectedSchedules.count {
                //var time = getScheduleTime(index: (nextScheduleIndex), selectedSchedules: selectedSchedules)
                //let nextSchedules = Array(selectedSchedules[nextScheduleIndex..<selectedSchedules.count])
                
                var nextScheduleTime = selectedSchedules.first ?? 0.0
                
               
                return  getTimeFormatted(time: (nextScheduleTime))
                //                    runTimerToUpdateOnlineOfflineStatus(time: time)
                
                //return time
            } else {
                return ""
            }
            //}
        }
        return ""
    }
    
    func getTimeFormatted(time: Double) -> String {
        
        var formattedTime: Double = time
        
        var isAM = true
        
        if time >= 12 {
            if time > 12.5 {
                formattedTime = time - 12
            }
            isAM = false
        }
        
        let valueArray = "\(formattedTime)".components(separatedBy: ".")
        if let intValue = valueArray.first, let decimalValue = valueArray.last, let decimalDouble = Double(decimalValue) {
            return "\(intValue):\(decimalDouble == 5.0 ? "30" : "00") \(isAM ? "AM" : "PM")"
        }
        
        return ""
    }

    
    public func createEntryIntoConsultsTable(completion: @escaping (Bool) -> ()) {
        
//        ["patId":id,"calldoccode":patientcdcode[pagerIndex], "relation":patrelation[pagerIndex]/*"self"*/, "patientname": patientNames[pagerIndex], "patprofileid":patientimageurl[pagerIndex]/*GlobalDocdata.gprofileURL*/, "gender":patientgender[pagerIndex], "age":patientage[pagerIndex], "consultrecordID":consultrecordIDpassed ]
        paymentCompleted = false
        
        let calldoccode = patientcdcode[pagerIndexValue]//userInfo["calldoccode"]
        let relation = patrelation[pagerIndexValue]//userInfo["relation"]
        let patfullname = patientNames[pagerIndexValue]//userInfo["patientname"]
        let patprofileid = patientimageurl[pagerIndexValue]//userInfo["patprofileid"]
        let patgender = patientgender[pagerIndexValue]//userInfo["gender"]
        let patage = patientage[pagerIndexValue]//userInfo["age"]
        
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["patientuserid"] =  String((QBSession.current.currentUser?.id) ?? 0)//userID
        object.fields["docfullname"] = (DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String) ?? ""//GlobalDocData.gdocname
        object.fields["doctoruserid"] =  (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)//(DoctorsArray[myindexval].id/*fields?.value(forKey: "doctorfullname")*/ as? UInt) ?? (0 as UInt)
        object.fields["patfullname"] = patfullname
        object.fields["patrelation"] = relation
        object.fields["patcalldoccode"] = calldoccode
        object.fields["patprofileUID"] = patprofileid
        object.fields["prescriptionid"] = ""
        object.fields["amount"] =  String(GlobalVariables.gcurrentdocfee)//GlobalVariables.gDoctorFee
        object.fields["status"] = "pending"
        
        let permissions = QBCOPermissions()
        permissions.updateAccess = QBCOPermissionsAccessOpen
        //permissions.updateAccess(QBCOPermissionsAccessOpen)
        object.permissions = permissions
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
           // GlobalVariables.recordIDConsulttable = (contributors?.id)!  // this record id will be used later to update the record for
            // prescription related information
            
            let consultId = (contributors?.id)!
            currentConsultID = consultId
            self.consultID = consultId
            if teamDocConsulted == true {
                self.createEntryInTeamConsultsTable(consultId: consultId)
            }
            //PayTm payment initialization
            //self.paymentInstance.createOrderWith(sender: self, customerID: self.generateRandomCharacters(with: "CUST"), amount: String(GlobalVariables.gcurrentdocfee), orderID:self.generateRandomCharacters(with: "ORDER"))
            completion(true)
        }) { (response) in
            //Handle Error
            print("CallViewIncomingCallController:CreateNewEntrytoConsultTable: Response error: \(String(describing: response.error?.description))")
            completion(false)
        }
        

    }
    
    func createEntryInTeamConsultsTable(consultId: String) {
        
        
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        object.fields["consult_id"] =  consultId
        object.fields["team_doctor_name"] = ((teamDocSelected.fields?.value(forKey: "inviteedocfullname") as? String) ?? "" )//(DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String) ?? ""//GlobalDocData.gdocname
        object.fields["team_doctor_id"] =  ((teamDocSelected.fields?.value(forKey: "inviteedocuserid") as? UInt) ?? (0 as UInt) )//(DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)//(DoctorsArray[myindexval].id/*fields?.value(forKey: "doctorfullname")*/ as? UInt) ?? (0 as UInt)
        
        
        let permissions = QBCOPermissions()
        permissions.updateAccess = QBCOPermissionsAccessOpen
        //permissions.updateAccess(QBCOPermissionsAccessOpen)
        object.permissions = permissions
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("SUCCESS")
            teamDocConsultID = object.id ?? ""
        }) { (response) in
            //Handle Error
            print("CallViewIncomingCallController:CreateNewEntrytoConsultTable: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func configureScreenStatus() {
        var id = 0 as UInt
        if teamDocConsulted == true {
            id = ((teamDocSelected.fields?.value(forKey: "inviteedocuserid") as? UInt) ?? (0 as UInt) )
        } else {
            id = ((DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid")) as? UInt) ?? 0
        }

        
        self.getSchedularDatafromBackend(doctorId: id){ (success) in
            if success {
                
                let dayOfTheWeek = Calendar.current.component(.weekday, from: Date())
                
                var currentDayArray = [Bool]()
                if dayOfTheWeek == 1 {
                    //sunday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.sundaySchedularData) as! [Bool]
                }
                
                if dayOfTheWeek == 2 {
                    //monday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.mondaySchedularData) as! [Bool]
                }
                
                if dayOfTheWeek == 3 {
                    //tuesday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.tuesdaySchedularData) as! [Bool]
                    
                }
                
                if dayOfTheWeek == 4 {
                    //wednesday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.wednesdaySchedularData) as! [Bool]
                }
                
                if dayOfTheWeek == 5 {
                    //thursday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.thursdaySchedularData) as! [Bool]
                }
                
                if dayOfTheWeek == 6 {
                    //friday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.fridaySchedularData) as! [Bool]
                }
                
                if dayOfTheWeek == 7 {
                    //saturday
                    currentDayArray = UserDefaults.standard.value(forKey: Constants.saturdaySchedularData) as! [Bool]
                }
                let currentStatus = self.getCurrentOnlineOfflineStatus(currentDayBoolArray: currentDayArray)
                if currentStatus {
                    self.currentStatus = "online"
                    self.setScreenAsOnline()
                    if self.doctorResumeConsult == false {
                        self.currentStatus = "pause"
                        self.setScreenAsOnPause()
                        
                    }
                    if self.doctorSchedularStatus == false {
                        self.currentStatus = "offline"
                        self.setScreenAsOffline()
                    }
                } else {
                    self.currentStatus = "offline"
                    self.setScreenAsOffline()
                }
                
                //                if currentStatus && !self.doctorResumeConsult && !self.doctorSchedularStatus {
                //                    self.setScreenAsOnline()
                //                } else if !currentStatus {
                //                    self.setScreenAsOffline()
                //                } else if self.doctorSchedularStatus {
                //                    self.setScreenAsOffline()
                //                } else  if self.doctorResumeConsult {
                //                    self.setScreenAsOnPause()
                //                } else {
                //                    self.setScreenAsOnline()
                //                }
            }
        }
    }
    
    func getCurrentOnlineOfflineStatus(currentDayBoolArray: [Bool]) -> Bool {
        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute
        let returnValue = currentDayBoolArray[Int(currentTime * 2)]
        return returnValue
    }
    
    func getDoctorGender(doctoruserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctoruserid
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                
                let gender = (contributors![0].fields?.value(forKey: "gender") as? String) ?? ""
                let vc = SelfProfileViewController.storyboardInstance()
                let profileurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
                let selecteddocuid =  (DoctorsArray[self.myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
                vc.imageURLpassed = profileurl
                vc.docuseridpassed = selecteddocuid
                vc.docGender = gender
                vc.docnamepassed = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorfullname") as? String)!
                vc.docspecialitypassed = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorspeciality") as? String)!
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("getDoctorGender() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func ChatUploadButton1(_ sender: UIButton) {
        dropdownspeaknow.isHidden = true
        dropdownchatview.isHidden = false
        chatUploadButtonview1.isHidden = true
        COTButtonview.isHidden = true
        
        if phone && maxLength == 812 {
            let newMultiplier:CGFloat = 0.461
            COTButtonview.isHidden = false
            topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
        } else {
            let newMultiplier:CGFloat = 0.350
            COTButtonview.isHidden = true
            topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
        }
        
    }
    
    @IBAction func CancelChatdropdownButton(_ sender: UIButton) {
        dropdownchatview.isHidden = true
        bottombarview.isHidden = false
        COTButtonview.isHidden = false
        let newMultiplier:CGFloat = 0.50
        topViewhightConstraint = topViewhightConstraint.setMultiplier(multiplier: newMultiplier)
    }
    
    // finally go to chatviewcontroller from here
    @IBAction func dropdownChatNowButton(_ sender: UIButton) {
        
        if (ConsultType == "NEW") {
            consultCategory = "CHAT"
            //createEntryIntoConsultsTable()
            paymentInstance.createOrderWith(sender: self, customerID: generateRandomCharacters(with: "CUST"), amount: String(GlobalVariables.gcurrentdocfee), orderID:generateRandomCharacters(with: "ORDER"))
            
            /*let docselected = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
            
            let user = QBUUser()
            user.id =   docselected
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.occupantIDs = [user.id] as [NSNumber]
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                guard let chatvc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { fatalError("Error in opening ChatViewController") }
                chatvc.dialog = createdDialog
                chatvc.consultrecordIDpassed = ""
                chatvc.docprofileimageurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
                chatvc.opponentDoctorId = docselected
                chatvc.profPicURL = patientimageurl[self.pagerIndexValue]
                self.createPushNotification()
                self.navigationController?.pushViewController(chatvc, animated: true)
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(String(describing: response))")
            }) */
        } else {

            guard let chatConsultViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatConsultViewController") as? ChatConsultViewController else { fatalError("Error in opening ChatConsultViewController") }
            
            let docselected = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
            chatConsultViewController.docselected = docselected
            chatConsultViewController.pagerIndex = self.pagerIndexValue
            chatConsultViewController.indexVal = self.myindexval
            chatConsultViewController.consultrecordIDpassed = PatientConsultsdetailsArray[0].id! as String
            chatConsultViewController.currentConsult = PatientConsultsdetailsArray[0]
            chatConsultViewController.docprofileimageurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
            self.navigationController?.pushViewController(chatConsultViewController, animated: true)
        }
    }
    
    func createPushNotification() {
        
        guard let _ = self.chatdialog else { return }
        
        let payload = NSMutableDictionary()
        let patName = QBSession.current.currentUser?.fullName ?? ""
        let alertDict = ["alert":"New consult request from \(String(describing: patName))","sound":"default"]
        
        let opponentsIDs = String((DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? 0)//(self.session?.opponentsIDs as NSArray?)?.componentsJoined(by: ",")
        
        payload.setValue("request", forKey: "alert")
        payload.setValue("TITLE", forKey: "title")
        payload.setValue(201, forKey: "code")
        payload.setValue(alertDict, forKey: "aps")
        //        payload.setValue("5", forKey: "ios_badge")
        //        payload.setValue("mysound.wav", forKey: "ios_sound")
        //        payload.setValue("New Chat Message", forKey: "ios_alert")
        payload.setValue(String(Int((self.chatdialog.userID) )), forKey: "user_id")
        payload.setValue("10", forKey: "thread_id")
        payload.setValue(QBSession.current.currentUser?.fullName, forKey: "patientName")
        payload.setValue(GlobalVariables.gCurrentUser.id, forKey: "patientId")
        payload.setValue(/*self.profPicURL*/GlobalVariables.gcurrentprofileURL, forKey: "patProfilePic")
        //payload.setValue(GlobalVariables.gCurrentUser, forKey: "patientDetails")
        
        
        let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
        
        let message = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = opponentsIDs
        event.type = QBMEventType.oneShot
        event.message = message
        
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        
        
        
        QBRequest.createEvent(event, successBlock: { (_, _) in
            print("EVENT CREATED SUCCESSFULLY")
            self.view.makeToast("Notification sent to the doctor", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }) { (error:QBResponse?) in
            print("ERROR: \(String(describing: error))")
            self.view.makeToast("Couldn't notify doctor. Please try again later.", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("DocConsultViewController:prepare")
    }
    
    //for the chat functionality
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if (QBChat.instance.isConnected) {
            self.getDialogs()
        }
    }
    // MARK: - Notification handling
    
    func didEnterBackgroundNotification() {
        self.didEnterBackgroundDate = NSDate()
    }
    
    // MARK: - DataSource Action
    
    func getDialogs() {
        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
                // chatDialog = dialogObjects
            }, completionBlock: { (response) -> Void in
                if (response.isSuccess) {
                    ServicesManager.instance().lastActivityDate = NSDate() as Date as? NSDate
                }
            })
        } else {
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            }, completion: { (response: QBResponse?) -> Void in
                guard response != nil && response!.isSuccess else {
                    return
                }
                ServicesManager.instance().lastActivityDate = NSDate() as Date as? NSDate
            })
        }
    }
    
    func reloadtheDialogs() {
        print("Reload the Dialogs")
        dialog = self.dialogs()?[0]
        
    }
    // MARK: - DataSource
    
    func dialogs() -> [QBChatDialog]? {
        return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
    }
    
    // MARK: - QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        reloadtheDialogs()
    }
    
    func chatService(_ chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
        reloadtheDialogs()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        reloadtheDialogs()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        reloadtheDialogs()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        reloadtheDialogs()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        reloadtheDialogs()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        reloadtheDialogs()
    }
    
    // MARK: QMChatConnectionDelegate
    
    func chatServiceChatDidFail(withStreamError error: Error) {
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        self.getDialogs()
    }
    
    func chatService(_ chatService: QMChatService,chatDidNotConnectWithError error: Error){
        SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        self.getDialogs()
    }
    
    func DocConsultReadyConsultsDetails() {
        PatientConsultsdetailsArray.removeAll()
        
        let getRequest = NSMutableDictionary()
//        if sourceView == "HOME" {
//            getRequest[/*"user_id"*/"doctoruserid"] = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
//        } else {
//            getRequest[/*"user_id"*/"doctoruserid"] = (teamDocSelected.fields?.value(forKey: "inviteedocuserid") as? UInt) ?? (0 as UInt)
//        }
        
        getRequest[/*"user_id"*/"doctoruserid"] = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
        if patientcdcode.count > pagerIndexValue {
            getRequest["patcalldoccode"] =  patientcdcode[pagerIndexValue]
        }
        //include status to ease fetching of data
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {

                print("DocConsultViewController:DocConsultReadyConsultsDetails: PatientConsultsdetailsArray not empty")
                PatientConsultsdetailsArray = contributors!
                PatientConsultsdetailsArray.reverse()
                print("DocConsultViewController:DocConsultReadyConsultsDetails:reversed")
//                let currenttime: Int = Int(NSDate().timeIntervalSince1970)
//                let createdat  = PatientConsultsdetailsArray[0].createdAt! as NSDate
//                let createdatepochtime = Int(createdat.timeIntervalSince1970)
//
//                let diff = currenttime - createdatepochtime
//                let hourslasped = diff/3600
                
//                if ( hourslasped >= 72) {
//                    self.ConsultType = "NEW"
//                } else {
//                    self.ConsultType = "EXISTING"
//                }
                
                let status = (PatientConsultsdetailsArray[0].fields.value(forKey: "status") as? String) ?? ""
                if status == "closed" {
                    self.ConsultType = "NEW"
                } else {
                    self.ConsultType = "EXISTING"
                }
                self.speakNowButtonView.isEnabled = true
            } else {
                PatientConsultsdetailsArray = contributors ?? []
                self.ConsultType = "NEW"
                self.speakNowButtonView.isEnabled = true
            }

        }) { (response) in
            //Error handling
            print("DocConsultViewController:DocConsultReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func GetDoctorConsultCharges() {
        
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                
                if let tmpdocfee = (contributors![0].fields?.value(forKey: "fee") as? Int)
                {
                    GlobalVariables.gcurrentdocfee = tmpdocfee
                }
                
                if let verified = (contributors![0].fields?.value(forKey: "profileverified") as? Bool) {
                    
                    if verified {
                        self.doctorVerified = true
                    } else {
                        self.doctorVerified = false
                    }
                    
                }
                
                print("DocConsultViewController:GetDoctorConsultCharges()= \(GlobalVariables.gcurrentdocfee)")
                
            }
        }) { (response) in
            //Error handling
            print("DocConsultViewController:GetDoctorConsultCharges: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    
    //MARK:- Storyboard Instance
    
    static func storyboardInstance() -> DocConsultViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return (storyboard.instantiateViewController(withIdentifier: "DocConsult") as? DocConsultViewController) ?? DocConsultViewController()
    }
    
}

class DialogTableViewCellModel: NSObject {
    
    var detailTextLabelText: String = ""
    var textLabelText: String = ""
    var unreadMessagesCounterLabelText : String?
    var unreadMessagesCounterHiden = true
    var dialogIcon : UIImage?
    
    init(dialog: QBChatDialog) {
        super.init()
        
        switch (dialog.type){
        case .publicGroup:
            self.detailTextLabelText = "public group"
        case .group:
            self.detailTextLabelText = "group"
        case .private:
            self.detailTextLabelText = "private"
            
            if dialog.recipientID == -1 {
                return
            }
            
            // Getting recipient from users service.
            if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(dialog.recipientID)) {
                self.textLabelText = recipient.login ?? recipient.email!
            }
        }
        
        if self.textLabelText.isEmpty {
            // group chat
            if let dialogName = dialog.name {
                self.textLabelText = dialogName
            }
        }
        
        // Unread messages counter label
        
        if (dialog.unreadMessagesCount > 0) {
            
            var trimmedUnreadMessageCount : String
            
            if dialog.unreadMessagesCount > 99 {
                trimmedUnreadMessageCount = "99+"
            } else {
                trimmedUnreadMessageCount = String(format: "%d", dialog.unreadMessagesCount)
            }
            
            self.unreadMessagesCounterLabelText = trimmedUnreadMessageCount
            self.unreadMessagesCounterHiden = false
            
        } else {
            
            self.unreadMessagesCounterLabelText = nil
            self.unreadMessagesCounterHiden = true
        }
        
        // Dialog icon
        
        if dialog.type == .private {
            self.dialogIcon = UIImage(named: "user")
        } else {
            self.dialogIcon = UIImage(named: "group")
        }
    }
}

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

// MARK: - PGTransaction delegate methods

extension DocConsultViewController: PayTmDelegateMethods {
    func didFinishedResponse(data: JSON) {
        guard data != JSON.null else {
            showAlert(title: "Error", message: "Transection failed due to some error if your amount will deducted, we will refund you in 7 working days")
            //showAlertView(title: "Error", message: "Transection failed due to some error if your amount will deducted, we will refund you in 7 working days")
            return
        }
        if data["STATUS"].stringValue == "TXN_SUCCESS" {
            print("Success: \n\(String(describing: data))")
            
            createEntryIntoConsultsTable { (response) in
                if response == true {
                    paymentCompleted = true
                    //Change the controller to chat
                    switch self.consultCategory {
                    case "CALL":
                        
                        let docselected =  (DoctorsArray[self.myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
                        let user = QBUUser()
                        user.id =   docselected
                        let VC1 = CallViewControllerswft.storyboardInstance()
                        VC1.callingPatientName = self.currentPatientName
                        VC1.pagerIndex = self.pagerIndexValue
                        VC1.callingnumber = docselected
                        VC1.isFirstConsult = true
                        VC1.doctorname = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorfullname") as? String) ?? ""
                        VC1.profileImage = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
                        VC1.consultrecordIDpassed = self.consultID
                        VC1.currentConsult = PatientConsultsdetailsArray[0]
                        if !GlobalDocdata.mobilenumber.isEmpty {
                            print("PaymentWebViewController:storing mobile number in first attempt")
                        } else {
                            if let tmpgmobile = UserDefaults.standard.string(forKey: "userphonenumber")  {
                                print("PaymentWebViewController:storing mobile number in second attempt")
                                GlobalDocdata.mobilenumber = tmpgmobile
                            }
                        }
                        
                        //VC1.consultrecordIDpassed = String(user.id)//PatientConsultsdetailsArray[0].id! as String
                        self.navigationController?.pushViewController(VC1, animated: true)
                        
                        
                    case "CHAT":
                        
                        guard let chatConsultViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatConsultViewController") as? ChatConsultViewController else { fatalError("Error in opening ChatConsultViewController") }
                        
                        let docselected = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
                        chatConsultViewController.docselected = docselected
                        chatConsultViewController.pagerIndex = self.pagerIndexValue
                        chatConsultViewController.indexVal = self.myindexval
                        chatConsultViewController.consultrecordIDpassed = (PatientConsultsdetailsArray[0].id) ?? ""
                        chatConsultViewController.currentConsult = PatientConsultsdetailsArray[0]
                        chatConsultViewController.docprofileimageurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
                        self.navigationController?.pushViewController(chatConsultViewController, animated: true)
                        
                        
                    default:
                        break
                    }
                    
                } else {
                    paymentCompleted = false
                }
            }
        } else {
            paymentCompleted = false
            showAlert(title: "Error", message: "Transaction Failed")
            self.dismiss(animated: true, completion: nil)
            QBRequest.deleteObject(withID: self.consultID, className: "ConsultsTable", successBlock: { (response) in
                print("DELETION SUCCESSFUL")
            }) { (errorResponse) in
               print("Response error: \(String(describing: errorResponse.error?.description))")
            }
            if teamDocConsultID != "" {
                QBRequest.deleteObject(withID: teamDocConsultID, className: "TeamConsultsTable", successBlock: { (response) in
                    print("DELETION SUCCESSFUL")
                }) { (errorResponse) in
                    print("Response error: \(String(describing: errorResponse.error?.description))")
                }
            }
            //showAlertView(title: "Error", message: "Transection Fail")
        }
    }
    
    func didCancelTrasaction() {
        showAlert(title: "Error", message: "Transection cancel by user")
    }
    
    func errorMisssingParameter(error: Error!) {
        showAlert(title: "Error", message: error.localizedDescription)
    }
}

//extension DocConsultViewController : PGTransactionDelegate{
//
//    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
//        print("DocConsultViewController:didFinishedResponse:PGTransactionViewController")
//    }
//
//    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
//         print("DocConsultViewController:didCancelTrasaction:PGTransactionViewController")
//    }
//
//    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
//        print("DocConsultViewController:errorMisssingParameter:PGTransactionViewController: Error = \(error)")
//        print(error.localizedDescription)
//    }
//
//    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
//        print(response)
//        showAlert(title: "Transaction Successfull", message: NSString.localizedStringWithFormat("Response- %@", response) as String)
//    }
//
//
//    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
//        print(error)
//        showAlert(title: "Transaction Failed", message: error.localizedDescription)
//    }
//    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
//
//        showAlert(title: "Transaction Cancelled", message: error.localizedDescription)
//
//    }
//
//    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
//        print(response)
//        showAlert(title: "cas", message: "")
//    }
//
//
//
//}

extension DocConsultViewController {
    
    func getSchedularDatafromBackend(doctorId: UInt, completion: @escaping (Bool) -> ()) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctorId//QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //contributors![0].value(forKey: "resumeConsult")
            //contributors![0].fields.value(forKey: "schedularStatus")
            if ((contributors?.count)! > 0) {
                
                self.mondayArrayOfBools = contributors![0].fields?.value(forKey: "Monday") as! [Bool]
                self.tuesdayArrayOfBools = contributors![0].fields?.value(forKey: "Tuesday") as! [Bool]
                self.wednesdayArrayOfBools = contributors![0].fields?.value(forKey: "Wednesday") as! [Bool]
                self.thursdayArrayOfBools = contributors![0].fields?.value(forKey: "Thursday") as! [Bool]
                self.fridayArrayOfBools = contributors![0].fields?.value(forKey: "Friday") as! [Bool]
                self.saturdayArrayOfBools = contributors![0].fields?.value(forKey: "Saturday") as! [Bool]
                self.sundayArrayOfBools = contributors![0].fields?.value(forKey: "Sunday") as! [Bool]
                
                UserDefaults.standard.set(self.mondayArrayOfBools, forKey: Constants.mondaySchedularData)
                UserDefaults.standard.set(self.tuesdayArrayOfBools, forKey: Constants.tuesdaySchedularData)
                UserDefaults.standard.set(self.wednesdayArrayOfBools, forKey: Constants.wednesdaySchedularData)
                UserDefaults.standard.set(self.thursdayArrayOfBools, forKey: Constants.thursdaySchedularData)
                UserDefaults.standard.set(self.fridayArrayOfBools, forKey: Constants.fridaySchedularData)
                UserDefaults.standard.set(self.saturdayArrayOfBools, forKey: Constants.saturdaySchedularData)
                UserDefaults.standard.set(self.sundayArrayOfBools, forKey: Constants.sundaySchedularData)
                
                
                self.doctorResumeConsult = (contributors![0].fields.value(forKey: "resumeConsult") as? Bool) ?? false
                self.doctorSchedularStatus = (contributors![0].fields.value(forKey: "SchedularStatus") as? Bool) ?? false
            }
            completion(true)
            
        }) { (errorresponse) in
            print("HomeViewController: getSchedularDatafromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func setScreenAsOnline() {
        COTButtonview.setImage(#imageLiteral(resourceName: "COT"), for: .normal)
        self.view.layer.contents = #imageLiteral(resourceName: "bg_green").cgImage
        bgviewimg.image = #imageLiteral(resourceName: "bg_green")
        self.bottombarview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "bg_bottombar"))
        bottombarview.backgroundColor = UIColor(red:0.33, green:0.76, blue:0.44, alpha:0.60)
        profileimageviewContainer.layer.borderColor = UIColor(red:0.33, green:0.76, blue:0.44, alpha:0.60).cgColor
    }
    
    func setScreenAsOnPause() {
        //        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PauseDocConsultViewController") as? PauseDocConsultViewController else {
        //            COTButtonview.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
        //            self.view.layer.contents = #imageLiteral(resourceName: "bg_orange").cgImage
        //            bgviewimg.image = #imageLiteral(resourceName: "bg_orange")
        //            self.bottombarview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "bg_bottombar"))
        //            bottombarview.backgroundColor = UIColor(red:0.98, green:0.61, blue:0, alpha:0.40)
        //            profileimageviewContainer.layer.borderColor = UIColor(red:0.98, green:0.61, blue:0, alpha:0.40).cgColor
        //            return
        //        }
        //        viewController.myindexval = myindexval
        //        self.navigationController?.pushViewController(viewController, animated: true)
        //self.present(viewController, animated: true, completion: nil)
        COTButtonview.setImage(#imageLiteral(resourceName: "COTOrange"), for: .normal)
        self.view.layer.contents = #imageLiteral(resourceName: "bg_orange").cgImage
        bgviewimg.image = #imageLiteral(resourceName: "bg_orange")
        self.bottombarview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "bg_bottombar"))
        bottombarview.backgroundColor = UIColor(red:0.98, green:0.61, blue:0, alpha:0.40)
        profileimageviewContainer.layer.borderColor = UIColor(red:0.98, green:0.61, blue:0, alpha:0.40).cgColor
        self.speakNowButtonView.setTitle("Consult trusted team", for: .normal)
    }
    
    func setScreenAsOffline() {
        COTButtonview.setImage(#imageLiteral(resourceName: "COTRed"), for: .normal)
        self.view.layer.contents = #imageLiteral(resourceName: "bg_red").cgImage
        bgviewimg.image = #imageLiteral(resourceName: "bg_red")
        self.bottombarview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "bg_bottombar"))
        bottombarview.backgroundColor = UIColor(red:250/255, green:55/255, blue:70/255, alpha:0.40)
        profileimageviewContainer.layer.borderColor = UIColor(red:250/255, green:55/255, blue:70/255, alpha:0.40).cgColor
        self.speakNowButtonView.setTitle("Consult trusted team", for: .normal)
    }
    
    func generateRandomCharacters(with prefix:String) -> String {

        let letters : NSString = "0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 9 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return prefix + randomString
    }
}
