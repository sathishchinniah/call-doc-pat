import UIKit

class OnBoardViewController: UIViewController, UIScrollViewDelegate {

    //MARK:- Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var PageCtrl: UIPageControl!
    @IBOutlet weak var ButtonSignIn: UIButton! {
        didSet {
            ButtonSignIn.layer.borderColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1.0).cgColor
            ButtonSignIn.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var ButtonRegistration: UIButton!

    //MARK:- Variables and Constants
    var titleTextArray = ["Keep your number private!", "Real time communication", "Manage own Doctors", "Add and Search Doctors"]
    var descTextArray = ["Connect with your patients using Chat, Videos or Voice","Do real time Audio/Video calls and text chat with you trusted Doctor", "Manage your doctors and for your family members", "Add and Search your trusted doctor from Specialities"]
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        self.initializeView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- IBAction methods
    
    @IBAction func SignInButtonClicked(_ sender: UIButton!) {
        performSegue(withIdentifier: "gotoMain", sender: self)
    }
    
    @IBAction func RegisterBtnClicked(_ sender: UIButton!) {
        performSegue(withIdentifier: "gotoRegister", sender: self)
    }

    //MARK: UIScrollView Delegate

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let viewWidth: CGFloat = UIScreen.main.bounds.width
        var pageNumber = floor((scrollView.contentOffset.x - viewWidth / 70) / viewWidth) + 1
        if Int(pageNumber) > 3 {
            scrollView.contentOffset.x = 0
            pageNumber = 0
        }
        PageCtrl.currentPage = Int(pageNumber)
        titleLabel.text = titleTextArray[PageCtrl.currentPage]
        descLabel.text = descTextArray[PageCtrl.currentPage]
    }

    // MARK: - Custom Methods
    private func initializeView() {
        self.initializeWalkThrough()
    }

    private func initializeWalkThrough() {
        let pageCount : CGFloat = 4
        scrollView.backgroundColor = UIColor.clear
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width * pageCount, height: scrollView.frame.size.height)
    }
    
    @objc func timerAction() {
        let xOffSet = scrollView.contentOffset.x + UIScreen.main.bounds.width
        DispatchQueue.main.async() {
            UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollView.contentOffset.x = xOffSet
            }, completion: nil)
        }
    }

}

