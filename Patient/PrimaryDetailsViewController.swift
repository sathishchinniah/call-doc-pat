import UIKit
import DropDown

struct GlobalDocdata {
    static var patientname = ""
    static var displayname = ""
    static var email = ""
    static var mobilenumber = ""
    static var bloodgroup = ""
    static var gender = ""
    static var age = ""
    static var heightft = ""
    static var heightinch = ""
    static var weight = ""
    static var allergies = ""
    static var chronichealthissues = ""
    static var currentmedication = ""
    static var gprofileURL = ""
    static var gthefinalcalldoccode = ""
    static var firsttimeview = true
    static var regcalldoccode = ""
}

class PrimaryDetailsViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet var patientnametxt: UILabel!
    @IBOutlet var profileimageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileimgButtonview: UIButton!
    @IBOutlet var topView: UIView!
    @IBOutlet weak var bottomBarView: UIView!
    @IBOutlet var docimg: UIImageView! {
        didSet {
            self.docimg.layer.cornerRadius = self.docimg.frame.size.width / 2;
            self.docimg.clipsToBounds = true;
            self.docimg.layer.borderWidth = 0.2
            self.docimg.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var cancelButton: UIButton!
    
    var nav: UINavigationController?
    var profileimgbool: Bool = false
    let dropDownGender = DropDown()
    let dropDownage = DropDown()
    let dropDownblood = DropDown()
    let dropDownhightfeet = DropDown()
    let dropDownhightinch = DropDown()
    let dropDownweight = DropDown()
    var activeField : UITextView!
    var primaryDetailsTVC: PrimaryDetailsTableViewController?
    var theprofileurl = ""
    var theprofileimageUID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        profileimageActivityIndicator.isHidden = true
        bottomBarView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)

        primaryDetailsTVC?.addressfield.delegate = self
        primaryDetailsTVC?.addressfield.textContainer.maximumNumberOfLines = 3
        primaryDetailsTVC?.cityTextField.delegate = self
        primaryDetailsTVC?.stateTextField.delegate = self
        primaryDetailsTVC?.pincodeTextField.delegate = self
        primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
        primaryDetailsTVC?.pincodeTextField.isHidden = true
        primaryDetailsTVC?.cityTextField.isHidden = true
        primaryDetailsTVC?.stateTextField.isHidden = true
        primaryDetailsTVC?.addressfield.isHidden = true
        primaryDetailsTVC?.addressLabel.isHidden = true
        primaryDetailsTVC?.addressSeparator.isHidden = true
        primaryDetailsTVC?.pincodeLabel.isHidden = true
        primaryDetailsTVC?.pincodeSeparator.isHidden = true
        primaryDetailsTVC?.cityStackView.isHidden = true
        
        self.hideKeyboardWhenTappedAround()
        
        LoadProfileImage()
        
        if GlobalDocdata.firsttimeview {
            print( "PrimaryDetailsViewDocController: this is for first time user" )
            
            // do this for existing user
            if isKeyPresentInUserDefaults(key: "existinguser") {
                // This is first time user from Existing User flow
                let isexistinguser = UserDefaults.standard.object(forKey: "existinguser") as! Bool
                if(isexistinguser) {
                    //This will get called when user delets the app and comes to App after fresh install
                    self.cancelButton.isHidden = true
                    self.firsttimeReadingofExistingUser()
                } else {
                    // We are coming here as first time and not as existing user then it has to be First time Registration flow
                    // First time Registration flow hence we should generate CallDoc code here
                    self.GenerateCallDocCode()
                    self.cancelButton.isHidden = true
                    self.updateUserEmail()
                    // On Registration page we store these values to persistance hence we read it here
                    // This is first time user from Registration flow
                    print( "PrimaryDetailsViewDocController:  This is first time user from Registration flow" )
                    if let tmpgdocname = UserDefaults.standard.string(forKey: "fullname")  {
                        GlobalDocdata.patientname = tmpgdocname
                    }
                    if let tmpgdocemail = UserDefaults.standard.string(forKey: "email")  {
                        GlobalDocdata.email = tmpgdocemail
                    }
                    if let tmpgdocmobile = UserDefaults.standard.string(forKey: "userphonenumber")  {
                        GlobalDocdata.mobilenumber = tmpgdocmobile
                    }
                    
                    if let tmpgdocdisplayname = UserDefaults.standard.string(forKey: "displayname")  {
                        GlobalDocdata.displayname = tmpgdocdisplayname
                    }
                    
                    // we are doing this here because we are coming as new registered user using calddoc code, this recalldoccode must have
                    // been entred by user at new Registration screen and this callcode he must have got from any doctor either by SMS invite
                    // or he may have got by verbal inputs
                    if let tmpgcalldoccode = UserDefaults.standard.string(forKey: "regcalldoccode")  {
                        GlobalDocdata.regcalldoccode = tmpgcalldoccode
                        if GlobalDocdata.regcalldoccode.count == 6 {
                            self.GetDoctorDatausingregcalldoccode()
                        }
                    }
                }
            } else {
                // Something not right
                print( "PrimaryDetailsViewDocController: Something is not right existinguser key is not found")
            }
        } else {
            // this part is not for first time user, that means user is on the app and he is coming from Hamberger Menu options
            
            print( "PrimaryDetailsViewController: this is for not first time user and he is coming from Hemberger Menu ")
            self.cancelButton.isHidden = false
            if isKeyPresentInUserDefaults(key: "fullname") {
                self.patientnametxt.text = UserDefaults.standard.string(forKey: "fullname")
            }
            if let tmpgdocemail = UserDefaults.standard.string(forKey: "email")  {
                GlobalDocdata.email = tmpgdocemail
                primaryDetailsTVC?.emailTextField.text = tmpgdocemail
            }
            
            if isKeyPresentInUserDefaults(key: "gender") {
                primaryDetailsTVC?.genderTextField.text = UserDefaults.standard.string(forKey: "gender")
            }

            if isKeyPresentInUserDefaults(key: "age") {
                primaryDetailsTVC?.ageTextField.text = UserDefaults.standard.string(forKey: "age")
            }

            if isKeyPresentInUserDefaults(key: "heightft") {
                primaryDetailsTVC?.heightFeetTextField.text = UserDefaults.standard.string(forKey: "heightft")
            }

            if isKeyPresentInUserDefaults(key: "heightinch") {
                primaryDetailsTVC?.heightInchesTextField.text = UserDefaults.standard.string(forKey: "heightinch")
            }

            if isKeyPresentInUserDefaults(key: "weight") {
                primaryDetailsTVC?.weightTextField.text = UserDefaults.standard.string(forKey: "weight")
            }

            if isKeyPresentInUserDefaults(key: "bloodgp") {
                primaryDetailsTVC?.bloodGroupTextField.text = UserDefaults.standard.string(forKey: "bloodgp")
            }

            if isKeyPresentInUserDefaults(key: "allergies") {
                primaryDetailsTVC?.allergiesTextField.text = UserDefaults.standard.string(forKey: "allergies")
            }

            if isKeyPresentInUserDefaults(key: "chronichealthissues") {
                primaryDetailsTVC?.chronicHealthIssuesTextField.text = UserDefaults.standard.string(forKey: "chronichealthissues")
            }

            if isKeyPresentInUserDefaults(key: "currentmedication") {
                primaryDetailsTVC?.currentMedicationTextField.text = UserDefaults.standard.string(forKey: "currentmedication")
            }

            ReadPrimaryDetailsFromServer()
        }
        
        patientnametxt.text = GlobalDocdata.patientname
        primaryDetailsTVC?.emailTextField.text = GlobalDocdata.email
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == self.primaryDetailsTVC?.pincodeTextField) {
            if (textField.text?.count)! < 5 {
                print("PrimaryDetailsViewDocController:textFieldDidBeginEditing:hide city,state")
                self.primaryDetailsTVC?.cityView.isHidden = true
                self.primaryDetailsTVC?.stateView.isHidden = true
            } else if (textField.text?.count)! >= 6 {
                print("PrimaryDetailsViewDocController:textFieldDidBeginEditing:unhide city,state")
                self.primaryDetailsTVC?.cityView.isHidden = false
                self.primaryDetailsTVC?.stateView.isHidden = false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField == self.primaryDetailsTVC?.pincodeTextField) {
            if (textField.text?.count)! < 5 {
                print("PrimaryDetailsViewDocController:textFieldDidEndEditing:hide city,state")
                self.primaryDetailsTVC?.cityView.isHidden = true
                self.primaryDetailsTVC?.stateView.isHidden = true
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == self.primaryDetailsTVC?.pincodeTextField {
            
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            
            if length == 0 || length == 6 {
                
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                let index = 0 as Int
                let formattedString = NSMutableString()
                
                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
                textField.text = formattedString as String

                if length == 6 {
                    self.fetchpincodebaseddata(pincode: textField.text!)
                }
                
                self.primaryDetailsTVC?.pincodeTextField.resignFirstResponder()
                return (newLength >= 6) ? false : true
                // return false
            }
            let index = 0 as Int
            let formattedString = NSMutableString()
            
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        } else {
            return true
        }
    }
    
    
    func fetchpincodebaseddata(pincode: String) {
        print("PrimaryDetailsViewDocController:fetchpincodebaseddata()pincode:\(pincode)")
        primaryDetailsTVC?.citystateactivityIndicator.isHidden = false
        primaryDetailsTVC?.citystateactivityIndicator.startAnimating()
        let location: String = pincode
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(location, completionHandler: {(placemarks: [CLPlacemark]?, error: Error?) -> Void in
            if (placemarks == nil) {
                print("PrimaryDetailsViewDocController:fetchpincodebaseddata()= returned NIL : Not a valid pincode")
                let alert = UIAlertController(title: "Alert", message: "Pincode entered not valid !", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    //Do Some action here
                    self.primaryDetailsTVC?.pincodeTextField.text = ""
                    self.primaryDetailsTVC?.cityTextField.text = ""
                    self.primaryDetailsTVC?.stateTextField.text  = ""
                    self.primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
                    self.primaryDetailsTVC?.citystateactivityIndicator.stopAnimating()
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
                return
            }
            if ((placemarks?.count)! > 0) {
                let placemark: CLPlacemark = (placemarks?[0])!
                let country : String = placemark.country!
                let state: String = placemark.administrativeArea!
                let city = placemark.locality!
                let description = placemark.description
                print(state)
                print(city)
                print(country)
                print(description)
                self.primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
                self.primaryDetailsTVC?.citystateactivityIndicator.stopAnimating()
                self.primaryDetailsTVC?.cityTextField.text = city
                self.primaryDetailsTVC?.stateTextField.text = state
                self.primaryDetailsTVC?.cityTextField.isHidden = false
                self.primaryDetailsTVC?.stateTextField.isHidden = false
                
                self.primaryDetailsTVC?.cityView.isHidden = false
                self.primaryDetailsTVC?.stateView.isHidden = false
            } else {
                print("PrimaryDetailsViewDocController:fetchpincodebaseddata() : Not a valid pincode")
                self.primaryDetailsTVC?.citystateactivityIndicator.isHidden = true
                self.primaryDetailsTVC?.citystateactivityIndicator.stopAnimating()
                self.primaryDetailsTVC?.pincodeTextField.text = ""
                self.primaryDetailsTVC?.cityTextField.text = ""
                self.primaryDetailsTVC?.stateTextField.text  = ""
            }
        } )
    }
    
    func LoadProfileImage() {
        if self.theprofileurl.count > 0 {
            self.docimg.layoutIfNeeded()
            self.docimg.sd_setImage(with: URL(string: self.theprofileurl), placeholderImage: UIImage(named: "profile"))
            self.docimg.layer.cornerRadius = self.docimg.frame.size.width / 2;
            self.docimg.clipsToBounds = true;
        }
    }
    
    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    @IBAction func OkButtonClicked(_ sender: UIButton) {
        
        if let bloodGroupTextField = primaryDetailsTVC?.bloodGroupTextField {
            if (bloodGroupTextField.text?.count)! < 1 {
                showAlert(msg: "Please Enter Blood group details!")
                return
            } else {
                UserDefaults.standard.setValue(bloodGroupTextField.text, forKey: "bloodgroup")
                UserDefaults.standard.synchronize()
                GlobalDocdata.bloodgroup = bloodGroupTextField.text!
            }
        }
        
        if let gendertxt = primaryDetailsTVC?.genderTextField {
            if (gendertxt.text?.count)! < 1 {
                showAlert(msg: "Please Enter Gender details!")
                return
            } else {
                UserDefaults.standard.setValue(gendertxt.text, forKey: "gender")
                UserDefaults.standard.synchronize()
                GlobalDocdata.gender = gendertxt.text!
            }
        }
        
        if let agetxt = primaryDetailsTVC?.ageTextField {
            if (agetxt.text?.count)! < 1 {
                showAlert(msg: "Please Enter Age details!")
                return
            } else {
                UserDefaults.standard.setValue(agetxt.text, forKey: "age")
                UserDefaults.standard.synchronize()
                GlobalDocdata.age = agetxt.text!
            }
        }
        
        if let weighttxt = primaryDetailsTVC?.weightTextField {
            if (weighttxt.text?.count)! < 1 {
                showAlert(msg: "Please Enter Weight details!")
                return
            } else {
                UserDefaults.standard.setValue(weighttxt.text, forKey: "weight")
                UserDefaults.standard.synchronize()
                GlobalDocdata.weight = weighttxt.text!
            }
        }
        
        if let hightfttxt = primaryDetailsTVC?.heightFeetTextField {
            if (hightfttxt.text?.count)! < 1 {
                showAlert(msg: "Please Enter Hight details!")
                return
            } else {
                UserDefaults.standard.setValue(hightfttxt.text, forKey: "hightft")
                UserDefaults.standard.synchronize()
                GlobalDocdata.heightft = hightfttxt.text!
            }
        }
        
        if let hightinchtxt = primaryDetailsTVC?.heightInchesTextField {
            if (hightinchtxt.text?.count)! < 1 {
                showAlert(msg: "Please Enter Hight details!")
                return
            } else {
                UserDefaults.standard.setValue(hightinchtxt.text, forKey: "hightinch")
                UserDefaults.standard.synchronize()
                GlobalDocdata.heightinch = hightinchtxt.text!
            }
        }
        
       /* if let allergiestxtview = primaryDetailsTVC?.allergiesTextField {
            if (allergiestxtview.text?.count)! > 1 {
                UserDefaults.standard.setValue(allergiestxtview.text, forKey: "allergies")
                UserDefaults.standard.synchronize()
                GlobalDocdata.allergies = allergiestxtview.text!
            }
        }
        
        if let chronichealthissuestxtview = primaryDetailsTVC?.chronicHealthIssuesTextField {
            if (chronichealthissuestxtview.text?.count)! > 1 {
                UserDefaults.standard.setValue(chronichealthissuestxtview.text, forKey: "chronichealth")
                UserDefaults.standard.synchronize()
                GlobalDocdata.chronichealthissues = chronichealthissuestxtview.text!
            }
        }
        
        if let currentmedicationtxtview = primaryDetailsTVC?.currentMedicationTextField {
            if (currentmedicationtxtview.text?.count)! > 1 {
                UserDefaults.standard.setValue(currentmedicationtxtview.text, forKey: "currentmedication")
                UserDefaults.standard.synchronize()
                GlobalDocdata.currentmedication = currentmedicationtxtview.text!
            }
        } */
        
        if (GlobalDocdata.gprofileURL.isEmpty == true) {
            showAlert(msg: "Please select profile image")
            return
        }
        
        /*if (primaryDetailsTVC?.addressfield.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Practice Address details!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.addressfield.text, forKey: "address")
        }
        
        if (primaryDetailsTVC?.pincodeTextField.text?.count)! <= 5 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter Area PIN Code !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.pincodeTextField.text, forKey: "pincode")
            UserDefaults.standard.synchronize()
        }
        
        if (primaryDetailsTVC?.cityTextField.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter City !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.cityTextField.text, forKey: "city")
            UserDefaults.standard.synchronize()
        }
        
        if (primaryDetailsTVC?.stateTextField.text?.count)! < 1 {
            let alert = UIAlertController(title: "Alert", message: "Please Enter State !", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        } else {
            UserDefaults.standard.setValue(self.primaryDetailsTVC?.stateTextField.text, forKey: "state")
            UserDefaults.standard.synchronize()
        } */
        
        
        if GlobalDocdata.firsttimeview {
            //testing
            CreateCustomObjectforPrimaryDetails()
        } else {
            UpDatePrimaryDetails()
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func showAlert(msg: String) {
        let cntrl = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        cntrl.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(cntrl, animated: true, completion: nil)
    }
    
    @IBAction func ProfileImageButton(_ sender: UIButton) {
        print("profile image Button is pressed")
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        profileimgbool = true
        let actionSheet = UIAlertController(title: "Photo Source", message: "choose a Source", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)}))

        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)}))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        //for ipad related crash issue fix
        if ipad && maxLength == 1024 {
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }

        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage

        if profileimgbool {
            self.profileimgbool = false
            self.profileimageActivityIndicator.isHidden = false
            self.profileimageActivityIndicator.startAnimating()

            let rzimage = resizeImage(image: selectedimage!, newWidth:68)
            let imageData: NSData = UIImagePNGRepresentation(rzimage)! as NSData

            QBRequest.tUploadFile(imageData as Data, fileName: "ProfileImageofPatient", contentType: "image/png", isPublic: true, successBlock: {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                    let userParams = QBUpdateUserParameters()
                    userParams.blobID = uploadedBlob.id
                    userParams.tags = ["Pat", "IOS"]
                    QBRequest.updateCurrentUser(userParams, successBlock:
                        {(_ response: QBResponse, _ user: QBUUser?) -> Void in
                            print("profile image Updated parameters and status is: \(String(describing: response))")
                    }, errorBlock:
                        {(_ response: QBResponse) -> Void in
                            print("profile imageUpdated parameters Error Response is: \(String(describing: response))")
                    })

                    let url: String = uploadedBlob.publicUrl()!
                    print("PrimaryDetailsViewControllerProfileImageButton():puburl = \(url)")
                    self.theprofileurl = url
                    GlobalDocdata.gprofileURL = url
                    SDImageCache.shared().store(selectedimage, forKey: url, completion: {
                    })

                    self.docimg.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "profile"))

                    // make profile image url persistance
                    let defaults: UserDefaults? = UserDefaults.standard
                    defaults?.set(URL(string: url), forKey: "keyforProfileurl")
                    defaults?.synchronize()
                    // Status
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                                    DispatchQueue.main.async {
                                            print(" ProfileImageButton() percent = \(String(describing: status?.percentOfCompletion))")
                                            if (status!.percentOfCompletion >= 0.5) {
                                                self.profileimageActivityIndicator.isHidden = true
                                                self.profileimageActivityIndicator.stopAnimating()
                                            }
                                    }
            }, errorBlock: {(response: QBResponse!) in
                    print("profile image QBRequest tUploadFile response Error \(String(describing: response))")
                    self.profileimageActivityIndicator.isHidden = true
                    self.profileimageActivityIndicator.stopAnimating()
            })
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let newHeight = newWidth
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    //Generare CallDoc Code
    func GenerateCallDocCode() {
        print( "PrimaryDetailsViewController: Generating CallDoc Code which has to be unique")
        var thefinalcode = ""
        let myCode = ShortCodeGenerator.getCode(length: 4)
        let thefullname = QBSession.current.currentUser?.fullName
        var components = thefullname?.components(separatedBy: " ")
        if((components?.count)! > 0) {
            let firstName = components?.removeFirst()
            let lastName = components?.joined(separator: " ")
            let firstNamefirstchar = firstName?.substring(to:(firstName?.index((firstName?.startIndex)!, offsetBy: 1))!)
            let lastNamefirstchar = lastName?.substring(to:(lastName?.index((lastName?.startIndex)!, offsetBy: 1))!)
            thefinalcode = firstNamefirstchar!  + myCode + lastNamefirstchar!
        }
        let codeuppsercase = thefinalcode.uppercased()
        GlobalDocdata.gthefinalcalldoccode = codeuppsercase
        print("CallDoc Referal code: \(codeuppsercase)")
        let updateParameters = QBUpdateUserParameters()
        updateParameters.customData = GlobalDocdata.gthefinalcalldoccode
        QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            // User updated successfully
            print("PrimaryDetailsDocViewController:GenerateCallDocCode() User data Updated Successfully !!")
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
            print("PrimaryDetailsDocViewController: GenerateCallDocCode() Error ()")
        })
    }
    
    func GetDoctorDatausingregcalldoccode() {
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = GlobalDocdata.regcalldoccode
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(DocTrustedNetworkViewController:GetDoctorDatausingregcalldoccode(): Successfully  !")
            if ((contributors?.count)! > 0) {
                let docuserid = contributors![0].userID
                let docname = contributors![0].fields?.value(forKey: "fullname") as? String
                let speciality = contributors![0].fields?.value(forKey: "specialisation") as? String
                let profileurl = contributors![0].fields?.value(forKey: "profileUID") as? String
                self.createEntryinRelDocPatTable(invitoruid: docuserid, docname: docname!, speciality: speciality!, profileurl: profileurl!)
            }

        }) { (errorresponse) in
            print("PrimaryDetailsViewController:GetDoctorDatausingregcalldoccode() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createEntryinRelDocPatTable(invitoruid: UInt, docname: String, speciality: String, profileurl: String ) {
        print("PrimaryDetailsViewController: createEntryinRelDocPatTable(): Creating new entry on RelDocPatable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = invitoruid
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = QBSession.current.currentUser?.id
        object.fields["doctorfullname"] = docname
        object.fields["lock"] = "true"
        object.fields["doctorspeciality"] = speciality
        object.fields["doctorprofileimageURL"] = profileurl
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("PrimaryDetailsViewController: createEntryinRelDocPatTable(): Successfully created entry")
        }) { (response) in
            //Handle Error
            LoadingIndicatorView.hide()
            print("PrimaryDetailsViewController:createEntryinRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    // This function is called when at the Registration time user first time make entries to text fields and press OK Button
    func  CreateCustomObjectforPrimaryDetails() {
        print("CreateCustomObjectforPrimaryDetails: Creating New Custom Object for patient Primary Details")
        
        let object = QBCOCustomObject()
        object.className = "PatPrimaryDetailsTable"
        
        let fullName = GlobalDocdata.patientname
        var firstName = ""
        var lastName = ""
        var components = fullName.components(separatedBy: " ")
        if(components.count > 0) {
            firstName = components.removeFirst()
            lastName = components.joined(separator: " ")
        }
        
        object.fields["firstname"] = firstName
        object.fields["lastname"] =  lastName
        object.fields["displayname"] = GlobalDocdata.displayname
        object.fields["email"] =  GlobalDocdata.email
        object.fields["gender"] =  primaryDetailsTVC?.genderTextField.text
        object.fields["age"] =  primaryDetailsTVC?.ageTextField.text
        object.fields["heightfeet"] =  primaryDetailsTVC?.heightFeetTextField.text
        object.fields["heightinch"] = primaryDetailsTVC?.heightInchesTextField.text
        object.fields["weight"] =  primaryDetailsTVC?.weightTextField.text
        object.fields["bloodgp"] =  primaryDetailsTVC?.bloodGroupTextField.text
        object.fields["allergies"] =  primaryDetailsTVC?.allergiesTextField.text
        object.fields["chronichealthissue"] =  primaryDetailsTVC?.chronicHealthIssuesTextField.text
        object.fields["currentmed"] =  primaryDetailsTVC?.currentMedicationTextField.text
        object.fields["relation"] =  "self"
        object.fields["calldoccode"] =  GlobalDocdata.gthefinalcalldoccode
        object.fields["profileimageURL"] = self.theprofileurl  //self.theprofileimageUID
        object.fields["address"] = primaryDetailsTVC?.addressfield.text
        object.fields["city"] = primaryDetailsTVC?.cityTextField.text ?? ""
        object.fields["state"] = primaryDetailsTVC?.stateTextField.text ?? ""
        object.fields["pincode"] = primaryDetailsTVC?.pincodeTextField.text ?? ""
        object.fields["userid"] = UserDefaults.standard.value(forKey: "userid")
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            // we are creating the record ID for doctors Primary details table , this record = customobjIDPrimarydetails will be used later to update record
            UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDPrimarydetails")
            UserDefaults.standard.synchronize()
            let paymentcontroller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDetails") as! PaymentDetailsViewController
            self.present(paymentcontroller, animated: true, completion: nil)
            if UserDefaults.standard.value(forKey: "calldoccode") != nil &&  ((UserDefaults.standard.value(forKey: "calldoccodetblname") as? String) ?? "") == "DocPrimaryDetails"{
                self.updateRelDocPatTable(forUser: contributors)
            }
            //UserDefaults.standard.value(forKey: "calldoccode")!
            //UserDefaults.standard.value(forKey: "calldoccodetblname")!
            
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsViewController:CreateCustomObjectforPrimaryDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func updateRelDocPatTable(forUser: QBCOCustomObject?) {
        
        guard let user = forUser else { fatalError("No User") }
        guard let callDocCode = UserDefaults.standard.value(forKey: "calldoccode") else { fatalError("No User") }
        //guard let callDocCodeTable = UserDefaults.standard.value(forKey: "calldoccodetblname") else { fatalError("No User") }
        
        //Get data from calldoccodetable and then update reldocpat
        let getRequest = NSMutableDictionary()
        getRequest["calldoccode"] = callDocCode
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {

                let object = QBCOCustomObject()
                object.className = "RelDocPat"
                
                object.fields["doctoruserid"] = contributors?.first?.userID
                object.fields["patientuserid"] = forUser?.userID//QBSession.current.currentUser?.id
                object.fields["doctorfullname"] = contributors?.first?.fields?.value(forKey: "fullname") as? String
                object.fields["lock"] = "true"
                object.fields["doctorspeciality"] = contributors?.first?.fields?.value(forKey: "specialisation") as? String
                object.fields["doctorprofileimageURL"] = contributors?.first?.fields?.value(forKey: "profileUID") as? String
                QBRequest.createObject(object, successBlock: { (response, contributors) in
                    //Handle Success
                    print("PrimaryDetailsViewController: createEntryinRelDocPatTable(): Successfully created entry")
                }) { (response) in
                    //Handle Error
                    LoadingIndicatorView.hide()
                    print("PrimaryDetailsViewController:createEntryinRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
                }
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("getDoctorGender() Response error: \(String(describing: errorresponse.error?.description))")
        }
        
        
    }
    
    func firsttimeuserRegistrationdataUpdate() {
        print( "PrimaryDetailsViewController: firsttimeuserdataUpdate()")
        updateUserEmail()
        UserDefaults.standard.setValue("", forKey: "fullname")
        UserDefaults.standard.setValue("", forKey: "gender")
        UserDefaults.standard.setValue("", forKey: "age")
        UserDefaults.standard.setValue("", forKey: "heightft")
        UserDefaults.standard.setValue("", forKey: "heightinch")
        UserDefaults.standard.setValue("", forKey: "bloodgp")
        UserDefaults.standard.setValue("", forKey: "allergies")
        UserDefaults.standard.setValue("", forKey: "chronichealthissues")
        UserDefaults.standard.setValue("", forKey: "currentmedication")
        UserDefaults.standard.setValue("", forKey: "relation")
        UserDefaults.standard.setValue("", forKey: "calldoccode")
    }
    
    func firsttimeReadingofExistingUser() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "PatPrimaryDetailsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(DocTrustedNetworkViewController:firsttimeReadingofExistingUser(): Successfully  !")
            if ((contributors?.count)! > 0) {
                self.primaryDetailsTVC?.emailTextField.text = contributors![0].fields?.value(forKey: "email") as? String
                let firstname =  contributors![0].fields?.value(forKey: "firstname") as? String
                let lastname = contributors![0].fields?.value(forKey: "lastname") as? String
                self.patientnametxt.text = firstname! + " " + lastname!
                self.primaryDetailsTVC?.genderTextField.text = contributors![0].fields?.value(forKey: "gender") as? String
                self.primaryDetailsTVC?.ageTextField.text = contributors![0].fields?.value(forKey: "age") as? String
                self.primaryDetailsTVC?.heightFeetTextField.text = contributors![0].fields?.value(forKey: "heightfeet") as? String
                self.primaryDetailsTVC?.heightInchesTextField.text = contributors![0].fields?.value(forKey: "heightinch") as? String
                self.primaryDetailsTVC?.weightTextField.text = contributors![0].fields?.value(forKey: "weight") as? String
                self.primaryDetailsTVC?.bloodGroupTextField.text = contributors![0].fields?.value(forKey: "bloodgp") as? String
                self.primaryDetailsTVC?.allergiesTextField.text = contributors![0].fields?.value(forKey: "allergies") as? String
                self.primaryDetailsTVC?.chronicHealthIssuesTextField.text = contributors![0].fields?.value(forKey: "chronichealthissue") as? String
                self.primaryDetailsTVC?.currentMedicationTextField.text = contributors![0].fields?.value(forKey: "currentmed") as? String

                self.primaryDetailsTVC?.addressfield.text = contributors![0].fields?.value(forKey: "address") as? String
                self.primaryDetailsTVC?.cityTextField.text = contributors![0].fields?.value(forKey: "city") as? String
                self.primaryDetailsTVC?.stateTextField.text = contributors![0].fields?.value(forKey: "state") as? String
                let val =  contributors![0].fields?.value(forKey: "pincode") as? Int
                self.primaryDetailsTVC?.pincodeTextField.text = String(val ?? 0)

                print("PrimaryDetailsViewController:firsttimeReadingofExistingUser():Successfully Read Primary Details from Server !")
                GlobalDocdata.patientname =  (QBSession.current.currentUser?.fullName)!
                self.patientnametxt.text = GlobalDocdata.patientname
                GlobalDocdata.gprofileURL = (contributors![0].fields?.value(forKey: "profileimageURL") as? String)!
                self.docimg.sd_setImage(with: URL(string: (contributors![0].fields?.value(forKey: "profileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"))

            }
        }) { (errorresponse) in
            print("PrimaryDetailsViewController:firsttimeReadingofExistingUser() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    // This function updates table of Backend PrimaryDetails table with the actuall data entered by user
    func  UpDatePrimaryDetails() {
        print("PrimaryDetailsViewController:UpDatePrimaryDetails()")
        
        let fullName = GlobalDocdata.patientname
        var firstName = ""
        var lastName = ""
        var components = fullName.components(separatedBy: " ")
        if(components.count > 0) {
            firstName = components.removeFirst()
            lastName = components.joined(separator: " ")
        }
        
        let object = QBCOCustomObject()
        object.className = "PatPrimaryDetailsTable"
        object.fields["email"] =  GlobalDocdata.email
        object.fields["firstname"] = firstName
        object.fields["lastname"] = lastName
        object.fields["gender"] = self.primaryDetailsTVC?.genderTextField.text
        object.fields["age"] = self.primaryDetailsTVC?.ageTextField.text
        object.fields["heightfeet"] = self.primaryDetailsTVC?.heightFeetTextField.text
        object.fields["heightinch"] = self.primaryDetailsTVC?.heightInchesTextField.text
        object.fields["weight"] = self.primaryDetailsTVC?.weightTextField.text
        object.fields["bloodgp"] = self.primaryDetailsTVC?.bloodGroupTextField.text
        object.fields["allergies"] = self.primaryDetailsTVC?.allergiesTextField.text
        object.fields["chronichealthissue"] = self.primaryDetailsTVC?.chronicHealthIssuesTextField.text
        object.fields["currentmed"] = self.primaryDetailsTVC?.currentMedicationTextField.text
        object.fields["relation"] = "self"
        object.fields["calldoccode"] = GlobalDocdata.gthefinalcalldoccode
        object.fields["profileimageURL"] = GlobalDocdata.gprofileURL //self.theprofileurl
        object.fields["address"] = primaryDetailsTVC?.addressfield.text
        object.fields["city"] = primaryDetailsTVC?.cityTextField.text
        object.fields["state"] = primaryDetailsTVC?.stateTextField.text
        object.fields["pincode"] = primaryDetailsTVC?.pincodeTextField.text
        
        object.id = UserDefaults.standard.string(forKey: "customobjIDPrimarydetails")!      //record id of Doctor Primary details table
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("PrimaryDetailsViewController:: UpDatePrimaryDetails() Successfully Updated Primary Details !")
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: false, completion: nil)
            
        }) { (response) in
            //Handle Error
            print("PrimaryDetailsViewController:UpDatePrimaryDetails(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func ReadPrimaryDetailsFromServer() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "PatPrimaryDetailsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(DocTrustedNetworkViewController:ReadPrimaryDetailsFromServer(): Successfully  !")
            if ((contributors?.count)! > 0) {
                UserDefaults.standard.setValue((contributors![0].id)!, forKey: "customobjIDPrimarydetails")
                UserDefaults.standard.synchronize()
                self.primaryDetailsTVC?.emailTextField.text = contributors![0].fields?.value(forKey: "email") as? String
                let firstname =  contributors![0].fields?.value(forKey: "firstname") as? String
                let lastname = contributors![0].fields?.value(forKey: "lastname") as? String
                self.patientnametxt.text = (firstname ?? "") + " " + (lastname ?? "")
                self.primaryDetailsTVC?.genderTextField.text = contributors![0].fields?.value(forKey: "gender") as? String
                self.primaryDetailsTVC?.ageTextField.text = contributors![0].fields?.value(forKey: "age") as? String
                self.primaryDetailsTVC?.heightFeetTextField.text = contributors![0].fields?.value(forKey: "heightfeet") as? String
                self.primaryDetailsTVC?.heightInchesTextField.text = contributors![0].fields?.value(forKey: "heightinch") as? String
                self.primaryDetailsTVC?.weightTextField.text = contributors![0].fields?.value(forKey: "weight") as? String
                self.primaryDetailsTVC?.bloodGroupTextField.text = contributors![0].fields?.value(forKey: "bloodgp") as? String
                self.primaryDetailsTVC?.allergiesTextField.text = contributors![0].fields?.value(forKey: "allergies") as? String
                self.primaryDetailsTVC?.chronicHealthIssuesTextField.text = contributors![0].fields?.value(forKey: "chronichealthissue") as? String
                self.primaryDetailsTVC?.currentMedicationTextField.text = contributors![0].fields?.value(forKey: "currentmed") as? String
                self.docimg.sd_setImage(with: URL(string: (contributors![0].fields?.value(forKey: "profileimageURL") as? String)!), placeholderImage: UIImage(named: "profile"))
                self.theprofileimageUID = (contributors![0].fields?.value(forKey: "profileimageURL") as? String)!
                GlobalDocdata.gprofileURL = (contributors![0].fields?.value(forKey: "profileimageURL") as? String)!

                print("PrimaryDetailsViewController:ReadPrimaryDetailsFromServer():Successfully Read Primary Details from Server !")
                GlobalDocdata.patientname = self.patientnametxt.text!
                
                self.primaryDetailsTVC?.addressfield.text = contributors![0].fields?.value(forKey: "address") as? String
                self.primaryDetailsTVC?.cityTextField.text = contributors![0].fields?.value(forKey: "city") as? String
                self.primaryDetailsTVC?.stateTextField.text = contributors![0].fields?.value(forKey: "state") as? String
                let val =  contributors![0].fields?.value(forKey: "pincode") as? Int
                self.primaryDetailsTVC?.pincodeTextField.text = String(val ?? 0)
                GlobalDocdata.gthefinalcalldoccode = (contributors![0].fields?.value(forKey: "calldoccode") as? String)!
            }
        }) { (errorresponse) in
            print("PrimaryDetailsViewController:ReadPrimaryDetailsFromServer() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func updateUserEmail() {
        let updateParameters = QBUpdateUserParameters()
        updateParameters.email = GlobalDocdata.email
        QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            // User updated successfully
            print("PrimaryDetailsViewController:updateUserEmail() User data Updated Success for Email, this will trigger email verification email to be sent to user!!")
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
            print("PrimaryDetailsViewController: Error in updateUserEmail()")
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PrimaryDetailsTableViewControllerSegue" {
            self.primaryDetailsTVC = segue.destination as? PrimaryDetailsTableViewController
            self.primaryDetailsTVC?.primaryDetailsVC =  self
        }
    }
}


// random unique code generation
struct ShortCodeGenerator {
    private static let base62chars = [Character]("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".characters)
    private static let maxBase : UInt32 = 62
    static func getCode(withBase base: UInt32 = maxBase, length: Int) -> String {
        var code = ""
        for _ in 0..<length {
            let random = Int(arc4random_uniform(min(base, maxBase)))
            code.append(base62chars[random])
        }
        return code
    }
}


