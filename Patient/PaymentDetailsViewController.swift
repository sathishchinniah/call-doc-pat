import UIKit

class PaymentDetailsViewController: UIViewController {

    @IBOutlet var docimg: UIImageView!{
        didSet {
            self.docimg.layer.cornerRadius = self.docimg.frame.size.width / 2;
            self.docimg.clipsToBounds = true;
            self.docimg.layer.borderWidth = 0.2
            self.docimg.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet var patientname: UILabel!
    @IBOutlet var bottombarView: UIView!
    
    var nav: UINavigationController?
    
     var merchant:PGMerchantConfiguration!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.hideKeyboardWhenTappedAround()
        patientname.text = GlobalDocdata.patientname
        bottombarView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)
        LoadProfileImage()
    }

    func LoadProfileImage() {
        self.docimg.sd_setImage(with: URL(string: GlobalDocdata.gprofileURL), placeholderImage: UIImage(named: "profile"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func OkButtonClicked(_ sender: UIButton) {
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
        
    }
    
    @IBAction func PayTMButtonClicked(_ sender: UIButton) {
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let VC = storyboard.instantiateViewController(withIdentifier: "PaymentWebView") as! PaymentWebViewController
//        self.navigationController?.pushViewController(VC, animated: true)
        setMerchant()//initialize merchant with config.
    }
    
    func setMerchant(){
        merchant  = PGMerchantConfiguration.default()!
        //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
        merchant.checksumGenerationURL = "https://pguat.paytm.com/paytmchecksum/paytmCheckSumGenerator.jsp";
        merchant.checksumValidationURL = "https://pguat.paytm.com/paytmchecksum/paytmCheckSumVerify.jsp";
        
        // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
        merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
        merchant.clientSSLCertPassword = nil; //@"password";
        
        //configure the PGMerchantConfiguration object specific to your requirements
        merchant.merchantID = "CallDo40204357794438";//paste here your merchant id  //mandatory
        merchant.website = "www.calldoc.com";//mandatory
        merchant.industryID = "Retail";//mandatory
        merchant.channelID = "WAP"; //provided by PG WAP //mandatory
    }
}
