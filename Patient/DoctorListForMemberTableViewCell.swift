import UIKit

class DoctorListForMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var dataContainerView: UIView!
    @IBOutlet weak var ImageViewContainer: UIView!
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var BadgeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

