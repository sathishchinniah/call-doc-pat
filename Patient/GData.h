//
//  GData.h
//  Patient
//
//  Created by Manish Verma on 15/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GData : NSObject
{
    QBUUser *theCurrentUser;
}

@property (nonatomic ,assign)BOOL isPatientFetched, ischatuserfound, isalreadyloggedin;
@property (nonatomic, retain) QBUUser *theCurrentUser;
//@property (retain, nonatomic) CLLocation *glocationcurrent, *glocationdestination;
@property int scanidfor, chatdialogusersindex;
@property NSString *PatientName, *incomingcallUserName, *calledUserName;
@property (nonatomic,retain) NSMutableArray *populatedArray, *doctorslistArray, *chatDialoguserscontext;


+ (GData *)sharedData;


@end
