import UIKit
import Toast_Swift

var PatientAllConsultsdetailsArray : [QBCOCustomObject] = []
let openColor = UIColor(red:0.23, green:0.70, blue:0.55, alpha:1.0)
let closesColor = UIColor(red:0.99, green:0.64, blue:0.33, alpha:1.0)
let closedColor = UIColor(red:0.89, green:0.32, blue:0.27, alpha:1.0)

class AllConsultsViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate  {
    
    @IBOutlet var allConsultTableView: UITableView! {
        didSet {
            allConsultTableView.tableFooterView = UIView()
        }
    }
    
    var consultsTableDoctorsList: [QBCOCustomObject] = []
    var currentConsult = QBCOCustomObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My All Consults"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 51/255, green: 102/255, blue: 153/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        //self.ReadAllConsultsDetails()
        
        PatientAllConsultsdetailsArray = /*self.consultsTableDoctorsList*/selectedUserDoctorsArray
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PatientConsultsdetailsArray.contains(currentConsult) {
            PatientConsultsdetailsArray.remove(at: PatientConsultsdetailsArray.index(of: currentConsult)!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //code
    }
    
    override func viewDidLayoutSubviews() {
        //code
//        if (self.parent as? UIViewController) is ChatConsultViewController {
//
//        } else {
//
//        }
    }
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    public func tableView(_ allConsultTableView : UITableView, numberOfRowsInSection section: Int) -> Int {
        return PatientAllConsultsdetailsArray.count
    }

    public func tableView(_ allConsultTableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        UITableViewCell.appearance().backgroundColor = UIColor.clear
        guard let cell = allConsultTableView.dequeueReusableCell(withIdentifier: "allConsultsCell", for: indexPath) as? allConsultsTableViewCell else { return allConsultsTableViewCell() }
        
        cell.selectionStyle = .none
        
        cell.consultdocname.text = "Dr. " + ((PatientAllConsultsdetailsArray[indexPath.row].fields.value(forKey: "docfullname") as? String)!)
        //docfullname
        
        let currenttime: Int = Int(NSDate().timeIntervalSince1970)
        let createdat  = PatientAllConsultsdetailsArray[indexPath.row].createdAt! as NSDate
        let createdatepochtime = Int(createdat.timeIntervalSince1970)
        
        let diff = currenttime - createdatepochtime
        let hourslasped = diff/3600
        print("hours gone by = \(hourslasped)")

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
        let lastdatetimelb = fulldate[0]
        let lastdatebtn = fulldate[1]
        
        cell.allconsultsDatecellButton.setTitle(lastdatebtn, for: .normal)
        cell.consulttimelb.text = "|  " + lastdatetimelb
        
        cell.consultStatusLb.layer.masksToBounds = true
        cell.consultStatusLb.layer.cornerRadius = 5
        let status = (PatientAllConsultsdetailsArray[indexPath.row].fields?.value(forKey: "status") as? String)?.lowercased() ?? ""
        if status == "pending" {
            cell.consultStatusLb.text = "Pending"
            cell.consultStatusLb.backgroundColor = UIColor.lightGray
            
        } else if status == "open" {
            cell.consultStatusLb.text = "Open"
            cell.consultStatusLb.backgroundColor = openColor
           
        } else if status == "closing soon" {
            cell.consultStatusLb.text = "closes soon "
            cell.consultStatusLb.backgroundColor = closesColor
            
        } else if status == "closed" {
            cell.consultStatusLb.text = "Closed"
            cell.consultStatusLb.backgroundColor = closedColor
            
        } else {
            //uncomment for new
            //            cell.statuslabel.text = ""
            //            cell.statuslabel.backgroundColor = UIColor.clear
            //            cell.extendButton.isHidden = true
            //            cell.extendButton.isEnabled = false
            //for old consults
            cell.consultStatusLb.text = "Closed"
            cell.consultStatusLb.backgroundColor = closedColor
            
        }
        
        cell.allconsultscellButton.tag = indexPath.row
//        cell.allconsultscellButton.addTarget(self,action:#selector(consultsShareButtonTouchUpInside/*allconsultcellButtonAction*/), for:.touchUpInside)
        cell.consultsShareButton.tag = indexPath.row
//        //cell.consultsShareButton.imageEdgeInsets = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)
        cell.consultsShareButton.addTarget(self,action:#selector(consultsShareButtonTouchUpInside), for:.touchUpInside)
        return cell
    }
    
    @objc func allconsultcellButtonAction(sender: UIButton) {
        print("AllConsultsViewController:allconsultcellButtonAction")
    }
    
    @objc func consultsShareButtonTouchUpInside(sender: UIButton) {
        print("AllConsultsViewController:consultsShareButtonTouchUpInside")
        if (self.parent as? UIViewController) is ChatConsultViewController {
            let consultId = (self.parent as? ChatConsultViewController)?.consultrecordIDpassed
            let currentConsult = (self.parent as? ChatConsultViewController)?.currentConsult
            let object = QBCOCustomObject()
            object.className = "ConsultsTable"
            //object.fields["sharedconsults"] = consultToBeShared.id
            object.id = consultId//currentConsult?.id//consultId//doctor.id
            var sharedconsults = (currentConsult?.fields.value(forKey: "sharedconsults") as? String) ?? ""
            if sharedconsults.contains(PatientAllConsultsdetailsArray[sender.tag].id!) {
                var style = ToastStyle()
                style.messageColor = .white
                ToastManager.shared.style = style
                self.view.makeToast("Consult already shared", duration: 2.0, position: .center, title: nil, image: nil, style: style, completion: nil)
            } else {
                sharedconsults = sharedconsults + "," + PatientAllConsultsdetailsArray[sender.tag].id!//consultid
                //            if sharedconsults == nil {
                //                sharedconsults = PatientAllConsultsdetailsArray[sender.tag].id
                //            } else {
                //                sharedconsults = sharedconsults! + "," + PatientAllConsultsdetailsArray[sender.tag].id!//consultid
                //            }
                object.fields["sharedconsults"] = sharedconsults//consultToBeShared.id
                
                QBRequest.update(object, successBlock: { (response, contributors) in
                    //self.dismiss(animated: true, completion: nil)
                    
                    //                (self.parent as? ChatConsultViewController)?.becomeFirstResponder()
                    //                (self.parent as? ChatConsultViewController)?.loadView()
                    //                (self.parent as? ChatConsultViewController)?.loadChatViewController(withDestination: ChatViewController())
                    (self.parent as? ChatConsultViewController)?.viewDidLoad()
                    (self.parent as? ChatConsultViewController)?.chatButton.alpha = 0.5
                    (self.parent as? ChatConsultViewController)?.shareConsultButton.alpha = 1
                    (self.parent as? ChatConsultViewController)?.allConsultsContainer.isHidden = true
                    (self.parent as? ChatConsultViewController)?.chatViewContainer.isHidden = false
                    var style = ToastStyle()
                    style.messageColor = .white
                    ToastManager.shared.style = style
                    self.view.makeToast("Consult shared succesfully", duration: 2.0, position: .center, title: nil, image: nil, style: style, completion: nil)
                    //
                }) { (response) in
                    self.dismiss(animated: true, completion: nil)
                    print("consultsShareButtonTouchUpInside: Response error: \(String(describing: response.error?.description))")
                }
            }

        } else {
            guard let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "shareToDoctorsListViewController") as? shareToDoctorsListViewController else { return }
            popoverContent.consultToBeShared = PatientAllConsultsdetailsArray[sender.tag]
            let nav = UINavigationController(rootViewController: popoverContent)
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController
            popoverContent.preferredContentSize = CGSize(width: 300, height: 400)//CGSizeMake(500,600)
            popoverContent.modalPresentationStyle = UIModalPresentationStyle.popover
            popover?.delegate = self
            popover?.sourceView = sender//self.view
            popover?.sourceRect = sender.bounds//CGRect(x: 100, y: 100, width: 0, height: 0)//CGRectMake(100,100,0,0)
            popoverContent.navigationController?.navigationBar.isHidden = true
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    // UIPopoverPresentationControllerDelegate method
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        // Force popover style
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }

    func ReadAllConsultsDetails() {
        let getRequest = NSMutableDictionary()
        if gindexval > patientcdcode.count {
            getRequest["patcalldoccode"] = patientcdcode[gindexval]
        }
        
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("AllConsultsViewController:ReadAllConsultsDetails: Successfully Read consults from Server !")
            PatientAllConsultsdetailsArray.removeAll()
            if ((contributors?.count)! > 0) {
                print("AllConsultsViewController:ReadAllConsultsDetails: PatientAllConsultsdetailsArray not empty")
                PatientAllConsultsdetailsArray = contributors!
                PatientAllConsultsdetailsArray.reverse()
                print("AllConsultsViewController:reversed")
            } else {
                print("AllConsultsViewController:ReadAllConsultsDetails: PatientAllConsultsdetailsArray is empty")
                PatientAllConsultsdetailsArray = contributors!
            }
            
            self.allConsultTableView.reloadData()
            
        }) { (response) in
            //Error handling
            print("AllConsultsViewController:ReadAllConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (self.parent as? UIViewController) is ChatConsultViewController {
            return
        }
        //Code
        print(indexPath.row)//sender.tag)
        let createdat  = PatientAllConsultsdetailsArray[indexPath.row].createdAt! as NSDate //consultsTableDoctorsList
        let currenttime: Int = Int(NSDate().timeIntervalSince1970)
        let createdatepochtime = Int(createdat.timeIntervalSince1970)
        let diff = currenttime - createdatepochtime
        let hourslasped = diff/3600
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"
        let dateString = dayTimePeriodFormatter.string(from: createdat as Date)
        let fulldate = dateString.components(separatedBy: "|")
        let createdattimelb = fulldate[0]
        let createdatdatelb = fulldate[1]
        let VC1 = ConsultDetailsViewController.storyboardInstance()
        VC1.docnamepassed = (PatientAllConsultsdetailsArray[indexPath.row].fields.value(forKey: "docfullname") as? String) ?? ""//self.docnamelb.text!
        VC1.datepassed = createdattimelb + " | " + createdatdatelb
        VC1.indexval = indexPath.row//sender.tag
        theindexval = indexPath.row//sender.tag
        thedateval = createdatdatelb
        VC1.hourslaspedpassed = hourslasped
        //VC1.docuseridpassed = (consultsTableDoctorsList[theindexval].value(forKey: "userID") as? UInt) ?? (0 as UInt)//(consultsTableDoctorsList[theindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)//docuseridpassed
        VC1.doctorsarrayindexval = theindexval//myindexval
        VC1.consultrecordIDpassed = /*consultsTableDoctorsList[indexPath.row]*/PatientAllConsultsdetailsArray[indexPath.row].id! as String
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        self.navigationController?.pushViewController(VC1, animated: true)
        let fullname = (PatientAllConsultsdetailsArray[indexPath.row].fields.value(forKey: "docfullname") as? String)
        QBRequest.users(withFullName: fullname!, page: nil, successBlock: { (response, responsePage, user) in
            let id = user.first?.id
            VC1.docuseridpassed = id!
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(VC1, animated: true)
        }) { (response) in
            print("Response error: \(String(describing: response.error?.description))")
        }
    }

}
