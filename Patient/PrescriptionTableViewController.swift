//
//  PrescriptionTableViewController.swift
//  Doctor
//
//  Created by Pavan Kumar C on 01/06/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit
import Toast_Swift

struct ConfirmMedicalAdviceData {
    var patientName = ""
    var symptoms: String?
    var provisionalDiagnosis: String?
    var rxMedicines = [RxMedicalAdviceData]()
    var dxMedicines = [DxMedicalAdviceData]()
    var advice: String?
    var followUp: String?
    var consultationCharges = ""
    var skipMedicalAdvice = false
    var charge = 500
    var reasonForSkip: String?
}

struct RxMedicalAdviceData {
    var medicineName = ""
    var dosage = ""
    var form = ""
    var genericName = ""
    var strength = ""
    var frequency = ""
    var duration = ""
    var durationType = ""
    var afterFood = true
    var specialRemarks: String?
}

struct DxMedicalAdviceData {
    var testName = ""
    var instructions = ""
}

class PrescriptionTableViewController: UITableViewController {

    //MARK:- Outlets

    @IBOutlet weak var prescriptiontable: UITableView!

    //MARK:- Variables and constants

    var medicalData: ConfirmMedicalAdviceData?
    var confirmMedicalAdviceHeight: [CGFloat] = [69, 71, 60, 60, 50, 180, 60, 145, 60, 61, 140, 122]
    var skipRxConfirmMedicalAdviceHeight: [CGFloat] = [69, 71, 60, 0, 0, 0, 0, 0, 0, 0, 140, 122]
    var prescriptionDetailsArray : [QBCOCustomObject] = []
    var isSkip: Bool = false

    //MARK:- Controller methods

    override func viewDidLoad() {
        super.viewDidLoad()

        var prescriptionid = ""
        if let vc = ((self.parent?.parent) as? UINavigationController)?.viewControllers[(((self.parent?.parent) as? UINavigationController)?.viewControllers.count)! - 2] {
            if /*((self.parent?.parent) as! UINavigationController).viewControllers[((self.parent?.parent) as! UINavigationController).viewControllers.count - 2]*/ vc is MyConsultsViewController {
                //from doctor's consult
                prescriptionid = (PatientConsultsdetailsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? ""
            } else if /*((self.parent?.parent) as! UINavigationController).viewControllers[((self.parent?.parent) as! UINavigationController).viewControllers.count - 2]*/vc is AllConsultsViewController {
                //from allconsults
                prescriptionid = (selectedUserDoctorsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? ""
            }
        } else {
            //prescriptionid = (PatientConsultsdetailsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? ""
            prescriptionid = (selectedUserDoctorsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? ""
        }
        
        
        //let prescriptionid: String = (PatientConsultsdetailsArray.count > theindexval) ? ((PatientConsultsdetailsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? "") : ""

        let charset = CharacterSet(charactersIn: ",")
        if prescriptionid.rangeOfCharacter(from: charset) != nil {
            self.ReadyPrescriptionDetails(prescriptionid: prescriptionid)
        } else {
            self.ReadSinglePrescriptionDetails(prescriptionid: prescriptionid)
        }
    }

    // MARK: - Table View delegate and datasource methods

    override func numberOfSections(in tableView: UITableView) -> Int {
        if prescriptionDetailsArray.count > 10 {
            return 10  // Currently restricted max prescriptions to 10. 
        }

        return prescriptionDetailsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell0
            cell.configureUI(dateVal: thedateval)
            return cell
        }

        if indexPath.row == 1 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell1
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 2 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell2
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 3 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell3
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 5 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell4
            let rxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "rxdata") as? String
            cell.configureRxView(rxMedicines: decodeRxData(rxText: rxdatastring))
            return cell
        }

        if indexPath.row == 7 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell5
            let dxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "dxdata") as? String
            cell.configureDxView(dxMedicines: decodeDXData(dxText: dxdatastring))
            return cell
        }

        if indexPath.row == 8 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell6
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 9 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell7
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 10 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell8
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        if indexPath.row == 11 {
            let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as! PrescriptionCell9
            cell.configureUI(data: prescriptionDetailsArray[indexPath.section])
            return cell
        }

        let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as UITableViewCell
        return cell

    }

    override func tableView(_ prescriptiontbl: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isSkip) {
            return skipRxConfirmMedicalAdviceHeight[indexPath.row]
        }

        let rxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "rxdata") as? String
        let rxMedicines = decodeRxData(rxText: rxdatastring)

        let dxdatastring  =  prescriptionDetailsArray[indexPath.section].fields.value(forKey: "dxdata") as? String
        let dxMedicines = decodeDXData(dxText: dxdatastring)

        if indexPath.row == 4 {
            if rxMedicines.count == 0 {
                return 0
            }
        }

        if indexPath.row == 5 {
            return CGFloat(180 * rxMedicines.count)
        }

        if indexPath.row == 6 {
            if dxMedicines.count == 0 {
                return 0
            }
        }

        if indexPath.row == 7 {
            return CGFloat(145 * dxMedicines.count)
        }

        return confirmMedicalAdviceHeight[indexPath.row]
    }

    //MARK:- API methods

    func ReadyPrescriptionDetails(prescriptionid: String) {
        QBRequest.objects(withClassName: "PrescriptionTable", ids: [prescriptionid], successBlock: { response, contributors in
            self.prescriptionDetailsArray = contributors as! [QBCOCustomObject]
            self.prescriptiontable.reloadData()
        }) { (response) in
            print("PrescriptionTableViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }

    func ReadSinglePrescriptionDetails(prescriptionid: String) {
        let getRequest = NSMutableDictionary()
        getRequest["_id"] = prescriptionid

        QBRequest.objects(withClassName: "PrescriptionTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in

            if let contributors = contributors, contributors.count > 0 {
                self.prescriptionDetailsArray = contributors
            }

            self.prescriptiontable.reloadData()

        }) { (response) in
            print("PrescriptionTableViewController:ReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
    }

    //MARK:- Custom methods

    func decodeRxData(rxText: String?) -> [RxMedicalAdviceData] {
        var rxDatas = [RxMedicalAdviceData]()

        if let rxStrings = rxText?.components(separatedBy: "<>") {
            for rx in rxStrings {
                if rx.count > 3 {
                    let rxContents = rx.components(separatedBy: "|")
                    if rxContents.count == 10 {
                        var rxData = RxMedicalAdviceData()
                        rxData.medicineName = rxContents[0]
                        rxData.genericName = rxContents[1]
                        rxData.dosage = rxContents[2]
                        rxData.form = rxContents[3]
                        rxData.strength = rxContents[4]
                        rxData.afterFood = "\(rxContents[5])" == "After Food" ? true : false
                        rxData.frequency = rxContents[6]
                        rxData.specialRemarks = rxContents[7]
                        rxData.duration = rxContents[8]
                        rxData.durationType = rxContents[9]
                        rxDatas.append(rxData)
                    }
                }
            }
        }

        return rxDatas
    }
    
    func decodeDXData(dxText: String?) -> [DxMedicalAdviceData] {
        var dxDatas = [DxMedicalAdviceData]()

        if let dxStrings = dxText?.components(separatedBy: "<>") {
            for dx in dxStrings {
                if dx.count > 3 {
                    let dxContents = dx.components(separatedBy: "|")
                    if dxContents.count == 2 {
                        var dxData = DxMedicalAdviceData()
                        dxData.testName = dxContents[0]
                        dxData.instructions = dxContents[1]
                        dxDatas.append(dxData)
                    }
                }
            }
        }

        return dxDatas
    }

    func getTotalHeight() -> Int {

        var height = 0

        for prescription in prescriptionDetailsArray {
            let rxdatastring  =  prescription.fields.value(forKey: "rxdata") as? String
            let rxMedicines = decodeRxData(rxText: rxdatastring)

            let dxdatastring  =  prescription.fields.value(forKey: "dxdata") as? String
            let dxMedicines = decodeDXData(dxText: dxdatastring)

            if rxMedicines.count > 0 { // Height of Rxdata and RXlogo
                height += 180 * rxMedicines.count
                height += 50
            }

            if dxMedicines.count > 0 { // Height of Dxdata and DXlogo
                height += 145 * dxMedicines.count
                height += 60
            }

            height += 69 + 71 + 60 + 60 + 60 + 61 + 140 + 122  //Heights of different cells
            height += 45 // Height of section header and spacing
        }

        return height
    }

}
