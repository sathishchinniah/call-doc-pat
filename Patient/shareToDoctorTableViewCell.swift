//
//  shareToDoctorTableViewCell.swift
//  Patient
//
//  Created by Kirthika Raukutam on 09/11/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit

class shareToDoctorTableViewCell: UITableViewCell {

    @IBOutlet weak var doctorNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
