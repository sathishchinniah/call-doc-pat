//
//  VerifyViewController.h
//  Patient
//
//  Created by Manish Verma on 09/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyViewController : UIViewController<UITextFieldDelegate>

@end
