import UIKit

class InviteYourDocViewController: UIViewController {

    @IBOutlet var docmbemtext: UITextField!
    @IBOutlet var docnametext: UITextField!
    @IBOutlet var docspacilitytext: UITextField!
    @IBOutlet var dochospitaltext: UITextField!
    @IBOutlet var doccitytext: UITextField!
    
    var nav: UINavigationController?
    var thelastvc: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        //left side Back Button
        let btnImage = UIImage(named: "backBtn-1")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func backBtnPressed(_ sender: Any) {
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    
    @IBAction func IntiveButton(_ sender: UIButton) {

    }

    @IBAction func SearchButton(_ sender: UIButton) {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
