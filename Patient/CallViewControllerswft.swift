import UIKit
import Quickblox
import QuickbloxWebRTC
import SVProgressHUD
import AVFoundation
import Toast_Swift
import UserNotifications

private let kRefreshTimeInterval = 1.0 as? TimeInterval

class CallViewControllerswft: UIViewController, QBRTCClientDelegate, UNUserNotificationCenterDelegate {

    let defRect = CGRect.init(x: 0, y: 0, width: 48, height: 48)
    let defBgClr = UIColor.init(red: 0.8118, green: 0.8118, blue: 0.8118, alpha: 1.0)
    let defSlctClr = UIColor.init(red: 0.3843, green: 0.3843, blue: 0.3843, alpha: 1.0)
    
    @IBOutlet weak var callinglabel: UILabel!
    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var toolBar: Toolbar!
    @IBOutlet weak var calltimelb: UILabel!
    @IBOutlet weak var oponentVideoView: QBRTCRemoteVideoView!
    @IBOutlet weak var StackView: UIStackView!
    @IBOutlet weak var localvideoView: UIView!
    @IBOutlet weak var inCallChatButton: UIButton!
    
    open var opponets: [QBUUser]?
    open var currentUser: QBUUser?
    
    var views: [UIView] = []
    var videoCapture: QBRTCCameraCapture!
    var session: QBRTCSession?
    var callTimer: Timer?
    var timeDuration = TimeInterval()
    var beepTimer: Timer?
    var callingnumber: UInt = 0
    var doctorname = ""
    var consultrecordIDpassed = ""
    var callingPatientName = ""
    var profileImage = ""
    var pagerIndex = 0
    var topView: UIView!
    var profileImageView: UIImageView!
    var profilePic: UIImageView!
    var videoCallButton: UIButton!
    var isFirstConsult = false
    var currentConsult = QBCOCustomObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
        
        cofigureVideo()
        configureAudio()
        
        
        navigationController?.isNavigationBarHidden = true
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        inCallChatButton?.isHidden = true
        endButton?.isHidden = true
        toolBar?.isHidden = true
        profilePic = UIImageView()
        /*GlobalVariables.gcurrentprofileURL*/ let url = patientimageurl.count > gindexval ? patientimageurl[gindexval] : ""
        profilePic.sd_setImage(with: URL(string: url)/*patientimageurl[pagerIndex])*/, placeholderImage: UIImage(named: "profile"))
        configureToolbar()
        configureTopScreen()
        
        self.endButton?.isHidden = false
        self.toolBar?.isHidden = false
        
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(), target: self, selector: #selector(self.playCallingSound), userInfo: nil, repeats: true)
        
        playCallingSound((Any).self)
        
        self.callinglabel?.text = "Calling : Dr. " + doctorname
        
        let remotenumber: NSNumber
        remotenumber = NSNumber(value: callingnumber)
        self.session = QBRTCClient.instance().createNewSession(withOpponents: [remotenumber] as [NSNumber], with: .video)
       
        self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
        let id = String((QBSession.current.currentUser?.id) ?? 0)
        let prescriptionId = (self.currentConsult.fields.object(forKey: "prescriptionid") as? String) ?? "null"
        var userInfo = [String:String]()
        if !patientcdcode.isEmpty, !patientNames.isEmpty, !patientgender.isEmpty, !patientage.isEmpty, !patientimageurl.isEmpty {
            userInfo = ["patId":id,"calldoccode":patientcdcode[pagerIndex], "relation":patrelation[pagerIndex]/*"self"*/, "patientname": patientNames[pagerIndex], "patprofileid":patientimageurl[pagerIndex]/*GlobalDocdata.gprofileURL*/, "gender":patientgender[pagerIndex], "age":patientage[pagerIndex], "consultrecordID":consultrecordIDpassed, "relationship": patrelation[pagerIndex], "primaryUser": patientNames[0], "userID":String((QBSession.current.currentUser?.id) ?? 0), "isFirstConsult": String(describing: isFirstConsult), "prescriptionId": prescriptionId ] //userID, consultTableID, isFirstConsult userID to be sent, the user's ID from table //prescriptionId
        } else {
            userInfo = ["calldoccode": "", "relation":"self", "patientname": "", "patprofileid":GlobalDocdata.gprofileURL, "gender":"", "age":"", "consultrecordID":consultrecordIDpassed ]
        }
        print("USER INFO: \(userInfo)")
        self.session?.startCall(userInfo)
        self.session?.remoteVideoTrack(withUserID: NSNumber(value: callingnumber)).isEnabled = false
        self.session?.localMediaStream.videoTrack.isEnabled = false
        
        sendPushToOpponentsAboutNewCall()
        
    }
   
    
    func configureTopScreen() {
        
        self.session?.remoteVideoTrack(withUserID: NSNumber(value: callingnumber)).isEnabled = false
        self.session?.localMediaStream.videoTrack.isEnabled = false
        topView = UIView(frame: self.view.frame)
        profileImageView = UIImageView(frame: CGRect(x: (self.view.frame.width/2)-60, y: (self.view.frame.height/2)-60, width: 120.0, height: 120.0))
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width / 2
        profileImageView.clipsToBounds = true
        profileImageView.sd_setImage(with: URL(string: profileImage), placeholderImage: UIImage(named: "profile"))// #imageLiteral(resourceName: "logo")
        topView.addSubview(profileImageView)
        topView.backgroundColor = UIColor.white
        
        self.oponentVideoView?.addSubview(topView)
        
        //self.toolBar.buttons[1].isEnabled = true
    
        self.localvideoView?.layer.addSublayer(profilePic.layer)
        
    }
    
    
    //MARK: WebRTC configuration
    
    func cofigureVideo() {
        
        QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.videoCapture.startSession {
            
            self.localvideoView?.layer.cornerRadius = self.localvideoView.frame.size.width / 2;
            self.localvideoView?.clipsToBounds = true;
            self.localvideoView?.layer.borderWidth = 0.2
            self.localvideoView?.layer.borderColor = UIColor.lightGray.cgColor
            
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
                self.localvideoView?.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
            } else {
                self.configureTopScreen()
            }
            

            let media = QBRTCMediaStreamConfiguration()
            
        }
    }
    
    func configureAudio() {
        
        QBRTCConfig.mediaStreamConfiguration().audioCodec = .codecOpus
        //Save current audio configuration before start call or accept call
        QBRTCAudioSession.instance().initialize()
        QBRTCAudioSession.instance().currentAudioDevice = .speaker
        //OR you can initialize audio session with a specific configuration
        QBRTCAudioSession.instance().initialize { (configuration: QBRTCAudioSessionConfiguration) -> () in
            
            var options = configuration.categoryOptions
            if #available(iOS 10.0, *) {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetoothA2DP)
                options = options.union(AVAudioSessionCategoryOptions.allowAirPlay)
            } else {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetooth)
            }
            
            configuration.categoryOptions = options
            configuration.mode = AVAudioSessionModeVideoChat
        }
        
    }
    
    //MARK: Helpers
    
    func configureToolbar() {
        
        let audioEnable = defButton()
        audioEnable.iconView = iconView(normalImage: "unmute", selectedImage: "mute")
        self.toolBar?.addButton(button: audioEnable) { (button) in
            self.session?.localMediaStream.audioTrack.isEnabled = !(self.session?.localMediaStream.audioTrack.isEnabled)!
        }
        
        
        let videoEnable = defButton()
        videoEnable.iconView = iconView(normalImage: "videoOn", selectedImage: "videoOff")
        self.toolBar?.addButton(button: videoEnable) { (button) in
           
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                
                //self.session?.localMediaStream.videoTrack.isEnabled = true
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                
                for subView in self.oponentVideoView.subviews {
                    if subView != self.oponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
 
                for layer in self.localvideoView.layer.sublayers! {
                    layer.removeFromSuperlayer()
                }
                
                self.profilePic.layer.removeFromSuperlayer()

                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
                self.localvideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
                self.createCallBackEvent(withCode: 101)
                
            } else {
                self.createCallBackEvent(withCode: 104)
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.configureTopScreen()
                
            }
        }
        
        let dynamicEnable = defButton()
        dynamicEnable.iconView = iconView(normalImage: "speakeron", selectedImage: "speakerOff")
        self.toolBar?.addButton(button: dynamicEnable) { (button) in
            let device = QBRTCAudioSession.instance().currentAudioDevice;
            
            if device == .speaker {
                QBRTCAudioSession.instance().currentAudioDevice = .receiver
            } else {
                QBRTCAudioSession.instance().currentAudioDevice = .speaker
            }
        }
        
        let endthecall = defButton()
        endthecall.iconView = iconView(normalImage: "disconnect", selectedImage: "disconnect")
        self.toolBar?.addButton(button: endthecall) { (button) in
            self.calldisconnected()
        }

        self.toolBar?.updateItems()
    }
    
    func iconView(normalImage: String, selectedImage: String) -> UIImageView {
        let icon = UIImage.init(named: normalImage)
        let selectedIcon = UIImage.init(named: selectedImage)
        let iconView = UIImageView.init(image: icon, highlightedImage: selectedIcon)
        iconView.contentMode = .scaleAspectFit
        return iconView
    }
    
    func defButton() -> ToolbarButton {
        let defButton = ToolbarButton.init(frame: defRect)
        defButton.backgroundColor = defBgClr
        defButton.selectedColor = defSlctClr
        defButton.isPushed = true
        return defButton
    }
    
    @IBAction func cameraSwitchAction(_ sender: UIButton) {
        let position = self.videoCapture.position
        if position == .back {
            self.videoCapture.position = .front
        } else {
            self.videoCapture.position = .back
        }
    }
    
    func resumeVideoCapture() {
        // ideally you should always stop capture session
        // when you are leaving controller in any way
        // here we should get its running state back
         if self.videoCapture != nil && !self.videoCapture.hasStarted {
         self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
         self.videoCapture.startSession(nil)
         }
        /*if self.videoCapture != nil && !self.videoCapture.hasStarted {
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture.startSession(nil)
            self.localvideoView.isHidden = true
            //self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = false
        } else {
            self.localvideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
            self.localvideoView.isHidden = false
            //self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
        } */
    }
    
    func removeRemoteView(with userID: UInt) {

    }
    
    @objc func playCallingSound(_ sender: Any) {
        QMSoundManager.playCallingSound()
    }
    
    @IBAction func didPressendButton(_ sender: UIButton) {
        if self.session != nil {
            self.session?.hangUp(nil)
            
            self.callTimer?.invalidate()
        }
    }
    
    @IBAction func incallChatButtonClicked(_ sender: UIButton) {
        
        if (!consultrecordIDpassed.isEmpty) {
          /*  let user = QBUUser()
            user.id =  callingnumber
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.occupantIDs = [user.id] as [NSNumber]
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatvc.consultrecordIDpassed = self.consultrecordIDpassed
                chatvc.dialog = createdDialog
                chatvc.docprofileimageurl = self.profileImage
                //let VC1 = self.storyboard!.instantiateViewControllerWithIdentifier("MyViewController") as! ViewController
                let navController = UINavigationController(rootViewController: chatvc) // Creating a navigation controller with VC1 at the root of the navigation stack.
                self.present(navController, animated:true, completion: nil)
                //self.navigationController?.pushViewController(chatvc, animated: true)
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(String(describing: response))")
            }) */
            
             guard let chatConsultViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatConsultViewController") as? ChatConsultViewController else { fatalError("Error in opening ChatConsultViewController") }
             
             //let docselected = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
             chatConsultViewController.docselected = callingnumber//docselected
             //chatConsultViewController.pagerIndex = self.pagerIndexValue
            // chatConsultViewController.indexVal = self.myindexval
             chatConsultViewController.consultrecordIDpassed = self.consultrecordIDpassed//PatientConsultsdetailsArray[0].id! as String
             //chatConsultViewController.currentConsult = PatientConsultsdetailsArray[0]
             chatConsultViewController.docprofileimageurl = profileImage//(DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
//             let navController = UINavigationController()
//             navController.pushViewController(chatConsultViewController, animated: true)
            let navigationController = UINavigationController(rootViewController: chatConsultViewController)
            self.present(navigationController, animated: true, completion: nil)
             //self.navigationController?.pushViewController(chatConsultViewController, animated: true)
        }
        
    }
   
    func relayout(with views: [UIView]) {
        self.StackView.removeAllArrangedSubviews()
        
        for v in views {
            if self.StackView.arrangedSubviews.count > 1 {
                let i = views.index(of: v)! % 2
                let s = self.StackView.arrangedSubviews[i] as? UIStackView
                s?.addArrangedSubview(v)
            } else {
                let hStack = UIStackView()
                hStack.axis = .horizontal
                hStack.distribution = .fillEqually
                hStack.spacing = 5
                hStack.addArrangedSubview(v)
                self.StackView.addArrangedSubview(hStack)
            }
        }
    }
    
    func calldisconnected() {
        print("Disconnecting the call...")
        if self.session != nil {
//            var userInfo = [String:String]()
//
//            userInfo["calldoccode"] = !patientcdcode.isEmpty ? patientcdcode[0] : ""
//            userInfo["relation"] = "self"
//            userInfo["patientname"] = (!patientNames.isEmpty) ? patientNames[0] : ""
//            userInfo["patprofileid"] = GlobalDocdata.gprofileURL
//            userInfo["gender"] = !patientgender.isEmpty ? patientgender[0] : ""
//            userInfo["age"] = !patientage.isEmpty ? patientage[0] : ""
//
//            self.session?.hangUp(userInfo)
            
            self.session?.hangUp(nil)
            
            self.callTimer?.invalidate()
        }

        guard let VC1 = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:true, completion: nil)
    }
    
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        
        if self.session == nil {
            self.session = session
        }
    }
    
    
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        
        if (session as? QBRTCSession)?.id == self.session?.id {
            
            self.callinglabel.isHidden = true
            
            if beepTimer != nil {
                beepTimer?.invalidate()
                beepTimer = nil
                QMSoundManager.instance().stopAllSounds()
            }
            
            if session.conferenceType == QBRTCConferenceType.video {
                
            }
            
            if callTimer == nil {
                callTimer = Timer.scheduledTimer(timeInterval: kRefreshTimeInterval!, target: self, selector: #selector(self.refreshCallTime), userInfo: nil, repeats: true)
            }
        }
    }
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        
        if session.id == self.session?.id {
            
            self.removeRemoteView(with: userID.uintValue)
            if userID == session.initiatorID {
                self.session?.hangUp(nil)
            }
            self.calldisconnected()
        }
    }
    
    func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        
        if (session as? QBRTCSession)?.id == self.session?.id {
            
            /*if (session as? QBRTCSession)?.id == self.session?.id*/ //{
                self.oponentVideoView.setVideoTrack(videoTrack)
                let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
                remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                remoteView.clipsToBounds = true
                remoteView.setVideoTrack(videoTrack)
                remoteView.tag = userID.intValue
                //self.views.append(remoteView)
                //self.relayout(with: self.views)
                inCallChatButton.isHidden = false
           // }
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        
        if session.id == self.session?.id {
            
            self.session = nil
            
            if beepTimer != nil {
                beepTimer?.invalidate()
                beepTimer = nil
                QMSoundManager.instance().stopAllSounds()
            }
        }
    }
    
    func session(_ session: QBRTCSession!, rejectedByUser userID: NSNumber!, userInfo: [NSObject : AnyObject]!) {
        print("CallViewControllerswft: Rejected by user \(String(describing: userID))")
        
        self.session?.hangUp(nil)
        self.callTimer?.invalidate()
        
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        ToastManager.shared.isTapToDismissEnabled = true

        self.view.makeToast("Call Rejected by remote user", duration: 3.0, position: .center, title: "", image:UIImage(named: "toast.png") , style: style) { (didTap) in
            if didTap {
                print("CallViewControllerswft:Toast completion from tap")
                if self.session != nil {
                    print("CallViewControllerswft: making session Nil")
                    self.session = nil
                }
                guard let VC1 = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
                let navController = UINavigationController(rootViewController: VC1)
                self.present(navController, animated:true, completion: nil)
            } else {
                print("CallViewControllerswft:Toast completion without tap")
                if self.session != nil {
                    print("CallViewControllerswft: making session Nil")
                    self.session = nil
                }
                guard let VC1 = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
                let navController = UINavigationController(rootViewController: VC1)
                self.present(navController, animated:true, completion: nil)
            }
        }

    }
    
    func session(_ session: QBRTCSession, acceptedByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        
    }
    
    func handleIncomingCall() {
        
        let alert = UIAlertController.init(title: "Incoming video call", message: "Accept ?", preferredStyle: .actionSheet)
        
        let accept = UIAlertAction.init(title: "Accept", style: .default) { action in
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.session?.acceptCall(nil)
            self.endButton.isHidden = false
            self.toolBar.isHidden = false
        }
        
        let reject = UIAlertAction.init(title: "Reject", style: .default) { action in
            self.session?.rejectCall(nil)
        }
        
        alert.addAction(accept)
        alert.addAction(reject)
        self.present(alert, animated: true)
    }
    
    
    @objc func refreshCallTime(_ sender: Timer) {
        timeDuration += kRefreshTimeInterval!
        calltimelb.text =  gowithTimeDuration(timeDuration:timeDuration)
        
    }
    
    func gowithTimeDuration( timeDuration: TimeInterval) -> String {

        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [ .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]
        let formattedDuration = formatter.string(from: timeDuration)
        return formattedDuration!
    }
    
    //MARK:- Storyboard Instance
    
    static func storyboardInstance() -> CallViewControllerswft {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return (storyboard.instantiateViewController(withIdentifier: "makeCall") as? CallViewControllerswft) ?? CallViewControllerswft()
    }
    
    func sendPushToOpponentsAboutNewCall() {
        
        let opponentsIDs = (self.session?.opponentsIDs as NSArray?)?.componentsJoined(by: ",")
        let currentUserLogin = QBSession.current.currentUser?.fullName
        let message = "\("Patient " + callingPatientName ) is calling you"
        var payload = [String: AnyObject]()
        var pushKeys = [String: AnyObject]()
        pushKeys["title"] = "Added to COT" as AnyObject
        pushKeys[QBMPushMessageAlertKey] = message as AnyObject?
        pushKeys["type"] = "COT" as AnyObject? //(NotificationArray[indexPath.row].fields?.value(forKey: "typeofinvite") as? String) ?? ""
        payload[QBMPushMessageApsKey] = pushKeys as AnyObject?
        let pushMessage = QBMPushMessage.init()
        pushMessage.payloadDict = NSMutableDictionary(dictionary: payload)
        QBRequest.sendPush(pushMessage, toUsers: (opponentsIDs ?? ""), successBlock: { (_, _) in
            print("NotificationTableViewController:sendPushToInvitor(): Push sent!")
        }) { (error:QBError?) in
            print("NotificationTableViewController:sendPushToInvitor():Can not send push: \(error!))")
        }
//        var pushData = NSMutableDictionary()//[String: String]()
//        pushData.setValue(QBSession.current.currentUser, forKey: QBMPushMessageApsKey)
//        pushData.setValue("\("Patient " + callingPatientName ) is calling you", forKey: QBMPushMessageAlertKey)
//
//        let data = pushData as? [String: String]
//        let message = QBMPushMessage(payload: data ?? [:])
//
//        QBRequest.sendPush(message, toUsers: opponentsIDs, successBlock: { (QBResponse, QBMEvent) in
//            print("sendPushToOpponentsAboutNewCall: Push sent!")
//        }) { (QBError) in
//            print("sendPushToOpponentsAboutNewCall:Can not send push:")
//        }
        
//        QBRequest.sendPush(withText: "\("Patient " + callingPatientName ) is calling you", toUsers: (opponentsIDs ?? ""), successBlock: { (response: QBResponse, event:[QBMEvent]?) in
//            print("sendPushToOpponentsAboutNewCall: Push sent!")
//        }) { (error:QBError?) in
//            print("sendPushToOpponentsAboutNewCall:Can not send push: \(error!))")
//        }
    }
    
    func videoTurnOnNotificationReceived() {
        print("videoTurnOnNotificationReceived")
        let msg = "Dr. \(doctorname) is requesting for your video."
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DENY", style: .default, handler: { (action) in
            //self.showCOTConfirmationAlert()
            //VIDEO_ALLOW_CODE = 101;
            self.createCallBackEvent(withCode: 102)
        }))
        alert.addAction(UIAlertAction(title: "ACCEPT", style: .default, handler: { (action) in
            //self.showCOTConfirmationAlert()
            //VIDEO_REJECT_CODE = 102;
            for subView in self.oponentVideoView.subviews {
                if subView != self.oponentVideoView.subviews.first {
                    subView.removeFromSuperview()
                }
            }
            self.topView.removeFromSuperview()
            
            for layer in self.localvideoView.layer.sublayers! {
                layer.removeFromSuperlayer()
            }
            
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture?.previewLayer.frame = self.localvideoView.bounds
            self.localvideoView.layer.addSublayer((self.videoCapture?.previewLayer)!)
            
            self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
            self.session?.localMediaStream.videoTrack.isEnabled = true
            
            //self.toolBar.buttons[1].isHidden = false
            //self.addVideoIconToToolBar()
            
            self.createCallBackEvent(withCode: 101)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addVideoIconToToolBar() {
        
        let videoEnable = defButton()
        videoEnable.iconView = iconView(normalImage: "videoOn", selectedImage: "videoOff")
        self.toolBar.buttons[1] = videoEnable
        self.toolBar.addButton(button: videoEnable) { (button) in
            
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                
                //self.session?.localMediaStream.videoTrack.isEnabled = true
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                
                for subView in self.oponentVideoView.subviews {
                    if subView != self.oponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
                
                for layer in self.localvideoView.layer.sublayers! {
                    layer.removeFromSuperlayer()
                }
                
                self.profilePic.layer.removeFromSuperlayer()
                
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localvideoView.bounds
                self.localvideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
                
                
            } else {
                
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.configureTopScreen()
                
            }
        }
    }
    
    func createCallBackEvent(withCode: Int) {
        
        let payload = NSMutableDictionary()
        let opponentsIDs = (self.session?.opponentsIDs as NSArray?)?.componentsJoined(by: ",")
        
        payload.setValue("response", forKey: "title")
        payload.setValue(withCode, forKey: "code")
        payload.setValue("5", forKey: "ios_badge")
        payload.setValue("mysound.wav", forKey: "ios_sound")
        payload.setValue(String(Int((self.session?.currentUserID) ?? 0)), forKey: "user_id")
        payload.setValue("10", forKey: "thread_id")
        
        
        
        let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
        
        let message = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = opponentsIDs
        event.type = QBMEventType.oneShot
        event.message = message
        
        QBRequest.createEvent(event, successBlock: { (_, _) in
            print("EVENT CREATED SUCCESSFULLY")
        }) { (error:QBResponse?) in
            print("ERROR: \(String(describing: error))")
        }
    }
    
}

