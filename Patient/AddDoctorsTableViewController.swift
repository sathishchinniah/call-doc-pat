import UIKit
import Contacts
import ContactsUI
import DropDown

class AddDoctorsTableViewController: UITableViewController, UITextFieldDelegate, CNContactPickerDelegate {
    @IBOutlet weak var docMobileOrEmailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var specialityTextField: UITextField!
    @IBOutlet weak var hospitalTextField: UITextField!
    @IBOutlet weak var CityTextField: UITextField!
    @IBOutlet weak var countrycodetxt: UITextField!
    @IBOutlet weak var dropdowncodeview: UIView!

    var selectedIndex = Int.max
    var nav: UINavigationController?
    var doctorname = ""
    let CountryDropDown = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 160
        }
        if indexPath.row == 1 || indexPath.row == 3 {
            return 50
        }
        if indexPath.row == 2 && indexPath.row == selectedIndex {
            return 120
        }
        if indexPath.row == 4  && indexPath.row == selectedIndex {
            return 234
        }
        return 50
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        let cell = super.tableView(self.tableView, cellForRowAt: indexPath) as UITableViewCell
        if indexPath.row == 2 || indexPath.row == 4 {
            if cell.frame.size.height > 50 {
                selectedIndex = Int.max
            }
        }
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
    }
    
    //MARK:- UITextField delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (textField == countrycodetxt) {
            
            hideKeyboardWhenTappedAround()
            textField.resignFirstResponder()
            textField.endEditing(true)
            
            CountryDropDown.anchorView = dropdowncodeview
            CountryDropDown.dataSource = ["+91 India", "+44 UK", "+61 Singapore", "+1 United States"]
            CountryDropDown.show()
            CountryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                var ccode = ""
                
                var components = item.components(separatedBy: " ")
                if(components.count > 0) {
                    ccode = components.removeFirst()
                }
                self.countrycodetxt.text = ccode
                GlobalVariables.gcountrycode = item
            }
        }
    }
    
    @IBAction func cotBtnClicked(_ sender: UIButton) {
        print("cotClicked")
        guard let adddocfrommynwViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddDocFromDocNetwork") as? AddDocFromDocNetworkViewController else { fatalError("Error") }
        // these lines of code removes left and right animation
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        let animation = CATransition()
        animation.type = kCATransitionFade
        self.navigationController?.view.layer.add(animation, forKey: "someAnimation")
        self.nav = UINavigationController(rootViewController: adddocfrommynwViewController)
        self.present(self.nav!, animated: false, completion: nil)
        CATransaction.commit()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == docMobileOrEmailTextField {
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = (newString as NSString).components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            if length == 0 || length == 10 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                let index = 0 as Int
                let formattedString = NSMutableString()
                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
                textField.text = formattedString as String
                docMobileOrEmailTextField.resignFirstResponder()
                return (newLength >= 10) ? false : true
            }
            let index = 0 as Int
            let formattedString = NSMutableString()
            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        } else {
            return true
        }
    }
    
    @IBAction func AddressBookButton(_ sender: UIButton) {
        let entityType = CNEntityType.contacts
        let authStatus = CNContactStore.authorizationStatus(for: entityType)
        if authStatus == CNAuthorizationStatus.notDetermined {
            let contactStore = CNContactStore.init()
            contactStore.requestAccess(for: entityType, completionHandler: { (success, nil) in
                
                if success {
                    self.openContacts()
                } else {
                    print("Addressboon Not Authorised")
                }
            })
        } else if authStatus == CNAuthorizationStatus.authorized {
            
            self.openContacts()
        }
    }
    
    func openContacts() {
        let contactPicker = CNContactPickerViewController.init()
        contactPicker.delegate = self
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        picker.dismiss(animated: true) {
        }
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        var phoneNo = "NA"
        let phoneString = (((((contact.phoneNumbers[0] as  AnyObject).value(forKey: "labelValuePair") as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "stringValue")) as? String) ?? ""
        let phone = phoneString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        phoneNo = phone//(phone as? String) ?? ""
        self.docMobileOrEmailTextField.text = phoneNo
//        if phoneNo.count >= 11 {
//            let substring = phoneNo.substring(from: phoneNo.index(phoneNo.startIndex, offsetBy: 3))
//            self.docMobileOrEmailTextField.text = substring
//        } else {
//            self.docMobileOrEmailTextField.text = phoneNo
//        }
    }

    @IBAction func searchBtnClicked(_ sender: UIButton) {

        print("AddDoctorsTableViewController:searchBtnClicked")
    }

    @IBAction func inviteBtnClicked(_ sender: UIButton) {

        if (docMobileOrEmailTextField.text?.count)! < 10 {
            let alert = UIAlertController(title: "Alert", message: "Please Entre 10 Digit Mobile Number!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                LoadingIndicatorView.hide()
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return

        } else if (docMobileOrEmailTextField.text?.count)! > 10 {

            let alert = UIAlertController(title: "Alert", message: "Please make Mobile Number to 10 digits without + country code!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Do Some action here
                LoadingIndicatorView.hide()
            })
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            return
        }
        self.checkifUserAlreadyExist()

    }
    // This is to check if user is already registered in CalDoc, if registered then check RelDocPat Table else send SMS
    func checkifUserAlreadyExist() {
        var totalmobilenumber = ""
        totalmobilenumber = self.countrycodetxt.text! + docMobileOrEmailTextField.text!
        let stringWithoutpluswith_p: String = totalmobilenumber.replacingOccurrences(of: "+", with: "d_")
        LoadingIndicatorView.show("Send Invite....")
        
        QBRequest.users(withLogins: [stringWithoutpluswith_p], page: QBGeneralResponsePage(currentPage: 1, perPage: 10), successBlock: {(_ response: QBResponse, _ page: QBGeneralResponsePage, _ users: [Any]) -> Void in
            if users.count > 0 {
                let selectedUser = users[0] as? QBUUser
                self.doctorname = (selectedUser?.fullName)!
                self.ReadCallDoctable_RelDocPat(invitoruid: (QBSession.current.currentUser?.id)!, inviteeuid: (selectedUser?.id)!)
            } else {
                print("AddDoctorsTableViewConteroller:The Doctor does not exist on CallDoc Database hence send SMS to Doctor")
                self.SendSMSInvitetoDoctor()
                
                
            }
        }, errorBlock: {(_ response: QBResponse) -> Void in
            // Handle error
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:CheckifUserAlreadyExist - Response error")
        })
    }
    
    // check if there is entry of doctor and patient relation in RelDocPat table
    func ReadCallDoctable_RelDocPat(invitoruid: UInt, inviteeuid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["doctoruserid"] =  inviteeuid
        getRequest["patientuserid"] = invitoruid
        
        QBRequest.objects(withClassName: "RelDocPat", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Successfully  !")
            if ((contributors?.count)! > 0) {
                print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat():Match Entry Found  !")
                let alert = UIAlertController(title: "Alert", message: "User Already Connected !", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
                    guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: false, completion: nil)
                    })
                alert.addAction(ok)
                LoadingIndicatorView.hide()
                self.present(alert, animated: true, completion: nil)
            } else {
                print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Relation Match NOT Found  !")
                self.readdoctordetails(doctoruserid: inviteeuid, invitoruserid: invitoruid)
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func readdoctordetails(doctoruserid: UInt, invitoruserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctoruserid
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                
                let profileVerifed = (contributors![0].fields?.value(forKey: "profileverified") as? Bool) ?? false
                if profileVerifed == false {
                    let alert = UIAlertController(title: "Action currently unavailable. Please try later", message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    return
                } else {
                    let speciality = contributors![0].fields?.value(forKey: "specialisation") as? String
                    var profileurl = contributors![0].fields?.value(forKey: "profileUID") as? String
                    
                    if (profileurl == nil || profileurl!.isEmpty) {
                        profileurl = "none"
                    }
                    self.createEntryinRelDocPatTable(invitoruid: invitoruserid, inviteeuid: doctoruserid, speciality: speciality!, profileurl: profileurl! )
                }
                
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:ReadPrimaryDetailsFromServer() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createEntryinRelDocPatTable(invitoruid: UInt, inviteeuid: UInt, speciality: String, profileurl: String ) {
        print("AddDoctorsTableViewConteroller: createEntryinRelDocPatTable(): Creating new entry on RelDocPatable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = inviteeuid
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = invitoruid //QBSession.current.currentUser?.id
        object.fields["doctorfullname"] = self.doctorname
        object.fields["lock"] = "true"
        object.fields["doctorspeciality"] = speciality
        object.fields["doctorprofileimageURL"] = profileurl
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("AddDoctorsTableViewConteroller: createEntryinRelDocPatTable(): Successfully created entry")
            LoadingIndicatorView.hide()
            
            let alert = UIAlertController(title: "Congratulations", message: " Your Dr. \(self.doctorname) - \(speciality)  already Registered on CallDoc. Invite has been sent !!", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)

            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }) { (response) in
            //Handle Error
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:createEntryinRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func SendSMSInvitetoDoctor() {
        self.SendSMSusingMSG91API()
    }

    func  SendSMSusingMSG91API() {
        let mobilenumber = self.docMobileOrEmailTextField.text!
        let patientname = patientNames[0]
        let patnameparts = patientname.components(separatedBy: " ")
        var patfirstname = ""
        var patlastname = ""
        GlobalDocdata.gthefinalcalldoccode = (QBSession.current.currentUser?.customData)!
        let calldoccode = GlobalDocdata.gthefinalcalldoccode
        patfirstname = patnameparts[0]
        patlastname = patnameparts[1]
        let headers = [
            "authkey": "191254A7N7Dkz1Y5a4db3e0",
            "content-type": "application/json"
        ]
        let parameters = [
            "sender": "CALDOC",
            "route": "4",
            "country": "91",
            "sms": [
                [
                    "message": "Hi Doctor - \(mobilenumber). You are invited by Patient \(patfirstname) \(patlastname). Use CallDoc Code \(calldoccode) to install/Register 'https://www.calldoc.com' ",
                    "to": [mobilenumber]
                ]
            ]
            ] as [String : Any]
        
        do {
            let postData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let request = NSMutableURLRequest(url: NSURL(string: "http://api.msg91.com/api/v2/sendsms")! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    print("AddDoctorsTableViewConteroller:SendSMSusingMSG91API():SMS Sent ERROR")
                    print(error!)
                    LoadingIndicatorView.hide()
                    return
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print("AddDoctorsTableViewConteroller:SendSMSusingMSG91API():SMS Sent Successfully")
                    print(httpResponse!)
                    LoadingIndicatorView.hide()
                    DispatchQueue.main.async {
                        self.SMSSendshowAlert(mobilenum: mobilenumber)
                    }
                }
            })
            dataTask.resume()
            
        } catch let error as NSError {
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:SendSMSusingMSG91API():JSON error Failed to load: \(error.localizedDescription)")
            return
        }
    }
    
    func SMSSendshowAlert(mobilenum: String) {
        // create the alert
        let alert = UIAlertController(title: "Alert", message: "Congrats ! The Invite SMS has been send to \(mobilenum)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
            self.ActiononOK()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func ActiononOK() {
        LoadingIndicatorView.hide()
        self.navigationController?.isNavigationBarHidden = false
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }

}
