import UIKit

let greenProfileBorder = UIColor(red: 39/255, green: 134/255, blue: 57/255, alpha: 1).cgColor
let orangeProfileBorder = UIColor(red: 238/255, green: 79/255, blue: 7/255, alpha: 1).cgColor
let redProfileBorder = UIColor(red: 200/255, green: 24/255, blue: 39/255, alpha: 1).cgColor

class TheDoctorTeamViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var mainImageViewContainer: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var docteamtable: UITableView!
    @IBOutlet weak var nodoctorteamlb: UILabel!
    
    let doctorNames = ["Dr.Ayisha Ibrahim", "Dr.Suneel Kumar", "Dr.Parthiban", "Dr.Sunitha Pandian", "Dr. Mary Joseph"]
    let doctorImages = [#imageLiteral(resourceName: "doctor1"),#imageLiteral(resourceName: "doctor2"),#imageLiteral(resourceName: "doctor3"),#imageLiteral(resourceName: "doctor4"),#imageLiteral(resourceName: "doctor1")]
    let doctorsStatus = [greenProfileBorder, redProfileBorder, greenProfileBorder, orangeProfileBorder,redProfileBorder]

    let cell_ReuseIdentifier = "DoctorTeamCell"
    var nav: UINavigationController?
    var myindexval: Int = 0
    var docuserid: UInt = 0
    var DocCounttablerecordID = ""
    var teamArray:[QBCOCustomObject] = []
    var sourceController = ""
    var docStatus = ""
    
    static let defaultArray = [false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    
    var mondayArrayOfBools = defaultArray
    var tuesdayArrayOfBools = defaultArray
    var wednesdayArrayOfBools = defaultArray
    var thursdayArrayOfBools = defaultArray
    var fridayArrayOfBools = defaultArray
    var saturdayArrayOfBools = defaultArray
    var sundayArrayOfBools = defaultArray
    var currentlyOnline = false
    
    var doctorResumeConsult = true
    var doctorSchedularStatus = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.updateTheView()
        self.docteamtable.delegate = self
        self.docteamtable.dataSource = self
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navBarColor.topItem?.title = "Doctor’s Trusted Team"
        title =  "Doctor’s Trusted Team"
        
        mainImageViewContainer.layer.cornerRadius = mainImageView.frame.size.width / 2;
        mainImageViewContainer.clipsToBounds = true;
        mainImageViewContainer.layer.borderWidth = 2.0
        mainImageViewContainer.layer.borderColor = greenProfileBorder
        if DoctorsArray.count > myindexval {
            var profileurl = (DoctorsArray[myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
            if (profileurl == nil || profileurl.isEmpty) {
                /* string is  blank */
                profileurl = ""
            }
            mainImageView.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "Profile@2x.png"), options: .cacheMemoryOnly)
        }
        
     
        mainImageView.layer.cornerRadius = mainImageView.frame.size.width / 2;
        mainImageView.clipsToBounds = true;
        mainImageView.layer.borderWidth = 3.0
        mainImageView.layer.borderColor = UIColor.white.cgColor
        
        let button = UIButton()
        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        var rightsideimage = UIImage(named: "patient3")!
        QBRequest.downloadFile(withUID: patientimageurl[0], successBlock: { (response : QBResponse, imgData: Data)in
            let oImage = UIImage(data: imgData)
            if oImage != nil {
                rightsideimage = oImage!
                UIGraphicsBeginImageContextWithOptions(button.frame.size, false, rightsideimage.scale)
                let rect  = CGRect(x: 0, y: 0, width: button.frame.size.width, height: button.frame.size.height)
                UIBezierPath(roundedRect: rect, cornerRadius: rect.width/2).addClip()
                rightsideimage.draw(in: rect)
                
                let newImage = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                let color = UIColor(patternImage: newImage!)
                button.backgroundColor = color
                button.layer.cornerRadius = 0.5 * button.bounds.size.width
                let barButton = UIBarButtonItem()
                barButton.customView = button
                self.navigationItem.rightBarButtonItem = barButton
            } else {
                rightsideimage =  UIImage(named: "profile")!
                print("*****Image Load Failure")
            }
        }, statusBlock: nil, errorBlock: nil)
        //back button
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        if docStatus == "online" {
            mainImageViewContainer.layer.borderColor = UIColor.green.cgColor
        } else if docStatus == "offline" {
            mainImageViewContainer.layer.borderColor = UIColor.red.cgColor
        } else if docStatus == "pause" {
            mainImageViewContainer.layer.borderColor = UIColor.orange.cgColor
        }
        
    }

    func profileBtnPressed(sender: UIBarButtonItem) {
        print("profileBtnPressed Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
//        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
//        self.nav = UINavigationController(rootViewController: homeViewController)
//        self.present(self.nav!, animated: false, completion: nil)
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.teamArray.count
    }
    
    func numberOfSections(in docteamtable: UITableView) -> Int {
        if teamArray.count <= 0 {
            nodoctorteamlb.isHidden = false
        } else {
            nodoctorteamlb.isHidden = true
        }
        
        return teamArray.count
    }
    
    
    public func tableView(_ docteamtable: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = docteamtable.dequeueReusableCell(withIdentifier: cell_ReuseIdentifier) as! DoctorTeamTableViewCell
        
        cell.ImageView.layer.borderColor = UIColor.clear.cgColor
        cell.ImageViewContainer.layer.borderColor = UIColor.clear.cgColor
        let docID = ((teamArray[indexPath.section].fields?.value(forKey: "doctoruserid")) as? UInt) ?? 0
        self.configureDoctorListForMemberTableViewCell(cell: cell, docID: docID)
        
        cell.dataContainerView.layer.cornerRadius = 50;
        cell.dataContainerView.layer.masksToBounds = false
        cell.dataContainerView.layer.shadowColor = UIColor.black.cgColor
        cell.dataContainerView.layer.shadowOpacity = 0.5
        cell.dataContainerView.layer.shadowOffset = CGSize(width: -0.7, height: 1.5)
        cell.dataContainerView.layer.shadowRadius = 1

        cell.ImageViewContainer.layer.cornerRadius = cell.ImageViewContainer.frame.size.width / 2;
        cell.ImageViewContainer.clipsToBounds = true;
        cell.ImageViewContainer.layer.borderWidth = 2.0
        //cell.ImageViewContainer.layer.borderColor = UIColor.blue.cgColor

        cell.ImageView.layer.cornerRadius = cell.ImageView.frame.size.width / 2;
        cell.ImageView.clipsToBounds = true;
        cell.ImageView.layer.borderWidth = 3.0
        //cell.ImageView.layer.borderColor = UIColor.white.cgColor

        
        
        
        cell.nameLabel.text = "Dr. " + (teamArray[indexPath.section].fields?.value(forKey: "inviteedocfullname") as? String)!
        cell.ImageView.sd_setImage(with: URL(string: (teamArray[indexPath.section].fields?.value(forKey: "inviteeprofileURL") as? String)!), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        //Get speciality and load the data
        //inviteedocuserid
    
        cell.designationLabel.text = "General Medicine"

        return cell
    }

    func tableView(_ docteamtable: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("INDEXPATH: \(indexPath)")
        
        if sourceController == "COT" {
            let vc = COTProfileViewController.storyboardInstance()
            vc.docfullname = (teamArray[indexPath.section].fields?.value(forKey: "inviteedocfullname") as? String) ?? ""//doc1fullname
            vc.imageURLpassed = (teamArray[indexPath.section].fields?.value(forKey: "inviteeprofileURL") as? String) ?? ""//doc1imgURL
//            vc.docspeciality = doc1speciality
            vc.docuserid = Int((teamArray[indexPath.section].fields?.value(forKey: "inviteedocuserid") as? UInt) ?? (0 as UInt))
            vc.doctorSelected = teamArray[indexPath.section]
            vc.sourceView = "TEAM"
            // this line will hide back button title on next viewcontroller navigation bar
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let VC1 = DocConsultViewController.storyboardInstance()
            VC1.myindexval = indexPath.section
            VC1.sourceView = "TEAM"
            VC1.teamDocSelected = teamArray[indexPath.section]
//            VC1.currentPatientName = patientNames[pagerviewindex]
//            VC1.pagerIndexValue = pagerviewindex
            self.navigationController?.pushViewController(VC1, animated: true)

        }
        
        
    }
    
    
    
    func updateTheView() {
        // we need to read RelDocTeam Table using Invitor userid and Invitee Speciality
        let getRequest = NSMutableDictionary()
        getRequest["invitordocuserid"] = docuserid
        getRequest["status"] = true
        QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("MyTeamViewController:updateTheView(): Success ")
            
            if ((contributors?.count)! > 0) {
                self.teamArray = contributors!
                self.docteamtable.reloadData()
            }
        }) { (response) in
            //Handle Error
            print("MyTeamViewController:updateTheView(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func configureDoctorListForMemberTableViewCell(cell: DoctorTeamTableViewCell, docID: UInt) {
        self.getSchedularDataArrayfromBackend(doctorId: docID) { (success) in
            if success {
                
                let dayOfTheWeek = Calendar.current.component(.weekday, from: Date())
                
                var currentDayArray = [Bool]()
                if dayOfTheWeek == 1 {
                    //sunday
                    currentDayArray = self.sundayArrayOfBools
                }
                
                if dayOfTheWeek == 2 {
                    //monday
                    currentDayArray = self.mondayArrayOfBools
                }
                
                if dayOfTheWeek == 3 {
                    //tuesday
                    currentDayArray = self.tuesdayArrayOfBools
                    
                }
                
                if dayOfTheWeek == 4 {
                    //wednesday
                    currentDayArray = self.wednesdayArrayOfBools
                }
                
                if dayOfTheWeek == 5 {
                    //thursday
                    currentDayArray = self.thursdayArrayOfBools
                }
                
                if dayOfTheWeek == 6 {
                    //friday
                    currentDayArray = self.fridayArrayOfBools
                }
                
                if dayOfTheWeek == 7 {
                    //saturday
                    currentDayArray = self.saturdayArrayOfBools
                }
                let currentStatus = self.getCurrentOnlineOfflineStatus(currentDayBoolArray: currentDayArray)
                
                if currentStatus {
                    cell.ImageView.layer.borderColor = greenProfileBorder
                    cell.ImageViewContainer.layer.borderColor = UIColor.clear.cgColor//greenProfileBorder
                    if self.doctorResumeConsult == false {
                        cell.ImageView.layer.borderColor = orangeProfileBorder
                        cell.ImageViewContainer.layer.borderColor = UIColor.clear.cgColor//orangeProfileBorder
                    }
                    if self.doctorSchedularStatus == false {
                        cell.ImageView.layer.borderColor = redProfileBorder
                        cell.ImageViewContainer.layer.borderColor = UIColor.clear.cgColor//redProfileBorder
                    }
                } else {
                    cell.ImageView.layer.borderColor = redProfileBorder
                    cell.ImageViewContainer.layer.borderColor = UIColor.clear.cgColor//redProfileBorder
                }
                
            } else {
                cell.ImageView.layer.borderColor = redProfileBorder
                cell.ImageViewContainer.layer.borderColor = UIColor.clear.cgColor//redProfileBorder
            }
        }
    }
    
    func getSchedularDataArrayfromBackend(doctorId: UInt, completion: @escaping (Bool) -> ()) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctorId//QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            
            if ((contributors?.count)! > 0) {
                self.mondayArrayOfBools = contributors![0].fields?.value(forKey: "Monday") as! [Bool]
                self.tuesdayArrayOfBools = contributors![0].fields?.value(forKey: "Tuesday") as! [Bool]
                self.wednesdayArrayOfBools = contributors![0].fields?.value(forKey: "Wednesday") as! [Bool]
                self.thursdayArrayOfBools = contributors![0].fields?.value(forKey: "Thursday") as! [Bool]
                self.fridayArrayOfBools = contributors![0].fields?.value(forKey: "Friday") as! [Bool]
                self.saturdayArrayOfBools = contributors![0].fields?.value(forKey: "Saturday") as! [Bool]
                self.sundayArrayOfBools = contributors![0].fields?.value(forKey: "Sunday") as! [Bool]
                
                UserDefaults.standard.set(self.mondayArrayOfBools, forKey: Constants.mondaySchedularData)
                UserDefaults.standard.set(self.tuesdayArrayOfBools, forKey: Constants.tuesdaySchedularData)
                UserDefaults.standard.set(self.wednesdayArrayOfBools, forKey: Constants.wednesdaySchedularData)
                UserDefaults.standard.set(self.thursdayArrayOfBools, forKey: Constants.thursdaySchedularData)
                UserDefaults.standard.set(self.fridayArrayOfBools, forKey: Constants.fridaySchedularData)
                UserDefaults.standard.set(self.saturdayArrayOfBools, forKey: Constants.saturdaySchedularData)
                UserDefaults.standard.set(self.sundayArrayOfBools, forKey: Constants.sundaySchedularData)
                
                self.doctorResumeConsult = (contributors![0].fields.value(forKey: "resumeConsult") as? Bool) ?? false
                self.doctorSchedularStatus = (contributors![0].fields.value(forKey: "SchedularStatus") as? Bool) ?? false
                
            }
            completion(true)
            
        }) { (errorresponse) in
            print("HomeViewController: getSchedularDatafromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func getCurrentOnlineOfflineStatus(currentDayBoolArray: [Bool]) -> Bool {
        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute
        let returnValue = currentDayBoolArray[Int(currentTime * 2)]
        return returnValue
    }
    
}
