//
//  ChatViewController.h
//  Patient
//
//  Created by Manish Verma on 21/08/17.
//  Copyright © 2017 calldoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMChatViewController.h"

@class GData;

@interface ChatViewController : QMChatViewController

@property (nonatomic, strong) QBChatDialog *dialog;

@property (nonatomic,retain) GData *dataPointer;

@property (strong, nonatomic) IBOutlet UIView *topframeview;


@end
