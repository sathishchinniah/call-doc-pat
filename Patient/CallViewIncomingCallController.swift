import UIKit
import Quickblox
import QuickbloxWebRTC
import SVProgressHUD
import Toast_Swift

private let kRefreshTimeInterval = 1.0 as? TimeInterval

class CallViewIncomingCallController: UIViewController, QBRTCClientDelegate {

    let defRect = CGRect.init(x: 0, y: 0, width: 48, height: 48)
    let defBgClr = UIColor.init(red: 0.8118, green: 0.8118, blue: 0.8118, alpha: 1.0)
    let defSlctClr = UIColor.init(red: 0.3843, green: 0.3843, blue: 0.3843, alpha: 1.0)
    
    @IBOutlet weak var toolBar: Toolbar!
    @IBOutlet weak var opponentVideoView: QBRTCRemoteVideoView!
    @IBOutlet weak var localVideoView: UIView!
    @IBOutlet weak var locallablel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var incallChatButton: UIButton!
    
    var nav: UINavigationController?
    open var opponets: [QBUUser]?
    open var currentUser: QBUUser?
    var views: [UIView] = []
    var videoCapture: QBRTCCameraCapture!
    var session: QBRTCSession?
    var callTimer: Timer?
    var timeDuration = TimeInterval()
    var callingnumber: UInt = 0
    var doctorID = UInt()
    var userID = UInt()
    
    var topView: UIView!
    var profileImageView: UIImageView!
    var profilePic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)

        incallChatButton.isHidden = true
        
        
        cofigureVideo()
        configureAudio()
        
        navigationController?.isNavigationBarHidden = true
        
        self.navigationItem.setHidesBackButton(true, animated:true)
        toolBar.isHidden = true
        
        configureToolbar()
        configureTopScreen()
        self.session?.localMediaStream.videoTrack.isEnabled = false
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.resumeVideoCapture()
    }
    
    func configureTopScreen() {
        
        topView = UIView(frame: self.view.frame)
        profileImageView = UIImageView(frame: CGRect(x: (self.view.frame.width/2)-60, y: (self.view.frame.height/2)-60, width: 120.0, height: 120.0))
        profileImageView.image = #imageLiteral(resourceName: "logo")
//        videoCallButton = UIButton(frame: CGRect(x: (self.view.frame.width/2)+40, y: (self.view.frame.height/2)-40, width: 80.0, height: 80.0))
//        videoCallButton.setImage(#imageLiteral(resourceName: "callblk"), for: .normal)
//        videoCallButton.addTarget(self, action: #selector(indicateVideo(_:)), for: .touchUpInside)
        
//        self.view.addSubview(videoCallButton)
        topView.addSubview(profileImageView)
        topView.backgroundColor = UIColor.white
        
        self.opponentVideoView.addSubview(topView)
        if profilePic != nil {
                self.localVideoView?.layer.addSublayer(profilePic.layer)
        }
        
    }
    
    //MARK: WebRTC configuration
    
    func cofigureVideo() {
        
        /*QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.videoCapture.startSession {
            
            if !((self.session?.localMediaStream.videoTrack.isEnabled) ?? false) {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
                self.videoCapture!.startSession()
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localVideoView.isHidden = false
            } else {
                self.localVideoView.isHidden = true
            }
            
            self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
            self.localVideoView.layer.cornerRadius = self.localVideoView.frame.size.width / 2
            self.localVideoView.clipsToBounds = true
            self.localVideoView.layer.borderWidth = 0.2
            self.localVideoView.layer.borderColor = UIColor.lightGray.cgColor
        } */
        print("CONFIGUREVIDEO")
        
        QBRTCConfig.mediaStreamConfiguration().videoCodec = .H264
        
        let videoFormat = QBRTCVideoFormat.init()
        videoFormat.frameRate = 21
        videoFormat.pixelFormat = .format420f
        videoFormat.width = 640
        videoFormat.height = 480
        
        self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: .front)
        self.videoCapture.previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        self.videoCapture.startSession {
            
            self.localVideoView.layer.cornerRadius = self.localVideoView.frame.size.width / 2;
            self.localVideoView.clipsToBounds = true;
            self.localVideoView.layer.borderWidth = 0.2
            self.localVideoView.layer.borderColor = UIColor.lightGray.cgColor
            
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
            } else {
                self.configureTopScreen()
            }
            
            
            let media = QBRTCMediaStreamConfiguration()
            
        }
    }
    
    func configureAudio() {
        
        QBRTCConfig.mediaStreamConfiguration().audioCodec = .codecOpus
        //Save current audio configuration before start call or accept call
        QBRTCAudioSession.instance().initialize()
        QBRTCAudioSession.instance().currentAudioDevice = .speaker
        //OR you can initialize audio session with a specific configuration
        QBRTCAudioSession.instance().initialize { (configuration: QBRTCAudioSessionConfiguration) -> () in
            
            var options = configuration.categoryOptions
            if #available(iOS 10.0, *) {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetoothA2DP)
                options = options.union(AVAudioSessionCategoryOptions.allowAirPlay)
            } else {
                options = options.union(AVAudioSessionCategoryOptions.allowBluetooth)
            }
            
            configuration.categoryOptions = options
            configuration.mode = AVAudioSessionModeVideoChat
        }
        
    }
    
    //MARK: Helpers
    
    func configureToolbar() {
        
        let audioEnable = defButton()
        audioEnable.iconView = iconView(normalImage: "unmute", selectedImage: "mute")
        self.toolBar.addButton(button: audioEnable) { (button) in
            self.session?.localMediaStream.audioTrack.isEnabled = !(self.session?.localMediaStream.audioTrack.isEnabled)!
        }
        
        
        let videoEnable = defButton()
        /*videoEnable.iconView = iconView(normalImage: "videoOff", selectedImage: "videoOn")
        self.toolBar.addButton(button: videoEnable) { (button) in
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if (self.session?.localMediaStream.videoTrack.isEnabled)! {
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localVideoView.isHidden = false
            } else {
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
                self.localVideoView.isHidden = true
            }
        }*/
        videoEnable.iconView = iconView(normalImage: "videoOn", selectedImage: "videoOff")
        self.toolBar.addButton(button: videoEnable) { (button) in
            
            self.session?.localMediaStream.videoTrack.isEnabled = !(self.session?.localMediaStream.videoTrack.isEnabled)!
            if self.session?.localMediaStream.videoTrack.isEnabled == true {
                
                //self.session?.localMediaStream.videoTrack.isEnabled = true
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                
                for subView in self.opponentVideoView.subviews {
                    if subView != self.opponentVideoView.subviews.first {
                        subView.removeFromSuperview()
                    }
                }
                self.topView.removeFromSuperview()
                
                for layer in self.localVideoView.layer.sublayers! {
                    layer.removeFromSuperlayer()
                }
                
                self.profilePic?.layer.removeFromSuperlayer()
                
                self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
                self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
                self.localVideoView.layer.addSublayer(self.videoCapture!.previewLayer)
                
                self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
                self.session?.localMediaStream.videoTrack.isEnabled = true
                
                
            } else {
                
                self.session?.localMediaStream.videoTrack.videoCapture = nil
                self.configureTopScreen()
                
            }
        }

        
        let dynamicEnable = defButton()
        dynamicEnable.iconView = iconView(normalImage: "speakeron", selectedImage: "speakerOff")
        self.toolBar.addButton(button: dynamicEnable) { (button) in
            let device = QBRTCAudioSession.instance().currentAudioDevice;
            
            if device == .speaker {
                QBRTCAudioSession.instance().currentAudioDevice = .receiver
            } else {
                QBRTCAudioSession.instance().currentAudioDevice = .speaker
            }
        }
        
        let endthecall = defButton()
        endthecall.iconView = iconView(normalImage: "disconnect", selectedImage: "disconnect")
        self.toolBar.addButton(button: endthecall) { (button) in
            self.calldisconnected()
        }

        self.toolBar.updateItems()
        
        if (gsession != nil) {
            print("CallViewIncomingCallController:with gsession:")
            handleIncomingCall()
        }
    }
    
    func iconView(normalImage: String, selectedImage: String) -> UIImageView {
        let icon = UIImage.init(named: normalImage)
        let selectedIcon = UIImage.init(named: selectedImage)
        let iconView = UIImageView.init(image: icon, highlightedImage: selectedIcon)
        iconView.contentMode = .scaleAspectFit // .scaleAspectFill     //.scaleAspectFit
        return iconView
    }
    
    func defButton() -> ToolbarButton {
        let defButton = ToolbarButton.init(frame: defRect)
        defButton.backgroundColor = defBgClr
        defButton.selectedColor = defSlctClr
        defButton.isPushed = true
        return defButton
    }

    @IBAction func CameraSwitchButton(_ sender: UIButton) {
        let position = self.videoCapture.position
        if position == .back {
            self.videoCapture.position = .front
        } else {
            self.videoCapture.position = .back
        }
    }
    
    func resumeVideoCapture() {
        // ideally you should always stop capture session
        // when you are leaving controller in any way
        // here we should get its running state back
        if self.videoCapture != nil && !self.videoCapture.hasStarted {
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture.startSession(nil)
        }
        
       /* if self.videoCapture != nil && !self.videoCapture.hasStarted {
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture.startSession(nil)
            self.localVideoView.isHidden = true
        } else {
            self.localVideoView.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)
            self.localVideoView.isHidden = false
        } */
        
    }
    
    func relayout(with views: [UIView]) {
        self.stackView.removeAllArrangedSubviews()
        
        for v in views {
            
            if self.stackView.arrangedSubviews.count > 1 {
                let i = views.index(of: v)! % 2
                let s = self.stackView.arrangedSubviews[i] as? UIStackView
                s?.addArrangedSubview(v)
            } else {
                let hStack = UIStackView()
                hStack.axis = .horizontal
                hStack.distribution = .fillEqually
                hStack.spacing = 5
                hStack.addArrangedSubview(v)
                self.stackView.addArrangedSubview(hStack)
            }
        }
    }
    
    func removeRemoteView(with userID: UInt) {

    }
    
    func calldisconnected() {
        print("CallViewIncomingCallController: Disconnecting the call...")
        if self.session != nil {
            self.session?.hangUp(nil)
            gsession = nil
            self.callTimer?.invalidate()
        }
        
        QBRTCClient.instance().remove(self)
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        
        if (session as? QBRTCSession)?.id == self.session?.id {
            if session.conferenceType == QBRTCConferenceType.video {
                
            }
            
            if callTimer == nil {
                callTimer = Timer.scheduledTimer(timeInterval: kRefreshTimeInterval!, target: self, selector: #selector(self.refreshCallTime), userInfo: nil, repeats: true)
            }
        }
    }
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        
        if session.id == self.session?.id {
            
            self.removeRemoteView(with: userID.uintValue)
            if userID == session.initiatorID {
                self.session?.hangUp(nil)
                self.calldisconnected()
            }
        }
    }
    
    func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        
        if (session as? QBRTCSession)?.id == self.session?.id {
            
            let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
            remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
            remoteView.clipsToBounds = true
            remoteView.setVideoTrack(videoTrack)
            remoteView.tag = userID.intValue
            self.views.append(remoteView)
            self.relayout(with: self.views)
            incallChatButton.isHidden = false
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if session.id == self.session?.id {
            gsession = nil
            self.session = nil
            self.calldisconnected()
        }
    }

    func handleIncomingCall() {
        print("CallViewIncomingCallController:with handleIncomingCall()")
        self.session = gsession
        self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
        self.session?.acceptCall(nil)
        toolBar.isHidden = false
    }
    
    @IBAction func inCallChatButtonPressed(_ sender: UIButton) {
        
        if userID != nil { //(!userID.isEmpty) {
            let user = QBUUser()
            user.id =  doctorID
            let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
            chatDialog.occupantIDs = [user.id] as [NSNumber]
            
            QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatvc.consultrecordIDpassed = "dabkh"
                chatvc.dialog = createdDialog
                //chatvc.docprofileimageurl = self.profileImage
                let navController = UINavigationController(rootViewController: chatvc) // Creating a navigation controller with VC1 at the root of the navigation stack.
                self.present(navController, animated:true, completion: nil)
                //self.navigationController?.pushViewController(chatvc, animated: true)
            }, errorBlock: {(response: QBResponse!) in
                print("Error response + \(String(describing: response))")
            })
        }
        
    }
    
    
    @objc func refreshCallTime(_ sender: Timer) {
        timeDuration += kRefreshTimeInterval!
        // let extraTitle = " "
        locallablel.text =  gowithTimeDuration(timeDuration:timeDuration)
    }
    
    func gowithTimeDuration( timeDuration: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [ .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]
        let formattedDuration = formatter.string(from: timeDuration)
        return formattedDuration!
    }
    
    func videoTurnOnNotificationReceived() {
        print("videoTurnOnNotificationReceived")
        //let msg = "Dr. \(doctorname) is requesting for your video."
        let msg = "Doctor has requested for video"
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DENY", style: .default, handler: { (action) in
            //self.showCOTConfirmationAlert()
            //VIDEO_ALLOW_CODE = 101;
            self.createCallBackEvent(withCode: 102)
        }))
        alert.addAction(UIAlertAction(title: "ACCEPT", style: .default, handler: { (action) in
            //self.showCOTConfirmationAlert()
            //VIDEO_REJECT_CODE = 102;
            for subView in self.opponentVideoView.subviews {
                if subView != self.opponentVideoView.subviews.first {
                    subView.removeFromSuperview()
                }
            }
            self.topView.removeFromSuperview()
            
            for layer in self.localVideoView.layer.sublayers! {
                layer.removeFromSuperlayer()
            }
            
            self.session?.localMediaStream.videoTrack.videoCapture = self.videoCapture
            self.videoCapture!.previewLayer.frame = self.localVideoView.bounds
            self.localVideoView.layer.addSublayer(self.videoCapture!.previewLayer)
            
            self.session?.remoteVideoTrack(withUserID: NSNumber(value: self.callingnumber)).isEnabled = true
            self.session?.localMediaStream.videoTrack.isEnabled = true
            
            //self.toolBar.buttons[1].isHidden = false
            //self.addVideoIconToToolBar()
            
            self.createCallBackEvent(withCode: 101)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func createCallBackEvent(withCode: Int) {
        
        let payload = NSMutableDictionary()
        let opponentsIDs = (self.session?.opponentsIDs as NSArray?)?.componentsJoined(by: ",")
        
        payload.setValue("Video turn on Response", forKey: "title")
        payload.setValue(withCode, forKey: "code")
        payload.setValue("5", forKey: "ios_badge")
        payload.setValue("mysound.wav", forKey: "ios_sound")
        payload.setValue(String(Int((self.session?.currentUserID) ?? 0)), forKey: "user_id")
        payload.setValue("10", forKey: "thread_id")
        
        
        
        let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
        
        let message = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = opponentsIDs
        event.type = QBMEventType.oneShot
        event.message = message
        
        QBRequest.createEvent(event, successBlock: { (_, _) in
            print("EVENT CREATED SUCCESSFULLY")
        }) { (error:QBResponse?) in
            print("ERROR: \(String(describing: error))")
        }
    }

}

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}

/* extension CallViewControllerswft: UIApplicationDelegate, UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let userinfo = userInfo
        print("\(userinfo)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //
    }
    
    //
    //
    //    // This method will be called when app received push notifications in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
}
*/
