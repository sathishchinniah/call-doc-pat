import UIKit

class InCallViewController: UIViewController, QBRTCClientDelegate {

    @IBOutlet weak var callStatusLabel: UILabel!
    
    var nav: UINavigationController?
    weak var dialignTimer: Timer?
    var session: QBRTCSession?
    var doctorID = UInt()
    var userID = UInt()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let localuser = QBUUser()
        localuser.id = (session?.initiatorID as? UInt) ?? (0 as UInt)
        QBRequest.user(withID: localuser.id, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
            let calltype: String = self.session?.conferenceType == QBRTCConferenceType.video ? "Video call from:  " : "Audio call from: "
            self.callStatusLabel.text = "\(calltype)  \(user.fullName ?? "Doctor")"
            self.doctorID = user.id
            self.userID = localuser.id
            
        }, errorBlock: {(_ response: QBResponse) -> Void in
            print("IncomingCallViewController: QBRequest() Error")
        })
        
        
        QMSoundManager.playRingtoneSound()
        dialignTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(), target: self, selector: #selector(self.dialing), userInfo: nil, repeats: true)
    }


    @objc func dialing(_ timer: Timer) {
        QMSoundManager.playRingtoneSound()
    }
    
    @IBAction func AcceptCall(_ sender: UIButton!) {
        dialignTimer?.invalidate()
        dialignTimer = nil
        
        QMSoundManager.instance().stopAllSounds()
        
        guard let inCall = self.storyboard?.instantiateViewController(withIdentifier: "ReceiveCall") as? CallViewIncomingCallController else { fatalError("Error")
        }
        inCall.userID = self.userID
        inCall.doctorID = self.doctorID
        self.nav = UINavigationController(rootViewController: inCall)
        self.present(self.nav!, animated: false, completion: nil)
        
    }
    
    @IBAction func RejectCall(_ sender: UIButton!) {
        self.cleanUp()
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    
    func cleanUp() {
        dialignTimer?.invalidate()
        dialignTimer = nil
        QBRTCClient.instance().remove(self)
        QMSoundManager.instance().stopAllSounds()
    }

    func sessionDidClose(_ session: QBRTCSession) {
        if self.session == session {
            print("IncomingCallViewController: sessionDidClose()")
            cleanUp()
        }
    }

}
