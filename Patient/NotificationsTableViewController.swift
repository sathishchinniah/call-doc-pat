import UIKit

struct cellData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class NotificationsTableViewController: UITableViewController {

    var nav: UINavigationController?
    var tableViewData = [cellData]()
    let expandedCellHeight: CGFloat = 150
    let collapsedCellHeight: CGFloat = 80
    var selectedIndex = Int.max

    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewData = [cellData(opened: false, title: "Title", sectionData: ["cell1"])]

        let navBarColor = navigationController!.navigationBar
        navBarColor.topItem?.title = "NOTIFICATIONS"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }

    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationArray.count  //20
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedIndex {
            return expandedCellHeight
        }
        return collapsedCellHeight
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell) ?? NotificationCell()
        cell.selectionStyle = .none
        //loading profile image
        QBRequest.downloadFile(withUID: ((NotificationArray[indexPath.row].fields?.value(forKey: "profileUID") as? String)!), successBlock: { (response : QBResponse, imgData: Data)in
            let oImage = UIImage(data: imgData)
            if oImage != nil {
                cell.docImageView.image = oImage
            } else {
                cell.docImageView.image =  UIImage(named: "profile")
                print("*****NotificationTableViewController:LoadProfileImage() Image Load Failure")
            }
        }, statusBlock: nil, errorBlock: nil)

        cell.docNameLabel.text = "Dr. " + ((NotificationArray[indexPath.row].fields?.value(forKey: "invitorfullname") as? String)!)

        //date and time
        let epochtime =  NotificationArray[indexPath.row].createdAt
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat =  "hh:mm a | dd MMM YYYY"    //"MMM dd YYYY hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: epochtime!)
        cell.docDetailsLabel.text = dateString

        cell.notificationlabel.text = "Connect with this Doctor"

        let accepted = ((NotificationArray[indexPath.row].fields?.value(forKey: "accepted") as? Bool) ?? false)
        if accepted {
            cell.addDocSwitch.isOn = true
            cell.addDocSwitch.isEnabled = false
        }

        cell.addDocSwitch.addTarget(self, action: #selector(switchTriggered), for: .valueChanged)
        cell.addDocSwitch.tag = indexPath.row

        return cell
    }
    
    @objc func switchTriggered(sender: UISwitch) {
        if sender.isOn {
            sender.isOn = true
            sender.isEnabled = false
            // here update Invite table with acceped = true and create entry in InviteTablewith invitee = doctor and invitor = patient and typeofinvite = ACK
            self.updateaccepttotrueInviteTable(indexval: sender.tag)
            self.createEntryinRelDocPatTable(indexval: sender.tag)
        }
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row

        let cellFrame = tableView.rectForRow(at: IndexPath(item: selectedIndex, section: 0))
        if cellFrame.size.height == expandedCellHeight {
            selectedIndex = Int.max
        }

        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        self.tableView.scrollToRow(at: indexPath, at: .none, animated: true)
    }

    
    func updateaccepttotrueInviteTable(indexval: Int) {
        let object = QBCOCustomObject()
        object.className = "InviteTable"
        object.fields["accepted"] =  true
        object.fields["notificationread"] = true
        object.id = NotificationArray[indexval].id      //record id of Doctor Primary details table
        
        QBRequest.update(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController:: updateaccepttotrueInviteTable() Successfully Updated!")
            
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:: updateaccepttotrueInviteTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
    
    
    func CreateEntryInviteTableforACK(indexval: Int) {
        print("NotificationTableViewController: CreateEntryInviteTableforACK(): Creating new entry on InviteTable")
        let object = QBCOCustomObject()
        object.className = "InviteTable"
        let invitoruserid = ((NotificationArray[indexval].fields?.value(forKey: "invitoruserid") as? Int)!)
        object.fields["inviteeuserid"] = invitoruserid
        object.fields["invitoruserid"] = QBSession.current.currentUser?.id
        object.fields["typeofinvite"] = "ACK"
        object.fields["invitorfullname"] = QBSession.current.currentUser?.fullName
        object.fields["notificationread"] = "true"
        object.fields["accepted"] = "true"
        object.fields["calldoccode"] = QBSession.current.currentUser?.customData
        object.fields["typeofinvite"] = "patient"
        
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController: CreateEntryInviteTableforACK(): Successfully created ACK entry")
            // we should send Push notification to invittor
            self.sendPushToInvitor(invitoruserid: invitoruserid)
            
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:CreateEntryInviteTableforACK(): Response error: \(String(describing: response.error?.description))")
        }
        
    }
    
    func createEntryinRelDocPatTable(indexval: Int) {
        print("NotificationTableViewController: CreateEntryInviteTableforACK(): Creating new entry on InviteTable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = ((NotificationArray[indexval].fields?.value(forKey: "invitoruserid") as? Int)!)
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = QBSession.current.currentUser?.id
        object.fields["doctorfullname"] = ((NotificationArray[indexval].fields?.value(forKey: "invitorfullname") as? String)!)
        object.fields["lock"] = "true"
        object.fields["typeofinvite"] = "patient"
        object.fields["doctorspeciality"] = ((NotificationArray[indexval].fields?.value(forKey: "docspeciality") as? String)!)
        object.fields["doctorprofileimageURL"] = ((NotificationArray[indexval].fields?.value(forKey: "profileUID") as? String)!)
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("NotificationTableViewController: CreateEntryInviteTableforACK(): Successfully created ACK entry")
            
        }) { (response) in
            //Handle Error
            print("NotificationTableViewController:CreateEntryInviteTableforACK(): Response error: \(String(describing: response.error?.description))")
        }
    }

    func sendPushToInvitor(invitoruserid: Int) {
        let currentUserLogin = QBSession.current.currentUser!.fullName
        
        QBRequest.sendPush(withText: "\("Patient " + currentUserLogin! ) has accepted your Invitation", toUsers: String(invitoruserid), successBlock: { (response: QBResponse, event:[QBMEvent]?) in
            print("NotificationTableViewController:sendPushToInvitor(): Push sent!")
        }) { (error:QBError?) in
            print("NotificationTableViewController:sendPushToInvitor():Can not send push: \(error!))")
        }
    }
    
    func showReferenceAlert() {
        let msg = "Dr. Sameer (Cardiologist) would like to refer his patients to you and has invited you to be a part of his Circle of Trust."
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "DECLINE", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "ACCEPT", style: .default, handler: { (action) in
            self.showCOTConfirmationAlert()
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func showCOTConfirmationAlert() {
        let msg = "Include Dr. Sameer in your Circle of Trust and refer your patients to him?"
        let alert = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "SKIP", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
            //Perform API actions
        }))
        self.present(alert, animated: true, completion: nil)
    }

}
