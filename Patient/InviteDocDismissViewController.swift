import UIKit

class InviteDocDismissViewController: UIViewController {

    @IBOutlet weak var congratslabel: UILabel!
    
    var nav: UINavigationController?
    var specialitystring = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        congratslabel.text = "You have added " + specialitystring + " Successfully !!"
        let timer = Timer.scheduledTimer(timeInterval: 2.3, target: self, selector: #selector(timeToMoveOn), userInfo: nil, repeats: false)
        print(timer)
    }

    @objc func timeToMoveOn() {
        navigationController?.isNavigationBarHidden = false
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as?  TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
}
