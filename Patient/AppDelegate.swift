//
//  AppDelegate.swift
//  Patient
//
//  Created by Manish Verma on 05/06/18.
//  Copyright © 2018 calldoc. All rights reserved.
//





//if which swiftlint >/dev/null; then
//swiftlint
//else
//echo "warning: SwiftLint not installed, download from https://github.com/realm/SwiftLint"
//fi

import UIKit
import Fabric
import Crashlytics
import Firebase
import UserNotifications
import Toast_Swift

var gsession: QBRTCSession?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NotificationServiceDelegate, QBRTCClientDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var isLoggedin = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {

        application.applicationIconBadgeNumber = 0
        window?.backgroundColor = UIColor.white;
        // Set QuickBlox credentials (You must create application in admin.quickblox.com).
        QBSettings.applicationID = 5
        QBSettings.authKey = "kQhRpZfJMwseN24"
        QBSettings.authSecret = "4MNHHZg9N3NRXKb"
        QBSettings.accountKey = "VAA3hEMsuMmBtg34kWg4"
        // enabling carbons for chat
        QBSettings.carbonsEnabled = true
        // Enables Quickblox REST API calls debug console output.
        QBSettings.logLevel = .nothing

        // Enables detailed XMPP logging in console output.
        // QBSettings.enableXMPPLogging()

        gsession = nil
        QBRTCClient.initializeRTC()

        QBSettings.apiEndpoint = "https://api.calldoc.com"
        QBSettings.chatEndpoint = "chat.calldoc.com"
        
        FirebaseApp.configure()
        
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
        
        // Intialized fabric
        Fabric.with([Crashlytics.self()])
        
        registerForRemoteNotification()
        // set the delegate in didFinishLaunchingWithOptions
        UNUserNotificationCenter.current().delegate = self
        
        // app was launched from push notification, handling it
        let remoteNotification: NSDictionary! = launchOptions?[.remoteNotification] as? NSDictionary
        if (remoteNotification != nil) {
            ServicesManager.instance().notificationService.pushDialogID = remoteNotification["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        isLoggedin = Bool(UserDefaults.standard.bool(forKey: "isLoggedIn") )
        if isLoggedin {
            print("AppDelegate:Already Loggedin successfully before")
            let VC = storyboard.instantiateViewController(withIdentifier: "HomeNavController") as? UINavigationController
            window?.rootViewController = VC
            return true
        }
        
        var ovc: UIViewController
        
        if (UserDefaults.standard.value(forKey: "OnBoard")as? String) == nil {
            // show the onboarding screen
            ovc = (storyboard.instantiateViewController(withIdentifier: "OnBoardVC") as? UIViewController)!
        } else {
            // show main screen
            ovc = storyboard.instantiateInitialViewController()!
        }
        
        window?.rootViewController = ovc
        
        return true
    }
    
    func registerForRemoteNotification() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: ([.sound, .alert, .badge]), completionHandler: {(_ granted: Bool, _ error: Error?) -> Void in
                DispatchQueue.main.async(execute: {() -> Void in
                    UIApplication.shared.registerForRemoteNotifications()
                })
                print("APPDELEGATE:registerForRemoteNotification granted: \(granted), error: \(error)")
            })
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }

    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        print("AppDelegate:didReceiveNewSession")
        if gsession == nil {
            gsession = session
            handleIncomingCall()
        }
    }
    
    func handleIncomingCall() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let VC = storyboard.instantiateViewController(withIdentifier: "InCallViewController") as? InCallViewController else { fatalError("Error") }
        VC.session = gsession
        self.window?.rootViewController =  VC
    }
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        print("AppDelegate:hungUpByUser")
        session.hangUp(nil)
        gsession = nil
        
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        let subscription = QBMSubscription()
        
        subscription.notificationChannel = .APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("DEVICE TOKEN: \(token)")
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            
        }) { (response: QBResponse!) -> Void in
            
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Push failed to register with error: %@", error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("Appdelegate:didReceiveRemoteNotification()")
        print("Appdelegate:didReceiveRemoteNotification my push is: %@", userInfo)
        guard application.applicationState == UIApplicationState.inactive else {
            return
        }
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String else {
            return
        }
        
        guard !dialogID.isEmpty else {
            return
        }
        
        
        let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
        if dialogWithIDWasEntered == dialogID {
            return
        }
        
        ServicesManager.instance().notificationService.pushDialogID = dialogID
        
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //code
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Logging in to chat.
        ServicesManager.instance().chatService.connect(completionBlock: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Logging out from chat.
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
        
        if paymentCompleted == false {
            DispatchQueue.main.async {
                QBRequest.deleteObject(withID: currentConsultID, className: "ConsultsTable", successBlock: { (response) in
                    print("DELETION SUCCESSFUL")
                }) { (errorResponse) in
                    print("Response error: \(String(describing: errorResponse.error?.description))")
                }
                if teamDocConsultID != "" {
                    QBRequest.deleteObject(withID: teamDocConsultID, className: "TeamConsultsTable", successBlock: { (response) in
                        print("DELETION SUCCESSFUL")
                    }) { (errorResponse) in
                        print("Response error: \(String(describing: errorResponse.error?.description))")
                    }
                }
                
            }
        }
    }
    
    // MARK: NotificationServiceDelegate protocol
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        let navigatonController: UINavigationController! = self.window?.rootViewController as? UINavigationController
        let chatController: ChatViewController = (UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController) ?? ChatViewController()
        chatController.dialog = chatDialog
        
        let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
        if !dialogWithIDWasEntered.isEmpty {
            // some chat already opened, return to dialogs view controller first
            navigatonController.popViewController(animated: false);
        }
        
        navigatonController.pushViewController(chatController, animated: true)
    }
    
    func notificationServiceDidStartLoadingDialogFromServer() {
        print("AppDelegate:notificationServiceDidStartLoadingDialogFromServer")
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
        print("AppDelegate:notificationServiceDidFinishLoadingDialogFromServer")
    }
    
    func notificationServiceDidFailFetchingDialog() {
        print("AppDelegate:notificationServiceDidFailFetchingDialog")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print(notification.request.content.userInfo)
        if (notification.request.content.userInfo["code"] as? Int) == 100 {
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "makeCall") as? CallViewControllerswft
//            vc?.videoTurnOnNotificationReceived()
//            let vc = self.window?.rootViewController as? CallViewControllerswft
//            vc?.videoTurnOnNotificationReceived()
            if ((self.window?.rootViewController as? UINavigationController)?.visibleViewController) is CallViewControllerswft {
                let vc = ((self.window?.rootViewController as? UINavigationController)?.visibleViewController) as? CallViewControllerswft
                vc?.videoTurnOnNotificationReceived()
            } else {
                let vc = CallViewControllerswft()
                vc.videoTurnOnNotificationReceived()
            }
        }
        //notification.request.content.userInfo["code"]!
        //(self.window?.rootViewController as! UINavigationController).visibleViewController
        //((self.window?.rootViewController as! UINavigationController).visibleViewController) is CallViewControllerswft
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //code
         print(response.notification.request.content.userInfo)
    }
    
    
    
}
