import UIKit
import Crashlytics

var tempdoclist = ["Dr. Kirshore", "Dr. Sujata Mishra","Dr. Ravi Mehta"]
var tempspecialisationlist = ["General Medicine","Internal Medicine","Internal Medicine"]
var docimageNames = ["doctor1","doctor4","doctor3", "doctor2"]
var tempagelist = ["22","48",""]
var gindexval: Int = 0
var isSlideMenuHidden = true
var glastVC = ""
var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
var ipad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
var width:CGFloat { return UIScreen.main.bounds.size.width }
var height:CGFloat { return UIScreen.main.bounds.size.height }
var maxLength:CGFloat { return max(width, height) }
var DoctorsArray : [QBCOCustomObject] = []
var selectedUserDoctorsArray : [QBCOCustomObject] = []
var selectedUserDoctorNames : [String] = []
var NotificationArray : [QBCOCustomObject] = []
let imageNames = ["patient1","patient2","patient3","patient4"]
var patientNames = [GlobalDocdata.patientname]
var patientcdcode = [String]()
var patientimageurl = [String]()
var patientgender = [String]()
var patientage = [String]()
var patrelation = [String]()
var patientemail = [String]()
var iscameasexistinguser = false
var istoastbar = false


struct GlobalVariables {
    static var globalString = "MyString"
    static var gCurrentUser = QBUUser()
    static var gcountrycode = ""
    static var gcurrentprofilename = ""
    static var gcurrentprofilerelation = ""
    static var gcurrentprofileURL = ""
    static var gcurrentprofilegender = ""
    static var gcurrentprofileage = ""
    static var gcurrentprofilecalldoccode = ""
    static var gcurrentprofilecommand = ""
    static var gcurrentdocfee = 0
}

class TheHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, QBRTCClientDelegate, QBChatDelegate, FSPagerViewDataSource, FSPagerViewDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bottomBarView: UIView!
    @IBOutlet var optionspatientnamelb: UILabel!
    @IBOutlet weak var OptionsView: UIView!
    @IBOutlet weak var nodoctorslb: UILabel!
    @IBOutlet weak var OptionsViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var Optionsprofileimg: UIImageView!{
        didSet {
            Optionsprofileimg.layer.cornerRadius = Optionsprofileimg.frame.size.width / 2;
            Optionsprofileimg.clipsToBounds = true;
            Optionsprofileimg.layer.borderWidth = 0.3
            Optionsprofileimg.layer.borderColor = UIColor.gray.cgColor
        }
    }

    @IBOutlet  var ViewbehindOptionsView: UIView!
    @IBOutlet  var ViewbehildoptionsButtonView: UIButton!
    @IBOutlet weak var rightsideArchwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftsideArchwidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightsideplusoffsetConstraint: NSLayoutConstraint!
    @IBOutlet weak var doctable: UITableView!
    
    
    var doctorResumeConsult = true
    var doctorSchedularStatus = true
    
    
    
    let doctorListCell_ReuseIdentifier = "DoctorListForMemberCell"
    var myindex = 0
    var badgeCount = Int()
    var nav: UINavigationController?
    var pagerviewindex = 0
    
    fileprivate let transformerNames = ["linear"]
    fileprivate let transformerTypes: [FSPagerViewTransformerType] = [.linear]
    fileprivate var typeIndex = 4 {
        didSet {
            let type = self.transformerTypes[0]
            self.pageView.transformer = FSPagerViewTransformer(type:type)
            self.pageView.removesInfiniteLoopForSingleItem = true
            switch type {
            case .linear:
                if phone && maxLength == 812 {  // iphone X
                    let transform = CGAffineTransform(scaleX: 0.25, y: 0.35)
                    self.pageView.itemSize = self.pageView.frame.size.applying(transform)
                } else if phone && maxLength == 736 { // iphone plus
                    let transform = CGAffineTransform(scaleX: 0.25, y: 0.35)
                    self.pageView.itemSize = self.pageView.frame.size.applying(transform)
                    
                } else {
                    let transform = CGAffineTransform(scaleX: 0.25, y: 0.35)
                    self.pageView.itemSize = self.pageView.frame.size.applying(transform)
                }
            case .crossFading:
                print("non")
            case .zoomOut:
                print("non")
            case .depth:
                print("non")
            case .overlap:
                print("non")
            case .coverFlow:
                print("non")
            case .ferrisWheel:
                print("non")
            case .invertedFerrisWheel:
                print("non")
            case .cubic:
                print("non")
            }
        }
    }
    
    static let defaultArray = [false, false, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false]
    
    var mondayArrayOfBools = defaultArray
    var tuesdayArrayOfBools = defaultArray
    var wednesdayArrayOfBools = defaultArray
    var thursdayArrayOfBools = defaultArray
    var fridayArrayOfBools = defaultArray
    var saturdayArrayOfBools = defaultArray
    var sundayArrayOfBools = defaultArray
    var currentlyOnline = false
    
    @IBOutlet weak var pageView: FSPagerView!{
        didSet {
            self.pageView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.typeIndex = 4
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gsession = nil
        bottomBarView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)
        self.ReadAllNotifications()
        self.ReadNotificationCounts()
        self.badgeCount = 11
        
        patientimageurl.append(GlobalDocdata.gprofileURL)
        
        
        //self.navigationController?.visibleViewController = self
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()

        let pageControl:FSPageControl
        pageControl = FSPageControl()
        pageControl.numberOfPages = 3
        pageControl.contentHorizontalAlignment = .center
        pageView.itemSize = CGSize(width: 80, height: 80)
        pageView.numberOfSections = 0

        headerView.layer.backgroundColor = UIColor.white.cgColor
        headerView.layer.shadowOffset = CGSize(width: 0, height: 4)
        headerView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.14).cgColor
        headerView.layer.shadowOpacity = 1
        headerView.layer.shadowRadius = 1

        bottomBarView.layer.backgroundColor = UIColor.white.cgColor
        bottomBarView.layer.shadowOffset = CGSize(width: 0, height: -2)
        bottomBarView.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.14).cgColor
        bottomBarView.layer.shadowOpacity = 1
        bottomBarView.layer.shadowRadius = 1

        if phone && maxLength == 667 { // iphone 6
            rightsideArchwidthConstraint.constant = 100
            leftsideArchwidthConstraint.constant = 100
            rightsideplusoffsetConstraint.constant = 22
        }
        
        if phone && maxLength == 736 { // iphone 6/7/8 plus
            rightsideArchwidthConstraint.constant = 120
            leftsideArchwidthConstraint.constant = 120
            rightsideplusoffsetConstraint.constant = 40
        }
        
        if phone && maxLength == 812 {
            //iphone x
            rightsideArchwidthConstraint.constant = 100
            leftsideArchwidthConstraint.constant = 100
            rightsideplusoffsetConstraint.constant = 25
        }

        alignoptionsview()
        
        let isexistinguser = UserDefaults.standard.object(forKey: "existinguser") as! Bool
        if(isexistinguser) {
            iscameasexistinguser = true
        }
        
        let isLoggedin = UserDefaults.standard.object(forKey: "isLoggedIn") as! Bool
        if(isLoggedin) {
            VerifiedUserLoginNow()
        } else {
            //video specific
            QBRTCClient.initializeRTC()
            QBRTCClient.instance().add(self)
        }
        getAllDoctorsforDefaultPatient()
        //getDoctorsForSelectedPatient(patID: (QBSession.current.currentUser?.id)!)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "CallDoc For"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        
        //self.getUserdatafromBackend()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getUserdatafromBackend()
        //self.doctable.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = ""
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pagerviewindex = targetIndex
        print("PATIENT NAME: \(patientNames[targetIndex])")
        print("TARGET INDEX: \(targetIndex)")
        print("PATIENT CODE: \(patientcdcode[targetIndex])")
        self.getConsultsForPat(patCDCode: patientcdcode[targetIndex])
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if patientimageurl[index].isEmpty {
            cell.imageView?.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "profile"))
        } else {
            GlobalDocdata.gprofileURL = patientimageurl[0]
            cell.imageView?.sd_setImage(with: URL(string: patientimageurl[index]), placeholderImage: UIImage(named: "profile"))
        }
        
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        cell.textLabel?.text = patientNames[index]
        //self.pagerviewindex = index
        
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
        //self.doctable.reloadData()
        return cell
    }
    
    func getConsultsForPat(patCDCode: String) {
        //let patientCode = patientcdcode[pagerviewindex]
        let getRequest = NSMutableDictionary()
        //getRequest["user_id"] = (DoctorsArray[myindex].fields?.value(forKey: "doctoruserid") as? UInt) ?? (0 as UInt)
        //if patientcdcode.count > gindexval {
            getRequest["patcalldoccode"] =  patCDCode//patientcdcode[gindexval]
        //}
        
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            //selectedUserDoctorsArray.removeAll(keepingCapacity: false)
            //selectedUserDoctorNames.removeAll(keepingCapacity: false)
            //selectedUserDoctorsArray = []
            selectedUserDoctorNames = []
            if ((contributors?.count)! > 0) {
                let selected = contributors!
                selectedUserDoctorsArray = selected //?? []
                
                for doctor in selectedUserDoctorsArray {
                    selectedUserDoctorNames.append((doctor.fields?.value(forKey: "docfullname") as? String) ?? "")
                }
              
                for data in selectedUserDoctorsArray/*selectedUserDoctorsArray*/ {
                   
                    for item in DoctorsArray {
                        
                        if (item.fields?.value(forKey: "doctorfullname") as? String) == data.fields?.value(forKey: "docfullname") as? String {
                            let element = DoctorsArray.remove(at: DoctorsArray.index(of: item)!)
                            DoctorsArray.insert(element, at: 0)
                            //                            let indexPath = DoctorsArray.index(of: element) as? IndexPath
                            //                            self.doctable.reloadRows(at: [indexPath!], with: .automatic)
                        }
                        
                        //self.doctable.reloadData()
                        
                    }
                    
                }
                self.doctable.reloadData()

            } else {
                //selectedUserDoctorsArray.removeAll()
                selectedUserDoctorsArray = []
                self.doctable.reloadData()
//                PatientConsultsdetailsArray = contributors!
//                self.ConsultType = "NEW"
//                self.speakNowButtonView.isEnabled = true
            }
            
        }) { (response) in
            //Error handling
            print("DocConsultViewController:DocConsultReadyConsultsDetails: Response error: \(String(describing: response.error?.description))")
        }
        
        
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pagerviewindex = index
        gindexval = self.pagerviewindex
        if index > patientcdcode.count {
            self.getConsultsForPat(patCDCode: patientcdcode[index])
        }
        print("TheHomeViewController:didSelectItemAt: \(self.pagerviewindex)")
    }
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return patientNames.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if DoctorsArray.count <= 0 {
            nodoctorslb.isHidden = false
        } else {
            nodoctorslb.isHidden = true
        }
        
        return DoctorsArray.count
    }

    public func tableView(_ doctable: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: doctorListCell_ReuseIdentifier) as! DoctorListForMemberTableViewCell

        cell.dataContainerView.layer.cornerRadius = 45;
        cell.dataContainerView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.dataContainerView.layer.shadowOpacity = 0.5
        cell.dataContainerView.layer.shadowRadius = 3.0
        cell.dataContainerView.layer.shadowOffset = .zero
        cell.dataContainerView.layer.masksToBounds = true
        
        cell.dataContainerView.layer.borderColor = UIColor.lightGray.cgColor
        cell.dataContainerView.layer.borderWidth = 0.5
        cell.dataContainerView.backgroundColor = UIColor(red: 240.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)

        if selectedUserDoctorNames.contains(((DoctorsArray[indexPath.section].fields?.value(forKey: "doctorfullname")) as? String) ?? "") {
            cell.dataContainerView.backgroundColor = UIColor.white
        } else {
            cell.dataContainerView.backgroundColor = UIColor(red: 240.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        }
        cell.dataContainerView.layer.borderColor = UIColor.lightGray.cgColor
        cell.dataContainerView.layer.borderWidth = 0.5
        //cell.dataContainerView.backgroundColor = UIColor(red: 240.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        
        cell.ImageViewContainer.layer.cornerRadius = cell.ImageViewContainer.frame.size.width / 2;
        cell.ImageViewContainer.clipsToBounds = true;
        cell.ImageViewContainer.layer.borderWidth = 2.0
        let docID = ((DoctorsArray[indexPath.section].fields?.value(forKey: "doctoruserid")) as? UInt) ?? 0
        self.configureDoctorListForMemberTableViewCell(cell: cell, docID: docID)
        
//        cell.ImageViewContainer.layer.borderColor = UIColor.init(red: 0/255, green: 177/255, blue: 100/255, alpha: 1).cgColor

        cell.ImageView?.layer.cornerRadius = ((cell.ImageView?.frame.size.width) ?? 0) / 2
        cell.ImageView.clipsToBounds = true
        cell.ImageView.layer.borderWidth = 3.0
        cell.ImageView.layer.borderColor = UIColor.white.cgColor

        var profileurl = (DoctorsArray[indexPath.section].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
        if (profileurl == nil || profileurl.isEmpty) {
            profileurl = ""
        }
        cell.ImageView.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)
        //cell.dataContainerView.backgroundColor = UIColor.white
        if let fullname = (DoctorsArray[indexPath.section].fields?.value(forKey: "doctorfullname") as? String), let speciality = (DoctorsArray[indexPath.section].fields?.value(forKey: "doctorspeciality") as? String) {
            cell.nameLabel.text = "Dr " + fullname
            cell.designationLabel.text =  speciality
        }
        
        if (cell.designationLabel.text == "ent") || (cell.designationLabel.text == "gp") {
            cell.designationLabel.text = cell.designationLabel.text?.uppercased()
        } else {
            cell.designationLabel.text = cell.designationLabel.text?.capitalized
        }
        cell.BadgeLabel.text = ""
        cell.BadgeLabel.isHidden = true
        cell.cellButton.tag = indexPath.section
        cell.cellButton.addTarget(self,action:#selector(DocConsultAction), for:.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myindex = indexPath.row
    }
    
    func configureDoctorListForMemberTableViewCell(cell: DoctorListForMemberTableViewCell, docID: UInt) {
        self.getSchedularDataArrayfromBackend(doctorId: docID) { (success) in
            if success {
                
                let dayOfTheWeek = Calendar.current.component(.weekday, from: Date())
                
                var currentDayArray = [Bool]()
                if dayOfTheWeek == 1 {
                    //sunday
                    currentDayArray = self.sundayArrayOfBools
                }
                
                if dayOfTheWeek == 2 {
                    //monday
                   currentDayArray = self.mondayArrayOfBools
                }
                
                if dayOfTheWeek == 3 {
                    //tuesday
                    currentDayArray = self.tuesdayArrayOfBools
                    
                }
                
                if dayOfTheWeek == 4 {
                    //wednesday
                    currentDayArray = self.wednesdayArrayOfBools
                }
                
                if dayOfTheWeek == 5 {
                    //thursday
                    currentDayArray = self.thursdayArrayOfBools
                }
                
                if dayOfTheWeek == 6 {
                    //friday
                    currentDayArray = self.fridayArrayOfBools
                }
                
                if dayOfTheWeek == 7 {
                    //saturday
                    currentDayArray = self.saturdayArrayOfBools
                }
                let currentStatus = self.getCurrentOnlineOfflineStatus(currentDayBoolArray: currentDayArray)
                
                if currentStatus {
                    cell.ImageViewContainer.layer.borderColor = greenProfileBorder
                    if self.doctorResumeConsult == false {
                        cell.ImageViewContainer.layer.borderColor = orangeProfileBorder
                    }
                    if self.doctorSchedularStatus == false {
                        cell.ImageViewContainer.layer.borderColor = redProfileBorder
                    }
                } else {
                    cell.ImageViewContainer.layer.borderColor = redProfileBorder
                }
                
            } else {
                cell.ImageViewContainer.layer.borderColor = redProfileBorder
            }
        }
    }
    
    func getCurrentOnlineOfflineStatus(currentDayBoolArray: [Bool]) -> Bool {
        let todayDate = Date()
        let hour = Calendar.current.component(.hour, from: todayDate)
        let minute = Calendar.current.component(.minute, from: todayDate) > 30 ? 0.5 : 0.0
        let currentTime = Double(hour) + minute
        let returnValue = currentDayBoolArray[Int(currentTime * 2)]
        return returnValue
    }
    
    func showAlert(msg: String) {
        let cntrl = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        cntrl.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(cntrl, animated: true, completion: nil)
    }
    
    // This is Notification Bell and help button on top bar
    func setUpBadgeCountAndBarButton() {
        // badge label
        let label = UILabel(frame: CGRect(x: 16, y: -09, width: 15, height: 15))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 1
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.textColor = .white
        label.font = label.font.withSize(8)
        label.backgroundColor = .clear//.black
        label.text = "\(self.badgeCount)"
        
        // Bell Bar button
        let rightBellButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightBellButton.setBackgroundImage(UIImage(named: "bellicon"), for: .normal)
        rightBellButton.addTarget(self, action: #selector(didTapBellButton), for: .touchUpInside)
        rightBellButton.addSubview(label)

        // Bar button item
        let rightBarBellButtomItem = UIBarButtonItem(customView: rightBellButton)

        // Help Bar button
        let rightHelpButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        rightHelpButton.setBackgroundImage(UIImage(named: "helpblk"), for: .normal)
        rightHelpButton.addTarget(self, action: #selector(didTapHelpButton), for: .touchUpInside)

        // Bar button item
        let rightBarHelpButtomItem = UIBarButtonItem(customView: rightHelpButton)
        navigationItem.rightBarButtonItems = [rightBarHelpButtomItem, rightBarBellButtomItem]
    }
    
    @objc func didTapHelpButton(sender: UIBarButtonItem) {
        print("didTapHelpButton Bar icon pressed")
    }
    
    @objc func didTapBellButton(sender: UIBarButtonItem) {
        print("didTapBellButton Bar icon pressed")
        let notificationbellViewController = self.storyboard!.instantiateViewController(withIdentifier: "NotificationsView") as! NotificationsTableViewController
        self.nav = UINavigationController(rootViewController: notificationbellViewController)
        self.present(self.nav!, animated: true, completion: nil)
    }
    
    func ReadNotificationCounts() {
        let getRequest = NSMutableDictionary()
        getRequest["inviteeuserid"] = QBSession.current.currentUser?.id
        getRequest["notificationread"] = "false"
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(TheHomeViewController:ReadNotificationCounts(): Successfully  !")
            if ((contributors?.count)! > 0) {
                self.badgeCount = (contributors?.count)!
                self.setUpBadgeCountAndBarButton()
            } else {
                self.badgeCount = (contributors?.count)!
                self.setUpBadgeCountAndBarButton()
            }
            
        }) { (errorresponse) in
            print("TheHomeViewController:ReadNotificationCounts() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func ReadAllNotifications() {
        let getRequest = NSMutableDictionary()
        getRequest["inviteeuserid"] = QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "InviteTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(TheHomeViewController:ReadAllNotifications(): Successfully  !")
            if ((contributors?.count)! > 0) {
                NotificationArray = contributors!
            } else {
                NotificationArray = contributors!
            }
        }) { (errorresponse) in
            print("TheHomeViewController:ReadAllNotifications() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    @objc func DocConsultAction(sender: UIButton) {
        print(sender.tag)
        print("TheHomeViewController: myIndex = \(sender.tag)")
        myindex = sender.tag
        let VC1 = DocConsultViewController.storyboardInstance()
        VC1.myindexval = myindex
        VC1.currentPatientName = patientNames[pagerviewindex]
        VC1.pagerIndexValue = pagerviewindex
        VC1.sourceView = "HOME"
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    func alignoptionsview() {
        if phone && maxLength == 568 { // iphone 5S
            OptionsViewConstraint.constant = -253
            isSlideMenuHidden = true
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
        }

        if phone && maxLength == 667 {
            //iphone 6
            OptionsViewConstraint.constant = -296.5
            isSlideMenuHidden = true
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
        }

        if phone && maxLength == 736 {
            //iphone 6 plus
            OptionsViewConstraint.constant = -327
            isSlideMenuHidden = true
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
        }

        if phone && maxLength == 812 {
            //iphone X
            OptionsViewConstraint.constant = -296.67
            isSlideMenuHidden = true
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
        }

        if ipad && maxLength == 1024 {
            //iPad
            OptionsViewConstraint.constant = -640.0
            isSlideMenuHidden = true
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
        }
    }
    
    @IBAction func OptionsClicked(_ sender: UIBarButtonItem) {
        GlobalDocdata.gprofileURL =  patientimageurl[self.pagerviewindex]
        self.Optionsprofileimg.sd_setImage(with: URL(string: GlobalDocdata.gprofileURL), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        optionspatientnamelb.text = patientNames[pagerviewindex]
        Optionsprofileimg.layer.cornerRadius = Optionsprofileimg.frame.size.height / 2;
        Optionsprofileimg.clipsToBounds = true;
        if phone && maxLength == 568 { // iphone 5S
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = 0
                ViewbehindOptionsView.isHidden = false
                ViewbehildoptionsButtonView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = -253
                ViewbehindOptionsView.isHidden = true
                ViewbehildoptionsButtonView.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = 0
                ViewbehindOptionsView.isHidden = false
                ViewbehildoptionsButtonView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = -296.5
                ViewbehindOptionsView.isHidden = true
                ViewbehildoptionsButtonView.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = 0
                ViewbehindOptionsView.isHidden = false
                ViewbehildoptionsButtonView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = -327
                ViewbehindOptionsView.isHidden = true
                ViewbehildoptionsButtonView.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }

        if phone && maxLength == 812 {
            //iphone X
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = 0
                ViewbehindOptionsView.isHidden = false
                ViewbehildoptionsButtonView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = -296.67
                ViewbehindOptionsView.isHidden = true
                ViewbehildoptionsButtonView.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            if isSlideMenuHidden {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = 0
                ViewbehindOptionsView.isHidden = false
                ViewbehildoptionsButtonView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                isSlideMenuHidden = !isSlideMenuHidden
                OptionsViewConstraint.constant = -620.0
                ViewbehindOptionsView.isHidden = true
                ViewbehildoptionsButtonView.isHidden = true
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func ViewBehindOptionsvieButtonClicked(_ sender: UIButton) {
        
        if phone && maxLength == 568 {
            //iphone 5
            isSlideMenuHidden = true
            OptionsViewConstraint.constant = -253
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            isSlideMenuHidden = true
            OptionsViewConstraint.constant = -296.5
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            isSlideMenuHidden = true
            OptionsViewConstraint.constant = -327
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if phone && maxLength == 812 {
            //iphone X
            isSlideMenuHidden = true
            OptionsViewConstraint.constant = -296.67
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            isSlideMenuHidden = true
            OptionsViewConstraint.constant = -620.0
            ViewbehindOptionsView.isHidden = true
            ViewbehildoptionsButtonView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func VerifiedUserLoginNow() {
        let userlogin = UserDefaults.standard.object(forKey: "userlogin") as! String
        let userpwd = UserDefaults.standard.object(forKey: "userpwd") as! String
        
        QBRequest.logIn(withUserLogin: userlogin, password: userpwd,  successBlock: { (response, user) in
            print("TheHomeViewController:VerifiedUserLoginNow():Success Login uring login: \(response)")
            let globalloginUser = QBUUser()
            GlobalVariables.gCurrentUser = globalloginUser
            globalloginUser.id = (user.id)
            globalloginUser.login = userlogin
            globalloginUser.password = userpwd
            if !QBChat.instance.isConnected {
                QBChat.instance.connect(with: globalloginUser) { (err) in
                    if err != nil {
                        print(" QB Chat ERROR") // No error here
                        print(err.debugDescription.description)
                    }
                    print(" QB Chat Success")
                    GlobalDocdata.patientname = (QBSession.current.currentUser?.fullName) ?? ""
                    self.optionspatientnamelb.text = GlobalDocdata.patientname
                    self.getUserdatafromBackend()
                    self.VerifiedUserDone()
                    self.UpdateUserData()
                }
            } else {
                print("TheHomeViewController: Already connected to Chat")
                GlobalDocdata.patientname = (QBSession.current.currentUser?.fullName)!
                self.optionspatientnamelb.text = GlobalDocdata.patientname
                self.getUserdatafromBackend()
                self.VerifiedUserDone()
            }
        })
        { (errorResponse) in
            print("TheHomeViewController Login Error: \(errorResponse)")
        }
    }
    
    func UpdateUserData() {
        if (GlobalDocdata.firsttimeview) || (iscameasexistinguser) {
            let ver = versionBuild()
            let ver1 = ver.components(separatedBy: CharacterSet.decimalDigits.inverted)
                .joined()
            let version = "Ver" + ver1
            print("TheHomeViewController: UpdateUserData()- Updating  User data for first time")
            GlobalDocdata.firsttimeview = false
            iscameasexistinguser = false
            let updateParameters = QBUpdateUserParameters()
            updateParameters.tags =  ["Pat1","IOS", version]
            QBRequest.updateCurrentUser(updateParameters, successBlock: {(_ response: QBResponse, _ user: QBUUser) -> Void in
                // User updated successfully
                print("TheHomeViewController: User data Updated Success!!")
                //self.doctable.reloadData()
            }, errorBlock: {(_ response: QBResponse) -> Void in
                // Handle error
                print("TheHomeViewController: Error in User Update data")
            })
        }
    }
    
    func VerifiedUserDone() {
        //video specific
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
    }

    @IBAction func OptionsMyProfileButton(_ sender: UIButton) {
        GlobalDocdata.firsttimeview = false
        let docprimaryViewController = self.storyboard?.instantiateViewController(withIdentifier: "PrimaryDetails") as! PrimaryDetailsViewController
        present(docprimaryViewController, animated: true, completion: nil)
    }
    
    @IBAction func OptionsMyAccountsButton(_ sender: UIButton) {
        GlobalDocdata.firsttimeview = false
        let docprimaryViewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentDetails") as! PaymentDetailsViewController
        self.navigationController?.pushViewController(docprimaryViewController, animated: true)
//        present(docprimaryViewController, animated: true, completion: nil)
    }
    
    @IBAction func OptionsMyCallRecordButton(_ sender: UIButton) {
        //GlobalDocdata.firsttimeview = false
        let inviteFriendViewController:InviteFriendViewController = self.storyboard?.instantiateViewController(withIdentifier: "InviteFriendViewController") as! InviteFriendViewController
        self.navigationController?.pushViewController(inviteFriendViewController, animated: true)
    }
    
    @IBAction func CallDocCodeButton(_ sender: UIButton) {
        let calldoccodeviewcontroller:CalldocCodeViewController = self.storyboard?.instantiateViewController(withIdentifier: "CalldocCodeView") as! CalldocCodeViewController
        self.navigationController?.pushViewController(calldoccodeviewcontroller, animated: true)
    }
    
    @IBAction func AddProfileButton(_ sender: UIButton) {
        let addprofileViewController = self.storyboard!.instantiateViewController(withIdentifier: "AddProfileView") as! AddProfileViewController
        self.nav = UINavigationController(rootViewController: addprofileViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @IBAction func InviteDocButtonAction(_ sender: UIButton) {
        let invitedocViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddDoctorsView") as! AddDoctorsViewController
        present(invitedocViewController, animated: true, completion: nil)
    }

    @IBAction func myConsultsButtonClicked(_ sender: UIButton) {
        print("my consults")
        let allconsultsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AllConsultsView") as! AllConsultsViewController
        allconsultsViewController.consultsTableDoctorsList = selectedUserDoctorsArray
//selectedUserDoctorsArray
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(allconsultsViewController, animated: true)
        }
        
        
    }

    @IBAction func FAQButtonAction(_ sender:UIButton) {
        /*let message = "Hello Doc this is my Push Message!"
        var payload = [String: AnyObject]()
        var aps = [String: AnyObject]()
        aps[QBMPushMessageSoundKey] = "default" as AnyObject?
        aps[QBMPushMessageAlertKey] = message as AnyObject?
        payload[QBMPushMessageApsKey] = aps as AnyObject?
        let pushMessage = QBMPushMessage.init()
        pushMessage.payloadDict = NSMutableDictionary(dictionary: payload)
        let useid = DoctorsArray[0].id
        QBRequest.sendPush(pushMessage, toUsers: String(useid!) , successBlock: { response, event in
            // Successful response with event
            print("Successfully Push sent!")
        }, errorBlock: { error in
            print("Can not send push: \(String(describing: error))")
        }) */
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        GlobalDocdata.firsttimeview = true
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
//        UserDefaults.standard.synchronize()
       // UserDefaults.standard.setValue(false, forKey: "isLoggedIn")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var ovc: UIViewController
        
        // show the onboarding screen
        ovc = (storyboard.instantiateViewController(withIdentifier: "OnBoardVC") as? UIViewController)!
        present(ovc, animated: true, completion: nil)
        
    }
    
    // this function will load Patients on FSPage
    func getUserdatafromBackend() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = (GlobalVariables.gCurrentUser.id) as! UInt//QBSession.current.currentUser?.id
        print("CURRENT USERID: \(String(describing: QBSession.current.currentUser?.id)), \(GlobalVariables.gCurrentUser)")
        QBRequest.objects(withClassName: "PatPrimaryDetailsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(TheHomeViewController:getUserdatafromBackend(): Successfully  !")
            if ((contributors?.count)! > 0) {
                let receivedrecordcount = (contributors?.count)!
                patientimageurl.removeAll()
                patientcdcode.removeAll()
                patientNames.removeAll()
                patientgender.removeAll()
                patientage.removeAll()
                patrelation.removeAll()
                patientemail.removeAll()
                
                for i in 0..<receivedrecordcount {
                    patientimageurl.append((contributors![i].fields?.value(forKey: "profileimageURL") as? String) ?? "")
                    patientcdcode.append((contributors![i].fields?.value(forKey: "calldoccode") as? String) ?? "")
                    patientgender.append((contributors![i].fields?.value(forKey: "gender") as? String) ?? "")
                    patientage.append((contributors![i].fields?.value(forKey: "age") as? String) ?? "")
                    patrelation.append((contributors![i].fields?.value(forKey: "relation") as? String) ?? "")
                    patientemail.append((contributors![i].fields?.value(forKey: "email") as? String) ?? "")
                    let firstname = ((contributors![i].fields?.value(forKey: "firstname") as? String) ?? "")
                    let lastname = ((contributors![i].fields?.value(forKey: "lastname") as? String) ?? "")
                    let thename = firstname + " " + lastname
                    patientNames.append(thename)
                }
                self.getConsultsForPat(patCDCode: patientcdcode[0])
                //self.doctable.reloadData()
                self.pageView.reloadData()
            }
        }) { (errorresponse) in
            print("TheHomeViewController:getUserdatafromBackend(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func getAllDoctorsforDefaultPatient() {
        let getRequest = NSMutableDictionary()
        getRequest["patientuserid"] = QBSession.current.currentUser?.id//(GlobalVariables.gCurrentUser.id) //QBSession.current.currentUser?.id
        QBRequest.objects(withClassName: "RelDocPat", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(TheHomeViewController: getAllDoctorsforDefaultPatient(): Successfully  !")
            if ((contributors?.count)! > 0) {
                DoctorsArray = contributors!// all doctor for this account
               self.doctable.reloadData()
                
                // check profile selected, and get CallDocCode and check in ConsultsTable and get for unique entry using hash map
                // now compare this unique entries with DoctorsArray and get those entry
                
                self.getUserdatafromBackend()
                
                print("TheHomeViewController: getAllDoctorsforDefaultPatient():Got all the Doctors")
            }
        }) { (errorresponse) in
            print("TheHomeViewController:getAllDoctorsforDefaultPatient() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    
    
    func getDoctorsForSelectedPatient(patID: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["patientuserid"] = patID
    
        QBRequest.objects(withClassName: "ConsultsTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(TheHomeViewController: getAllDoctorsforDefaultPatient(): Successfully  !")
            if ((contributors?.count)! > 0) {
                selectedUserDoctorsArray.removeAll()
                selectedUserDoctorsArray = contributors! // all doctor for this account
                // check profile selected, and get CallDocCode and check in ConsultsTable and get for unique entry using hash map
                // now compare this unique entries with DoctorsArray and get those entry
                
                for item in DoctorsArray/*selectedUserDoctorsArray*/ {
//                    if item.userID == ((DoctorsArray[indexPath.section].fields?.value(forKey: "doctoruserid")) as? UInt) {
//                        cell.dataContainerView.backgroundColor = UIColor.white
//                    } else {
//                        cell.dataContainerView.layer.borderColor = UIColor.lightGray.cgColor
//                        cell.dataContainerView.layer.borderWidth = 0.5
//                        cell.dataContainerView.backgroundColor = UIColor(red: 240.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)
//                    }
                    for data in selectedUserDoctorsArray {
//                        if (item.fields?.value(forKey: "doctoruserid") as? UInt) == data.userID {
//                            let element = DoctorsArray.remove(at: DoctorsArray.index(of: item)!)
//                            DoctorsArray.insert(element, at: 0)
//                        }
                        
                        if (item.fields?.value(forKey: "doctorfullname") as? String) == data.fields?.value(forKey: "doctorfullname") as? String {
                            let element = DoctorsArray.remove(at: DoctorsArray.index(of: item)!)
                            DoctorsArray.insert(element, at: 0)
                        }
                    }
                    
                }
                self.doctable.reloadData()
                
                print("TheHomeViewController: getAllDoctorsforDefaultPatient():Got all the Doctors")
            }
        }) { (errorresponse) in
            print("TheHomeViewController:getAllDoctorsforDefaultPatient() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    //MARK: Helpers
    
    func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func build() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    func versionBuild() -> String {
        let version = self.appVersion()
        let build = self.build()
        var versionBuild = String(format:"v%@",version)
        if version != build {
            versionBuild = String(format:"%@(%@)", versionBuild, build )
        }
        return versionBuild
    }

}

//Extension to get the doctor schedule
extension TheHomeViewController {
    
    func getSchedularDatafromBackend(doctorId: UInt, completion: @escaping (Bool) -> ()) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctorId//QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //contributors![0].value(forKey: "resumeConsult")
            //contributors![0].fields.value(forKey: "schedularStatus")
            if ((contributors?.count)! > 0) {
                self.doctorResumeConsult = (contributors![0].fields.value(forKey: "resumeConsult") as? Bool) ?? false
                self.doctorSchedularStatus = (contributors![0].fields.value(forKey: "SchedularStatus") as? Bool) ?? false
            }
            completion(true)
            
        }) { (errorresponse) in
            print("HomeViewController: getSchedularDatafromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func getSchedularDataArrayfromBackend(doctorId: UInt, completion: @escaping (Bool) -> ()) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctorId//QBSession.current.currentUser?.id
        
        QBRequest.objects(withClassName: "DocSchedularDayView", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            
            if ((contributors?.count)! > 0) {
                self.mondayArrayOfBools = contributors![0].fields?.value(forKey: "Monday") as! [Bool]
                self.tuesdayArrayOfBools = contributors![0].fields?.value(forKey: "Tuesday") as! [Bool]
                self.wednesdayArrayOfBools = contributors![0].fields?.value(forKey: "Wednesday") as! [Bool]
                self.thursdayArrayOfBools = contributors![0].fields?.value(forKey: "Thursday") as! [Bool]
                self.fridayArrayOfBools = contributors![0].fields?.value(forKey: "Friday") as! [Bool]
                self.saturdayArrayOfBools = contributors![0].fields?.value(forKey: "Saturday") as! [Bool]
                self.sundayArrayOfBools = contributors![0].fields?.value(forKey: "Sunday") as! [Bool]
                
                UserDefaults.standard.set(self.mondayArrayOfBools, forKey: Constants.mondaySchedularData)
                UserDefaults.standard.set(self.tuesdayArrayOfBools, forKey: Constants.tuesdaySchedularData)
                UserDefaults.standard.set(self.wednesdayArrayOfBools, forKey: Constants.wednesdaySchedularData)
                UserDefaults.standard.set(self.thursdayArrayOfBools, forKey: Constants.thursdaySchedularData)
                UserDefaults.standard.set(self.fridayArrayOfBools, forKey: Constants.fridaySchedularData)
                UserDefaults.standard.set(self.saturdayArrayOfBools, forKey: Constants.saturdaySchedularData)
                UserDefaults.standard.set(self.sundayArrayOfBools, forKey: Constants.sundaySchedularData)
                
                self.doctorResumeConsult = (contributors![0].fields.value(forKey: "resumeConsult") as? Bool) ?? false
                self.doctorSchedularStatus = (contributors![0].fields.value(forKey: "SchedularStatus") as? Bool) ?? false
                
            }
            completion(true)
            
        }) { (errorresponse) in
            print("HomeViewController: getSchedularDatafromBackend() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
}
