#import "GData.h"
#import "IncomingCallViewController.h"
#import "QMSoundManager.h"

@interface IncomingCallViewController () <QBRTCClientDelegate>

@property (weak, nonatomic) IBOutlet UILabel *callStatusLabel;
@property (weak, nonatomic) IBOutlet UITextView *callInfoTextView;
@property (weak, nonatomic) NSTimer *dialignTimer;




@end

@implementation IncomingCallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataPointer = [GData sharedData];

    QBUUser *localuser = [QBUUser user];
    localuser.ID = self.session.initiatorID.integerValue;
    
    [QBRequest userWithID:localuser.ID successBlock:^(QBResponse *response, QBUUser *user)
     {
         self.dataPointer.incomingcallUserName = user.fullName;
         NSString *calltype = self.session.conferenceType == QBRTCConferenceTypeVideo ? @"Video call from:  " : @"Audio call from: ";
         self.callStatusLabel.text = [NSString stringWithFormat:@"%@  %@",calltype, self.dataPointer.incomingcallUserName];
     } errorBlock:^(QBResponse *response) {
         // Handle error
         NSLog(@"IncomingCallViewController: QBRequest() Error");
     }];

    [QMSoundManager playRingtoneSound];
    
    self.dialignTimer =
    [NSTimer scheduledTimerWithTimeInterval:[QBRTCConfig dialingTimeInterval]
                                     target:self
                                   selector:@selector(dialing:)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationItem setHidesBackButton:YES];
}

- (void)dialing:(NSTimer *)timer {
    [QMSoundManager playRingtoneSound];
}

- (IBAction)AcceptCallButton:(UIButton *)sender {
    
    __weak __typeof(self)weakSelf = self;
    NSLog(@"IncomingCallViewController: AcceptCallButton()");
    [weakSelf cleanUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCall"];
    [self.navigationController presentViewController:VC animated:YES completion:nil];
}


- (IBAction)RejectCallButton:(UIButton *)sender {
    
    __weak __typeof(self)weakSelf = self;
    NSLog(@"IncomingCallViewController: RejectCallButton()");
    [weakSelf cleanUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *VC=[storyboard  instantiateViewControllerWithIdentifier:@"TheHomeViewController"];
    
    [self.navigationController presentViewController:VC animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (void)cleanUp {
    [self.dialignTimer invalidate];
    self.dialignTimer = nil;
    
    [QBRTCClient.instance removeDelegate:self];
    [[QMSoundManager instance] stopAllSounds];
}

- (void)sessionDidClose:(QBRTCSession *)session {
    if (self.session == session) {
        NSLog(@"IncomingCallViewController: sessionDidClose()");
        [self cleanUp];
    }
}

@end
