import UIKit
import MIBadgeButton_Swift

class myConsultsTableViewCell: UITableViewCell {
 
    @IBOutlet var myconsultcellButton: UIButton!
    @IBOutlet var consultclosedttmlb: UILabel!
    @IBOutlet var consultstatuslb: UILabel!
    @IBOutlet var timelb: UILabel!
    @IBOutlet var dateButton: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
