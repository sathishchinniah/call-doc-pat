import UIKit

class DoctorProfileViewController: UIViewController {

    @IBOutlet weak var bottonBarView: UIView!
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottonBarView.backgroundColor = UIColor(patternImage: UIImage(named: "schbottombarbg")!)
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        title = tempdoclist[gindexval]
        
        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
    }

    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
