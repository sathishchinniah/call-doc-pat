#import <UIKit/UIKit.h>

@class GData;

@protocol IncomingCallViewControllerDelegate;

@interface IncomingCallViewController : UIViewController

@property (weak, nonatomic) id <IncomingCallViewControllerDelegate> delegate;

@property (strong, nonatomic) QBRTCSession *session;

@property (nonatomic,retain) GData *dataPointer;

@end

@protocol IncomingCallViewControllerDelegate <NSObject>

- (void)incomingCallViewController:(IncomingCallViewController *)vc didAcceptSession:(QBRTCSession *)session;
- (void)incomingCallViewController:(IncomingCallViewController *)vc didRejectSession:(QBRTCSession *)session;

@end
