import UIKit

class PtnAddDoctorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var downVerticalLineView: UIView!
    @IBOutlet weak var upVerticalLineView: UIView!
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var recommendsContainerView: UIView!
    @IBOutlet weak var recommendsLabel: UILabel!
    @IBOutlet weak var recommmendsValueLabel: UILabel!
    @IBOutlet weak var headDoctorImageContainerView: UIView!
    @IBOutlet weak var headDoctorImageView: UIImageView!
    @IBOutlet weak var subDoctorImageContainerView: UIView!
    @IBOutlet weak var subDoctorImageView: UIImageView!
    //@IBOutlet weak var headDoctorNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.containerView1.bringSubview(toFront: headDoctorNameLabel)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
