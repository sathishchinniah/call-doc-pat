import UIKit
import Toast_Swift

class ConsultDetailsViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var docnameld: UILabel!
    @IBOutlet weak var datelb: UILabel!
    @IBOutlet weak var chatContainerView: UIView!
    @IBOutlet weak var prescriptionContainerView: UIView!
    @IBOutlet weak var invoicesContainerView: UIView!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var prescriptionBtn: UIButton!
    @IBOutlet weak var invoicesBtn: UIButton!
    @IBOutlet weak var uploadpdfBtn: UIButton!
    @IBOutlet weak var bottomtabView: UIView!
    @IBOutlet weak var callButtonView: UIButton!
    @IBOutlet weak var chatButtonView: UIButton!
    
    // MARK: - Variables/Constants
    
    var nav: UINavigationController?
    var docnamepassed = ""
    var datepassed = ""
    var indexval: Int = 0
    var doctorsarrayindexval: Int = 0
    var pdffileURL = ""
    var hourslaspedpassed: Int = 0
    var docuseridpassed: UInt = 0
    var consultrecordIDpassed = ""
    var prescriptionTVC: PrescriptionTableViewController?
    
    //MARK: - Controller methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callButtonView.isHidden = true
        
        chatBtn.alpha = 0.5
        prescriptionContainerView.isHidden = true
        invoicesContainerView.isHidden = true
        uploadpdfBtn.isHidden = true
        
        let navBarColor = self.navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.title = "Consult Details"
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.white
        
        UINavigationBar.appearance().barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIApplication.shared.statusBarStyle = .default
        
        navigationController?.navigationBar.tintColor = UIColor.white
        
        docnameld.text = docnamepassed
        datelb.text = datepassed
        
        print("ConsultDetailsViewController: hours remaining for consult (hourslasped) = \(hourslaspedpassed)" )
        if ( hourslaspedpassed > 72 ) {
            callButtonView.isHidden = true
            chatButtonView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "Consult Details"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.topItem?.title = "My Consults With"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - IBAction methods
    
    // For naking video call
    @IBAction func CallButtonClicked(_ sender: UIButton) {
        let VC1 = CallViewControllerswft.storyboardInstance()
        VC1.callingnumber = docuseridpassed
        VC1.doctorname =  docnamepassed
        VC1.consultrecordIDpassed = consultrecordIDpassed
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    // for doing text chat
    @IBAction func ChatButtonClicked(_ sender: UIButton) {
        print("Top Bar Chat button pressed")
        let user = QBUUser()
        user.id =  docuseridpassed
        let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
        chatDialog.occupantIDs = [user.id] as [NSNumber]
        
        QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
            print("sucess + \(String(describing: response))")
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            guard let chatvc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { fatalError("Error") }
            chatvc.dialog = createdDialog
            if DoctorsArray.count > self.doctorsarrayindexval {
                if let profileurl = DoctorsArray[self.doctorsarrayindexval].fields?.value(forKey: "doctorprofileimageURL") as? String {
                    chatvc.docprofileimageurl = profileurl
                }
            }
           
            
            self.navigationController?.pushViewController(chatvc, animated: true)
        }, errorBlock: {(response: QBResponse!) in
            print("Error response + \(String(describing: response))")
        })
    }
    
    //MARK:- IBAction methods for tab bar bottom view
    
    @IBAction func chatBtnClicked(_ sender: UIButton) {
        chatBtn.alpha = 0.5
        prescriptionBtn.alpha = 1
        invoicesBtn.alpha = 1
        prescriptionContainerView.isHidden = true
        invoicesContainerView.isHidden = true
        chatContainerView.isHidden = false
        uploadpdfBtn.isHidden = true
    }
    
    @IBAction func prescriptionBtnClicked(_ sender: UIButton) {
        chatBtn.alpha = 1
        prescriptionBtn.alpha = 0.5
        invoicesBtn.alpha = 1
        chatContainerView.isHidden = true
        invoicesContainerView.isHidden = true
        prescriptionContainerView.isHidden = false
        uploadpdfBtn.isHidden = false
    }
    
    @IBAction func invoicedBtnClicked(_ sender: UIButton) {
        chatBtn.alpha = 1
        prescriptionBtn.alpha = 1
        invoicesBtn.alpha = 0.5
        chatContainerView.isHidden = true
        prescriptionContainerView.isHidden = true
        invoicesContainerView.isHidden = false
        uploadpdfBtn.isHidden = true
    }
    
    @IBAction func uploadpdfButtonClicked(_ sender: UIButton) {
        //this is to display the pdf
        
        if let height = prescriptionTVC?.getTotalHeight(), height > 10 {
            self.bottomtabView.isHidden = true
            prescriptionContainerView.frame = CGRect(x: 0, y: 0, width: Int(prescriptionContainerView.frame.width), height: height)
            prescriptionContainerView.sizeToFit()
            let pdfPageBounds: CGRect = prescriptionContainerView.frame
            let pdfData: NSMutableData = NSMutableData()
            
            // Rendering spreadsheetView
            UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds, nil)
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            prescriptionContainerView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsEndPDFContext()
            
            let epochtime = Date().timeIntervalSince1970
            let numberString = String(epochtime)
            let numberComponent = numberString.components(separatedBy :".")
            let epochfirstpart = (numberComponent [0])
            var patientname = ""
            if PatientConsultsdetailsArray.isEmpty {
                patientname = (selectedUserDoctorsArray[theindexval].fields?.value(forKey: "prescriptionid") as? String) ?? ""
            } else {
                patientname = (PatientConsultsdetailsArray[indexval].fields?.value(forKey: "patfullname") as? String) ?? ""
            }
            
            let pdffilename = "Prescription" + "_" + patientname + "_" + epochfirstpart + ".pdf"
            pdffileURL = NSTemporaryDirectory().appending(pdffilename)
            pdfData.write(toFile: pdffileURL, atomically: true)
            
            let theURL = NSURL(fileURLWithPath:pdffileURL)
            let urlToShare = theURL as URL
            let pdfDataToShare = NSData(contentsOf: urlToShare)
            let activityViewController = UIActivityViewController(activityItems: [pdfDataToShare as Any], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
           // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
//            let activityviewcontroller = UIActivityViewController(activityItems: [theURL as Any], applicationActivities: nil)
//            activityviewcontroller.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
//            present(activityviewcontroller, animated: true, completion: nil )
            
            activityViewController.completionWithItemsHandler = completionHandler
        } else {
            self.view.makeToast("Prescriptions data is not available.", duration: 2.0, position: .bottom)
        }
        
    }
    
    func completionHandler(activityType: UIActivityType?, shared: Bool, items: [Any]?, error: Error?) {
        if (shared) {
            print("ConsultDetailsViewController:completionHandler :Cool user shared some stuff")
            self.bottomtabView.isHidden = false
            do {
                try FileManager.default.removeItem(atPath: self.pdffileURL)
                print("ConsultDetailsViewController:File removal done")
            } catch (let e) {
                print("ConsultDetailsViewController:File removal Error : \(e)")
                return
            }
        } else {
            print("ConsultDetailsViewController:completionHandler :Bad user canceled sharing :(")
            self.bottomtabView.isHidden = false
            do {
                try FileManager.default.removeItem(atPath: self.pdffileURL)
                print("ConsultDetailsViewController:File removal done")
            } catch (let e) {
                print("ConsultDetailsViewController:File removal Error : \(e)")
                return
            }
        }
    }
    
    //MARK:- Storyboard Instance
    
    static func storyboardInstance() -> ConsultDetailsViewController {
        let storyboard = UIStoryboard(name: "Consult", bundle: nil)
        return (storyboard.instantiateViewController(withIdentifier: "ConsultDetailsViewController") as? ConsultDetailsViewController) ?? ConsultDetailsViewController()
    }
    
    //MARK:- Segue Delegates
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "prescriptionViewSegue" {
            if let childVC = segue.destination as? PrescriptionTableViewController {
                prescriptionTVC = childVC
            }
        }
        if segue.identifier == "ChatViewSegue" {
            if (!consultrecordIDpassed.isEmpty) {
                let user = QBUUser()
                user.id =  docuseridpassed
                let chatDialog: QBChatDialog = QBChatDialog(dialogID: nil, type: QBChatDialogType.private)
                chatDialog.occupantIDs = [user.id] as [NSNumber]
                
                QBRequest.createDialog(chatDialog, successBlock: {(response: QBResponse?, createdDialog: QBChatDialog?) in
                    //let storyboard = UIStoryboard.init(name: "Consult", bundle: nil)
                    let chatvc = segue.destination as? ChatLogsViewController//storyboard.instantiateViewController(withIdentifier: "ChatLogsViewController") as! ChatLogsViewController
                    chatvc?.ConsultRecordID = self.consultrecordIDpassed
                    chatvc?.dialog = createdDialog
                    chatvc?.reloadInputViews()
//                    self.prescriptionContainerView.isHidden = true
//                    self.invoicesContainerView.isHidden = true
//                    self.chatContainerView.isHidden = false
                    
                }, errorBlock: {(response: QBResponse!) in
                    print("Error response + \(String(describing: response))")
                })
            }
        }
    }
    
}
