import UIKit

class PrescriptionCell6: UITableViewCell {

    @IBOutlet var advicelbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(data: QBCOCustomObject) {
        advicelbl.text = data.fields.value(forKey: "advice") as? String
    }

}
