import UIKit

var  mainHeadDoctorArray = ["a", "b"]
var numberOfRowinSectionArray = NSMutableArray()
var numberOfRowinTapedSection = 1
var tapedSection = 5000
var isSectionExpend = false

class PntAddDoctorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var SubTableView: UITableView!
    @IBOutlet weak var specialitylbtxt: UILabel!
    
    var nav: UINavigationController?
    var speciality = ""
    var specialityDoctors : [QBCOCustomObject] = []
    var copyofhomeDoctorsArray : [QBCOCustomObject] = []
    var docCOTSpeciality = [QBCOCustomObject: [QBCOCustomObject]]()
    var expandTapped = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBarColor = self.navigationController!.navigationBar
        self.title = "Add a Doctor"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        // this removes navigation bar horizental line
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        SubTableView.tableFooterView = UIView()
        //numberOfRowinSectionArray = [1, 1]
        SubTableView.reloadData()
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.recommendsTapAction))
        SubTableView.isUserInteractionEnabled = true
        SubTableView.addGestureRecognizer(gesture)
        specialitylbtxt.text = speciality
        
        //copy home doctor array to local array
        copyofhomeDoctorsArray = DoctorsArray
        for doc in copyofhomeDoctorsArray {
            numberOfRowinSectionArray.insert(1, at: copyofhomeDoctorsArray.index(of: doc)!)
        }
        
        self.getSpecialityDoctors()
        
      //  let thedata = copyofhomeDoctorsArray.flatMap { $0.fields.filter { $0.doc}}
        print("specialityDoctors\(specialityDoctors)")
        print("PntAddDoctorViewController")
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.navigationItem.backBarButtonItem?.title = ""
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "AddDocFromDocNetwork")
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:true, completion: nil)
    }
    
    func getSpecialityDoctors() {
        
        /* for doctor in copyofhomeDoctorsArray {
            if ( doctor.fields.value(forKey: "doctorspeciality") as? String ) == (speciality.lowercased()) {
                //return copyofhomeDoctorsArray[i]
                specialityDoctors.append(doctor)
            }
        } */
        //enter the keys into cot dict
//        for doctor in copyofhomeDoctorsArray {
//
//        }
        populateDocCOTSpeciality()
        
    }
    
    func populateDocCOTSpeciality() {
        
        for doc in copyofhomeDoctorsArray {
            let getRequest = NSMutableDictionary()
            getRequest["invitordocuserid"] =  (doc.fields?.value(forKey: "doctoruserid") as? UInt) ?? 1//doc.id
            if speciality == "E.N.T." {
                speciality = "ent"
            }
            getRequest["inviteespeciality"] = speciality.lowercased()
            getRequest["status"] = true
            /*equestBuilder.eq("invitordocuserid", userId);
             requestBuilder.eq("inviteespeciality", speciality);
             requestBuilder.eq("status", true);*/
            QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                //Handle Success
                print("Success ")
                if contributors?.count == 0 {
                    // No entry found
                    let getRequest = NSMutableDictionary()
                    getRequest["invitordocuserid"] = (doc.fields?.value(forKey: "doctoruserid") as? UInt) ?? 1
                    getRequest["status"] = true
                    QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                        //Handle Success
                        print("Success ")
                        
                        if ((contributors?.count)! > 0) {
                            self.docCOTSpeciality[doc] = contributors ?? []
                            numberOfRowinSectionArray.insert(contributors?.count ?? 1, at: self.copyofhomeDoctorsArray.index(of: doc)!)
                        }
                        //self.SubTableView.reloadData()
                    }) { (response) in
                        //Handle Error
                        print("Response error: \(String(describing: response.error?.description))")
                    }
                    
                } else {
                    // entry found
                    self.docCOTSpeciality[doc] = contributors ?? []
                    
                }
                self.SubTableView.reloadData()
                
            }) { (response) in
                //Handle Error
                print("Response error: \(String(describing: response.error?.description))")
            }
            let indexSet = copyofhomeDoctorsArray.index(of: doc)!
            //SubTableView.reloadSections(indexSet, with: .automatic)//reloadData()
            let index = IndexSet(integer: indexSet)
            //self.SubTableView.reloadSections(index, with: .automatic)
            self.SubTableView.reloadData()
        }
        
    }
    

    //MARK: UITableView methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return copyofhomeDoctorsArray.count//1//mainHeadDoctorArray.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if expandTapped == true {
//            if docCOTSpeciality.keys.contains(copyofhomeDoctorsArray[tapedSection/*section*/]) {
//                let index = docCOTSpeciality.index(forKey: copyofhomeDoctorsArray[tapedSection/*section*/])
//                let recommmendedCount = docCOTSpeciality[index!].value.count
//                return recommmendedCount
//            } else {
//                return 1
//            }
            if numberOfRowinSectionArray.count > 0 {
                return numberOfRowinSectionArray[section] as! Int
            } else {
                return 0
            }

        } else {
            return 1
        }
        
        
        //let count = docCOTSpeciality//[section].values.count
        //return copyofhomeDoctorsArray.count//docCOTSpeciality.count//specialityDoctors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell  = tableView.dequeueReusableCell(withIdentifier: "cellAddDoctor") as! PtnAddDoctorTableViewCell
        
        cell.headDoctorImageContainerView.layer.cornerRadius = cell.headDoctorImageContainerView.frame.size.width / 2;
        cell.headDoctorImageContainerView.clipsToBounds = true;
        cell.headDoctorImageContainerView.layer.borderWidth = 2.0
        cell.headDoctorImageContainerView.layer.borderColor = UIColor.green.cgColor
        
        cell.headDoctorImageView.layer.cornerRadius = cell.headDoctorImageView.frame.size.width / 2;
        cell.headDoctorImageView.clipsToBounds = true;
        cell.headDoctorImageView.layer.borderWidth = 3.0
        cell.headDoctorImageView.layer.borderColor = UIColor.white.cgColor
        let imgURL = (copyofhomeDoctorsArray[indexPath.section].fields.value(forKey: "doctorprofileimageURL") as? String ) ?? ""
        cell.headDoctorImageView.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "profile"))
        
        cell.subDoctorImageContainerView.layer.cornerRadius = cell.subDoctorImageContainerView.frame.size.width / 2;
        cell.subDoctorImageContainerView.clipsToBounds = true;
        cell.subDoctorImageContainerView.layer.borderWidth = 2.0
        cell.subDoctorImageContainerView.layer.borderColor = UIColor.green.cgColor

        cell.subDoctorImageView.layer.cornerRadius = cell.subDoctorImageView.frame.size.width / 2;
        cell.subDoctorImageView.clipsToBounds = true;
        cell.subDoctorImageView.layer.borderWidth = 3.0
        cell.subDoctorImageView.layer.borderColor = UIColor.white.cgColor
        
//        for doc in copyofhomeDoctorsArray {
//            let index = docCOTSpeciality.index(forKey: copyofhomeDoctorsArray[indexPath.row])
//        }
        //let index =  ? docCOTSpeciality.index(forKey: copyofhomeDoctorsArray[indexPath.row]) : docCOTSpeciality.startIndex
        var no = 0
        var index: IndexPath!
        if docCOTSpeciality.keys.contains(copyofhomeDoctorsArray[indexPath.section]) {
            let index = docCOTSpeciality.index(forKey: copyofhomeDoctorsArray[indexPath.section])
            //index = index
            let recommmendedCount = docCOTSpeciality[index!].value.count
            no = recommmendedCount
            cell.recommmendsValueLabel.text = String(describing: recommmendedCount)
        } else {
            cell.recommmendsValueLabel.text = "0"
        }
    
        if expandTapped && no > 0  && tapedSection == indexPath.section {
            //let index = docCOTSpeciality.index(forKey: copyofhomeDoctorsArray[indexPath.section])
            //let docsArray = docCOTSpeciality[index!].value//docCOTSpeciality[copyofhomeDoctorsArray[indexPath.section]] ?? []
            //(docsArray[indexPath.row].fields.value(forKey: "doctorprofileimageURL") as? String ) ?? ""
             let imgURL = ((docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1].fields.value(forKey: "inviteeprofileimageURL") as? String) ?? ""
            //invitorprofileimageURL
            cell.subDoctorImageView.sd_setImage(with: URL(string: imgURL), placeholderImage: UIImage(named: "profile"))
            
            cell.containerView2.isHidden = false
            cell.recommendsContainerView.isHidden = true

            if (no/2 - 1 == indexPath.row) || (no == 1) {
                cell.containerView1.isHidden = false
            } else {
                cell.containerView1.isHidden = true
            }

            if indexPath.row == 0 {
                cell.upVerticalLineView.isHidden = true
            } else {
                cell.upVerticalLineView.isHidden = false
            }

            if indexPath.row == no - 1 {
                cell.downVerticalLineView.isHidden = true
            } else {
                cell.downVerticalLineView.isHidden = false
            }

        } else {
            cell.recommendsContainerView.isHidden = false
            cell.containerView2.isHidden = true
            cell.containerView1.isHidden = false
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    //MARK: Recommends tap action
    @objc func recommendsTapAction(gesture : UITapGestureRecognizer) {
        
        let tapLocation = gesture.location(in: SubTableView)
        if let tapIndexPath = SubTableView.indexPathForRow(at: tapLocation) {
            if let tappedCell = SubTableView.cellForRow(at: tapIndexPath) as? PtnAddDoctorTableViewCell {
                
                let touchpoint:CGPoint = gesture.location(in: tappedCell)
                
                if tappedCell.recommendsContainerView.frame.contains(touchpoint) || tappedCell.recommendsLabel.frame.contains(touchpoint) ||
                    tappedCell.recommmendsValueLabel.frame.contains(touchpoint){
                    print("tap is working")
                    isSectionExpend = true
                    tapedSection = tapIndexPath.section
                    numberOfRowinTapedSection = Int(tappedCell.recommmendsValueLabel.text!)!
                    if numberOfRowinTapedSection != 0 {
                        numberOfRowinSectionArray[tapIndexPath.section] = numberOfRowinTapedSection
                        if expandTapped {
                            inviteDoctor()
                        }
                        
                        expandTapped = true
                        SubTableView.reloadData()
                    }
                    
                } else {
                    expandTapped = false
                    for doc in copyofhomeDoctorsArray {
                        numberOfRowinSectionArray.insert(1, at: copyofhomeDoctorsArray.index(of: doc)!)
                    }
                    //numberOfRowinSectionArray = [1, 1]
                    SubTableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("INDEXPATH SECTION: \(indexPath.section) ROW: \(indexPath.row)")
    }
    
    static func storyboardInstance() -> PntAddDoctorViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PntAddDoctor") as! PntAddDoctorViewController
    }
    
    func inviteDoctor() {
        //(docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1].fields.value(forKey: "invitordocuserid")
        //invitorprofileimageURL
        //docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]]!
        //(docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1]
        
//        let getRequest = NSMutableDictionary()
//        getRequest["user_id"] = (docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1].fields.value(forKey: "invitordocuserid")//GlobalDocdata.regcalldoccode
//        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
//            //Handle Success
//            print("(DocTrustedNetworkViewController:GetDoctorDatausingregcalldoccode(): Successfully  !")
//            if ((contributors?.count)! > 0) {
//                let docuserid = contributors![0].userID
//                let docname = contributors![0].fields?.value(forKey: "fullname") as? String
//                let speciality = contributors![0].fields?.value(forKey: "specialisation") as? String
//                let profileurl = contributors![0].fields?.value(forKey: "profileUID") as? String
//                let doctor = contributors![0]
//            }
//
//        }) { (errorresponse) in
//            print("PrimaryDetailsViewController:GetDoctorDatausingregcalldoccode() Response error: \(String(describing: errorresponse.error?.description))")
//        }
        
        //patient id
        let invitoruid = (QBSession.current.currentUser?.id) ?? 0//0 as UInt
        //doctor's id
        let inviteeuid = ((docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1].fields.value(forKey: "inviteedocuserid") as? UInt) ?? 0//inviteedocuserid
        self.ReadCallDoctable_RelDocPat(invitoruid: invitoruid, inviteeuid: inviteeuid)
        
        
    }
    
    // check if there is entry of doctor and patient relation in RelDocPat table
    func ReadCallDoctable_RelDocPat(invitoruid: UInt, inviteeuid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["doctoruserid"] =  inviteeuid
        getRequest["patientuserid"] = invitoruid
        
        QBRequest.objects(withClassName: "RelDocPat", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Successfully  !")
            if ((contributors?.count)! > 0) {
                print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat():Match Entry Found  !")
                let alert = UIAlertController(title: "Alert", message: "User Already Connected !", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
                    guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
                    self.nav = UINavigationController(rootViewController: homeViewController)
                    self.present(self.nav!, animated: false, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Relation Match NOT Found  !")
                self.readdoctordetails(doctoruserid: inviteeuid, invitoruserid: invitoruid)
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func readdoctordetails(doctoruserid: UInt, invitoruserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctoruserid
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                
                let profileVerifed = (contributors![0].fields?.value(forKey: "profileverified") as? Bool) ?? false
                if profileVerifed == false {
                    let alert = UIAlertController(title: "Action currently unavailable. Please try later", message: "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    return
                } else {
                    let speciality = contributors![0].fields?.value(forKey: "specialisation") as? String
                    var profileurl = contributors![0].fields?.value(forKey: "profileUID") as? String
                    
                    if (profileurl == nil || profileurl!.isEmpty) {
                        profileurl = "none"
                    }
                    self.createEntryinRelDocPatTable(invitoruid: invitoruserid, inviteeuid: doctoruserid, speciality: speciality!, profileurl: profileurl! )
                }
                
                
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:ReadPrimaryDetailsFromServer() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createEntryinRelDocPatTable(invitoruid: UInt, inviteeuid: UInt, speciality: String, profileurl: String ) {
        print("AddDoctorsTableViewConteroller: createEntryinRelDocPatTable(): Creating new entry on RelDocPatable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = inviteeuid
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = invitoruid //QBSession.current.currentUser?.id
        let doctorname = (docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1].fields.value(forKey: "inviteefullname") as? String//(docCOTSpeciality[copyofhomeDoctorsArray[tapedSection]])?[numberOfRowinTapedSection - 1].fields.value(forKey: "invitorfullname") as? String
        object.fields["doctorfullname"] = doctorname//self.doctorname //invitorfullname
        object.fields["lock"] = "true"
        object.fields["doctorspeciality"] = speciality
        object.fields["doctorprofileimageURL"] = profileurl
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("AddDoctorsTableViewConteroller: createEntryinRelDocPatTable(): Successfully created entry")
            LoadingIndicatorView.hide()
            
            let alert = UIAlertController(title: "Congratulations", message: " Your Dr. \(String(describing: doctorname)) - \(speciality)  already Registered on CallDoc. Invite has been sent !!", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
                self.nav = UINavigationController(rootViewController: homeViewController)
                self.present(self.nav!, animated: true, completion: nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }) { (response) in
            //Handle Error
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:createEntryinRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }

}
