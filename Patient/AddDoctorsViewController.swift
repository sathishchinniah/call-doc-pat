import UIKit

class AddDoctorsViewController: UIViewController {

    var nav: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func BackBtnAction(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        
        self.present(self.nav!, animated: false, completion: nil)
        
    }

}
