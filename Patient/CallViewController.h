#import <UIKit/UIKit.h>

@class GData;
@class QBRTCSession;

@interface CallViewController : UIViewController

@property (nonatomic,retain) GData *dataPointer;
@property (strong, nonatomic) QBRTCSession *session;

@end
