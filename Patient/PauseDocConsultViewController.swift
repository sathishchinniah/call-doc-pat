import UIKit
import MIBadgeButton_Swift

class PauseDocConsultViewController: UIViewController {

    @IBOutlet var topView: UIView!
    @IBOutlet var bottombarview: UIView!
    @IBOutlet var docimagevieContainter: UIView!
    @IBOutlet var docprofileimg: UIImageView!
    @IBOutlet var docnamelb: UILabel!
    @IBOutlet var specialitylb: UILabel!
    @IBOutlet var statuslb: UILabel!
    @IBOutlet var dropdownchatView: UIView!
    @IBOutlet var consultanotherdocBtView: UIButton!
    @IBOutlet var chatuploadbtView: UIButton!
    @IBOutlet var ratetext: UILabel!
    @IBOutlet var dropdownconsultvaltxt: UILabel!
    @IBOutlet weak var cotButton: UIButton!
    
    var nav: UINavigationController?
    var myindexval: Int = 100
    var thelastvc: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "orangebarbg")?.draw(in: self.view.bounds)
        let bgimage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.bottombarview.backgroundColor = UIColor(patternImage: bgimage)

        let shadow0 = UIView(frame: CGRect(x: 0, y: 0, width: topView.frame.width, height: topView.frame.height))
        shadow0.layer.shadowOffset = CGSize.zero
        shadow0.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.15).cgColor
        shadow0.layer.shadowOpacity = 1
        shadow0.layer.shadowRadius = 2
        shadow0.isUserInteractionEnabled = false
        topView.addSubview(shadow0)
        
        let shadow1 = UIView(frame: CGRect(x: 0, y: 0, width: topView.frame.width, height: topView.frame.height))
        shadow1.layer.shadowOffset = CGSize(width: 0, height: 1)
        shadow1.layer.shadowColor = UIColor(red:0, green:0, blue:0, alpha:0.23).cgColor
        shadow1.layer.shadowOpacity = 1
        shadow1.layer.shadowRadius = 1
        shadow1.isUserInteractionEnabled = false
        topView.addSubview(shadow1)
        
        //topView.isUserInteractionEnabled = false
        
        dropdownchatView.layer.cornerRadius = 10
        dropdownchatView.isHidden = true
        self.title = "PAUSE"

        if(myindexval == 100) {// fix index range issue when coming back from consult details view controller
            myindexval = gindexval
            print(" PauseDocConsultViewController myindexval = \(myindexval)")
        }

        docprofileimg.image  =  UIImage(named: docimageNames[myindexval] + ".png")
        docprofileimg.layer.cornerRadius = docprofileimg.frame.size.width / 2;
        docprofileimg.clipsToBounds = true;
        docprofileimg.layer.borderWidth = 2.0
        docprofileimg.layer.borderColor = UIColor.white.cgColor
        
        docimagevieContainter.layer.cornerRadius = docimagevieContainter.frame.size.width / 2;
        docimagevieContainter.clipsToBounds = true;
        docimagevieContainter.layer.borderWidth = 2.0
        docimagevieContainter.layer.borderColor = UIColor.orange.cgColor
        
        docnamelb.text = tempdoclist[myindexval]
        specialitylb.text = tempspecialisationlist[myindexval]
        statuslb.text = dummpstatus[myindexval]

        //left side Back Button
        let btnImage = UIImage(named: "backBtn-1")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        navigationItem.leftBarButtonItem = backButton
        
        //right side profile button
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageview.sd_setImage(with: URL(string: patientimageurl[gindexval]), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        imageview.contentMode = UIViewContentMode.scaleToFill
        imageview.layer.cornerRadius = 20
        imageview.layer.masksToBounds = true
        containView.addSubview(imageview)
        let rightBarButton = UIBarButtonItem(customView: containView)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        self.navigationController?.navigationBar.tintColor = UIColor.orange
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.orange
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.orange
        
        //cotButton.addTarget(self, action: #selector(cotButtonTouchUpInside(_:)), for: .touchUpInside)

    }
    
    @objc func backBtnPressed(_ sender: Any) {
        let thehomeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
        let navController = UINavigationController(rootViewController: thehomeViewController)
        self.present(navController, animated:true, completion: nil)
    }

    @IBAction func LastPrescriptionButton(_ sender: MIBadgeButton) {

    }
    
    @IBAction func ConsultButton(_ sender: UIButton) {
        let vcobj = MyConsultsViewController(nibName: "MyConsultsViewController", bundle: nil)
        vcobj.thelastvc = "PauseDocConsult"
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MyConsult")
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:true, completion: nil)
    }

    @IBAction func COTButton(_ sender: UIButton) {
        let cotViewController = self.storyboard!.instantiateViewController(withIdentifier: "CircleOfTrust") as! CircleOftrustViewController
        
        // these lines of code removes left and right animation
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        let animation = CATransition()
        animation.type = kCATransitionFade
        self.navigationController?.view.layer.add(animation, forKey: "someAnimation")
        self.nav = UINavigationController(rootViewController: cotViewController)
        _ = self.present(self.nav!, animated: false, completion: nil)
        CATransaction.commit()
    }

    @IBAction func ChatUploadButton(_ sender: UIButton) {
        dropdownchatView.isHidden = false
    }
    
    @IBAction func ConsultOtherDocButtion(_ sender: UIButton) {
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        let myteamviewController = self.storyboard!.instantiateViewController(withIdentifier: "TheDoctorTeamView") as! TheDoctorTeamViewController
        self.nav = UINavigationController(rootViewController: myteamviewController)
        _ = self.present(self.nav!, animated: false, completion: nil)
    }
    
    
    @IBAction func ChatButtonAction(_ sender: UIButton) {
    }
    
    @IBAction func CancelButton(_ sender: UIButton) {
        dropdownchatView.isHidden = true
    }
    
    @IBAction func cotButtonTapped(_ sender: UIButton) {
        
        print("COT Button pressed")
        guard let cotViewController = self.storyboard!.instantiateViewController(withIdentifier: "CircleOfTrust") as? CircleOftrustViewController else { fatalError("Error") }
        cotViewController.myindexval = myindexval
        let selecteddocuid = (DoctorsArray[myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
        cotViewController.docuserid = selecteddocuid
        
        // these lines of code removes left and right animation
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        let animation = CATransition()
        animation.type = kCATransitionFade
        self.navigationController?.view.layer.add(animation, forKey: "someAnimation")
        self.nav = UINavigationController(rootViewController: cotViewController)
        _ = self.present(self.nav!, animated: false, completion: nil)
        CATransaction.commit()
        
    }
    
//    @objc func cotButtonTouchUpInside(_ sender : UIButton){
//
//    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
