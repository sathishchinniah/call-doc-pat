import UIKit

class SMSInviteDismissViewController: UIViewController {

    @IBOutlet weak var mobilenumber: UILabel!
    
    var nav: UINavigationController?
    var mobilenumberpassed = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobilenumber.text = mobilenumberpassed
        
        navigationController?.isNavigationBarHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 8) {
            self.navigationController?.isNavigationBarHidden = false
            let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
            self.nav = UINavigationController(rootViewController: homeViewController)
            self.present(self.nav!, animated: true, completion: nil)
        }
    }
    
}
