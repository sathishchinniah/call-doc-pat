import UIKit
import DropDown

class AddProfileViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    @IBOutlet weak var patientnamelb: UILabel!
    @IBOutlet weak var firstnametxt: UITextField!
    @IBOutlet weak var lastnametxt: UITextField!
    @IBOutlet weak var dispaynametxt: UITextField!
    @IBOutlet weak var agetxt: UITextField!
    @IBOutlet weak var gendertxt: UITextField!
    @IBOutlet weak var heightfttxt: UITextField!
    @IBOutlet weak var heightinchtxt: UITextField!
    @IBOutlet weak var weighttxt: UITextField!
    @IBOutlet weak var bloogptxt: UITextField!
    @IBOutlet weak var relationshiptxt: UITextField!
    @IBOutlet weak var allergiestxtview: UITextView!
    @IBOutlet weak var chronichealthetxtview: UITextView!
    @IBOutlet weak var currentmedicationtxtview: UITextView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var ageView: UIView!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var hightftView: UIView!
    @IBOutlet weak var hightinchView: UIView!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var bloodgpView: UIView!
    @IBOutlet weak var relationshipView: UIView!
    @IBOutlet var profileimageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var profileimgButtonview: UIButton!
    @IBOutlet var topView: UIView!
    @IBOutlet var docimg: UIImageView!{
        didSet {
            self.docimg.layer.cornerRadius = self.docimg.frame.size.width / 2;
            self.docimg.clipsToBounds = true;
            self.docimg.layer.borderWidth = 0.2
            self.docimg.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var allergiestextviewhightConstraint: NSLayoutConstraint!
    @IBOutlet weak var chronichealthissuestxtviewhightConstraint: NSLayoutConstraint!
    @IBOutlet weak var currentmedicationhightConstraint: NSLayoutConstraint!

    var nav: UINavigationController?
    var profileimgbool: Bool = false
    let dropDownGender = DropDown()
    let dropDownage = DropDown()
    let dropDownblood = DropDown()
    let dropDownhightfeet = DropDown()
    let dropDownhightinch = DropDown()
    let dropDownweight = DropDown()
    let dropDownrelationship = DropDown()
    var activeField : UITextView!
    var thefinalcode = ""
    var theprofileurl = ""
    var imagefileUID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navBarColor = navigationController!.navigationBar
        navBarColor.topItem?.title = "Add a Family Member"
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        title = "Add a Family Member"
        
        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        profileimageActivityIndicator.isHidden = true
        firstnametxt.delegate = self
        lastnametxt.delegate = self
        dispaynametxt.delegate = self
        agetxt.delegate = self
        gendertxt.delegate = self
        heightfttxt.delegate = self
        heightinchtxt.delegate = self
        weighttxt.delegate = self
        bloogptxt.delegate = self
        relationshiptxt.delegate = self
        chronichealthetxtview.delegate = self
        currentmedicationtxtview.delegate = self
        
        allergiestxtview.isScrollEnabled = false
        allergiestxtview.textContainer.maximumNumberOfLines = 3
        allergiestxtview.textContainer.lineBreakMode = .byWordWrapping
        
        chronichealthetxtview.isScrollEnabled = false
        chronichealthetxtview.textContainer.maximumNumberOfLines = 3
        chronichealthetxtview.textContainer.lineBreakMode = .byWordWrapping
        
        currentmedicationtxtview.isScrollEnabled = false
        currentmedicationtxtview.textContainer.maximumNumberOfLines = 3
        currentmedicationtxtview.textContainer.lineBreakMode = .byWordWrapping

        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 41.0
            docimg.clipsToBounds = true
        }
        
        if phone && maxLength == 812 {
            //iphone x
            docimg.layer.cornerRadius = 38.0
            docimg.clipsToBounds = true
        }
        
        if ipad && maxLength == 1024 {
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.clipsToBounds = true
        }
        
        self.hideKeyboardWhenTappedAround()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)

        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        title = "Add a Family Member"
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    // to check if key is present for persistance key value storage
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func textViewDidChange(_ textView: UITextView) {

        if textView == allergiestxtview {
            let size = CGSize(width: view.frame.width-40, height: 75)
            let estimatedSize = textView.sizeThatFits(size)
            allergiestextviewhightConstraint.constant = estimatedSize.height
        }
        
        if textView == chronichealthetxtview {
            let size = CGSize(width: view.frame.width-40, height: 75)
            let estimatedSize = textView.sizeThatFits(size)
            chronichealthissuestxtviewhightConstraint.constant = estimatedSize.height
        }
        
        if textView == currentmedicationtxtview {
            let size = CGSize(width: view.frame.width-40, height: 75)
            let estimatedSize = textView.sizeThatFits(size)
            currentmedicationhightConstraint.constant = estimatedSize.height
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return textView.text.characters.count + (text.characters.count - range.length) <= 125
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if (textField == self.gendertxt) {
            textField.endEditing(true)
            dropDownGender.anchorView = genderView
            dropDownGender.dataSource = ["MALE", "FEMALE"]
            dropDownGender.show()
            dropDownGender.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.gendertxt.text = item
            }
        }
        
        if (textField == self.agetxt) {
            textField.endEditing(true)
            
            dropDownage.anchorView = ageView
            dropDownage.dataSource = ["Infant", "3 mo - 6 mo", "6 mo - 1 yr", "1 yr","2 yrs","3 yrs", "4 yrs", "5 yrs", "6 yrs","7 yrs", "8 yrs", "9 yrs", "10 yrs", "11 yrs",
                                      "12 yrs", "13 yrs","14 yrs", "15 yrs", "16 yrs", "17 yrs", "18 yrs", "19 yrs", "20 yrs", "21 yrs", "22 yrs", "23 yrs", "24 yrs", "25 yrs",
                                      "26 yrs", "27 yrs", "28 yrs", "29 yrs", "30 yrs", "31 yrs", "32 yrs", "33 yrs", "34 yrs", "35 yrs", "36 yrs", "37 yrs","38 yrs", "39 yrs",
                                      "40 yrs", "41 yrs", "42 yrs", "43 yrs", "44 yrs", "45 yrs", "46 yrs", "47 yrs", "48 yrs", "49 yrs", "50 yrs", "51 yrs", "52 yrs", "53 yrs",
                                      "54 yrs", "55 yrs", "56 yrs", "57 yrs", "58 yrs", "59 yrs", "60 yrs" , "61 yrs", "62 yrs", "63 yrs", "64 yrs", "65 yrs", "66 yrs", "67 yrs",
                                      "68 yrs", "69 yrs", "70 yrs", "71 yrs", "72 yrs", "73 yrs","74 yrs", "75 yrs", "76 yrs", "77 yrs", "78 yrs", "79 yrs", "80 yrs", "81 yrs",
                                      "82 yrs", "83 yrs", "84 yrs", "85 yrs","86 yrs", "87 yrs", "88 yrs", "89 yrs", "90 yrs", "91 yrs", "92 yrs", "93 yrs", "94 yrs", "95 yrs",
                                      "96 yrs", "97 yrs", "98 yrs", "99 yrs"]
            
            dropDownage.direction = .bottom
            dropDownage.show()
            dropDownage.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.agetxt.text = item
            }
        }
        
        if (textField == self.bloogptxt) {
            textField.endEditing(true)
            dropDownblood.anchorView = bloodgpView
            dropDownblood.dataSource = ["A+","A-", "B+", "B-", "AB+","AB-", "O+", "O-"]
            dropDownblood.show()
            dropDownblood.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Boolgroup Selected item: \(item) at index: \(index)")
                self.bloogptxt.text = item
            }
        }
        
        if (textField == self.heightfttxt) {
            textField.endEditing(true)
            dropDownhightfeet.anchorView = hightftView
            dropDownhightfeet.dataSource = ["1 feet","2 feet", "3 feet", "4 feet", "5 feet","6 feet", "7 feet", "8 feet"]
            dropDownhightfeet.show()
            dropDownhightfeet.selectionAction = { [unowned self] (index: Int, item: String) in
                print("hightfeet Selected item: \(item) at index: \(index)")
                self.heightfttxt.text = item
            }
        }
        
        if (textField == self.heightinchtxt) {
            textField.endEditing(true)
            dropDownhightinch.anchorView = hightinchView
            dropDownhightinch.dataSource = ["0 inch","1 inch","2 inch", "3 inch", "4 inch", "5 inch","6 inch", "7 inch", "8 inch","9 inch", "10 inch","11 inch"]
            dropDownhightinch.show()
            dropDownhightinch.selectionAction = { [unowned self] (index: Int, item: String) in
                print("hightfeet Selected item: \(item) at index: \(index)")
                self.heightinchtxt.text = item
            }
        }
        
        if (textField == self.weighttxt) {
            textField.endEditing(true)
            dropDownweight.anchorView = weightView
            dropDownweight.dataSource = ["1 Kg","2 Kg", "3 Kg", "4 Kg", "5 Kg","6 Kg", "7 Kg", "8 Kg","9 Kg", "10 Kg","11 Kg", "12 Kg", "13 Kg", "14 Kg", "15 Kg","16 Kg", "17 Kg", "18 Kg","19 Kg", "20 Kg", "21 Kg","22 Kg", "23 Kg", "24 Kg", "25 Kg","26 Kg", "27 Kg", "28 Kg","29 Kg", "30 Kg","31 Kg", "32 Kg", "33 Kg", "34 Kg", "35 Kg","36 Kg", "37 Kg", "38 Kg","39 Kg", "40 Kg", "41 Kg","42 Kg", "43 Kg", "44 Kg", "45 Kg","46 Kg", "47 Kg", "48 Kg","49 Kg", "50 Kg","51 Kg", "52 Kg", "53 Kg", "54 Kg", "55 Kg","56 Kg", "57 Kg", "58 Kg","59 Kg", "60 Kg","61 Kg","62 Kg", "63 Kg", "64 Kg", "65 Kg","66 Kg", "67 Kg", "68 Kg","69 Kg", "70 Kg","71 Kg","72 Kg", "73 Kg", "74 Kg", "75 Kg","76 Kg", "77 Kg", "78 Kg","79 Kg", "80 Kg", "81 Kg","82 Kg", "83 Kg", "84 Kg", "85 Kg","86 Kg", "87 Kg", "88 Kg","89 Kg", "90 Kg", "91 Kg","92 Kg", "93 Kg", "94 Kg", "95 Kg","96 Kg", "97 Kg", "98 Kg","99 Kg", "100 Kg", "101 Kg","102 Kg", "103 Kg", "104 Kg", "105 Kg","106 Kg", "107 Kg", "108 Kg","109 Kg", "110 Kg", "111 Kg", "112 Kg","113 Kg", "114 Kg", "115 Kg", "116 Kg","117 Kg", "118 Kg", "119 Kg","120 Kg", "121 Kg"]
            dropDownweight.direction = .bottom
            dropDownweight.show()
            dropDownweight.selectionAction = { [unowned self] (index: Int, item: String) in
                print("hightfeet Selected item: \(item) at index: \(index)")
                self.weighttxt.text = item
            }
        }
        
        if (textField == self.relationshiptxt) {
            textField.endEditing(true)
            dropDownrelationship.anchorView = relationshipView
            dropDownrelationship.dataSource = ["Father","Mother", "Wife", "Daughter", "Son","Brother", "Sister", "Others"]
            dropDownrelationship.show()
            dropDownrelationship.selectionAction = { [unowned self] (index: Int, item: String) in
                print("relationship Selected item: \(item) at index: \(index)")
                self.relationshiptxt.text = item
            }
        }
    }
    
    @IBAction func ProfileImageButtonAction(_ sender: UIButton) {
        print("profile image Button is pressed")
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        profileimgbool = true
        let actionSheet = UIAlertController(title: "Photo Source", message: "choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)}))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)}))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        //for ipad related crash issue fix
        if ipad && maxLength == 1024 {
            actionSheet.popoverPresentationController?.sourceView = self.view
            actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
            actionSheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        }
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let selectedimage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if profileimgbool {
            self.profileimgbool = false
            self.profileimageActivityIndicator.isHidden = false
            self.profileimageActivityIndicator.startAnimating()
            let rzimage = resizeImage(image: selectedimage!, newWidth:150)
            
            let imageData: NSData = UIImagePNGRepresentation(rzimage)! as NSData
            QBRequest.tUploadFile(imageData as Data, fileName: "ProfileImageofPatientRelation", contentType: "image/png", isPublic: true, successBlock:
                {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                    let userParams = QBUpdateUserParameters()
                    userParams.blobID = uploadedBlob.id

                    let url: String = uploadedBlob.publicUrl()!
                    print("AddProfileViewControllerProfileImageButton():puburl = \(url)")
                    self.theprofileurl = url
                    self.imagefileUID = uploadedBlob.uid!

                    SDImageCache.shared().store(selectedimage, forKey: url, completion: {

                    })
                    self.docimg.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "profile"))
                    
                    // make profile image url persistance
                    let defaults: UserDefaults? = UserDefaults.standard
                    defaults?.set(URL(string: url), forKey: "keyforProfileurl")
                    defaults?.synchronize()
                    // Status
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                DispatchQueue.main.async {
                    print(" ProfileImageButton() percent = \(String(describing: status?.percentOfCompletion))")
                    if (status!.percentOfCompletion >= 0.5) {
                        self.profileimageActivityIndicator.isHidden = true
                        self.profileimageActivityIndicator.stopAnimating()
                    }
                }
            },
               errorBlock:
                {(response: QBResponse!) in
                    // NSLog("error: %@", response.error)
                    print("profile image QBRequest tUploadFile response Error \(String(describing: response))")
                    self.profileimageActivityIndicator.isHidden = true
                    self.profileimageActivityIndicator.stopAnimating()
            })
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        //give room at the bottom of the scroll view, so it doesn't cover up anything the user needs to tap
        
        guard let userInfo = notification.userInfo,
            let frame =  (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height+75, right: 0)
        self.scrollview.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        self.scrollview.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    

    @IBAction func OkButtonClicked(_ sender: UIButton) {

        guard let firstName = firstnametxt.text, firstName.count > 0 else {
            showAlert(msg: "Please enter first name")
            return
        }
        
        guard let lastName = lastnametxt.text, lastName.count > 0 else {
            showAlert(msg: "Please enter last name")
            return
        }
        
        guard let displayName = dispaynametxt.text, displayName.count > 0 else {
            showAlert(msg: "Please enter 8 character display name")
            return
        }
        
        guard let gender = gendertxt.text, gender.count > 0 else {
            showAlert(msg: "Please enter gender")
            return
        }
        
        guard let age = agetxt.text, age.count > 0 else {
            showAlert(msg: "Please enter age")
            return
        }
        
        guard let blood = bloogptxt.text, blood.count > 0 else {
            showAlert(msg: "Please enter blood group")
            return
        }
        
        guard let heightft = heightfttxt.text, heightft.count > 0 else {
            showAlert(msg: "Please enter hight in feet")
            return
        }
        
        guard let heightinch = heightinchtxt.text, heightinch.count > 0 else {
            showAlert(msg: "Please enter hight in inch")
            return
        }
        
        guard let weight = weighttxt.text, weight.count > 0 else {
            showAlert(msg: "Please enter weight")
            return
        }
        
        guard let relation = relationshiptxt.text, relation.count > 0 else {
            showAlert(msg: "Please enter your relationship")
            return
        }
        
        guard let allergies = allergiestxtview.text/*, allergies.count > 0 */else {
            showAlert(msg: "Please enter your allergies")
            return
        }
        
        guard let chronichealth = chronichealthetxtview.text/*, chronichealth.count > 0*/ else {
            showAlert(msg: "Please enter your chronichealth")
            return
        }
        
        guard let currentmedication = currentmedicationtxtview.text/*, currentmedication.count > 0*/ else {
            showAlert(msg: "Please enter your currentmedication")
            return
        }
        
        if (theprofileurl.isEmpty == true) {
            showAlert(msg: "Please select profile image")
            return
        }
        
        GenerateCallDocCode()

        patientNames.append(firstName + "" + lastName)
        
        addUserDatatoBackend(fname:firstName, lname:lastName,displayname: displayName, gender:gender, age:age, blood:blood, heightft:heightft, heightinch:heightinch, weight:weight, relationship:relation, allergies:allergies, chronichealth:chronichealth, currentmedication:currentmedication )
    }
    
    func showAlert(msg: String) {
        let cntrl = UIAlertController(title: msg, message: nil, preferredStyle: .alert)
        cntrl.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(cntrl, animated: true, completion: nil)
    }

    func addUserDatatoBackend(fname:String, lname:String, displayname: String, gender:String, age:String, blood:String, heightft:String, heightinch:String, weight:String, relationship:String, allergies:String, chronichealth:String, currentmedication:String) {

        let userqb = QBUUser()
        userqb.password = "family_f" + "1"
        userqb.login = thefinalcode//stringWithoutpluswith_p
        userqb.fullName = fname + " " + lname//fullname
        //userqb.phone = usermobilenumber
        //userqb.tags = [@[@"Pat0", @"IOS"] mutableCopy];
        
        QBRequest.signUp(userqb, successBlock: { (response, user) in
            print("user\(user)")
            
            let object = QBCOCustomObject()
            object.className = "PatPrimaryDetailsTable"
            object.fields["firstname"] = fname
            object.fields["lastname"] = lname
            object.fields["displayname"] = displayname
            object.fields["email"] = GlobalDocdata.email
            object.fields["gender"] = gender
            object.fields["age"] = age
            object.fields["heightfeet"] = heightft
            object.fields["heightinch"] = heightinch
            object.fields["weight"] = weight
            object.fields["bloodgp"] = blood
            object.fields["allergies"] = allergies
            object.fields["chronichealthissue"] = chronichealth
            object.fields["currentmed"] = currentmedication
            object.fields["relation"] =  relationship
            object.fields["calldoccode"] = user.login!//thefinalcode
            object.fields["profileimageURL"] = self.theprofileurl //self.imagefileUID
            object.fields["userid"] = user.id
            
            QBRequest.createObject(object, successBlock: { (response, contributors) in
             //Handle Success
             // we are creating the record ID for doctors Primary details table , this record = customobjIDPrimarydetails will be used later to update record
             UserDefaults.standard.setValue((contributors?.id)!, forKey: "customobjIDprofile")
             UserDefaults.standard.synchronize()
             print("AddProfileViewController:adduserdatatobackend: Successfully created !")
             
             guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
             self.nav = UINavigationController(rootViewController: homeViewController)
             self.present(self.nav!, animated: false, completion: nil)
             
             }) { (response) in
             //Handle Error
             print("PrimaryDetailsViewController:CreateCustomObjectforPrimaryDetails: Response error: \(String(describing: response.error?.description))")
             }
            
        }) { (response) in
             print("PrimaryDetailsViewController: Response error: \(String(describing: response.error?.description))")
        }
        
    }
    
    //Generare CallDoc Code
    func GenerateCallDocCode() {
        print( "PrimaryDetailsViewController: Generating CallDoc Code which has to be unique")
        let myCode = ShortCodeGenerator.getCode(length: 4)
        let firstNamefirstchar = firstnametxt.text?.substring(to:(firstnametxt.text?.index((firstnametxt.text?.startIndex)!, offsetBy: 1))!)
        let lastNamefirstchar = lastnametxt.text?.substring(to:(lastnametxt.text?.index((lastnametxt.text?.startIndex)!, offsetBy: 1))!)
        let code = firstNamefirstchar!  + myCode + lastNamefirstchar!
        thefinalcode = code.uppercased()
    }

}
