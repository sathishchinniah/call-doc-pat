import UIKit
import CoreTelephony
import SafariServices
import AVKit
import Toast_Swift

var messageTimeDateFormatter: DateFormatter {
    struct Static {
        static let instance : DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            return formatter
        }()
    }
    return Static.instance
}

extension String {
    var length: Int {
        return (self as NSString).length
    }
}

class ChatViewController: QMChatViewController, QMChatServiceDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QMChatAttachmentServiceDelegate, QMChatConnectionDelegate, QMChatCellDelegate, QMDeferredQueueManagerDelegate, QMPlaceHolderTextViewPasteDelegate {
    
    
    
    let maxCharactersNumber = 1024
    var consultrecordIDpassed = ""
    var docprofileimageurl = ""
    var dialog: QBChatDialog!
    var willResignActiveBlock: AnyObject?
    var attachmentCellsMap: NSMapTable<AnyObject, AnyObject>!
    var detailedCells: Set<String> = []
    var opponentDoctorId = 0 as UInt
    var profPicURL = ""
    var indexVal = 0
    var pagerIndex = 0
    
    var typingTimer: Timer?
    var popoverController: UIPopoverController?
    
    lazy var imagePickerViewController : UIImagePickerController = {
        let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.delegate = self
        
        return imagePickerViewController
    }()
    
    var unreadMessages: [QBChatMessage]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8117647059, green: 0.8666666667, blue: 0.9333333333, alpha: 1)
        self.navigationController?.navigationBar.topItem?.title = "Chat"
        ServicesManager.instance().chatService.addDelegate(self) //add this line
        ServicesManager.instance().chatService.chatAttachmentService.addDelegate(self) // add this line
        
        self.topContentAdditionalInset = ((self.navigationController?.navigationBar.frame.size.height) ?? 0) + UIApplication.shared.statusBarFrame.size.height
        
        view.backgroundColor = UIColor.white
        self.collectionView?.backgroundColor = .clear

        //Callbutton
        let btn = UIButton(frame: CGRect(x: 1, y: (self.view.frame.height/2)+100, width: 42, height: 85))
        btn.setImage(#imageLiteral(resourceName: "toCall"), for: .normal)
        btn.addTarget(self, action: #selector(self.toCallButtonTouchUpInside), for: .touchUpInside)
        self.view.addSubview(btn)
        
        //left side Back Button
        let btnImage = UIImage(named: "backBtn-1")
        let leftbtn = UIButton(type: .custom)
        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
        //leftbtn.bounds = CGRect(x: 50, y: 50, width: 45, height: 45)
        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
        leftbtn.setImage(btnImage, for: .normal)
        let backButton = UIBarButtonItem(customView: leftbtn)
        self.navigationItem.leftBarButtonItem = backButton
        
        // Right side Profile Button
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageview.sd_setImage(with: URL(string: patientimageurl[gindexval]), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
        imageview.contentMode = UIViewContentMode.scaleToFill
        imageview.layer.cornerRadius = 20
        imageview.layer.masksToBounds = true
        containView.addSubview(imageview)
        let rightBarButton = UIBarButtonItem(customView: containView)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        
        self.senderID = GlobalVariables.gCurrentUser.id
        self.senderDisplayName =  (QBSession.current.currentUser?.fullName) ?? ""
        self.updateTitle()
        
        self.inputToolbar?.contentView?.backgroundColor = UIColor.white
        self.collectionView?.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.968627451, blue: 0.9803921569, alpha: 1)
        self.inputToolbar?.contentView?.textView?.placeHolder = "Message"
        
        self.attachmentCellsMap = NSMapTable(keyOptions: NSPointerFunctions.Options.strongMemory, valueOptions: NSPointerFunctions.Options.weakMemory)
        
        if self.dialog?.type == QBChatDialogType.private {
            self.dialog?.onUserIsTyping = {
                [weak self] (userID)-> Void in
                if ServicesManager.instance().currentUser.id == userID {
                    return
                }
                self?.title = "Typing"
            }
            self.dialog?.onUserStoppedTyping = {
                [weak self] (userID)-> Void in
                if ServicesManager.instance().currentUser.id == userID {
                    return
                }
                self?.updateTitle()
            }
        }
        // Retrieving messages
        if (self.storedMessages()?.count ?? 0 > 0 && self.chatDataSource.messagesCount() == 0) {
            self.chatDataSource.add(self.storedMessages()!)
        }
        self.loadMessages()
        self.enableTextCheckingTypes = NSTextCheckingAllTypes
        let nav = self.navigationController?.navigationBar
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.orange]
        
        // load Patient profile selected related data
        GlobalVariables.gcurrentprofilename = patientNames.count > gindexval ? patientNames[gindexval] : ""
        GlobalVariables.gcurrentprofilerelation = patrelation.count > gindexval ? patrelation[gindexval] : ""
        GlobalVariables.gcurrentprofilecalldoccode = patientcdcode.count > gindexval ? patientcdcode[gindexval] : ""
        GlobalVariables.gcurrentprofileURL = patientimageurl.count > gindexval ? patientimageurl[gindexval] : ""
        GlobalVariables.gcurrentprofilegender = patientgender.count > gindexval ? patientgender[gindexval] : ""
        GlobalVariables.gcurrentprofileage = patientage.count > gindexval ? patientage[gindexval] : ""
        GlobalVariables.gcurrentprofilecommand = "start"
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //self.navigationController?.navigationBar.barTintColor = UIColor.red
        //self.navigationController?.navigationBar.frame = CGRect(x: 150.0, y: 150.0, width: self.view.frame.width, height: 244.0)
    }
    
    
    
    @IBAction func toCallButtonTapped(_ sender: Any) {
    }
    
    @objc func toCallButtonTouchUpInside(_ sender: Any) {
        let docselected =  (DoctorsArray[indexVal].fields?.value(forKey: "doctoruserid") as? UInt)!
        let user = QBUUser()
        user.id =   docselected
        let VC1 = CallViewControllerswft.storyboardInstance()
        VC1.callingnumber = docselected
        VC1.doctorname = (DoctorsArray[indexVal].fields?.value(forKey: "doctorfullname") as? String) ?? ""
        if (PatientConsultsdetailsArray.count ) > 0 {
             VC1.consultrecordIDpassed = (PatientConsultsdetailsArray[0].id) ?? ""
        }
       
        VC1.callingPatientName = GlobalVariables.gcurrentprofilename//currentPatientName
        VC1.pagerIndex = pagerIndex
        VC1.profileImage = (DoctorsArray[indexVal].fields?.value(forKey: "doctorprofileimageURL") as? String) ?? ""
        self.navigationController?.setViewControllers([VC1], animated: true)
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    @objc func backBtnPressed(_ sender: Any) {
        
        if ((self.parent as! UINavigationController).viewControllers.first) == self {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        let alert = UIAlertController(title: "", message: "Your Consult with Dr.  \(self.dialog.name ?? "Chat") is completed, Do you want to intimate Doctor to close this consult ??", preferredStyle: .alert)
        let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
        imageView.layer.cornerRadius = imageView.frame.size.width / 2;
        imageView.clipsToBounds = true;
        imageView.layer.borderWidth = 0.2
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.sd_setImage(with: URL(string: docprofileimageurl), placeholderImage: UIImage(named: "profile"))
        alert.view.addSubview(imageView)
        let ok = UIAlertAction(title: "YES", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //Send chat message to Doctor to close the consult
            let message = QBChatMessage()
            message.text = "Close the Consult"
            message.senderID = self.senderID
            message.deliveredIDs = [(NSNumber(value: self.senderID))]
            message.readIDs = [(NSNumber(value: self.senderID))]
            message.markable = true
            message.dateSent = Date()
            //Manish:22/7/12 - Sending Custom data to Doctor Side because we need to themm him specific for consult related
            //patientuserid, patfullname, patrelation, patcalldoccode, patprofileid, gender, age
            message.customParameters = ["patientuserid": self.senderID, "patfullname": GlobalVariables.gcurrentprofilename, "patrelation": GlobalVariables.gcurrentprofilerelation, "patcalldoccode": GlobalVariables.gcurrentprofilecalldoccode, "patprofileid": GlobalVariables.gcurrentprofileURL, "gender": GlobalVariables.gcurrentprofilegender, "age": GlobalVariables.gcurrentprofileage, "command": "close" ]
            print("ChatViewController:backBtnPressed: \(message)")
            self.sendMessageWithCloseCommand(message: message)
            
        })
        let no = UIAlertAction(title: "NO", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // continue with chat consult
            
            let navBarColor = self.navigationController!.navigationBar
            // navBarColor.topItem?.title = "Consult Details"
            self.title = "Consult Details"
            navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
            navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(ok)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.queueManager().add(self)
        
        self.willResignActiveBlock = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillResignActive, object: nil, queue: nil) { [weak self] (notification) in
            self?.fireSendStopTypingIfNecessary()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = "Consult Details"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Saving current dialog ID.
        ServicesManager.instance().currentDialogID = (self.dialog?.id) ?? ""
        
        // Retrieving messages
        if (self.storedMessages()?.count ?? 0 > 0 && self.chatDataSource.messagesCount() == 0) {
            
            self.chatDataSource.add(self.storedMessages()!)
        }
        self.loadMessages()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let willResignActive = self.willResignActiveBlock {
            NotificationCenter.default.removeObserver(willResignActive)
        }
        
        // Resetting current dialog ID.
        ServicesManager.instance().currentDialogID = ""
        
        // clearing typing status blocks
        self.dialog?.clearTypingStatusBlocks()
        
        self.queueManager().remove(self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        let keyboardSize = (sender.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        self.view.frame.origin.y = self.view.frame.origin.y - (keyboardSize?.height)! //150
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        let keyboardSize = (sender.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        self.view.frame.origin.y = self.view.frame.origin.y + (keyboardSize?.height)!//150
    }
    
    // MARK: Update
    
    func updateTitle() {
        
        if self.dialog?.type == QBChatDialogType.private {
            self.title = "Dr. " + self.dialog.name!
        } else {
            if let recipient = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt((self.dialog?.recipientID) ?? 0)) {
                self.title = recipient.login
            }
        }
    }
    
    func storedMessages() -> [QBChatMessage]? {
        return ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: (self.dialog?.id) ?? "")
    }
    
    func loadMessages() {
        // Retrieving messages for chat dialog ID.
        guard let currentDialogID = self.dialog?.id else {
            print ("Current chat dialog is nil")
            return
        }
        
        ServicesManager.instance().chatService.messages(withChatDialogID: currentDialogID, completion: {
            [weak self] (response, messages) -> Void in
            
            guard let strongSelf = self else { fatalError("Error") }
            
            guard response.error == nil else {
                SVProgressHUD.showError(withStatus: response.error?.error?.localizedDescription)
                return
            }
            
            if messages?.count ?? 0 > 0 {
                strongSelf.chatDataSource.add(messages)
            }
            
            SVProgressHUD.dismiss()
        })
    }
    
    func sendReadStatusForMessage(message: QBChatMessage) {
        
        guard QBSession.current.currentUser != nil else {
            return
        }
        guard message.senderID != QBSession.current.currentUser?.id else {
            return
        }
        
        if self.messageShouldBeRead(message: message) {
            ServicesManager.instance().chatService.read(message, completion: { (error) -> Void in
                guard error == nil else {
                    print("Problems while marking message as read! Error: %@", error!)
                    return
                }
                if UIApplication.shared.applicationIconBadgeNumber > 0 {
                    let badgeNumber = UIApplication.shared.applicationIconBadgeNumber
                    UIApplication.shared.applicationIconBadgeNumber = badgeNumber - 1
                }
            })
        }
    }
    
    func messageShouldBeRead(message: QBChatMessage) -> Bool {
        
        let currentUserID = NSNumber(value: QBSession.current.currentUser!.id as UInt)
        return !message.isDateDividerMessage
            && message.senderID != self.senderID
            && !(message.readIDs?.contains(currentUserID))!
    }
    
    func readMessages(messages: [QBChatMessage]) {
        
        if QBChat.instance.isConnected {
            
            ServicesManager.instance().chatService.read(messages, forDialogID: self.dialog.id!, completion: nil)

        } else {
            
            self.unreadMessages = messages
        }
        var messageIDs = [String]()
        for message in messages {
            messageIDs.append(message.id!)
        }
    }
    
    // MARK: Actions
    
    override func didPickAttachmentImage(_ image: UIImage!) {
        /*
        let message = QBChatMessage()
        message.senderID = self.senderID
        message.dialogID = self.dialog.id
        message.dateSent = Date()
        
        DispatchQueue.global().async { [weak self] () -> Void in
            
            guard let strongSelf = self else { fatalError("Error") }
            
            var newImage : UIImage! = image
            if strongSelf.imagePickerViewController.sourceType == UIImagePickerControllerSourceType.camera {
                newImage = newImage.fixOrientation()
            }
            
            let largestSide = newImage.size.width > newImage.size.height ? newImage.size.width : newImage.size.height
            let scaleCoeficient = largestSide/560.0
            let newSize = CGSize(width: newImage.size.width/scaleCoeficient, height: newImage.size.height/scaleCoeficient)
            
            // create smaller image
            
            UIGraphicsBeginImageContext(newSize)
            
            newImage.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            let attahcment = QBChatAttachment()
            //attahcment.url =
            message.attachments?.append(attahcment)
            UIGraphicsEndImageContext()
            
            // Sending attachment.
            DispatchQueue.main.async(execute: {
                self?.chatDataSource.add(message)
                // sendAttachmentMessage method always firstly adds message to memory storage
                ServicesManager.instance().chatService.sendAttachmentMessage(message, to: self!.dialog, withAttachmentImage: resizedImage!, completion: {
                    [weak self] (error) -> Void in
                    
                    self?.attachmentCellsMap.removeObject(forKey: message.id as AnyObject?)
                    
                    guard error == nil else {
                        self?.chatDataSource.delete(message)
                        fatalError("Error") }
                    
                    
                })
            })
        } */
        DispatchQueue.global().async { [weak self] () -> Void in
            
            guard let strongSelf = self else { fatalError("Error") }
            
            var newImage : UIImage! = image
            if strongSelf.imagePickerViewController.sourceType == UIImagePickerControllerSourceType.camera {
                newImage = newImage.fixOrientation()
            }
            
            let largestSide = newImage.size.width > newImage.size.height ? newImage.size.width : newImage.size.height
            let scaleCoeficient = largestSide/560.0
            let newSize = CGSize(width: newImage.size.width/scaleCoeficient, height: newImage.size.height/scaleCoeficient)
            
            // create smaller image
            
            UIGraphicsBeginImageContext(newSize)
            
            newImage.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            let imageData = UIImagePNGRepresentation(resizedImage!)
            
            QBRequest.tUploadFile(imageData!, fileName: "attachment\(Date())", contentType: "image/png", isPublic: false,
                                  successBlock: {(response: QBResponse!, uploadedBlob: QBCBlob!) in
                                    // Create and configure message
                                    let message: QBChatMessage = QBChatMessage()
                                    
                                    let uploadedFileID: UInt = uploadedBlob.id
                                    let attachment: QBChatAttachment = QBChatAttachment()
                                    attachment.type = "image"
                                    attachment.id = String(uploadedFileID)
                                    attachment.url = String(describing: uploadedBlob!.privateUrl())
                                    
                                    message.attachments = [attachment]
                                    message.senderID = (self?.senderID) ?? 0
                                    message.dialogID = self?.dialog.id
                                    message.dateSent = Date()
                                    
                                    // Send message
                                    DispatchQueue.main.async(execute: {
                                        self?.chatDataSource.add(message)
                                        // sendAttachmentMessage method always firstly adds message to memory storage
                                        ServicesManager.instance().chatService.sendAttachmentMessage(message, to: self!.dialog, withAttachmentImage: resizedImage!, completion: {
                                            [weak self] (error) -> Void in
                                            
                                            self?.attachmentCellsMap.removeObject(forKey: message.id as AnyObject?)
                                            
                                            guard error == nil else {
                                                self?.chatDataSource.delete(message)
                                                fatalError("Error") }
                                            
                                            
                                        })
                                    })
                                    
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                
            }, errorBlock: {(response: QBResponse!) in
                
                print(response.error?.reasons)
            })
        }
        
    }

    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: UInt, senderDisplayName: String!, date: Date!) {
        
        if !self.queueManager().shouldSendMessagesInDialog(withID: self.dialog.id!) {
            return
        }
        self.fireSendStopTypingIfNecessary()
        
        let message = QBChatMessage()
        message.text = text
        message.senderID = self.senderID
        message.deliveredIDs = [(NSNumber(value: self.senderID))]
        message.readIDs = [(NSNumber(value: self.senderID))]
        message.markable = true
        message.dateSent = date
        message.customParameters = ["patientuserid": self.senderID, "patfullname": GlobalVariables.gcurrentprofilename, "patrelation": GlobalVariables.gcurrentprofilerelation, "patcalldoccode": GlobalVariables.gcurrentprofilecalldoccode, "patprofileid": GlobalVariables.gcurrentprofileURL, "gender": GlobalVariables.gcurrentprofilegender, "age": GlobalVariables.gcurrentprofileage, "command": GlobalVariables.gcurrentprofilecommand ]
        print("ChatViewController:didPressSend: \(message)")
        
        self.sendMessage(message: message)
        
        GlobalVariables.gcurrentprofilecommand = "contine"
    }
    
    override func didPressSend(_ button: UIButton!, withTextAttachments textAttachments: [Any]!, senderId: UInt, senderDisplayName: String!, date: Date!) {
        
        if let attachment = textAttachments.first as? NSTextAttachment {
            
            if (attachment.image != nil) {
                let message = QBChatMessage()
                message.senderID = self.senderID
                message.dialogID = self.dialog.id
                message.dateSent = Date()
                ServicesManager.instance().chatService.sendAttachmentMessage(message, to: self.dialog, withAttachmentImage: attachment.image!, completion: {
                    [weak self] (error: Error?) -> Void in
                    
                    self?.attachmentCellsMap.removeObject(forKey: message.id as AnyObject?)
                    
                    guard error != nil else { fatalError("Error") }
                    
                    // perform local attachment message deleting if error
                    ServicesManager.instance().chatService.deleteMessageLocally(message)
                    
                    self?.chatDataSource.delete(message)
                    
                })
                
                self.finishSendingMessage(animated: true)
            }
        }
    }
    
    func sendMessage(message: QBChatMessage) {
        
        // Sending message.
        // ServicesManager.instance().chatService.send(message, toDialogID: self.dialog.id!, saveToHistory: true, saveToStorage: true)
        ServicesManager.instance().chatService.send(message, type: QMMessageType.text, to: self.dialog, saveToHistory: true, saveToStorage: true){ (error) ->
            Void in
            
            if error != nil {
                QMMessageNotificationManager.showNotification(withTitle: "Error", subtitle: error?.localizedDescription, type: QMMessageNotificationType.warning)
            }
        }
        createPushNotification()
        if (Date().timeIntervalSince((self.dialog.lastMessageDate) ?? Date()) > 3600) {
            createPushNotification()
        }
        self.finishSendingMessage(animated: true)
    }
    
    func createPushNotification() {
        
        guard let _ = self.dialog else { return }
        
        let payload = NSMutableDictionary()
        let patName = QBSession.current.currentUser?.fullName ?? ""
        let alertDict = ["alert":"New message from \(String(describing: patName))","sound":"default"]

        let opponentsIDs = String(opponentDoctorId)//(self.session?.opponentsIDs as NSArray?)?.componentsJoined(by: ",")

        payload.setValue("Video turn on Response", forKey: "alert")
        payload.setValue("TITLE", forKey: "title")
        payload.setValue(201, forKey: "code")
        payload.setValue(alertDict, forKey: "aps")
//        payload.setValue("5", forKey: "ios_badge") //message,type consults,patId, patName,profileUid,calldoccode,consultid,consulttableid, prescriptionid
//        payload.setValue("mysound.wav", forKey: "ios_sound")
//        payload.setValue("New Chat Message", forKey: "ios_alert")
        payload.setValue(String(Int((self.dialog.userID) )), forKey: "user_id")
        payload.setValue("10", forKey: "thread_id")
        payload.setValue(QBSession.current.currentUser?.fullName, forKey: "patientName")
        payload.setValue(GlobalVariables.gCurrentUser.id, forKey: "patientId")
        payload.setValue(/*self.profPicURL*/GlobalVariables.gcurrentprofileURL, forKey: "patProfilePic")
        //payload.setValue(GlobalVariables.gCurrentUser, forKey: "patientDetails")
        
        
        let data = try? JSONSerialization.data(withJSONObject: payload, options: .prettyPrinted)
        
        let message = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = opponentsIDs
        event.type = QBMEventType.oneShot
        event.message = message
        
        var style = ToastStyle()
        style.messageColor = .white
        ToastManager.shared.style = style
        

        
        QBRequest.createEvent(event, successBlock: { (_, _) in
            print("EVENT CREATED SUCCESSFULLY")
            self.view.makeToast("Notification sent to the doctor", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }) { (error:QBResponse?) in
            print("ERROR: \(String(describing: error))")
//            self.view.makeToast("Couldn't notify doctor. Please try again later.", duration: 4.0, position: .center, title: nil, image: nil, style: style, completion: nil)
        }
    }
    
    func sendMessageWithCloseCommand(message: QBChatMessage) {
        
        // Sending message.
        ServicesManager.instance().chatService.send(message, type: QMMessageType.text, to: self.dialog, saveToHistory: true, saveToStorage: true){ (error) ->
            Void in
            
            if error != nil {
                QMMessageNotificationManager.showNotification(withTitle: "Error", subtitle: error?.localizedDescription, type: QMMessageNotificationType.warning)
            }
        }
        
        self.finishSendingMessage(animated: true)
        
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController")
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated:true, completion: nil)
    }
    
    // MARK: Helper
    func canMakeACall() -> Bool {
        
        var canMakeACall = false
        
        if (UIApplication.shared.canOpenURL(URL.init(string: "tel://")!)) {
            
            // Check if iOS Device supports phone calls
            let networkInfo = CTTelephonyNetworkInfo()
            let carrier = networkInfo.subscriberCellularProvider
            if carrier == nil {
                return false
            }
            let mnc = carrier?.mobileNetworkCode
            if mnc?.length == 0 {
                // Device cannot place a call at this time.  SIM might be removed.
            } else {
                // iOS Device is capable for making calls
                canMakeACall = true
            }
        } else {
            // iOS Device is not capable for making calls
        }
        
        return canMakeACall
    }
    
    func placeHolderTextView(_ textView: QMPlaceHolderTextView, shouldPasteWithSender sender: Any) -> Bool {
        
        if UIPasteboard.general.image != nil {
            
            let textAttachment = NSTextAttachment()
            textAttachment.image = UIPasteboard.general.image!
            textAttachment.bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
            
            let attrStringWithImage = NSAttributedString.init(attachment: textAttachment)
            self.inputToolbar?.contentView.textView.attributedText = attrStringWithImage
            self.textViewDidChange((self.inputToolbar?.contentView.textView)!)
            
            return false
        }
        
        return true
    }
    
    func showCharactersNumberError() {
        let title  = "Error";
        let subtitle = String(format: "The character limit is %lu.", maxCharactersNumber)
        QMMessageNotificationManager.showNotification(withTitle: title, subtitle: subtitle, type: .error)
    }
    
    /**
     Builds a string
     Read: login1, login2, login3
     Delivered: login1, login3, @12345
     
     If user does not exist in usersMemoryStorage, then ID will be used instead of login
     
     - parameter message: QBChatMessage instance
     
     - returns: status string
     */
    func statusStringFromMessage(message: QBChatMessage) -> String {
        
        var statusString = ""
        
        let currentUserID = NSNumber(value:self.senderID)
        
        var readLogins: [String] = []
        
        if message.readIDs != nil {
            
            let messageReadIDs = message.readIDs!.filter { (element) -> Bool in
                
                return !element.isEqual(to: currentUserID)
            }
            
            if !messageReadIDs.isEmpty {
                for readID in messageReadIDs {
                    let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(readID))
                    
                    guard let unwrappedUser = user else {
                        let unknownUserLogin = "@\(readID)"
                        readLogins.append(unknownUserLogin)
                        
                        continue
                    }
                    
                    readLogins.append(unwrappedUser.login!)
                }
                
                statusString += message.isMediaMessage() ? "Seen" : "Read";
                statusString += ": " + readLogins.joined(separator: ", ")
            }
        }
        
        if message.deliveredIDs != nil {
            var deliveredLogins: [String] = []
            
            let messageDeliveredIDs = message.deliveredIDs!.filter { (element) -> Bool in
                return !element.isEqual(to: currentUserID)
            }
            
            if !messageDeliveredIDs.isEmpty {
                for deliveredID in messageDeliveredIDs {
                    let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(deliveredID))
                    
                    guard let unwrappedUser = user else {
                        let unknownUserLogin = "@\(deliveredID)"
                        deliveredLogins.append(unknownUserLogin)
                        
                        continue
                    }
                    
                    if readLogins.contains(unwrappedUser.login!) {
                        continue
                    }
                    
                    deliveredLogins.append(unwrappedUser.login!)
                    
                }
                
                if readLogins.count > 0 && deliveredLogins.count > 0 {
                    statusString += "\n"
                }
                
                if deliveredLogins.count > 0 {
                    statusString += "Delivered" + ": " + deliveredLogins.joined(separator: ", ")
                }
            }
        }
        
        if statusString.isEmpty {
            
            let messageStatus: QMMessageStatus = self.queueManager().status(for: message)
            
            switch messageStatus {
            case .sent:
                statusString = "Sent"
            case .sending:
                statusString = "Sending"
            case .notSent:
                statusString = "Didn't send"
            }
            
        }
        
        return statusString
    }
    
    // MARK: Override
    
    override func viewClass(forItem item: QBChatMessage) -> AnyClass {
        // TODO: check and add QMMessageType.AcceptContactRequest, QMMessageType.RejectContactRequest, QMMessageType.ContactRequest
        
        if item.isDateDividerMessage {
            return QMChatNotificationCell.self
        }
        
        if (item.senderID != self.senderID) {
            
            if (item.isMediaMessage() && item.attachmentStatus != QMMessageAttachmentStatus.error) {
                
                return QMChatAttachmentIncomingCell.self
                
            } else {
                
                return QMChatIncomingCell.self
            }
            
        } else {
            
            if (item.isMediaMessage() && item.attachmentStatus != QMMessageAttachmentStatus.error) {
                
                return QMChatAttachmentOutgoingCell.self
                
            } else {
                
                return QMChatOutgoingCell.self
            }
        }
    }
    
    // MARK: Strings builder
    
    override func attributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString? {
        
        guard messageItem.text != nil else {
            return nil
        }
        
        var textColor = messageItem.senderID == self.senderID ? UIColor.white : UIColor.black
        if messageItem.isDateDividerMessage {
            textColor = UIColor.black
        }
        
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = textColor
        attributes[NSAttributedStringKey.font] = UIFont(name: "Helvetica", size: 17)
        
        let attributedString = NSAttributedString(string: messageItem.text!, attributes: attributes)
        
        return attributedString
    }
    
    
    /**
     Creates top label attributed string from QBChatMessage
     
     - parameter messageItem: QBCHatMessage instance
     
     - returns: login string, example: @SwiftTestDevUser1
     */
    override func topLabelAttributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString? {
        
        guard messageItem.senderID != self.senderID else {
            return nil
        }
        
        guard self.dialog.type != QBChatDialogType.private else {
            return nil
        }
        
        let paragrpahStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = UIColor(red: 11.0/255.0, green: 96.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        attributes[NSAttributedStringKey.font] = UIFont(name: "Helvetica", size: 17)
        attributes[NSAttributedStringKey.paragraphStyle] = paragrpahStyle
        
        var topLabelAttributedString : NSAttributedString?
        
        if let topLabelText = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: messageItem.senderID)?.login {
            topLabelAttributedString = NSAttributedString(string: topLabelText, attributes: attributes)
        } else { // no user in memory storage
            topLabelAttributedString = NSAttributedString(string: "@\(messageItem.senderID)", attributes: attributes)
        }
        
        return topLabelAttributedString
    }
    
    /**
     Creates bottom label attributed string from QBChatMessage using self.statusStringFromMessage
     
     - parameter messageItem: QBChatMessage instance
     
     - returns: bottom label status string
     */
    override func bottomLabelAttributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString! {
        
        let textColor = messageItem.senderID == self.senderID ? UIColor.white : UIColor.black
        
        let paragrpahStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = textColor
        attributes[NSAttributedStringKey.font] = UIFont(name: "Helvetica", size: 13)
        attributes[NSAttributedStringKey.paragraphStyle] = paragrpahStyle
        
        var text = messageItem.dateSent != nil ? messageTimeDateFormatter.string(from: messageItem.dateSent!) : ""
        
        if messageItem.senderID == self.senderID {
            text = text + "\n" + self.statusStringFromMessage(message: messageItem)
        }
        
        let bottomLabelAttributedString = NSAttributedString(string: text, attributes: attributes)
        
        return bottomLabelAttributedString
    }
    
    // MARK: Collection View Datasource
    
    override func collectionView(_ collectionView: QMChatCollectionView!, dynamicSizeAt indexPath: IndexPath!, maxWidth: CGFloat) -> CGSize {
        
        var size = CGSize.zero
        
        guard let message = self.chatDataSource.message(for: indexPath) else {
            return size
        }
        
        let messageCellClass: AnyClass! = self.viewClass(forItem: message)
        
        
        if messageCellClass === QMChatAttachmentIncomingCell.self {
            
            size = CGSize(width: min(200, maxWidth), height: 200)

        } else if messageCellClass === QMChatAttachmentOutgoingCell.self {
            
            let attributedString = self.bottomLabelAttributedString(forItem: message)
            
            let bottomLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: min(200, maxWidth), height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            size = CGSize(width: min(200, maxWidth), height: 200 + ceil(bottomLabelSize.height))

        } else if messageCellClass === QMChatNotificationCell.self {
            
            let attributedString = self.attributedString(forItem: message)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)

        } else {
            
            let attributedString = self.attributedString(forItem: message)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
        }
        
        return size
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, minWidthAt indexPath: IndexPath!) -> CGFloat {
        
        var size = CGSize.zero
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return 0
        }
        
        if self.detailedCells.contains(item.id!) {
            
            let str = self.bottomLabelAttributedString(forItem: item)
            let frameWidth = collectionView.frame.width
            let maxHeight = CGFloat.greatestFiniteMagnitude
            
            size = TTTAttributedLabel.sizeThatFitsAttributedString(str, withConstraints: CGSize(width:frameWidth - kMessageContainerWidthPadding, height: maxHeight), limitedToNumberOfLines:0)
        }
        
        if self.dialog.type != QBChatDialogType.private {
            
            let topLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(self.topLabelAttributedString(forItem: item), withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
            
            if topLabelSize.width > size.width {
                size = topLabelSize
            }
        }
        
        return size.width
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, layoutModelAt indexPath: IndexPath!) -> QMChatCellLayoutModel {
        
        var layoutModel: QMChatCellLayoutModel = super.collectionView(collectionView, layoutModelAt: indexPath)
        
        layoutModel.avatarSize = CGSize(width: 42, height: 42)
        layoutModel.topLabelHeight = 0.0
        layoutModel.spaceBetweenTextViewAndBottomLabel = 5
        layoutModel.maxWidthMarginSpace = 20.0
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return layoutModel
        }
        
        let viewClass: AnyClass = self.viewClass(forItem: item) as AnyClass
        
        if viewClass === QMChatIncomingCell.self || viewClass === QMChatAttachmentIncomingCell.self {
            
            if self.dialog.type != QBChatDialogType.private {
                let topAttributedString = self.topLabelAttributedString(forItem: item)
                let size = TTTAttributedLabel.sizeThatFitsAttributedString(topAttributedString, withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:1)
                layoutModel.topLabelHeight = size.height
            }
            
            layoutModel.spaceBetweenTopLabelAndTextView = 5
        }
        
        var size = CGSize.zero
        
        if self.detailedCells.contains(item.id!) {
            
            let bottomAttributedString = self.bottomLabelAttributedString(forItem: item)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(bottomAttributedString, withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)
        }
        
        layoutModel.bottomLabelHeight = floor(size.height)

        return layoutModel
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, configureCell cell: UICollectionViewCell!, for indexPath: IndexPath!) {
        
        super.collectionView(collectionView, configureCell: cell, for: indexPath)
        
        // subscribing to cell delegate
        let chatCell = cell as? QMChatCell
        
        chatCell?.delegate = self
        
        let message = self.chatDataSource.message(for: indexPath)

        if let attachmentCell = cell as? QMChatAttachmentCell {
            
            if attachmentCell is QMChatAttachmentIncomingCell {
                chatCell?.containerView?.bgColor = UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 226.0/255.0, alpha: 1.0)
            } else if attachmentCell is QMChatAttachmentOutgoingCell {
                chatCell?.containerView?.bgColor = UIColor(red: 10.0/255.0, green: 95.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            }
            
            if let attachment = message?.attachments?.first {
                
                var keysToRemove: [String] = []
                
                let enumerator = self.attachmentCellsMap.keyEnumerator()
                
                while let existingAttachmentID = enumerator.nextObject() as? String {
                    let cachedCell = self.attachmentCellsMap.object(forKey: existingAttachmentID as AnyObject?)
                    if cachedCell === cell {
                        keysToRemove.append(existingAttachmentID)
                    }
                }
                
                for key in keysToRemove {
                    self.attachmentCellsMap.removeObject(forKey: key as AnyObject?)
                }
                
                self.attachmentCellsMap.setObject(attachmentCell, forKey: attachment.id as AnyObject?)
                
                attachmentCell.attachmentID = attachment.id
                
                // Getting image from chat attachment cache. //manish and video ??
                
                let Meditype = attachment.type
                
                if(Meditype?.isEqual("video"))! {
                    print("Manish:collectionView: configureCell cell: Video")
                    if(attachmentCell.attachmentID == nil) {
                        attachmentCell.setAttachmentImage(#imageLiteral(resourceName: "videocontentimg"))
                        cell.updateConstraints()
                    } else {
                        let videourl: String = QBCBlob.publicUrl(forFileUID: attachment.id!)!
                        let urlname = NSString(string: videourl).deletingPathExtension
                        print("Publicvideourl = \(urlname)")
                        attachmentCell.setAttachmentImage(#imageLiteral(resourceName: "videocontentimg"))
                        cell.updateConstraints()
                        
                    }
                } else {
                    ServicesManager.instance().chatService.chatAttachmentService.image(forAttachmentMessage: message!, completion: { [weak self] (error, image) in
                        
                        guard attachmentCell.attachmentID == attachment.id else {
                            return
                        }
                        
                        self?.attachmentCellsMap.removeObject(forKey: attachment.id as AnyObject?)
                        
                        guard error == nil else {
                            SVProgressHUD.showError(withStatus: error!.localizedDescription)
                            print("Error downloading image from server: \(String(describing: error)).localizedDescription")
                            return
                        }
                        if image == nil {
                            print("Manish Image is nil")
                        }
                        attachmentCell.setAttachmentImage(image)
                        cell.updateConstraints()
                    })
                }
            }
            
        } else if cell is QMChatIncomingCell || cell is QMChatAttachmentIncomingCell {
            
            chatCell?.containerView?.bgColor = UIColor(red:0.91, green:0.90, blue:0.92, alpha:1.0)
            chatCell?.avatarView.layer.cornerRadius = (chatCell?.avatarView.frame.width ?? 1) / 2
            chatCell?.avatarView.clipsToBounds = true
            chatCell?.avatarView.sd_setImage(with: URL(string: docprofileimageurl), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)

        } else if cell is QMChatOutgoingCell {
            
            chatCell?.avatarView.layer.cornerRadius = (chatCell?.avatarView.frame.width)! / 2
            chatCell?.avatarView.clipsToBounds = true
            chatCell?.avatarView.sd_setImage(with: URL(string: GlobalDocdata.gprofileURL), placeholderImage: UIImage(named: "profile"), options: .cacheMemoryOnly)
            
            let status: QMMessageStatus = self.queueManager().status(for: message!)
            
            switch status {
            case .sent:
                chatCell?.containerView?.bgColor = UIColor(red: 145.0/255.0, green: 176.0/255.0, blue: 249.0/255.0, alpha: 1.0)
            case .sending:
                chatCell?.containerView?.bgColor = UIColor(red: 166.3/255.0, green: 171.5/255.0, blue: 171.8/255.0, alpha: 1.0)
            case .notSent:
                chatCell?.containerView?.bgColor = UIColor(red: 254.6/255.0, green: 30.3/255.0, blue: 12.5/255.0, alpha: 1.0)
            }
            
        } else if cell is QMChatAttachmentOutgoingCell {

            chatCell?.containerView?.bgColor = UIColor(red: 145.0/255.0, green: 176.0/255.0, blue: 249.0/255.0, alpha: 1.0)

        } else if cell is QMChatNotificationCell {
            cell.isUserInteractionEnabled = false
            chatCell?.containerView?.bgColor = self.collectionView?.backgroundColor
        }
    }
    
    
    //Manish:15/11/17 get image from URL
    func createThumbnailOfVideoFromFileURL_first(videoURL: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: videoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 100)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            print("createThumbnailOfVideoFromFileURL_first :Error downloading image from server for Video URL")
            return nil // UIImage(named: "videocontentimg")
        }
    }
    //Manish
    func createThumbnailOfVideoFromFileURL(_ strVideoURL: String) -> UIImage?{
        
        let asset = AVAsset(url: URL(string: strVideoURL)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(Float64(1), 2)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            /* error handling here */
            print("createThumbnailOfVideoFromFileURL :Error downloading image from server for Video URL")
        }
        return nil
    }
    
    /**
     Allows to copy text from QMChatIncomingCell and QMChatOutgoingCell
     */
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return false
        }
        
        let viewClass: AnyClass = self.viewClass(forItem: item) as AnyClass
        
        if  viewClass === QMChatNotificationCell.self ||
            viewClass === QMChatContactRequestCell.self {
            return false
        }
        
        return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        let item = self.chatDataSource.message(for: indexPath)
        
        if (item?.isMediaMessage())! {
            ServicesManager.instance().chatService.chatAttachmentService.localImage(forAttachmentMessage: item!,
                                                                                    completion: { (image: UIImage?) in
                                                                                        if image != nil {
                                                                                            guard let imageData = UIImageJPEGRepresentation(image!, 1) else { fatalError("Error") }
                                                                                            let pasteboard = UIPasteboard.general
                                                                                            pasteboard.setValue(imageData, forPasteboardType:kUTTypeJPEG as String)
                                                                                        }
            })
        }
        else {
            UIPasteboard.general.string = item?.text
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lastSection = self.collectionView!.numberOfSections - 1
        
        if (indexPath.section == lastSection && indexPath.item == (self.collectionView?.numberOfItems(inSection: lastSection))! - 1) {

            guard let dialogID = self.dialog.id else {
                print("DialogID is nil")
                return super.collectionView(collectionView, cellForItemAt: indexPath)
            }
            
            ServicesManager.instance().chatService.loadEarlierMessages(withChatDialogID: dialogID).continueWith(block: {[weak self](task) -> Any? in
                
                guard let strongSelf = self else { return nil }
                
                if (task.result?.count ?? 0 > 0) {
                    
                    strongSelf.chatDataSource.add(task.result as? [QBChatMessage])
                }
                
                return nil
            })
        }
        
        // marking message as read if needed
        if let message = self.chatDataSource.message(for: indexPath) {
            self.sendReadStatusForMessage(message: message)
        }
        
        return super.collectionView(collectionView, cellForItemAt
            : indexPath)
    }
    
    // MARK: QMChatCellDelegate
    
    /**
     Removes size from cache for item to allow cell expand and show read/delivered IDS or unexpand cell
     */
    func chatCellDidTapContainer(_ cell: QMChatCell!) {
        
        print("Manish: chatCellDidTapContainer")
        let indexPath = self.collectionView?.indexPath(for: cell)
        
        guard let currentMessage = self.chatDataSource.message(for: indexPath) else {
            return
        }
        
        let messageStatus: QMMessageStatus = self.queueManager().status(for: currentMessage)
        
        if messageStatus == .notSent {
            self.handleNotSentMessage(currentMessage, forCell:cell)
            return
        }
        
        if self.detailedCells.contains(currentMessage.id!) {
            self.detailedCells.remove(currentMessage.id!)
        } else {
            self.detailedCells.insert(currentMessage.id!)
        }
        
        self.collectionView?.collectionViewLayout.removeSizeFromCache(forItemID: currentMessage.id)
        self.collectionView?.performBatchUpdates(nil, completion: nil)
        
        //video player calling by Manish
        let attachment = currentMessage.attachments?.first
        
        if(attachment == nil) { // this check to avoid crash when user tap on messages
            return
        }
        
        if(attachment?.type?.isEqual("video"))! {
            // lets get video player invoked here
            let videourl: String = QBCBlob.publicUrl(forFileUID: attachment!.id!)!
            let urlname = NSString(string: videourl).deletingPathExtension
            let player = AVPlayer(url: URL(string: urlname)!)
            let playerController = AVPlayerViewController()
            playerController.player = player
            self.present(playerController, animated: true) {
                player.play()
            }
        } else if(attachment?.type?.isEqual("image"))! {
            //show Image in full screen view
            
            ServicesManager.instance().chatService.chatAttachmentService.image(forAttachmentMessage: currentMessage, completion: { [weak self] (error, image) in

                self?.attachmentCellsMap.removeObject(forKey: attachment?.id as AnyObject?)
                
                guard error == nil else {
                    SVProgressHUD.showError(withStatus: error!.localizedDescription)
                    print("Error downloading image from server: \(String(describing: error)).localizedDescription")
                    return
                }
                if image == nil {
                    print("chatCellDidTapContainer:Manish Image is nil")
                } else {
                    print("chatCellDidTapContainer:Manish Image got")
                    guard let myimageviewcontroller:ImageViewController = self?.storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as? ImageViewController else { fatalError("Error") }
                    myimageviewcontroller.theimage = image
                    self?.navigationController?.pushViewController(myimageviewcontroller, animated: true)
                }
                
            })

        }
    }
    
    func chatCell(_ cell: QMChatCell!, didTapAtPosition position: CGPoint) {}
    
    func chatCell(_ cell: QMChatCell!, didPerformAction action: Selector!, withSender sender: Any!) {}
    
    func chatCell(_ cell: QMChatCell!, didTapOn result: NSTextCheckingResult) {
        
        switch result.resultType {
            
        case NSTextCheckingResult.CheckingType.link:
            
            let strUrl : String = (result.url?.absoluteString)!
            
            let hasPrefix = strUrl.lowercased().hasPrefix("https://") || strUrl.lowercased().hasPrefix("http://")
            
            if #available(iOS 9.0, *) {
                if hasPrefix {
                    
                    let controller = SFSafariViewController(url: URL(string: strUrl)!)
                    self.present(controller, animated: true, completion: nil)
                    
                    break
                }
                
            }
            // Fallback on earlier versions
            
            if UIApplication.shared.canOpenURL(URL(string: strUrl)!) {
                
                UIApplication.shared.openURL(URL(string: strUrl)!)
            }
            
            break
            
        case NSTextCheckingResult.CheckingType.phoneNumber:
            
            if !self.canMakeACall() {
                
                SVProgressHUD.showInfo(withStatus: "Your Device can't make a phone call", maskType: .none)
                break
            }
            
            let urlString = String(format: "tel:%@",result.phoneNumber!)
            let url = URL(string: urlString)
            
            self.view.endEditing(true)
            
            let alertController = UIAlertController(title: "",
                                                    message: result.phoneNumber,
                                                    preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancle", style: .cancel) { (action) in
                
            }
            
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Call", style: .destructive) { (action) in
                UIApplication.shared.openURL(url!)
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true) {
            }
            
            break
            
        default:
            break
        }
    }
    
    func chatCellDidTapAvatar(_ cell: QMChatCell!) {
    }
    
    // MARK: QMDeferredQueueManager
    
    func deferredQueueManager(_ queueManager: QMDeferredQueueManager, didAddMessageLocally addedMessage: QBChatMessage) {
        
        if addedMessage.dialogID == self.dialog.id {
            self.chatDataSource.add(addedMessage)
        }
    }
    
    func deferredQueueManager(_ queueManager: QMDeferredQueueManager, didUpdateMessageLocally addedMessage: QBChatMessage) {
        
        if addedMessage.dialogID == self.dialog.id {
            self.chatDataSource.update(addedMessage)
        }
    }
    
    // MARK: QMChatServiceDelegate
    
    func chatService(_ chatService: QMChatService, didLoadMessagesFromCache messages: [QBChatMessage], forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.add(messages)
        }
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            // Insert message received from XMPP or self sent
            if self.chatDataSource.messageExists(message) {
                
                self.chatDataSource.update(message)

            } else {
                
                self.chatDataSource.add(message)
            }
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        
        if self.dialog.type != QBChatDialogType.private && self.dialog.id == chatDialog.id {
            self.dialog = chatDialog
            self.title = self.dialog.name
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdate message: QBChatMessage, forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.update(message)
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdate messages: [QBChatMessage], forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.update(messages)
        }
    }
    
    // MARK: UITextViewDelegate
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Prevent crashing undo bug
        let currentCharacterCount = textView.text?.length ?? 0
        
        if (range.length + range.location > currentCharacterCount) {
            return false
        }
        
        if !QBChat.instance.isConnected { return true }
        
        if let timer = self.typingTimer {
            timer.invalidate()
            self.typingTimer = nil
            
        } else {
            
            self.sendBeginTyping()
        }
        
        self.typingTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(ChatViewController.fireSendStopTypingIfNecessary), userInfo: nil, repeats: false)
        
        if maxCharactersNumber > 0 {
            
            if currentCharacterCount >= maxCharactersNumber && text.length > 0 {
                
                self.showCharactersNumberError()
                return false
            }
            
            let newLength = currentCharacterCount + text.length - range.length
            
            if  newLength <= maxCharactersNumber || text.length == 0 {
                return true
            }
            
            let oldString = textView.text ?? ""
            
            let numberOfSymbolsToCut = maxCharactersNumber - oldString.length
            
            var stringRange = NSMakeRange(0, min(text.length, numberOfSymbolsToCut))
            
            
            // adjust the range to include dependent chars
            stringRange = (text as NSString).rangeOfComposedCharacterSequences(for: stringRange)
            
            // Now you can create the short string
            let shortString = (text as NSString).substring(with: stringRange)
            
            let newText = NSMutableString()
            newText.append(oldString)
            newText.insert(shortString, at: range.location)
            textView.text = newText as String
            
            self.showCharactersNumberError()
            
            self.textViewDidChange(textView)
            
            return false
        }
        
        return true
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        
        super.textViewDidEndEditing(textView)
        
        self.fireSendStopTypingIfNecessary()
    }
    
    @objc func fireSendStopTypingIfNecessary() -> Void {
        
        if let timer = self.typingTimer {
            
            timer.invalidate()
        }
        
        self.typingTimer = nil
        self.sendStopTyping()
    }
    
    func sendBeginTyping() -> Void {
        self.dialog.sendUserIsTyping()
    }
    
    func sendStopTyping() -> Void {
        
        self.dialog.sendUserStoppedTyping()
    }
    
    // MARK: QMChatAttachmentServiceDelegate
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChange status: QMMessageAttachmentStatus, for message: QBChatMessage) {
        
        if status != QMMessageAttachmentStatus.notLoaded {
            
            if message.dialogID == self.dialog.id {
                self.chatDataSource.update(message)
            }
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeLoadingProgress progress: CGFloat, for attachment: QBChatAttachment) {
        
        if let attachmentCell = self.attachmentCellsMap.object(forKey: attachment.id! as AnyObject?) {
            attachmentCell.updateLoadingProgress(progress)
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeUploadingProgress progress: CGFloat, for message: QBChatMessage) {
        
        guard message.dialogID == self.dialog.id else {
            return
        }
        var cell = self.attachmentCellsMap.object(forKey: message.id as AnyObject?)
        
        if cell == nil && progress < 1.0 {
            
            if let indexPath = self.chatDataSource.indexPath(for: message) {
                cell = self.collectionView?.cellForItem(at: indexPath) as? QMChatAttachmentCell
                self.attachmentCellsMap.setObject(cell, forKey: message.id as AnyObject?)
            }
        }
        
        cell?.updateLoadingProgress(progress)
    }
    
    // MARK : QMChatConnectionDelegate
    
    func refreshAndReadMessages() {
        
        SVProgressHUD.show(withStatus: "Loading Messages", maskType: SVProgressHUDMaskType.clear)
        self.loadMessages()
        
        if let messagesToRead = self.unreadMessages {
            self.readMessages(messages: messagesToRead)
        }
        
        self.unreadMessages = nil
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        
        self.refreshAndReadMessages()
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        
        self.refreshAndReadMessages()
    }
    
    func queueManager() -> QMDeferredQueueManager {
        return ServicesManager.instance().chatService.deferredQueueManager
    }
    
    func handleNotSentMessage(_ message: QBChatMessage,
                              forCell cell: QMChatCell!) {
        
        let alertController = UIAlertController(title: "", message: "Message failed to send", preferredStyle:.actionSheet)
        
        let resend = UIAlertAction(title: "Try again", style: .default) { (action) in
            self.queueManager().perfromDefferedAction(for: message, withCompletion: nil)
        }
        alertController.addAction(resend)
        
        let delete = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            self.queueManager().remove(message)
            self.chatDataSource.delete(message)
        }
        alertController.addAction(delete)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        alertController.addAction(cancelAction)
        
        if alertController.popoverPresentationController != nil {
            self.view.endEditing(true)
            alertController.popoverPresentationController!.sourceView = cell.containerView
            alertController.popoverPresentationController!.sourceRect = cell.containerView.bounds
        }
        
        self.present(alertController, animated: true) {
        }
    }
}
