import UIKit

class allConsultsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var consultdocname: UILabel!
    @IBOutlet weak var allconsultscellButton: UIButton!
    @IBOutlet weak var allconsultsDatecellButton: UIButton!
    @IBOutlet weak var consultsShareButton: UIButton!
    @IBOutlet weak var consultclosedtimelb: UILabel!
    @IBOutlet weak var consultsstatustimelb: UILabel!
    @IBOutlet weak var consulttimelb: UILabel!
    @IBOutlet weak var consultStatusLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
