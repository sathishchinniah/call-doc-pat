import UIKit

class AddDocFromDocNetworkViewController: UIViewController {

    @IBOutlet var specialityviewButton: UIButton!
    @IBOutlet weak var gestroYConstraint: NSLayoutConstraint!
    @IBOutlet weak var altmedYConstraint: NSLayoutConstraint!
    @IBOutlet weak var toplabelYConstraint: NSLayoutConstraint!
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if phone && maxLength == 667 {
            //iphone 6
            gestroYConstraint.constant = 43
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            gestroYConstraint.constant = 47
            altmedYConstraint.constant = 15
            toplabelYConstraint.constant = 25
        }
        
        if phone && maxLength == 812 {
            //iphone x
            gestroYConstraint.constant = 50
            toplabelYConstraint.constant = -8
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        title = "Add a Doctor"
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }

    @IBAction func SpecialityButton(_ sender: UIButton) {
        
    }

    @IBAction func CardiologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Cardiologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Cardiologist")
        self.navigationController?.pushViewController(VC1, animated: true)
        
    }
    
    @IBAction func Dermatologist(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Dermatologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Dermatologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func DieticianButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Dietician"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Dietician")
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    @IBAction func EndocrinologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Endocrinologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Endocrinologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func ENTButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "E.N.T."
        //self.populateNetworkDocData(viewController: VC1, speciality: "E.N.T")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func GPButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "G.P."
        //self.populateNetworkDocData(viewController: VC1, speciality: "G.P.")
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    @IBAction func GastroentrologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Gastroentrologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Gastroentrologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func GynaecolologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Gynaecolologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Gynaecolologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func NephrologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Nephrologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Nephrologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func NeurologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Neurologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Neurologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func OncologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Oncologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Oncologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func OrthopaedicButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Orthopaedic"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Orthopaedic")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func PaediatricianButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Paediatrician"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Paediatrician")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func PsychiatristButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Psychiatrist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Psychiatrist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func PulmonoligistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Pulmonoligist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Pulmonoligist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func SexologistButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "Sexologist"
        //self.populateNetworkDocData(viewController: VC1, speciality: "Sexologist")
        self.navigationController?.pushViewController(VC1, animated: true)
    }

    @IBAction func AltMedicineButton(_ sender: UIButton) {
        // this should hide backbutton text on next VC
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let VC1 = PntAddDoctorViewController.storyboardInstance()
        VC1.speciality = "AltMedicine"
        //self.populateNetworkDocData(viewController: VC1, speciality: "AltMedicine")
        self.navigationController?.pushViewController(VC1, animated: true)
    }
    
    func populateNetworkDocData(viewController: PntAddDoctorViewController, speciality: String) {
        
        viewController.copyofhomeDoctorsArray = DoctorsArray
        
        for doc in viewController.copyofhomeDoctorsArray {
            let getRequest = NSMutableDictionary()
            getRequest["invitordocuserid"] =  (doc.fields?.value(forKey: "doctoruserid") as? UInt) ?? 1//doc.id
            getRequest["inviteespeciality"] = speciality.lowercased()
            QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                //Handle Success
                print("Success ")
                if contributors?.count == 0 {
                    // No entry found
                    let getRequest = NSMutableDictionary()
                    getRequest["invitordocuserid"] = (doc.fields?.value(forKey: "doctoruserid") as? UInt) ?? 1
                    getRequest["status"] = true
                    QBRequest.objects(withClassName: "RelDocTeam", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                        //Handle Success
                        print("Success ")
                        
                        if ((contributors?.count)! > 0) {
                            viewController.docCOTSpeciality[doc] = contributors ?? []
                            numberOfRowinSectionArray.insert(contributors?.count ?? 0, at: viewController.copyofhomeDoctorsArray.index(of: doc)!)
                        }
                        if viewController.copyofhomeDoctorsArray.last == doc {
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }) { (response) in
                        //Handle Error
                        print("Response error: \(String(describing: response.error?.description))")
                    }
                    
                } else {
                    // entry found
                    viewController.docCOTSpeciality[doc] = contributors ?? []
                    
                }
                //viewController.SubTableView.reloadData()
               
                
            }
            
            ) { (response) in
                //Handle Error
                print("Response error: \(String(describing: response.error?.description))")
            }
            //let indexSet = copyofhomeDoctorsArray.index(of: doc)!
            //SubTableView.reloadSections(indexSet, with: .automatic)//reloadData()
            
        }
        
    }
    
}
