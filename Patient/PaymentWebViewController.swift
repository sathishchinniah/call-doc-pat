import UIKit
import WebKit

var passchecksumdata = ""

class PaymentWebViewController: UIViewController {

    @IBOutlet weak var backbuttonview: UIButton!
    @IBOutlet weak var forwardbuttonview: UIButton!
    @IBOutlet weak var WebView: WKWebView!
    
    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
//        self.navigationController?.navigationBar.tintColor = UIColor.white
//
//        // this removes navigation bar horizental line
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navigationController?.navigationBar.shadowImage = UIImage()
        
        navigationItem.title = "CallDoc"
        
        
        //left side Back Button
//        let btnImage = UIImage(named: "backBtn-1")
//        let leftbtn = UIButton(type: .custom)
//        leftbtn.bounds = CGRect(x: 0, y: 0, width: 45, height: 45)
//        leftbtn.addTarget(self, action: #selector(self.backBtnPressed), for: .touchUpInside)
//        leftbtn.setImage(btnImage, for: .normal)
//        let backButton = UIBarButtonItem(customView: leftbtn)
//        navigationItem.leftBarButtonItem = backButton
        
        if !GlobalDocdata.mobilenumber.isEmpty {
            print("PaymentWebViewController:storing mobile number in first attempt")
        } else {
            if let tmpgmobile = UserDefaults.standard.string(forKey: "userphonenumber")  {
                print("PaymentWebViewController:storing mobile number in second attempt")
                GlobalDocdata.mobilenumber = tmpgmobile
            }
        }
        
        let txnid = randomString(length:10)
        
        var arr = ["test-calldoc-p2m", txnid, "1", "qdZaU895101PFYiNqyr6a328o3vi7CHC"]
        var checksum_str = ""
        
        for i in 0..<arr.count{
            
            checksum_str +=  arr[i] + "|"
        }
        let truncatedlastpipechar = checksum_str.substring(to: checksum_str.index(before: checksum_str.endIndex))
        let Datatofunc =  truncatedlastpipechar.data(using:.utf8)!
        let checksumData = self.hashSHA512(data: Datatofunc)
        passchecksumdata = checksumData!
        print("checksumData:\n\(String(describing: checksumData))")
        
        let mobilenumberwithoutcc = GlobalDocdata.mobilenumber.dropFirst(3)    // removing country code +91
        
        let urlstring = "https://testpgupi.mypoolin.com/payment"
        let url = URL(string: urlstring)!
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let query :[String:String] = ["checksum":passchecksumdata,"amount":"1", "merchant_name":"test-calldoc-p2m", "merchant_txn_id":txnid, "paymentoptions":"paytm,upi", "payer_email":GlobalDocdata.email, "merchant_message":"Call Doc Payments", "payer_mobile":String(mobilenumberwithoutcc)]
        let baseurl = URL(string:urlstring)!
        let postString = (baseurl.withQueries(query)!).query
        request.httpBody = postString?.data(using: .utf8)
        WebView.load(request as URLRequest)
        
    }

    func hashSHA512(data:Data) -> String? {
        var hashData = Data(count: Int(CC_SHA512_DIGEST_LENGTH))
        
        _ = hashData.withUnsafeMutableBytes {digestBytes in
            data.withUnsafeBytes {messageBytes in
                CC_SHA512(messageBytes, CC_LONG(data.count), digestBytes)
            }
        }
        
        // For hexadecimal output:
        return hashData.map { String(format: "%02hhx", $0) }.joined()
    }

    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
//    func backBtnPressed(_ sender: Any) {
//
//        print("PaymentWebViewController:leftButtonAction pressed")
//        self.navigationController?.popViewController(animated: true)
////        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: "TheHomeViewController") as! TheHomeViewController
////        self.nav = UINavigationController(rootViewController: homeViewController)
////        self.present(self.nav!, animated: false, completion: { _ in })
//    }
    
}


extension URL {

    func withQueries(_ queries:[String:String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.flatMap{URLQueryItem(name: $0.0, value: $0.1)}
        return components?.url
    }
    
    func justQueries(_ queries:[String:String]) -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.queryItems = queries.flatMap{URLQueryItem(name: $0.0, value: $0.1)}
        return components?.url
    }
    
}
