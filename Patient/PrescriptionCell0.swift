import UIKit

class PrescriptionCell0: UITableViewCell {

    @IBOutlet var datelbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configureUI(dateVal: String) {
        datelbl.text = dateVal
    }

}
