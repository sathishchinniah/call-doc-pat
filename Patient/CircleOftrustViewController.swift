import UIKit

struct COTglobalVariables {
    static var specialityString = ""
}

class CircleOftrustViewController: UIViewController {

    @IBOutlet var donamelb: UILabel!
    @IBOutlet var docimg: UIImageView!
    @IBOutlet var myteamButtonview: UIButton!

    @IBOutlet var cardioButtonview: UIButton!
    @IBOutlet var dermatoioButtonview: UIButton!
    @IBOutlet var dieticianButtonview: UIButton!
    @IBOutlet var endocrinoButtonview: UIButton!
    @IBOutlet var ENTButtonview: UIButton!
    @IBOutlet var GPButtonview: UIButton!
    @IBOutlet var GetroButtonview: UIButton!
    @IBOutlet var GynoButtonview: UIButton!
    @IBOutlet var NephroButtonview: UIButton!
    @IBOutlet var NeuroButtonview: UIButton!
    @IBOutlet var OncologyButtonview: UIButton!
    @IBOutlet var OrthoButtonview: UIButton!
    @IBOutlet var PaediatricButtonview: UIButton!
    @IBOutlet var PsychButtonview: UIButton!
    @IBOutlet var PolmonoButtonview: UIButton!
    @IBOutlet var SexologyButtonview: UIButton!
    @IBOutlet var AltMedButtonview: UIButton!
    
    @IBOutlet var myteamLabel: UILabel!
    @IBOutlet var DermatoLabel: UILabel!
    @IBOutlet var DieticianLabel: UILabel!
    @IBOutlet var EndocrinoLabel: UILabel!
    @IBOutlet var GpLabel: UILabel!
    @IBOutlet var GestroLabel: UILabel!
    @IBOutlet var GynaecoLabel: UILabel!
    @IBOutlet var NephroLabel: UILabel!
    @IBOutlet var NeuroLabel: UILabel!
    @IBOutlet var OncoloLabel: UILabel!
    @IBOutlet var PaediatricLabel: UILabel!
    @IBOutlet var PsychiatristLabel: UILabel!
    @IBOutlet var PulmonoLabel: UILabel!
    @IBOutlet var SexoLabel: UILabel!
    @IBOutlet var AltMedLabel: UILabel!
    @IBOutlet var CardioLabel: UILabel!
    @IBOutlet var EntLabel: UILabel!
    @IBOutlet var OrthoLabel: UILabel!
    
    @IBOutlet weak var docnamelbConstraintY: NSLayoutConstraint!
    @IBOutlet weak var recomendlbConstraintY: NSLayoutConstraint!
    @IBOutlet weak var cardiologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var cardiologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var DermatologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var DermatologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var dieticiancontraintsX: NSLayoutConstraint!
    @IBOutlet weak var dieticiancontraintsY: NSLayoutConstraint!
    @IBOutlet weak var endocrinologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var endocrinologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var ENTcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var ENTcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var GPcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var GPcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var GestrocontraintsX: NSLayoutConstraint!
    @IBOutlet weak var GestrocontraintsY: NSLayoutConstraint!
    @IBOutlet weak var GynocontraintsX: NSLayoutConstraint!
    @IBOutlet weak var GynocontraintsY: NSLayoutConstraint!
    @IBOutlet weak var NephrologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var NephrologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var NeurologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var NeurologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var OncologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var OncologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var OrthocontraintsX: NSLayoutConstraint!
    @IBOutlet weak var OrthocontraintsY: NSLayoutConstraint!
    @IBOutlet weak var PaediatriciancontraintsX: NSLayoutConstraint!
    @IBOutlet weak var PaediatriciancontraintsY: NSLayoutConstraint!
    @IBOutlet weak var PsychiatristcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var PsychiatristcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var PolmonoligistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var PolmonoligistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var SexologistcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var SexologistcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var AltMedcontraintsX: NSLayoutConstraint!
    @IBOutlet weak var AltMedcontraintsY: NSLayoutConstraint!
    @IBOutlet weak var myteamhrzlinewidthConstraint: NSLayoutConstraint!
    
    var nav: UINavigationController?
    var myindexval: Int = 100
    var docuserid: UInt = 0
    var myteamonoffbit:  Bool = false
    var DocCountTableRecordID = ""
    var docStatus = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.myteamonoffbit = UserDefaults.standard.bool(forKey: "teamswitch")
        
        donamelb.text = "Dr " + ((DoctorsArray[myindexval].fields?.value(forKey: "doctorfullname") as? String) ?? "")
        
        if (self.myteamonoffbit) {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "team"), for: .normal)
            }
        } else {
            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "team"), for: .normal)
            }
        }
        
        if phone && maxLength == 568 {
            //iphone 5
            docimg.layer.cornerRadius = 32.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            NephrologistcontraintsY.constant = 33
        }
        
        if phone && maxLength == 667 {
            //iphone 6
            docimg.layer.cornerRadius = 37.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docnamelbConstraintY.constant = 15.0
            recomendlbConstraintY.constant = 8.0
        }
        
        if phone && maxLength == 736 {
            //iphone 6 plus
            docimg.layer.cornerRadius = 40.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docnamelbConstraintY.constant = 12.0
            recomendlbConstraintY.constant = 20.0
        }
        
        if phone && maxLength == 812 {
            //iphone x
            docimg.layer.cornerRadius = 38.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            docnamelbConstraintY.constant = 15.0
            recomendlbConstraintY.constant = 8.0
            GestrocontraintsY.constant = 60
            NephrologistcontraintsY.constant = 50
        }
        
        
        if ipad && maxLength == 1024 {
            //iPad
            docimg.layer.cornerRadius = 77.0
            docimg.layer.borderWidth = 2.2
            docimg.layer.borderColor = UIColor.green.cgColor
            docimg.clipsToBounds = true
            myteamhrzlinewidthConstraint.constant = 5
            cardiologistcontraintsY.constant = 10
            GestrocontraintsY.constant = 110
            NephrologistcontraintsY.constant = 91
            dermatoioButtonview.frame.size = CGSize(width: 50, height: 50)
        }
        
        LoadProfileImage()
        
        
        let isonline = (UserDefaults.standard.object(forKey: "doctoronline") as? Bool) ?? false
        
        if(isonline) {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }

        let helpButton = UIBarButtonItem(image: UIImage(named: "help"), style: .plain, target: self,  action: #selector(rightButtonAction(sender:)))
        self.navigationItem.rightBarButtonItem = helpButton;
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        let backButton = UIBarButtonItem(image: UIImage(named: "backBtnwhite"), style: .plain, target: self, action: #selector(leftButtonAction(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.addbadgebuttoncounts(docuserid: docuserid)
        if docStatus == "online" {
            docimg.layer.borderColor = UIColor.green.cgColor
        } else if docStatus == "offline" {
            docimg.layer.borderColor = UIColor.red.cgColor
        } else if docStatus == "pause" {
            docimg.layer.borderColor = UIColor.orange.cgColor
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func LoadProfileImage() {
        var profileurl = (DoctorsArray[myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
        if (profileurl == nil || profileurl.isEmpty) {
            /* string is  blank */
            profileurl = ""
        }
        self.docimg.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profilenonedit"), options: .cacheMemoryOnly)
    }

    @objc func rightButtonAction(sender: UIBarButtonItem) {
        print("Search Bar icon pressed")
    }
    
    @objc func leftButtonAction(sender: UIBarButtonItem) {
        print("leftButtonAction pressed")
        guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
        self.nav = UINavigationController(rootViewController: homeViewController)
        self.present(self.nav!, animated: false, completion: nil)
    }
    
    @IBAction func MyTeamButton(_ sender: UIButton) {
        let transition = CATransition.init()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        navigationController?.view.layer.add(transition, forKey: kCATransition)
        guard let myteamviewController = self.storyboard?.instantiateViewController(withIdentifier: "TheDoctorTeamView") as? TheDoctorTeamViewController else { fatalError("Error") }
        myteamviewController.myindexval = myindexval
        myteamviewController.DocCounttablerecordID = self.DocCountTableRecordID
        myteamviewController.docuserid = docuserid
        myteamviewController.sourceController = "COT"
        myteamviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(myteamviewController, animated: true)
        
    }
    
    @IBAction func DoctorProfileButton(_ sender: UIButton) {
//        guard let docprofileviewController = self.storyboard?.instantiateViewController(withIdentifier: "DoctorProfile") as? DoctorProfileViewController else { fatalError("Error") }
//        self.navigationController?.pushViewController(docprofileviewController, animated: true)
        self.getDoctorGender(doctoruserid: docuserid)
    }
    
    @IBAction func HeartButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Cardiologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func AlternateMedicineButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Alt Medicine"//Alternate Medicine"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func DermatologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Dermatologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func GastroentrologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Gastroentrologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func ENTButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "E.N.T."
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func EndocrinologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Endocrinologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func PulmonoligistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Pulmonoligist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func PediatricianButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Pediatrician"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func UrinaryButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Urinary"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func GeneralHealthButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "G.P."//"General Practioner"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func GynaecologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Gynaecologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func NeurologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Neurologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func UrologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Urologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func DieticianButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Dietician"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func PsychiatristButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Psychiatrist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func OncologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Oncologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func OrthopedicButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Orthopedic"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    @IBAction func SexologistButton(_ sender: UIButton) {
        guard let mytrustedviewController = self.storyboard?.instantiateViewController(withIdentifier: "TrustedNetwork") as? TrustedNetworkViewController else { fatalError("Error") }
        mytrustedviewController.stringpassed = "Sexologist"
        mytrustedviewController.myindexval = myindexval
        mytrustedviewController.docuserid = docuserid
        mytrustedviewController.docStatus = self.docStatus
        self.navigationController?.pushViewController(mytrustedviewController, animated: true)
    }
    
    func getDoctorGender(doctoruserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctoruserid
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                
                let gender = (contributors![0].fields?.value(forKey: "gender") as? String) ?? ""
                let vc = SelfProfileViewController.storyboardInstance()
                let profileurl = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorprofileimageURL") as? String)!
                let selecteddocuid =  (DoctorsArray[self.myindexval].fields?.value(forKey: "doctoruserid") as? UInt)!
                vc.imageURLpassed = profileurl
                vc.docuseridpassed = selecteddocuid
                vc.docGender = gender
                vc.docnamepassed = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorfullname") as? String)!
                vc.docspecialitypassed = (DoctorsArray[self.myindexval].fields?.value(forKey: "doctorspeciality") as? String)!
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("getDoctorGender() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func addbadgebuttoncounts(docuserid: UInt) {
        //Read DocCount Table
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = docuserid
        QBRequest.objects(withClassName: "DocCountTable", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("NotificationTableViewController:addbadgebuttoncounts(): Reading DocCount Table !")
            let invitorRecordID = (contributors![0].id)!
            self.DocCountTableRecordID = invitorRecordID
            let dermatologist =  contributors![0].fields?.value(forKey: "dermatologist") as? Int
            let dietician =  contributors![0].fields?.value(forKey: "dietician") as? Int
            let cardiologist =  contributors![0].fields?.value(forKey: "cardiologist") as? Int
            let ent =  contributors![0].fields?.value(forKey: "ent") as? Int
            let general =  contributors![0].fields?.value(forKey: "general") as? Int
            let endocrinologist =  contributors![0].fields?.value(forKey: "endocrinologist") as? Int
            let gynaecologist =  contributors![0].fields?.value(forKey: "gynaecologist") as? Int
            let nephrologist =  contributors![0].fields?.value(forKey: "nephrologist") as? Int
            let neurologist =  contributors![0].fields?.value(forKey: "neurologist") as? Int
            let oncologist =  contributors![0].fields?.value(forKey: "oncologist") as? Int
            let gastroentrologist =  contributors![0].fields?.value(forKey: "gastroentrologist") as? Int
            let paediatrician =  contributors![0].fields?.value(forKey: "paediatrician") as? Int
            let psychiatrist =  contributors![0].fields?.value(forKey: "psychiatrist") as? Int
            let orthopaedic =  contributors![0].fields?.value(forKey: "orthopaedic") as? Int
            let sexologist =  contributors![0].fields?.value(forKey: "sexologist") as? Int
            let altmedicine =  contributors![0].fields?.value(forKey: "altmedicine") as? Int
            let pulmonoligist =  contributors![0].fields?.value(forKey: "pulmonoligist") as? Int
            let myteam = contributors![0].fields?.value(forKey: "myTeam") as? Int
            let teamstatus = contributors![0].fields?.value(forKey: "teamStatus") as? Bool
            
            self.updateDocCountOnCOTView(myteam: myteam!, teamstatus: teamstatus!, dermatologist: dermatologist!, dietician: dietician!, cardiologist: cardiologist!, ent: ent!, general: general!, endocrinologist: endocrinologist!,  gynaecologist: gynaecologist!, nephrologist: nephrologist!, neurologist: neurologist!, oncologist: oncologist!, gastroentrologist: gastroentrologist!, paediatrician: paediatrician!, psychiatrist: psychiatrist!, orthopaedic: orthopaedic!, sexologist: sexologist!, altmedicine: altmedicine!, pulmonoligist: pulmonoligist!)
            
        }) { (errorresponse) in
            print("NotificationTableViewController:addbadgebuttoncounts() Response error: \(String(describing: errorresponse.error?.description))")
        }
        
    }
    
    func updateDocCountOnCOTView(myteam: Int, teamstatus: Bool,  dermatologist: Int, dietician: Int, cardiologist: Int, ent: Int, general: Int, endocrinologist: Int,  gynaecologist: Int, nephrologist: Int, neurologist: Int, oncologist: Int, gastroentrologist: Int, paediatrician: Int, psychiatrist: Int, orthopaedic: Int, sexologist: Int, altmedicine: Int, pulmonoligist: Int) {
        // team button color for ON/Off of team button

        if (teamstatus) { // team is ON

            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteam_ipad"), for: .normal)
                    self.myteamonoffbit = true
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamcotbg"), for: .normal)
                self.myteamonoffbit = true
            }
        } else { // team is OFF

            if UI_USER_INTERFACE_IDIOM() == .pad {
                let iOSDeviceScreenSize: CGSize = UIScreen.main.bounds.size
                if iOSDeviceScreenSize.height == 1024 {
                    myteamButtonview.setImage(UIImage(named: "myteamred_ipad"), for: .normal)
                    self.myteamonoffbit = false
                }
            } else {
                myteamButtonview.setImage(UIImage(named: "myteamredcotbg"), for: .normal)
                self.myteamonoffbit = false
            }
        }
        myteamLabel.layer.borderColor = UIColor.clear.cgColor
        myteamLabel.layer.borderWidth = 1
        myteamLabel.layer.cornerRadius = myteamLabel.bounds.size.height / 2
        myteamLabel.textAlignment = .center
        myteamLabel.layer.masksToBounds = true
        myteamLabel.textColor = .black
        myteamLabel.font = donamelb.font.withSize(11)
        myteamLabel.backgroundColor = .white
        myteamLabel.text = String(myteam)
        
        CardioLabel.layer.borderColor = UIColor.clear.cgColor
        CardioLabel.layer.borderWidth = 1
        CardioLabel.layer.cornerRadius = CardioLabel.bounds.size.height / 2
        CardioLabel.textAlignment = .center
        CardioLabel.layer.masksToBounds = true
        CardioLabel.textColor = .black
        CardioLabel.font = donamelb.font.withSize(11)
        CardioLabel.backgroundColor = .white
        CardioLabel.text = String(cardiologist)
        
        DermatoLabel.layer.borderColor = UIColor.clear.cgColor
        DermatoLabel.layer.borderWidth = 1
        DermatoLabel.layer.cornerRadius = DermatoLabel.bounds.size.height / 2
        DermatoLabel.textAlignment = .center
        DermatoLabel.layer.masksToBounds = true
        DermatoLabel.textColor = .black
        DermatoLabel.font = donamelb.font.withSize(11)
        DermatoLabel.backgroundColor = .white
        DermatoLabel.text = String(dermatologist)
        
        DieticianLabel.layer.borderColor = UIColor.clear.cgColor
        DieticianLabel.layer.borderWidth = 1
        DieticianLabel.layer.cornerRadius = DieticianLabel.bounds.size.height / 2
        DieticianLabel.textAlignment = .center
        DieticianLabel.layer.masksToBounds = true
        DieticianLabel.textColor = .black
        DieticianLabel.font = donamelb.font.withSize(11)
        DieticianLabel.backgroundColor = .white
        DieticianLabel.text = String(dietician)
        
        GpLabel.layer.borderColor = UIColor.clear.cgColor
        GpLabel.layer.borderWidth = 1
        GpLabel.layer.cornerRadius = GpLabel.bounds.size.height / 2
        GpLabel.textAlignment = .center
        GpLabel.layer.masksToBounds = true
        GpLabel.textColor = .black
        GpLabel.font = donamelb.font.withSize(11)
        GpLabel.backgroundColor = .white
        GpLabel.text = String(general)
        
        EndocrinoLabel.layer.borderColor = UIColor.clear.cgColor
        EndocrinoLabel.layer.borderWidth = 1
        EndocrinoLabel.layer.cornerRadius = EndocrinoLabel.bounds.size.height / 2
        EndocrinoLabel.textAlignment = .center
        EndocrinoLabel.layer.masksToBounds = true
        EndocrinoLabel.textColor = .black
        EndocrinoLabel.font = donamelb.font.withSize(11)
        EndocrinoLabel.backgroundColor = .white
        EndocrinoLabel.text = String(endocrinologist)
        
        GynaecoLabel.layer.borderColor = UIColor.clear.cgColor
        GynaecoLabel.layer.borderWidth = 1
        GynaecoLabel.layer.cornerRadius = GynaecoLabel.bounds.size.height / 2
        GynaecoLabel.textAlignment = .center
        GynaecoLabel.layer.masksToBounds = true
        GynaecoLabel.textColor = .black
        GynaecoLabel.font = donamelb.font.withSize(11)
        GynaecoLabel.backgroundColor = .white
        GynaecoLabel.text = String(gynaecologist)
        
        NephroLabel.layer.borderColor = UIColor.clear.cgColor
        NephroLabel.layer.borderWidth = 1
        NephroLabel.layer.cornerRadius = NephroLabel.bounds.size.height / 2
        NephroLabel.textAlignment = .center
        NephroLabel.layer.masksToBounds = true
        NephroLabel.textColor = .black
        NephroLabel.font = donamelb.font.withSize(11)
        NephroLabel.backgroundColor = .white
        NephroLabel.text = String(nephrologist)
        
        NeuroLabel.layer.borderColor = UIColor.clear.cgColor
        NeuroLabel.layer.borderWidth = 1
        NeuroLabel.layer.cornerRadius = NeuroLabel.bounds.size.height / 2
        NeuroLabel.textAlignment = .center
        NeuroLabel.layer.masksToBounds = true
        NeuroLabel.textColor = .black
        NeuroLabel.font = donamelb.font.withSize(11)
        NeuroLabel.backgroundColor = .white
        NeuroLabel.text = String(neurologist)
        
        OncoloLabel.layer.borderColor = UIColor.clear.cgColor
        OncoloLabel.layer.borderWidth = 1
        OncoloLabel.layer.cornerRadius = OncoloLabel.bounds.size.height / 2
        OncoloLabel.textAlignment = .center
        OncoloLabel.layer.masksToBounds = true
        OncoloLabel.textColor = .black
        OncoloLabel.font = donamelb.font.withSize(11)
        OncoloLabel.backgroundColor = .white
        OncoloLabel.text = String(oncologist)
        
        GestroLabel.layer.borderColor = UIColor.clear.cgColor
        GestroLabel.layer.borderWidth = 1
        GestroLabel.layer.cornerRadius = GestroLabel.bounds.size.height / 2
        GestroLabel.textAlignment = .center
        GestroLabel.layer.masksToBounds = true
        GestroLabel.textColor = .black
        GestroLabel.font = donamelb.font.withSize(11)
        GestroLabel.backgroundColor = .white
        GestroLabel.text = String(gastroentrologist)
        
        PaediatricLabel.layer.borderColor = UIColor.clear.cgColor
        PaediatricLabel.layer.borderWidth = 1
        PaediatricLabel.layer.cornerRadius = PaediatricLabel.bounds.size.height / 2
        PaediatricLabel.textAlignment = .center
        PaediatricLabel.layer.masksToBounds = true
        PaediatricLabel.textColor = .black
        PaediatricLabel.font = donamelb.font.withSize(11)
        PaediatricLabel.backgroundColor = .white
        PaediatricLabel.text = String(paediatrician)
        
        PsychiatristLabel.layer.borderColor = UIColor.clear.cgColor
        PsychiatristLabel.layer.borderWidth = 1
        PsychiatristLabel.layer.cornerRadius = PsychiatristLabel.bounds.size.height / 2
        PsychiatristLabel.textAlignment = .center
        PsychiatristLabel.layer.masksToBounds = true
        PsychiatristLabel.textColor = .black
        PsychiatristLabel.font = donamelb.font.withSize(11)
        PsychiatristLabel.backgroundColor = .white
        PsychiatristLabel.text = String(psychiatrist)
        
        SexoLabel.layer.borderColor = UIColor.clear.cgColor
        SexoLabel.layer.borderWidth = 1
        SexoLabel.layer.cornerRadius = SexoLabel.bounds.size.height / 2
        SexoLabel.textAlignment = .center
        SexoLabel.layer.masksToBounds = true
        SexoLabel.textColor = .black
        SexoLabel.font = donamelb.font.withSize(11)
        SexoLabel.backgroundColor = .white
        SexoLabel.text = String(sexologist)
        
        AltMedLabel.layer.borderColor = UIColor.clear.cgColor
        AltMedLabel.layer.borderWidth = 1
        AltMedLabel.layer.cornerRadius = AltMedLabel.bounds.size.height / 2
        AltMedLabel.textAlignment = .center
        AltMedLabel.layer.masksToBounds = true
        AltMedLabel.textColor = .black
        AltMedLabel.font = donamelb.font.withSize(11)
        AltMedLabel.backgroundColor = .white
        AltMedLabel.text = String(altmedicine)
        
        PulmonoLabel.layer.borderColor = UIColor.clear.cgColor
        PulmonoLabel.layer.borderWidth = 1
        PulmonoLabel.layer.cornerRadius = PulmonoLabel.bounds.size.height / 2
        PulmonoLabel.textAlignment = .center
        PulmonoLabel.layer.masksToBounds = true
        PulmonoLabel.textColor = .black
        PulmonoLabel.font = donamelb.font.withSize(11)
        PulmonoLabel.backgroundColor = .white
        PulmonoLabel.text = String(pulmonoligist)
        
        EntLabel.layer.borderColor = UIColor.clear.cgColor
        EntLabel.layer.borderWidth = 1
        EntLabel.layer.cornerRadius = EntLabel.bounds.size.height / 2
        EntLabel.textAlignment = .center
        EntLabel.layer.masksToBounds = true
        EntLabel.textColor = .black
        EntLabel.font = donamelb.font.withSize(11)
        EntLabel.backgroundColor = .white
        EntLabel.text = String(ent)
        
        OrthoLabel.layer.borderColor = UIColor.clear.cgColor
        OrthoLabel.layer.borderWidth = 1
        OrthoLabel.layer.cornerRadius = OrthoLabel.bounds.size.height / 2
        OrthoLabel.textAlignment = .center
        OrthoLabel.layer.masksToBounds = true
        OrthoLabel.textColor = .black
        OrthoLabel.font = donamelb.font.withSize(11)
        OrthoLabel.backgroundColor = .white
        OrthoLabel.text = String(orthopaedic)
        
    }

}
