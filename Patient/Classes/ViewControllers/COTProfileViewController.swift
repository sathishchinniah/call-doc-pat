import UIKit

class COTProfileViewController: UIViewController {
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var specialzationLabel: UILabel! {
        didSet {
            specialzationLabel.layer.cornerRadius = 15;
            specialzationLabel.layer.masksToBounds = true
            specialzationLabel.layer.shadowColor = UIColor.lightGray.cgColor
            specialzationLabel.layer.shadowOpacity = 0.5
            specialzationLabel.layer.shadowRadius = 3.0
            specialzationLabel.layer.shadowOffset = .zero
        }
    }
    
    @IBOutlet weak var addDocToCOTSwitch: UISwitch!/* {
        didSet {
            addDocToCOTSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }
    }*/
    @IBOutlet weak var referPatientsToDocSwitch: UISwitch! /*{
        didSet {
            referPatientsToDocSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }
    }*/
    
    var imageURLpassed = ""
    var docfullname = ""
    var docspeciality = ""
    var docuserid: Int = 0
    var docMobileNumber = ""
    var docGender = ""
    var doctorSelected: QBCOCustomObject!
    var sourceView = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if sourceView == "TEAM" {
            inviteButton.isHidden = true
        } else {
            inviteButton.isHidden = false
        }
        getDoctorGender()
        
        self.title =  "Dr. " + docfullname
        docImageView.sd_setImage(with: URL(string: imageURLpassed), placeholderImage: UIImage(named: "profile"))
        specialzationLabel.text = "    " + docspeciality + "    "
        
        genderLabel.text = ""
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.tintColor = UIColor.white // this makes back button color white
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as? COTProfileTableViewController
        destinationVC?.docSelected = Int(((doctorSelected?.fields?.value(forKey: "inviteedocuserid")) as? UInt) ?? 0)
    }
    
    //MARK:- Action Methods
    
    @IBAction func addDocToCOTSwitchValueChanged(_ sender: UISwitch) {
    }
    
    @IBAction func referPatientsToDocSwitchValueChanged(_ sender: UISwitch) {
    }
    
    @IBAction func inviteButtonTapped(_ sender: Any) {
        let invitorUID = (QBSession.current.currentUser?.id)!
        let invoteeUID = ((doctorSelected.fields?.value(forKey: "inviteedocuserid")) as? UInt) ?? 0
        self.ReadCallDoctable_RelDocPat(invitoruid: invitorUID, inviteeuid: invoteeUID)
    }
    //MARK:- Storyboard Instance
    
    static func storyboardInstance() -> COTProfileViewController {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        return (storyboard.instantiateViewController(withIdentifier: "COTProfileViewController") as? COTProfileViewController) ?? COTProfileViewController()
    }
    
    func getDoctorGender() {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = ((doctorSelected.fields?.value(forKey: "inviteedocuserid")) as? UInt) ?? 0//doctorSelected.userID
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                
                let gender = (contributors![0].fields?.value(forKey: "gender") as? String) ?? ""
                self.genderLabel.text = gender
                
                
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("getDoctorGender() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    
    
}

extension COTProfileViewController {
    // check if there is entry of doctor and patient relation in RelDocPat table
    func ReadCallDoctable_RelDocPat(invitoruid: UInt, inviteeuid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["doctoruserid"] =  inviteeuid
        getRequest["patientuserid"] = invitoruid
        
        QBRequest.objects(withClassName: "RelDocPat", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Successfully  !")
            if ((contributors?.count)! > 0) {
                print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat():Match Entry Found  !")
                let alert = UIAlertController(title: "Alert", message: "User Already Connected !", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) -> Void in
                    //Do Some action here
                    // go back to Home
                    LoadingIndicatorView.hide()
//                    guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
//                    let nav = UINavigationController(rootViewController: homeViewController)
//                    self.present(nav, animated: false, completion: nil)
                    self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            } else {
                print("(AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Relation Match NOT Found  !")
                self.readdoctordetails(doctoruserid: inviteeuid, invitoruserid: invitoruid)
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller::ReadCallDoctable_RelDocPat(): Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func readdoctordetails(doctoruserid: UInt, invitoruserid: UInt) {
        let getRequest = NSMutableDictionary()
        getRequest["user_id"] = doctoruserid
        QBRequest.objects(withClassName: "DocPrimaryDetails", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            if ((contributors?.count)! > 0) {
                let speciality = contributors![0].fields?.value(forKey: "specialisation") as? String
                var profileurl = contributors![0].fields?.value(forKey: "profileUID") as? String
                
                if (profileurl == nil || profileurl!.isEmpty) {
                    profileurl = "none"
                }
                self.createEntryinRelDocPatTable(invitoruid: invitoruserid, inviteeuid: doctoruserid, speciality: speciality!, profileurl: profileurl! )
            }
        }) { (errorresponse) in
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:ReadPrimaryDetailsFromServer() Response error: \(String(describing: errorresponse.error?.description))")
        }
    }
    
    func createEntryinRelDocPatTable(invitoruid: UInt, inviteeuid: UInt, speciality: String, profileurl: String ) {
        print("AddDoctorsTableViewConteroller: createEntryinRelDocPatTable(): Creating new entry on RelDocPatable")
        let object = QBCOCustomObject()
        object.className = "RelDocPat"
        let doctoruserid = inviteeuid
        let docName = ((doctorSelected.fields?.value(forKey: "inviteefullname")) as? String) ?? ""
        object.fields["doctoruserid"] = doctoruserid
        object.fields["patientuserid"] = invitoruid //QBSession.current.currentUser?.id
        object.fields["doctorfullname"] = docName
        object.fields["lock"] = "true"
        object.fields["doctorspeciality"] = speciality
        object.fields["doctorprofileimageURL"] = profileurl
        QBRequest.createObject(object, successBlock: { (response, contributors) in
            //Handle Success
            print("AddDoctorsTableViewConteroller: createEntryinRelDocPatTable(): Successfully created entry")
            LoadingIndicatorView.hide()
            
            let alert = UIAlertController(title: "Congratulations", message: " Your Dr. \(docName) - \(speciality)  already Registered on CallDoc. Invite has been sent !!", preferredStyle: .alert)
            let imageView = UIImageView(frame: CGRect(x: 10, y: -50, width: 70, height: 70))
            imageView.layer.cornerRadius = imageView.frame.size.width / 2;
            imageView.clipsToBounds = true;
            imageView.layer.borderWidth = 0.2
            imageView.layer.borderColor = UIColor.lightGray.cgColor
            imageView.sd_setImage(with: URL(string: profileurl), placeholderImage: UIImage(named: "profile"))
            alert.view.addSubview(imageView)
            
            let ok = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//                guard let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "TheHomeViewController") as? TheHomeViewController else { fatalError("Error") }
//                let nav = UINavigationController(rootViewController: homeViewController)
//                self.present(nav, animated: true, completion: nil)
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }) { (response) in
            //Handle Error
            LoadingIndicatorView.hide()
            print("AddDoctorsTableViewConteroller:createEntryinRelDocPatTable(): Response error: \(String(describing: response.error?.description))")
        }
    }
}
