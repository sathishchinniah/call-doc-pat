import UIKit

class SelfProfileTableViewController: UITableViewController {
    
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var medicalDegreesLabel: UILabel!
    @IBOutlet weak var fluentInLabel: UILabel!
    @IBOutlet weak var consultationChargesLabel: UILabel!
    @IBOutlet weak var mciRegNoLabel: UILabel!
    @IBOutlet weak var recommendedLabel: UILabel!
    @IBOutlet weak var profileVerifiedLabel: UILabel!
    @IBOutlet weak var connectedLabel: UILabel!
    @IBOutlet weak var mciView: UIView! {
        didSet {
            mciView.layer.cornerRadius = 5;
            mciView.layer.masksToBounds = false
            mciView.layer.shadowColor = UIColor.lightGray.cgColor
            mciView.layer.shadowOpacity = 0.5
            mciView.layer.shadowRadius = 3.0
            mciView.layer.shadowOffset = .zero
        }
    }
    var docSelected = 0
    var recommendedCount = 0
    var connectedToCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRecommendedByAndConnectedToCount()
        configureView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Custom methods
    
    func getRecommendedByAndConnectedToCount() {
        let getRequest = NSMutableDictionary()
        //getRequest["user_id"] = docSelected
        getRequest["inviteedocuserid"] =  docSelected
        QBRequest.objects(withClassName: "RelSpeciality", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
            //Handle Success
            print("getRecommendedByAndConnectedToCount Success ")
            self.recommendedCount = (contributors?.count) ?? 0
            
            let getRequest = NSMutableDictionary()
            getRequest["doctoruserid"] =  self.docSelected
            
            QBRequest.objects(withClassName: "RelDocPat", extendedRequest: getRequest, successBlock: { (response, contributors, nil) in
                //Handle Success
                self.connectedToCount = (contributors?.count) ?? 0
                self.configureView()
            }) { (errorresponse) in
                
                print("getRecommendedByAndConnectedToCount: Response error: \(String(describing: errorresponse.error?.description))")
            }
            
            
        }) { (response) in
            //Handle Error
            print("getRecommendedByAndConnectedToCount Response error: \(String(describing: response.error?.description))")
        }
    }
    
    func configureView() {
        let recommendedString = "Recommended\nby \(recommendedCount) Doctors"
        let rAttributedString = String(recommendedCount)//"8 Doctors"
        recommendedLabel.attributedText = attributeString(string: recommendedString, rangeStr: rAttributedString)
        
        let profileVerifiedString = "Profile Verified\nby CallDoc"
        let pAttributedString = "CallDoc"
        profileVerifiedLabel.attributedText = attributeString(string: profileVerifiedString, rangeStr: pAttributedString)
        
        let connectedString = "Connected to\n \(connectedToCount) Patients"
        let cAttributedString = String(connectedToCount)//"200+"
        connectedLabel.attributedText = attributeString(string: connectedString, rangeStr: cAttributedString)
    }
    
    func attributeString(string: String, rangeStr:String) -> NSMutableAttributedString {
        let range = (string as NSString).range(of: rangeStr)
        let attribute = NSMutableAttributedString.init(string: string)
        let font = UIFont(name: "Rubik-Medium", size: 12.0)
        attribute.addAttribute(NSAttributedStringKey.font, value: font! , range: range)
        return attribute
    }
    
}
