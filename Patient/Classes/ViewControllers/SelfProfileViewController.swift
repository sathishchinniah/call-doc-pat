import UIKit

class SelfProfileViewController: UIViewController {
    
    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var docgenderlb: UILabel!
    @IBOutlet weak var specialzationLabel: UILabel! {
        didSet {
            specialzationLabel.layer.cornerRadius = 15;
            specialzationLabel.layer.masksToBounds = true
            specialzationLabel.layer.shadowColor = UIColor.lightGray.cgColor
            specialzationLabel.layer.shadowOpacity = 0.5
            specialzationLabel.layer.shadowRadius = 3.0
            specialzationLabel.layer.shadowOffset = .zero
        }
    }
    
    var imageURLpassed:String = ""
    var docuseridpassed:UInt = 0
    var docnamepassed:String = ""
    var docspecialitypassed = ""
    var docGender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title =  "Dr. " + docnamepassed
        docImageView.sd_setImage(with: URL(string: imageURLpassed), placeholderImage: UIImage(named: "profile"))
        
        docgenderlb.text = docGender
        
        specialzationLabel.text = "   " + docspecialitypassed + "    "
        specialzationLabel.sizeToFit()
        
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        // this removes navigation bar horizental line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let navBarColor = navigationController!.navigationBar
        navBarColor.barTintColor = UIColor(red:0.2, green:0.4, blue:0.6, alpha:1)
        navBarColor.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as? SelfProfileTableViewController
        destinationVC?.docSelected = Int(docuseridpassed)
    }
    
    //MARK:- Storyboard Instance
    
    static func storyboardInstance() -> SelfProfileViewController {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        return (storyboard.instantiateViewController(withIdentifier: "SelfProfileViewController") as? SelfProfileViewController) ?? SelfProfileViewController()
    }
    
}
