//
//  shareToDoctorsListViewController.swift
//  Patient
//
//  Created by Kirthika Raukutam on 09/11/18.
//  Copyright © 2018 calldoc. All rights reserved.
//

import UIKit
import Toast_Swift

class shareToDoctorsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var doctorsList: [QBCOCustomObject] = []
    var selectedDoctors: [QBCOCustomObject] = []
    var selectedConsultIds: [String] = []
    var consultToBeShared: QBCOCustomObject = QBCOCustomObject()
    
    @IBAction func cancelButtonTouchUpInside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var inviteButton: UIButton!
    
    @IBAction func inviteButtonTouchUpInSide(_ sender: Any) {
    
        let object = QBCOCustomObject()
        object.className = "ConsultsTable"
        //object.fields["sharedconsults"] = consultToBeShared.id
        for doctor in selectedDoctors {
            object.id = doctor.id
            var sharedconsults = (doctor.fields.value(forKey: "sharedconsults") as? String) ?? ""
            if sharedconsults.contains(consultToBeShared.id!) {
                var style = ToastStyle()
                style.messageColor = .white
                ToastManager.shared.style = style
                self.view.makeToast("Consult already shared with \(doctor)", duration: 1.0, position: .center, title: nil, image: nil, style: style, completion: nil)
            } else {
                sharedconsults = sharedconsults + "," + consultToBeShared.id!//consultid
                object.fields["sharedconsults"] = sharedconsults//consultToBeShared.id
                QBRequest.update(object, successBlock: { (response, contributors) in
                    //self.dismiss(animated: true, completion: nil)
                    
                    
                }) { (response) in
                    self.dismiss(animated: true, completion: nil)
                    print("ConfirmMedicalAdviceBaseViewController:UpdateConsultTableforPrescriptionrID: Response error: \(String(describing: response.error?.description))")
                }
            }
            
//            if sharedconsults == nil {
//                sharedconsults = consultToBeShared.id
//            } else {
//                sharedconsults = sharedconsults! + "," + consultToBeShared.id!//consultid
//            }
            
            if doctor == selectedDoctors.last {
                self.dismiss(animated: true, completion: nil)
                var style = ToastStyle()
                style.messageColor = .white
                ToastManager.shared.style = style
                self.view.makeToast("Consult shared succesfully", duration: 2.0, position: .center, title: nil, image: nil, style: style, completion: nil)
            }
           
        }
        
      
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        doctorsList = selectedUserDoctorsArray//PatientAllConsultsdetailsArray
        if doctorsList.contains(consultToBeShared) {
            doctorsList.remove(at: doctorsList.index(of: consultToBeShared)!)
        }
        
        //Display a view stating no contacts that can be shared with

        self.view.isUserInteractionEnabled = true
        
        //self.modalPresentationStyle = .overFullScreen
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.modalPresentationStyle = .popover
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension shareToDoctorsListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctorsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //shareToDoctorTableViewCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "shareToDoctorTableViewCell", for: indexPath) as? shareToDoctorTableViewCell else { return allConsultsTableViewCell() }
        cell.doctorNameLabel.text = (doctorsList[indexPath.row].fields?.value(forKey: "docfullname") as? String) ?? ""
        cell.selectionStyle = .none
        
        if selectedConsultIds.contains(doctorsList[indexPath.row].id!) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
        //return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = self.tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
        for item in selectedDoctors {
            if item == doctorsList[indexPath.row] {
                selectedDoctors.remove(at: selectedDoctors.index(of: item)!)
            }
        }
        for item in selectedConsultIds {
            if item == doctorsList[indexPath.row].id {
                selectedConsultIds.remove(at: selectedConsultIds.index(of: item)!)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = self.tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
        }
        selectedDoctors.append(doctorsList[indexPath.row])
        selectedConsultIds.append(doctorsList[indexPath.row].id!)
    }
}

extension Array {
    func filterDuplicate<T>(_ keyValue:(Element)->T) -> [Element] {
        var uniqueKeys = Set<String>()
        return filter{uniqueKeys.insert("\(keyValue($0))").inserted}
    }
}
